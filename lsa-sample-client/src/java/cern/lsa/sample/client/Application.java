package cern.lsa.sample.client;

import cern.accsoft.commons.domain.Accelerator;
import cern.accsoft.commons.domain.CernAccelerator;
import cern.lsa.client.AcceleratorService;
import cern.lsa.client.DeviceService;
import cern.lsa.client.ServiceLocator;
import cern.lsa.domain.devices.factory.DevicesRequestBuilder;

public class Application {

    public static void main(String[] args) {
        AcceleratorService acceleratorService = ServiceLocator.getService(AcceleratorService.class);

        //Accelerator lhc = acceleratorService.findAccelerator("LHC");

        DeviceService deviceService = ServiceLocator.getService(DeviceService.class);

        System.out.println("Found LHC devices: " + deviceService.findDevices(DevicesRequestBuilder.byAccelerator(CernAccelerator.LHC)));
    }

}

