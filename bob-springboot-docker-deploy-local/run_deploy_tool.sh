#!/usr/bin/env bash

CERN_USER=mstachon
PROJECT_NAME=lsa-virtualization-demo-project
APPLICATION_NAME=dev-lsa-virtualization-demo-project

sudo su <<HERE

sudo ./deploy_tool.sh ${CERN_USER}

rm -R ${PROJECT_NAME}
rm -R /opt/${PROJECT_NAME}/${APPLICATION_NAME}/

sudo  /user/copera/deploy-tool-rep/PRO/deploy ${PROJECT_NAME} --dev -a ${APPLICATION_NAME} -d -f
sudo cp -r /opt/${PROJECT_NAME}/ .

HERE