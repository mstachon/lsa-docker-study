#!/usr/bin/env bash

yum install fuse-sshfs
modprobe fuse
lsmod | grep fuse



# Mount ~pcrops
CERN_USER=$1
LOCAL_PCROPS=/user/pcrops # CBNG deployments
LOCAL_COPERA=/user/copera # deploy-tool
REMOTE_HOST=cs-ccr-ap1

# Use real path instead of symlink on remote host
REMOTE_PCROPS=/nfs/cs-ccr-nfs5/vol11/u1/pcrops
REMOTE_COPERA=/nfs/cs-ccr-nfs6/vol5/u1/copera

mount | grep fuse

mkdir -p "$LOCAL_PCROPS"
mkdir -p "$LOCAL_COPERA"

sshfs -o allow_other "$CERN_USER"@"$REMOTE_HOST":"$REMOTE_PCROPS" "$LOCAL_PCROPS"
sshfs -o allow_other "$CERN_USER"@"$REMOTE_HOST":"$REMOTE_COPERA" "$LOCAL_COPERA"
# umount /user/pcrops
# umount /user/copera

# Verify
ls -la /user/pcrops/deployments
ls -la /user/copera/deploy-tool-rep/

