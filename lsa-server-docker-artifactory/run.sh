#!/usr/bin/env bash

export INSTALL_DIR=/usr/local/lsa-project
mkdir lib
PROJECT_NAME=$1
APPLICATION=$2
EXT=jar
wget  -r -np -nH --cut-dirs=100 -A ".jar" -P $INSTALL_DIR/lib http://abwww.cern.ch/ap/deployments/applications/cern/lsa/${PROJECT_NAME}/PRO/lib/

wget -P $INSTALL_DIR/lib http://abwww.cern.ch/ap/deployments/applications/cern/lsa/lsa-server/PRO/lsa-server.jar


CLASSPATH=`ls $INSTALL_DIR/lib/*.jar | tr -s '\n' ':'`
cd $INSTALL_DIR
/usr/bin/java -cp "$CLASSPATH" -Dmgt.jmx.port="1197"  $APPLICATION

