
DECLARE
  cur SYS_REFCURSOR;
  curid NUMBER;
  desctab DBMS_SQL.desc_tab;
  colcnt NUMBER;
  namevar VARCHAR2(4000);
  numvar NUMBER;
  datevar DATE;
  user_name VARCHAR(100);

  out_columns varchar2(10000);
  out_values varchar2(10000);
  out_table varchar2(10000);
  out_define varchar2(10000);
  
  
BEGIN
  SELECT user INTO user_name FROM dual;
  FOR rec IN (SELECT table_name 
                FROM user_tables 
               WHERE table_name in (select object_name from objects_to_export where object_type = 'DATA'))
  LOOP
    dbms_output.put_line('REM INSERTING into ' || rec.table_name );
    dbms_output.put_line('SET DEFINE OFF;');
  
    OPEN cur FOR 'SELECT * FROM '||rec.table_name||' ORDER BY 1';

    curid := DBMS_SQL.to_cursor_number(cur);
    DBMS_SQL.describe_columns(curid, colcnt, desctab);
   
    
    out_columns := 'INSERT INTO '||rec.table_name||'(';

    FOR indx IN 1 .. colcnt LOOP
      out_columns := out_columns||desctab(indx).col_name||',';
      IF desctab (indx).col_type = 2 
      THEN
        DBMS_SQL.define_column (curid, indx, numvar); 
      ELSIF desctab (indx).col_type = 12
      THEN
        DBMS_SQL.define_column (curid, indx, datevar); 
      ELSE
        DBMS_SQL.define_column (curid, indx, namevar, 4000); 
      END IF;
    END LOOP;

    out_columns := rtrim(out_columns,',')||') VALUES (';

    WHILE DBMS_SQL.fetch_rows (curid) > 0 
    LOOP
      out_values := '';
      FOR indx IN 1 .. colcnt 
      LOOP
        IF (desctab (indx).col_type = 1) 
        THEN
          DBMS_SQL.COLUMN_VALUE (curid, indx, namevar);
          out_values := out_values||''''||namevar||''',';
        ELSIF (desctab (indx).col_type = 2)
        THEN
          DBMS_SQL.COLUMN_VALUE (curid, indx, numvar);
          out_values := out_values||numvar||',';
        ELSIF (desctab (indx).col_type = 12)
        THEN
          DBMS_SQL.COLUMN_VALUE (curid, indx, datevar);
          out_values := out_values||
            'to_date('''||to_char(datevar,'DD.MM.YYYY HH24:MI:SS')||
             ''',''DD.MM.YYYY HH24:MI:SS''),';
        END IF;
      END LOOP; 
      dbms_output.put_line(out_table||out_define||out_columns||rtrim(out_values,',')||');');

    END LOOP;

    DBMS_SQL.close_cursor (curid);

  END LOOP;
  DBMS_OUTPUT.put_line('COMMIT;');
  
END;
/
