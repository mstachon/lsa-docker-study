
begin
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PRETTY',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'STORAGE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PARTITIONING',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'TABLESPACE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SEGMENT_ATTRIBUTES',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SQLTERMINATOR',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'CONSTRAINTS',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'REF_CONSTRAINTS',false);
end;
/
set long 1000000
set heading off  
set head off
set echo off
set pagesize 0
set verify off
set feedback off
set pages 999
set longchunksize 32000
set linesize 32000
set newpage none  
set tab off  
set trimspool on
SET SERVEROUTPUT ON SIZE 100000
column ddl format a50000
spool /opt/mstachon/oracle-data/5-schema_device_type.sql

@/opt/mstachon/export-files/export_tables.sql
/

@/opt/mstachon/export-files/export_ref_constraints.sql
/

@/opt/mstachon/export-files/export_types.sql
/

@/opt/mstachon/export-files/export_sequences.sql
/

@/opt/mstachon/export-files/export_indexes/sql
/

@/opt/mstachon/export-files/export_synonyms.sql
/

@/opt/mstachon/export-files/export_views.sql
/

@/opt/mstachon/export-files/export_user_connect.sql
/

@/opt/mstachon/export-files/export_data.sql
/

@/opt/mstachon/export-files/export_packages.sql
/

@/opt/mstachon/export-files/export_triggers.sql
/

spool off
/
