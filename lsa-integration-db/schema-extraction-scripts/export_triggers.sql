select
   dbms_metadata.GET_DDL('TRIGGER', u.trigger_name)
from
   user_triggers u
    where table_name in ('BEAMPROCESSES', 'BEAMPROCESS_TYPES', 'CYCLES');
/