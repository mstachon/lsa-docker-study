select
   dbms_metadata.GET_DDL('INDEX', u.index_name)
from
   user_indexes u
where table_name in (select object_name from objects_to_export);
/