select
   dbms_metadata.GET_DDL('REF_CONSTRAINT', u.constraint_name)
from
   user_constraints u
where
    owner = 'LSA' and u.constraint_type in ('R') and table_name in (select object_name from objects_to_export);
/