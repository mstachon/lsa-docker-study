alter user sys identified by user;
CONNECT sys/user as sysdba;

@$ORACLE_HOME/rdbms/admin/utlmail.sql
/

@$ORACLE_HOME/rdbms/admin/utltcp.sql
/

@$ORACLE_HOME/rdbms/admin/utlsmtp.sql
/

@$ORACLE_HOME/rdbms/admin/utlhttp.sql
/

@$ORACLE_HOME/rdbms/admin/utlfile.sql
/

GRANT EXECUTE ON utl_mail TO COMMONS;
GRANT EXECUTE ON utl_http TO COMMONS;
GRANT EXECUTE ON utl_tcp TO COMMONS;
GRANT EXECUTE ON utl_smtp TO COMMONS;
GRANT EXECUTE ON utl_file TO COMMONS;

GRANT EXECUTE ON utl_mail TO LSA;
GRANT EXECUTE ON utl_http TO LSA;
GRANT EXECUTE ON utl_tcp TO LSA;
GRANT EXECUTE ON utl_smtp TO LSA;
GRANT EXECUTE ON utl_file TO LSA;


ALTER USER COMMONS IDENTIFIED BY COMMONS;
CONNECT COMMONS/COMMONS;


@/opt/oracle/oracle-data/commons/install_in_commons.sql;
/
