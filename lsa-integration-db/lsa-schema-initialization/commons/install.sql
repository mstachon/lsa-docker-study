------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Installs the COMMONS environment and makes some initial configurations.
--
--    NOTE: A database user COMMONS must exist on the database and the script install_in_commons.sql
--          must be executed on the COMMONS schema before proceeding with this script.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-01  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

--  Exit if there are any errors
-- whenever sqlerror exit

--  run pre-installation checks for required privileges
@/opt/oracle/oracle-data/commons/install/pre_install_checks.sql

--  run all installation scripts
@/opt/oracle/oracle-data/commons/install/principal_objects.sql

@/opt/oracle/oracle-data/commons/install/packages.sql

@/opt/oracle/oracle-data/commons/install/configuration_data.sql

@/opt/oracle/oracle-data/commons/install/reg_part_management.sql

@/opt/oracle/oracle-data/commons/install/scheduler_jobs.sql

--  Requires editing of the following script before running
@/opt/oracle/oracle-data/commons/install/define_admins.sql

--  Requires editing of the following script before running
@/opt/oracle/oracle-data/commons/install/define_notifications.sql

@/opt/oracle/oracle-data/commons/install/define_analysis_rules.sql

