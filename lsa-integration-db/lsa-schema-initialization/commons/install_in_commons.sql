------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Installs database objects that should exist only in COMMONS schema and grants the appropriate
--    privileges to other users. 
--    This script should be executed in the COMMONS schema.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-14 Jose Rolland
--    Added check that script is being executed in the COMMONS schema - OCOMM-433
--  2014-01-20  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------

whenever sqlerror exit

--  Checks that script is being executed in the COMMONS schema.
begin
  if sys_context('userenv', 'session_user') != 'COMMONS' then
    raise_application_error(-20000, 'This script should be executed in the COMMONS schema. Exiting...'); 
  end if;
end;
/

--  Create a context for storing Commons related information
--  REMEMBER, these context names are unique per database, and can only be managed by one package
--  This means that it is necessary to grant execute to public and possibly create a public synonym  
--  for the com_ctx_mgr package.
create or replace context commons_ctx using commons.com_ctx_mgr;
/

@/opt/oracle/oracle-data/commons/com_ctx_mgr/com_ctx_mgr.pls;
/

@/opt/oracle/oracle-data/commons/com_ctx_mgr/com_ctx_mgr.plb;
/

-- Grant execute to public on com_ctx_mgr package
grant execute on com_ctx_mgr to public;
/

--  Option to create a public synonym for the commons.com_ctx_mgr requires additional privileges
--create or replace public synonym com_ctx_mgr for commons.com_ctx_mgr;

-- global context used by com_logger for cross-session communication
create or replace context log_ctx using commons.com_ctx_mgr accessed globally;
/

-- view needed by the code analysis packages
create or replace view all_source_v as
select owner, name, type, line, 
  case 
    when not regexp_like(text, '''.*''') then
      text
    else
      regexp_replace(text, '''.*''', '')
  end text
 from
 (select owner, name, type, line,
    case 
      when text not like '%--%' then
        text
      else
        substr(text, 1, instr(trim(text), '--') - 1)
    end text
  from all_source
 )
;
/

grant select on all_source_v to public;
/

-- table in commons accounts to store registered commons users
create table com_users (
  username                varchar2(30)  constraint  com_users_pk primary key,
  first_install_time_utc  timestamp     default sys_extract_utc(systimestamp) constraint com_users_first_install_nn not null,      
  last_update_time_utc    timestamp     default sys_extract_utc(systimestamp) constraint com_users_last_update_nn not null,
  installed_com_version   varchar2(10)
);

-- package in commons to support central registration of users of the commons 4 oracle framework
@/opt/oracle/oracle-data/commons/com_user_registration/com_user_registration.pls;
/

@/opt/oracle/oracle-data/commons/com_user_registration/com_user_registration.plb;
/

grant execute on com_user_registration to public;
/
