create or replace package com_action_trigger
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Provide utilities to manage table action triggers
--   More details and usage examples: https://wikis.cern.ch/display/com4ora/com_action_trigger
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-12 Jose Rolland
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-390
--  2015-12-14 Pascal Le Roux
--    OCOMM-368: Adaptation from first CCS version
--  2015-06-11 Lukasz Burdzanowski
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
is

  function generate_actions_trigger(
    i_table_name            in user_tables.table_name%type,
    i_actions_package_name  in user_objects.object_name%type default null,
    i_trigger_name          in user_objects.object_name%type default null)
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns DDL code to create a compound 'actions' trigger that will delegate the logic on a
--    PL/SQL 'actions trigger' package
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name - name of the trigger's table
--    i_actions_package_name - name of the 'actions trigger' package. If no name is provided, the package will be
--      named as i_table_name with suffix _actions_trg
--    i_trigger_name - name of the 'actions' trigger. If no name is provided, the trigger will be named as i_table_name
--      with suffix _trigger_action 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if no user table found with a name matching the value of i_table_name, if i_actions_package_name
--    or i_trigger_name are not valid Oracle object names
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-11 Lukasz Burdzanowski
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function generate_actions_trgr_pkg_hdr(
    i_table_name            in user_tables.table_name%type,
    i_actions_package_name  in user_objects.object_name%type default null)
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns DDL code to create 'actions trigger' PL/SQL package header
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name - name of the trigger's table
--    i_actions_package_name - name of the 'actions trigger' package
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Not known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-11 Lukasz Burdzanowski
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function generate_actions_trgr_pkg_body(
    i_table_name            in user_tables.table_name%type,
    i_actions_package_name  in user_objects.object_name%type default null)
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns DDL code to create 'actions trigger' PL/SQL package body
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name - name of the trigger's table
--    i_actions_package_name - name of the 'actions trigger' package
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Not known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-11 Lukasz Burdzanowski
--    Creation
------------------------------------------------------------------------------------------------------------------------

end com_action_trigger;