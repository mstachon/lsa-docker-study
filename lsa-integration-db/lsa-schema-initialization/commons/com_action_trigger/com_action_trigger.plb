create or replace package body com_action_trigger 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Provide utilities to manage table action triggers
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-01	Chris Roderick
--    Bug fix in function generate_actions_trigger. OCOMM-372
--  2015-12-14 Pascal Le Roux
--    Adaptation from first CCS version. OCOMM-368
--  2015-06-11 Lukasz Burdzanowski
--    Creation. CCS-6290
------------------------------------------------------------------------------------------------------------------------ 
is

  function generate_columns_mapping(i_table_columns in table_of_varchar)
  return varchar2
------------------------------------------------------------------------------------------------------------------------    
--  Change Log (descending):
--  2015-08-22 Lukasz
--    Creation. CCS-2630
------------------------------------------------------------------------------------------------------------------------  
  is
    l_output com_types.t_varchar2_max_size_plsql;  
  begin
    for i in 1..i_table_columns.count loop 
      l_output := l_output||'    l_row_old.'||i_table_columns(i)||' := :old.'||i_table_columns(i)||';'||chr(10);
    end loop;   
    l_output := l_output||chr(10);   
    for i in 1..i_table_columns.count loop 
      l_output := l_output||'    l_row_new.'||i_table_columns(i)||' := :new.'||i_table_columns(i)||';'||chr(10);
    end loop;
    return l_output;
  end generate_columns_mapping;  
  
  function generate_rows_mapping(i_table_columns in table_of_varchar)
  return varchar2
------------------------------------------------------------------------------------------------------------------------    
--  Change Log (descending):
--  2015-08-22 Lukasz
--    Creation. CCS-2630
------------------------------------------------------------------------------------------------------------------------  
  is
    l_output com_types.t_varchar2_max_size_plsql;  
  begin     
    for i in 1..i_table_columns.count loop 
      l_output := l_output||'      :new.'||i_table_columns(i)||' := l_row_new.'||i_table_columns(i)||';'||chr(10);
    end loop;
    return l_output;
  end generate_rows_mapping;  
  
  function generate_actions_trigger(
    i_table_name            in user_tables.table_name%type,
    i_actions_package_name  in user_objects.object_name%type default null,
    i_trigger_name          in user_objects.object_name%type default null)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-01	Chris Roderick
--    Moved assignment of local variables inside begin statement. 
--    Added assertion calls for input values.
--    Updated to use com_string_utils.get_as_valid_oracle_identifier to avoid generation of names > 30 chars. OCOMM-372
--  2015-12-14 Pascal Le Roux
--    Refactored to use com_template
--  2015-07-10 Jose
--  Set :new only when inserting or updating
--  2015-07-08 Jose
--    Fixed before_each_row (:new updated after before_each_row procedure), and added trigger name at the end of the code block
--  2015-07-02 Jose
--    Refactored
--  2015-06-11 Lukasz
--    Creation. CCS-6290
------------------------------------------------------------------------------------------------------------------------
  is
    c_actions_trigger_case constant com_types.t_varchar2_max_size_db :=
      q'[case
          when inserting then l_trigger_operation := com_constants.c_dml_event_insert;
          when updating then l_trigger_operation := com_constants.c_dml_event_update;
          when deleting then l_trigger_operation := com_constants.c_dml_event_delete;
          else com_event_mgr.raise_error_event('EXCEPTION_DUE_TO_MERGE_ERROR', 'A merge error provoked this trigger exception');
        end case;
        ]';  

    l_template com_template := com_template(
      q'[
      create or replace trigger #trigger_name
      for insert or update or delete on #table_name
      compound trigger
        l_row_old #table_name%rowtype;
        l_row_new #table_name%rowtype;
        l_trigger_operation com_dml_logs.op_type%type;
      
        before statement is
        begin
          #trigger_case
          #actions_package_name.before_statement (l_trigger_operation);    
        end before statement;
      
        before each row is
        begin
          #trigger_case
      #columns_mapping    
          #actions_package_name.before_each_row(l_row_old, l_row_new, l_trigger_operation);
          
          if l_trigger_operation in (com_constants.c_dml_event_insert,com_constants.c_dml_event_update) then
      #rows_mapping
          end if;   
        end before each row;
      
        after each row is
        begin
          #trigger_case
      #columns_mapping       
          #actions_package_name.after_each_row (l_row_old, l_row_new, l_trigger_operation);
        end after each row;
         
        after statement is
        begin
          #trigger_case
          #actions_package_name.after_statement (l_trigger_operation);
        end after statement;  
      end #trigger_name;
      ]'); 
    
    l_table_name              user_tables.table_name%type;
    l_actions_package_name    user_objects.object_name%type;
    l_trigger_name            user_tables.table_name%type;
    l_table_columns           table_of_varchar;
  begin
    com_assert.user_table_exists(i_table_name);
    l_table_name            := lower(i_table_name);
    com_assert.is_valid_oracle_object_name(i_actions_package_name);
    l_actions_package_name  := lower(coalesce(i_actions_package_name, com_string_utils.get_as_valid_oracle_identifier(
                                                                          l_table_name, null, '_trigger_action')));
    com_assert.is_valid_oracle_object_name(i_trigger_name);                                                                          
    l_trigger_name          := lower(coalesce(i_trigger_name, com_string_utils.get_as_valid_oracle_identifier(
                                                                  l_table_name, null, '_actions_trg')));
    select lower(column_name) 
    bulk collect into l_table_columns 
    from user_tab_columns 
    where table_name = upper(i_table_name)
    order by column_id;  
    
    return l_template.
          bind('#trigger_name', l_trigger_name).
          bind('#table_name', l_table_name).
          bind('#trigger_case', c_actions_trigger_case).
          bind('#actions_package_name', l_actions_package_name).
          bind('#columns_mapping', generate_columns_mapping(l_table_columns)).
          bind('#rows_mapping', generate_rows_mapping(l_table_columns)).
          generate;

  exception
    when others then
      com_event_mgr.raise_error_event('INTERNAL_ERROR',dbms_utility.format_error_stack);
  end generate_actions_trigger;
  

  function generate_actions_trgr_pkg_hdr(
    i_table_name in user_tables.table_name%type,
    i_actions_package_name in user_objects.object_name%type default null)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-02 Lukasz
--    Refactored to use com_template
--  2015-07-02 Jose
--    Refactored
--  2015-06-11 Lukasz
--    Creation. CCS-6290
------------------------------------------------------------------------------------------------------------------------
  is
    l_template com_template := com_template(
'create or replace package #l_actions_package_name
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Auto-generated stubs for #l_table_name action trigger
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    #gen_date #user_name
--    Auto-generated
------------------------------------------------------------------------------------------------------------------------
  
procedure before_statement (i_trigger_operation in com_dml_logs.op_type%type);
  
procedure before_each_row (
  i_row_old in #l_table_name%rowtype,
  i_row_new in out nocopy #l_table_name%rowtype,
  i_trigger_operation in com_dml_logs.op_type%type);

procedure after_each_row (
  i_row_old in #l_table_name%rowtype,
  i_row_new in #l_table_name%rowtype,
  i_trigger_operation in com_dml_logs.op_type%type);

procedure after_statement (i_trigger_operation in com_dml_logs.op_type%type);

end #l_actions_package_name;' 
);
  begin 
    return l_template.
      bind('#l_actions_package_name', lower(coalesce (i_actions_package_name, i_table_name||'_trigger_action'))).
      bind('#l_table_name', lower(i_table_name)).
      bind('#gen_date', to_char(sysdate,'YYYY-MM-DD')).
      bind('#user_name', sys_context('USERENV','OS_USER')).
      generate;
  exception
    when others then
      com_event_mgr.raise_error_event('INTERNAL_ERROR',dbms_utility.format_error_stack);
  end generate_actions_trgr_pkg_hdr;
  
  function generate_actions_trgr_pkg_body(
    i_table_name in user_tables.table_name%type,
    i_actions_package_name in user_objects.object_name%type default null)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-14 Pascal Le Roux
--    Refactored to use com_template
--  2015-07-02 Jose
--    Refactored
--  2015-06-11 Lukasz
--    Creation. CCS-6290
------------------------------------------------------------------------------------------------------------------------
  is
    l_template com_template := com_template(
'create or replace package body #l_actions_package_name
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  #gen_date #user_name
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------

procedure before_statement (
	i_trigger_operation	in	com_dml_logs.op_type%type
)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  #gen_date #user_name
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------
is
begin
	null;
end before_statement;

procedure before_each_row (
	i_row_old	in	#l_table_name%rowtype,
	i_row_new	in out nocopy	#l_table_name%rowtype,
	i_trigger_operation	in	com_dml_logs.op_type%type
)------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  #gen_date #user_name
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------
is
begin
	null;
end before_each_row;

procedure after_each_row (
	i_row_old	in	#l_table_name%rowtype,
	i_row_new	in	#l_table_name%rowtype,
	i_trigger_operation	in	com_dml_logs.op_type%type
)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  #gen_date #user_name
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------
is
begin
	null;
end after_each_row;

procedure after_statement (
	i_trigger_operation	in	com_dml_logs.op_type%type
)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  #gen_date #user_name
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------
is
begin
	null;
end after_statement;
end #l_actions_package_name;' 
);    
  begin
    return l_template.
      bind('#l_actions_package_name', lower(coalesce (i_actions_package_name, i_table_name||'_trigger_action'))).
      bind('#l_table_name', lower(i_table_name)).
      bind('#gen_date', to_char(sysdate,'YYYY-MM-DD')).
      bind('#user_name', sys_context('USERENV','OS_USER')).
      generate;
  exception
    when others then
      com_event_mgr.raise_error_event('INTERNAL_ERROR',dbms_utility.format_error_stack);
      return null;
  end generate_actions_trgr_pkg_body;  

  
end com_action_trigger;