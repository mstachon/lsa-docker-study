create or replace package body com_anydata_extensions
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2010-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Provides a consistent set of utility functions to interact with the SYS.ANYDATA data type which do not come 
--    out of the box from Oracle.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-04 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10. All functions have been renamed from access_xxx
--    to get_xxx - OCOMM-391
--  2015-10-06 Piotr Sowinski
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  function get_table_of_number(i_anydata_value in anydata) return table_of_number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Piotr Sowinski
--    Creation
------------------------------------------------------------------------------------------------------------------------    
  is
        l_table_of_number table_of_number; 
  begin
    com_assert.is_true(i_anydata_value.getcollection(l_table_of_number) = dbms_types.success
                      ,'Cannot get table of number from anydata');  
    return l_table_of_number;
    
  end get_table_of_number;
  
  function get_table_of_varchar(i_anydata_value in anydata) return table_of_varchar
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Piotr Sowinski
--    Creation
------------------------------------------------------------------------------------------------------------------------        
  is
    l_table_of_varchar table_of_varchar; 
  begin
    com_assert.is_true(i_anydata_value.getcollection(l_table_of_varchar) = dbms_types.success
                      ,'Cannot get table of varchar from anydata'); 
    return l_table_of_varchar;
    
  end get_table_of_varchar;
  
  function get_table_of_integer(i_anydata_value in anydata) return table_of_integer
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Piotr Sowinski
--    Creation
------------------------------------------------------------------------------------------------------------------------        
  is
    l_table_of_integer table_of_integer; 
  begin
    com_assert.is_true(i_anydata_value.getcollection(l_table_of_integer) = dbms_types.success
                      ,'Cannot get table of integer from anydata');  
    return l_table_of_integer;
    
  end get_table_of_integer;
  
  function get_table_of_timestamp(i_anydata_value in anydata) return table_of_timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Piotr Sowinski
--    Creation
------------------------------------------------------------------------------------------------------------------------        
  is
    l_table_of_timestamp table_of_timestamp; 
  begin
    com_assert.is_true(i_anydata_value.getcollection(l_table_of_timestamp) = dbms_types.success
                      ,'Cannot get table of timestamp from anydata');    
    return l_table_of_timestamp;
    
  end get_table_of_timestamp;
  
  function get_timestamp(i_anydata_value in anydata) return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Piotr Sowinski
--    Creation
------------------------------------------------------------------------------------------------------------------------        
  is
  begin
    return anydata.accesstimestamp(i_anydata_value);
  end get_timestamp;
  
  function get_date(i_anydata_value in anydata) return date
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Piotr Sowinski
--    Creation
------------------------------------------------------------------------------------------------------------------------        
  is
  begin
    return anydata.accesstimestamp(i_anydata_value);
  end get_date;
  
  function get_text(i_anydata_value in anydata) return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Piotr Sowinski
--    Creation
------------------------------------------------------------------------------------------------------------------------        
  is
  begin
    return coalesce( anydata.accessvarchar2(i_anydata_value),
                     anydata.accesschar(i_anydata_value));
  end get_text;
  
  function get_number(i_anydata_value in anydata) return number
  is
  begin
    return anydata.accessnumber(i_anydata_value);
  end get_number;

end com_anydata_extensions;
/
