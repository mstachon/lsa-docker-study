create or replace package com_code_analysis_syntax authid definer
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains code analysis rules related to syntax checks.
--  Scope:
--    Operates on the database level, but depends on individual code analysis rule implementations.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-10 Eve Fortescue-Beck
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 -  OCOMM-399
--  2013-08-08 Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  function analyse_nvl_use (
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks code matching the given filters, which use the function nvl instead of the function coalesce
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function analyse_join_use(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks code matching the given filters, which uses the '+' join notation instead of ANSI joins
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function analyse_decode_use (
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks code matching the given filters, which uses the decode function instead of a case statement
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function analyse_cursor_names(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks code matching the given filters, containing cursors without the prefix 'cu_'
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  
  function analyse_formal_param_mode(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks code matching the given filters, containing formal input parameters without the IN mode specified
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  
  function analyse_global_var_spec(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks code matching the given filters, containing global variables declared in a package
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------  
end com_code_analysis_syntax;