create or replace package body com_code_analysis_syntax
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains code analysis rules related to syntax checks.
--  Scope:
--    Operates on the database level, but depends on individual code analysis rule implementations.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-10 Eve Fortescue-Beck
--    OCOMM-399: Applied formatting changes identified in code review OCOMM-REVIEWS-10 
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  function analyse_nvl_use (
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
      select owner code_owner, type code_type, name code_name, line line_no, text line_text, instr(lower(text), 'nvl') line_column_pos, null extra_info
      from all_source_v
      where owner like i_obj_owner_like
      and type like i_obj_type_like
      and name like i_obj_name_like      
      and regexp_like(lower(text),'([[:space:]]+|\()nvl')
    );
    
    return l_result;
    
  end analyse_nvl_use;
  
  function analyse_decode_use (
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
      select owner code_owner, type code_type, name code_name, line line_no, text line_text, instr(lower(text), 'decode') line_column_pos, null extra_info
      from all_source_v
      where owner like i_obj_owner_like
      and type like i_obj_type_like
      and name like i_obj_name_like      
      and regexp_like(lower(text),'([[:space:]]+|\()decode')
    );
    
    return l_result;
    
  end analyse_decode_use; 
  
  function analyse_join_use(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
      with all_source_clean as (
        select /*+ materialize */  
          owner code_owner, type code_type, name code_name, line line_no, text line_text
        from all_source_v
        where owner = i_obj_owner_like
        and type like i_obj_type_like
        and name like i_obj_name_like
      )
      select code_owner, code_type, code_name, 
        line_no, line_text, regexp_instr(lower(line_text), '\(\+\)') line_column_pos, null extra_info
      from all_source_clean
      where regexp_like(lower(line_text),'\(\+\)')
    )
    ;
    
    return l_result;
  
  end analyse_join_use;  
  
  function analyse_cursor_names(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
          select ai.owner code_owner, src.type code_type, src.name code_name, ai.line line_no, src.text line_text,
            ai.col line_column_pos, null extra_info
          from all_identifiers ai
          join all_source src on (ai.owner = src.owner and 
                                  ai.object_name = src.name and
                                  ai.object_type = src.type and
                                  ai.line = src.line)
          where ai.owner like i_obj_owner_like
          and ai.object_type like i_obj_type_like
          and ai.object_name like i_obj_name_like
          and ai.usage = 'DECLARATION'
          and ai.type = 'CURSOR'
          and ai.name not like 'CU_%'
    );
    
    return l_result;    
  
  end analyse_cursor_names;  
   
  function analyse_formal_param_mode(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
         with identifiers as (
          select /*+ materialize */ 
            ai.owner, ai.object_name, ai.object_type, ai.line, ai.col, ai.type, ai.usage
          from all_identifiers ai
          where ai.owner like i_obj_owner_like
          and ai.object_type like i_obj_type_like
          and ai.object_name like i_obj_type_like
          and ai.type in ('FORMAL IN')
          and ai.usage = 'DECLARATION'
        ),
        source_code as (
          select /*+ materialize */
            owner, name, type, line, text
          from all_source src
          where owner like i_obj_owner_like
        )
        select ai.owner code_owner, src.type code_type, src.name code_name, ai.line line_no, src.text line_text,
          ai.col line_column_pos, null extra_info          
        from identifiers ai
        join source_code src on (ai.owner = src.owner 
                                and ai.object_name = src.name
                                and ai.object_type = src.type
                                and ai.line = src.line)
        where not regexp_like(lower(text), '([[:space:]]+)in([[:space:]]+)')        
    );
    
    return l_result;    
  
  end analyse_formal_param_mode;
  
  function analyse_global_var_spec(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
          select ai.owner code_owner, src.type code_type, src.name code_name, ai.line line_no, src.text line_text,
            ai.col line_column_pos, null extra_info
          from all_identifiers ai
          join all_source src on (ai.owner = src.owner and 
                                  ai.object_name = src.name and
                                  ai.object_type = src.type and
                                  ai.line = src.line)
          where ai.owner like i_obj_owner_like
          and ai.object_type like i_obj_type_like
          and ai.object_name like i_obj_name_like
          and ai.type = 'VARIABLE'
          and ai.usage = 'DECLARATION'
          and ai.usage_context_id = 1
          and ai.object_type = 'PACKAGE'
    );
    
    return l_result;    
  
  end analyse_global_var_spec;
  
end com_code_analysis_syntax;