create or replace package com_schema_analysis_tables
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to table analysis
--  Scope:
--    Operates on the database level, but depends on the implementation of analysis rules in each individual schema.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-14 Jose Rolland
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 -  OCOMM-421
--  2014-10-21  Hadrian Villar
--    OCOMM-289 Creation
------------------------------------------------------------------------------------------------------------------------
is

  function chk_for_no_relations (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for tables with no incoming or outgoing relations
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-10-21  Hadrian Villar
--    OCOMM-289 Creation
------------------------------------------------------------------------------------------------------------------------

end com_schema_analysis_tables;