create or replace package body com_schema_analysis_tables
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to table analysis
--  Scope:
--    Operates on the database level, but depends on individual schema analysis rule implementations.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-10-21  Hadrian Villar
--    OCOMM-289 Creation
------------------------------------------------------------------------------------------------------------------------

function chk_for_no_relations (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-04-28  Lukasz Burdzanowski
--    TODO: list of tables to ignore should be parametrized; see review OCOMM-REVIEWS-3
--  2014-10-21  Hadrian Villar
--    OCOMM-289 Creation
------------------------------------------------------------------------------------------------------------------------  
  is
    l_result  schema_analysis_rule_results;
  begin  
      with 
      excluded_tabs as (
         select uc.table_name as table_from , ucc2.table_name as table_to
         from all_constraints uc 
         join all_cons_columns ucc2 on uc.r_constraint_name = ucc2.constraint_name         
         where constraint_type = 'R' )     
        select schema_analysis_rule_result(alt.owner, alt.table_name, 'TABLE', 'This table does not have any incoming or outgoing relations', 1)
        bulk collect into l_result
        from all_tables alt
     where 
       alt.table_name  not in (select table_from from excluded_tabs) 
       and alt.table_name not in (select table_to from excluded_tabs) 
       and alt.table_name not like '%HIST'
       and alt.owner like i_obj_owner_like  
       and alt.table_name like i_obj_name_like
      order by alt.owner, alt.table_name;    
    return l_result;    
  end chk_for_no_relations; 
  
end com_schema_analysis_tables;