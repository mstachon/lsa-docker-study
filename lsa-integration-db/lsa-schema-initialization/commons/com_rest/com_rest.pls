create or replace package com_rest
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Utility package facilitating usage of REST APIs.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-13 Jose Rolland
--   Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-415
--  2016-01-16 Lukasz Burdzanowski
--    Creation OCOMM-364
------------------------------------------------------------------------------------------------------------------------
is
  
  subtype t_rest_method_type is varchar2(10);
  c_post constant t_rest_method_type := 'POST';
  c_get constant t_rest_method_type := 'GET';
  c_put constant t_rest_method_type := 'PUT';
  c_delete constant t_rest_method_type := 'DELETE';
    
  type t_rest_call is record (
    l_url varchar2(512),
    l_type t_rest_method_type := c_post,
    l_header_fields vc_vc_map,
    l_body clob
  );  

  function execute_call(i_rest_call in t_rest_call) 
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Executes passed REST call and returns a clob representation of the server response.
--    If no HTTP header fields are defined the defaults are used: 
--      'Content-Type', 'application/json'
--      'Content-Length', length(i_rest_call.l_body)
--    Returns server response as CLOB.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_rest_call - t_rest_call record providing needed info (URL, REST method, header and body)
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises PROCESSING_ERROR wrapping server error code (utl_http.get_detailed_sqlerrm).  
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):   
--  2016-01-22 Lukasz Burdzanowski
--    Creation OCOMM-364
------------------------------------------------------------------------------------------------------------------------
  
  function execute_call(
    i_url in varchar2, 
    i_body in varchar2, 
    i_type in t_rest_method_type default c_post)
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Wrapper over execute_call(i_rest_call in r_rest_call) aimed at a call from SQL.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_url  - string with URL to call
--    i_body - string with the content body
--    i_type - REST method (GET, POST, PUT, DELETE)
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises PROCESSING_ERROR wrapping server error code (utl_http.get_detailed_sqlerrm).
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):   
--  2016-01-22 Lukasz Burdzanowski
--    Creation OCOMM-364
------------------------------------------------------------------------------------------------------------------------

  function execute_get(i_url in varchar2)
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Wrapper over execute_call(i_rest_call in r_rest_call) return result of a GET. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_url - string with URL to call
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises PROCESSING_ERROR wrapping server error code (utl_http.get_detailed_sqlerrm).  
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):   
--  2016-01-22 Lukasz Burdzanowski
--    Creation OCOMM-364
------------------------------------------------------------------------------------------------------------------------

end com_rest;