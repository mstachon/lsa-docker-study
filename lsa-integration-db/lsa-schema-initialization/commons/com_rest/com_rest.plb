create or replace package body com_rest
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Utility package facilitating usage of REST APIs.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Modified procedure execute_call OCOMM-443
--  2016-01-16 Lukasz Burdzanowski
--    Creation OCOMM-364
------------------------------------------------------------------------------------------------------------------------
is
  c_http_header_content_type constant varchar2(12) := 'Content-Type';
  c_http_header_content_length constant varchar2(14) := 'Content-Length';
 
  function execute_call(i_rest_call in t_rest_call) 
  return clob  
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    adapted to changes in vc_vc_map definition OCOMM-443
--  2016-01-22 Lukasz Burdzanowski
--    Creation OCOMM-364
-----------------------------------------------------------------------------------------------------------------------
  is
    l_http_request              utl_http.req;
    l_http_response             utl_http.resp;
    l_output                    clob;
    l_server_error              com_types.t_varchar2_max_size_db;
    l_has_content_type_header   boolean := false;
    l_has_content_lenght_header boolean := false;
    l_header_field_key          com_types.t_varchar2_max_size_db;
  begin    
    com_assert.arg_not_null(i_rest_call.l_url, 'URL to call cant be null');
  
    l_http_request := utl_http.begin_request(i_rest_call.l_url, i_rest_call.l_type);
    utl_http.set_body_charset(l_http_request, 'UTF-8');
    
    if not i_rest_call.l_header_fields.is_empty then
      
      for r_key_idx in 1..i_rest_call.l_header_fields.entry_count loop
        l_header_field_key := i_rest_call.l_header_fields.key_set()(r_key_idx);
        
        utl_http.set_header(l_http_request, 
          l_header_field_key,
          i_rest_call.l_header_fields.get_varchar_value(l_header_field_key));
      end loop;
    end if;
    
    if not i_rest_call.l_header_fields.contains_key(c_http_header_content_type) then
      utl_http.set_header(l_http_request, c_http_header_content_type, 'application/json');
    end if;
    
    if not i_rest_call.l_header_fields.contains_key(c_http_header_content_length) then
      utl_http.set_header(l_http_request, c_http_header_content_length, dbms_lob.getlength(i_rest_call.l_body));
    end if;
    
    utl_http.write_text(l_http_request, i_rest_call.l_body);
    l_http_response := utl_http.get_response(l_http_request);

    utl_http.read_text(l_http_response, l_output);
    utl_http.end_response(l_http_response);
    
    return l_output;
  exception
    when others then
      l_server_error := utl_http.get_detailed_sqlerrm;
      com_event_mgr.raise_error_event('PROCESSING_ERROR', 'Rest call resulted in target server error: '||l_server_error);
  end execute_call;    
  
  function execute_call(
    i_url in varchar2, 
    i_body in varchar2, 
    i_type in t_rest_method_type default c_post)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):   
--  2016-01-22 Lukasz Burdzanowski
--    Creation OCOMM-364
-----------------------------------------------------------------------------------------------------------------------
  is
    l_request com_rest.t_rest_call;    
  begin
    l_request.l_url := i_url;
    l_request.l_body := i_body;    
    l_request.l_type := i_type;
    return execute_call(l_request);
  end execute_call;
  
   function execute_get(i_url in varchar2)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):   
--  2016-01-22 Lukasz Burdzanowski
--    Creation OCOMM-364
-----------------------------------------------------------------------------------------------------------------------
  is    
  begin    
    return execute_call(i_url, null, c_get);
  end execute_get;
end com_rest;