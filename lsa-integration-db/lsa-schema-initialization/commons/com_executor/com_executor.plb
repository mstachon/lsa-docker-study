create or replace package body com_executor
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-03 Chris Roderick and Lukasz Burdzanowski
--    Creation OCOMM-375
------------------------------------------------------------------------------------------------------------------------
is
  c_task_template constant user_jobs.what%type := '  
  begin
    execute immediate ''#task_command'';
    
    if #is_notify_success then
      com_event_mgr.raise_event(''COM_EXECUTOR_TASK_COMPLETE'', ''Sucessfully executed #task_name (#task_id)'');
    end if;
  exception
    when others then 
      if #is_notify_failure then
        com_event_mgr.raise_event(''COM_EXECUTOR_TASK_FAILED'', ''Failed to execute: #task_name (#task_id) Err: ''||dbms_utility.format_error_stack);
      end if;
  end;';

  procedure execute_task(i_task in t_execution_task)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-03 Chris Roderick and Lukasz Burdzanowski
--    Creation OCOMM-375
------------------------------------------------------------------------------------------------------------------------
  is
    l_job_run_date user_jobs.next_date%type;
    l_job_command user_jobs.what%type;
    
    function generate_task return user_jobs.what%type    
    is
      l_task_command clob;
    begin
      l_task_command := com_template(c_task_template).
        bind('#task_command', i_task.command).
        bind('#task_name', i_task.task_name).
        bind('#task_id', sys_guid()).
        bind('#is_notify_success', com_util.boolean_to_literal(i_task.notify_success)).
        bind('#is_notify_failure', com_util.boolean_to_literal(i_task.notify_failure)).
        generate;
      
      com_assert.arg_length_less_equal(dbms_lob.getlength(l_task_command), 4000, 
        'Generated task is '||dbms_lob.getlength(l_task_command)||' characters, max allowed is '||4000);
      return l_task_command;
    end generate_task;    
    
    procedure schedule_transactional
    is
      l_job_no user_jobs.job%type;
      l_job_cnt binary_integer;
    begin
      select count(*) into l_job_cnt from user_jobs where what = l_job_command;  
      if l_job_cnt = 0 then   
        dbms_job.submit(
          job => l_job_no,
          what => l_job_command,
          next_date => l_job_run_date
        );      
      end if;
    end schedule_transactional;
    
    procedure schedule_non_transactional
    is
      l_job_no user_jobs.job%type;
      l_job_cnt binary_integer;
    begin
      select count(*) into l_job_cnt from user_scheduler_jobs where job_action = l_job_command;  
      if l_job_cnt = 0 then
        dbms_scheduler.create_job(
          job_name=> i_task.task_name,
          job_type=>'PLSQL_BLOCK',
          job_action=> l_job_command,
          start_date=> l_job_run_date,
          enabled=>true);        
      end if;
    end schedule_non_transactional;
  
  begin  
    l_job_run_date := sysdate + numtodsinterval(i_task.delay_in_seconds, 'second');
    l_job_command := generate_task;
      
    com_logger.log_entry('DEBUG', l_job_command, true);
    
    if i_task.is_transactional then
      schedule_transactional;  
    else
      schedule_non_transactional;
    end if;
    
  exception
  when others then 
    com_event_mgr.raise_error_event('INTERNAL_ERROR', 
      'Failed to submit job for background execution. Err: '||dbms_utility.format_error_stack);
  end execute_task;
  
  procedure execute_task(
    i_task_command in user_jobs.what%type,
    i_delay_in_seconds in number default 0,
    i_is_transactional in boolean default true)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-03 Chris Roderick and Lukasz Burdzanowski
--    Creation OCOMM-375
------------------------------------------------------------------------------------------------------------------------
  is
    l_task com_executor.t_execution_task;
  begin
    com_assert.arg_length_less_equal(i_task_command, 4000, 'Task command can not be longer than 4000 characters.');

    l_task.command := i_task_command;
    l_task.delay_in_seconds := i_delay_in_seconds;    
    l_task.is_transactional := i_is_transactional; 
    
    execute_task(l_task);
  end execute_task;
end com_executor;