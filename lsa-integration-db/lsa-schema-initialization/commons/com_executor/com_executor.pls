create or replace package com_executor
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Dynamically executes given code in a separate session by wrapping it in a call to dbms_job or dbms_scheduler.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-13 Eve Fortescue-Beck
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 -  OCOMM-404
--  2016-02-03 Chris Roderick and Lukasz Burdzanowski
--    Creation OCOMM-375
------------------------------------------------------------------------------------------------------------------------
is
  
  type t_execution_task is record (
    task_name         varchar2(30) := 'BACKGROUND_EXECUTOR_TASK',
    command           user_jobs.what%type,
    delay_in_seconds  number,
    notify_success    boolean := true,
    notify_failure    boolean := true,
    is_transactional  boolean := true  
  );  
  
  procedure execute_task(i_task in t_execution_task);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Executes passed task. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_task - task to execute
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if any of the task parameter is invalid
--    INTERNAL_ERROR - on any error
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-03 Chris Roderick and Lukasz Burdzanowski
--    Creation OCOMM-375
------------------------------------------------------------------------------------------------------------------------

  procedure execute_task(
    i_task_command     in user_jobs.what%type,
    i_delay_in_seconds in number default 0,
    i_is_transactional in boolean default true);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Executes passed command as a background task. 
--    By default executes the task immediately within current transaction.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_task_command - command of the task to execute
--    i_delay_in_seconds - delay of the execution in seconds; default is 0
--    i_is_transactional - defines if the execution is transactional; default is true
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if any of the input arguments is invalid
--    INTERNAL_ERROR - on any other error
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-03 Chris Roderick and Lukasz Burdzanowski
--    Creation OCOMM-375
------------------------------------------------------------------------------------------------------------------------

end com_executor;