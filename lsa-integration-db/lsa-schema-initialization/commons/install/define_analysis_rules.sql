------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Defines the initial set of COMMONS Code and Schema Analysis Rules.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-05 Jose Rolland
--    Updated rule name chk_singular_tabs to chk_singular_table_names OCOMM-420
--  2014-07-23  Chris Roderick
--    Added schema analysis rules
--  2013-11-22  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
begin
  com_code_analyser.reg_code_analysis_rule (
    'Package Body Length', com_code_analyser.c_size, 'com_code_analysis_size.analyse_pkg_body_length',true, true, true, 7, 'Refactor into multiple packages'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Global variables in spec', com_code_analyser.c_syntax, 'com_code_analysis_syntax.analyse_global_var_spec',true, true, true, 7, 'Avoid global variables in package specification'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Package Body Module Length', com_code_analyser.c_size, 'com_code_analysis_size.analyse_pkg_body_mod_length',true, true, true, 7, 'Refactor into multiple modules'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Formal parameters names', com_code_analyser.c_format, 'com_code_analysis_format.analyse_formal_param_names',true, true, true, 1, ''
  );
  com_code_analyser.reg_code_analysis_rule (
    'Comments', com_code_analyser.c_format, 'com_code_analysis_format.analyse_comments',true, true, true, 1, 'Use -- for comments instead of /* */'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Constant definition names', com_code_analyser.c_format, 'com_code_analysis_format.analyse_constant_use',true, true, true, 1, 'Constants should begin with c_'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Global variables names', com_code_analyser.c_format, 'com_code_analysis_format.analyse_global_use',true, true, true, 1, 'Global variables should be prefixed with g_'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Formal parameters mode', com_code_analyser.c_syntax, 'com_code_analysis_syntax.analyse_formal_param_mode',true, true, true, 5, 'Always define the mode of formal parameters (in, out, in out)'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Uppercase usage', com_code_analyser.c_format, 'com_code_analysis_format.analyse_uppercase_use',true, true, true, 1, 'All code should be written in lowercase'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Linebreaks before keywords', com_code_analyser.c_format, 'com_code_analysis_format.analyse_linebreaks_use',true, true, true, 1, 'Use line breaks before: from, join, where, case, when, end, and, group by, having, order by, values'
  );
  com_code_analyser.reg_code_analysis_rule (
    'ANSI joins', com_code_analyser.c_syntax, 'com_code_analysis_syntax.analyse_join_use',true, true, true, 5, 'Only ANSI joins should be used'
  );
  com_code_analyser.reg_code_analysis_rule (
    'DECODE usage', com_code_analyser.c_syntax, 'com_code_analysis_syntax.analyse_decode_use',true, true, true, 5, 'Replace decode with case'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Local variables names', com_code_analyser.c_format, 'com_code_analysis_format.analyse_local_var_names',true, true, true, 1, 'Local variables should be prefixed with l_'
  );
  com_code_analyser.reg_code_analysis_rule (
    'Type definition names', com_code_analyser.c_format, 'com_code_analysis_format.analyse_type_def_names',true, true, true, 1, 'Type definitions should be prefixed with t_'
  );
  com_code_analyser.reg_code_analysis_rule (
    'NVL usage', com_code_analyser.c_syntax, 'com_code_analysis_syntax.analyse_nvl_use',true, true, true, 5, 'replace NVL with coalesce'
  );
  commit;
end;
/

begin
  com_schema_analyser.reg_schema_analysis_rule (
    'Invalid Objects', com_schema_analyser.c_invalid, 'com_schema_analysis_invalid.analyse_invalid_objects',true, true, true, 10, 'Compile / Re-factor / Remove Invalid Objects'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Missing FK Indexes', com_schema_analyser.c_indexes, 'com_schema_analysis_indexes.chk_missing_fk_indexes',true, true, true, 7, 'Ensure FK columns are indexed to avoid performance problems'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Unusable Indexes', com_schema_analyser.c_indexes, 'com_schema_analysis_indexes.chk_unusable_indexes',true, true, true, 7, 'Consider  to rebuild the index'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Missing PK Constraints', com_schema_analyser.c_constraints, 'com_schema_analysis_constraint.chk_missing_pk_constraints',true, true, true, 9, 'Tables should normally have a Primary Key constraint'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Disabled Constraints', com_schema_analyser.c_constraints, 'com_schema_analysis_constraint.chk_disabled_constraints',true, true, true, 6, 'Tables should normally not have disabled constraints, consider enabling or removing the constraint'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Nested Views', com_schema_analyser.c_views, 'com_schema_analysis_views.chk_view_nesting',true, true, true, 6, 'Views should not be built on top of other views, consider flattening the View'
  );
  com_schema_analyser.reg_schema_analysis_rule (
	'Deffered Constraints', com_schema_analyser.c_constraints, 'com_schema_analysis_constraint.chk_deferred_constraints', true, true, true, 3,'Table is using DEFERRABLE or DEFERRED constraints, consider revieweing data and the constraints'
  );
  com_schema_analyser.reg_schema_analysis_rule (
	'Not Validated Constraints', com_schema_analyser.c_constraints, 'com_schema_analysis_constraint.chk_not_validated_constraints', true, true, true, 5, 'Table is using NOT VALIDATED constraint, consider revieweing data and the constraints'
  );
  com_schema_analyser.reg_schema_analysis_rule (
	'Singular tables names', com_schema_analyser.c_naming, 'com_schema_analysis_naming.chk_singular_table_names', true, true, true, 5, 'Tables names are in singular form'
  );
  com_schema_analyser.reg_schema_analysis_rule (
	'Table without any relations', com_schema_analyser.c_tables, 'com_schema_analysis_tables.chk_for_no_relations', true, true, true, 5, 'Table has no incoming or outgoing relations'
  );
  com_schema_analyser.reg_schema_analysis_rule (
  'Views with order by clauses', com_schema_analyser.c_views, 'com_schema_analysis_views.chk_order_by', true, true, true, 6, 'Views should not contain order by clauses'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'IOTs with Secondary Indexes', com_schema_analyser.c_indexes, 'com_schema_analysis_indexes.chk_iot_secondary_indexes',true, true, true, 5, 'IOTs should not contain secondary indexes'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Views with rownum predicates', com_schema_analyser.c_views, 'com_schema_analysis_views.chk_rownum_predicates', true, true, true, 5, 'Views should not contain rownum predicates'
  );
  commit;
end;
/