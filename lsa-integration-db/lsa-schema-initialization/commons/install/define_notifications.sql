------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Defines the initial COMMONS event notifications.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-03  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

--  Example
/*
begin

  for rec in (
    select recipient_id 
    from com_recipients 
    where recipient_name like 'Chris%'
  ) loop  
    begin
--    Register a notification with SMS enabled to these recipients whenever the 
--    FAILED_MANAGE_PARTITION event occurs.
      com_event_mgr.register_event_notification('FAILED_MANAGE_PARTITION', rec.recipient_id, 'Y');
    exception
      when others then
--      If the recipient is already registered for this event, just ignore the exception, otherwise raise it      
        if not com_event_mgr.event_equals('RECIPIENT_OF_EVENT_EXISTS', sqlcode) then
          raise;
      end if;
    end;
  
  end loop;
  
  commit;
end;
/
*/