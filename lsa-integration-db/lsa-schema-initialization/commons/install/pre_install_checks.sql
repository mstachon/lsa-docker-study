------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Check all necessary privileges are found, and that the com_ctx_mgr is pubicly accesible
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-09-23  Chris Roderick
--    Modified the com_ctx_mgr execute privilge check to only be performed if the current user is not
--    the COMMONS user.
--  2011-08-03  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
declare
  l_privilege_object  varchar2(30);
  l_privilege         varchar2(30);
begin

--  Check that the com_ctx_mgr is pubicly accesible
  begin  
  
    if user != 'COMMONS' then 
      l_privilege_object  :=  'COM_CTX_MGR';
      l_privilege         :=  'EXECUTE';
    
      select distinct privilege
      into l_privilege
      from all_tab_privs_recd 
      where table_name = l_privilege_object 
      and privilege = l_privilege 
      and grantor = 'COMMONS' 
      and GRANTEE in ('PUBLIC', user);
    end if;
    
  exception
    when no_data_found then  
      raise_application_error(-20000, 'Required privilege not found: '||l_privilege||' on '||l_privilege_object||' for PUBLIC user or '||user);   
    when others then
      raise;
  
  end;

--  Check that ALL_SOURCE_V is visible  
  begin    
    l_privilege_object  :=  'ALL_SOURCE_V';
    l_privilege         :=  'SELECT';
  
    select distinct privilege
    into l_privilege
    from all_tab_privs_recd 
    where table_name = l_privilege_object 
    and privilege = l_privilege 
    and grantee in ('PUBLIC', user);
    
  exception
    when no_data_found then  
      raise_application_error(-20000, 'Required privilege not found: '||l_privilege||' on '||l_privilege_object||' for PUBLIC user or '||user);   
    when others then
      raise;
  
  end;
  
--  Check that UTL_MAIL is executable  
  begin    
    l_privilege_object  :=  'UTL_MAIL';
    l_privilege         :=  'EXECUTE';
  
    select distinct privilege
    into l_privilege
    from all_tab_privs_recd 
    where table_name = l_privilege_object 
    and privilege = l_privilege 
    and grantee in ('PUBLIC', user);
    
  exception
    when no_data_found then  
      raise_application_error(-20000, 'Required privilege not found: '||l_privilege||' on '||l_privilege_object||' for PUBLIC user or '||user);   
    when others then
      raise;
  
  end;

--  Ensure direct privileges (not only via a role) are available to create a table (e.g. required for history and partition management)
  begin    
    l_privilege  :=  'CREATE TABLE';
  
    select privilege
    into l_privilege
    from user_sys_privs 
    where privilege = l_privilege 
    and username = user;
    
  exception
    when no_data_found then  
      raise_application_error(-20000, 'Required direct privilege not found: '||l_privilege||' for '||user);   
    when others then
      raise;
  
  end;
  
--  Ensure direct privileges are available to create a trigger (e.g. required for history)
  begin    
    l_privilege  :=  'CREATE TRIGGER';
  
    select privilege
    into l_privilege
    from user_sys_privs 
    where privilege = l_privilege 
    and username = user;
    
  exception
    when no_data_found then  
      raise_application_error(-20000, 'Required direct privilege not found: '||l_privilege||' for '||user);   
    when others then
      raise;
  
  end;
  
  --  Check that DBMS_XMLGEN is executable (required by com_formatter) 
  begin    
    l_privilege_object  :=  'DBMS_XMLGEN';
    l_privilege         :=  'EXECUTE';
  
    select distinct privilege
    into l_privilege
    from all_tab_privs_recd 
    where table_name = l_privilege_object 
    and privilege = l_privilege 
    and grantee in ('PUBLIC', user);
    
  exception
    when no_data_found then  
      raise_application_error(-20000, 'Required privilege not found: '||l_privilege||' on '||l_privilege_object||' for PUBLIC user or '||user);   
    when others then
      raise;
  
  end;  
end;
/


  
--select * from user_sys_privs;
--select * from role_sys_privs;
--select * from all_tab_privs_recd where table_name like 'UTL%';