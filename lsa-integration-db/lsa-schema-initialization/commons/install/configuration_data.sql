------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Inserts all of the necessary configuration data, including look-up table values, and internal 
--    event definitions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-03-14  Chris Roderick
--    Added missing events 'PROCESSING_ERROR', 'ILLEGAL_ARGUMENT', and 'INTERNAL_ERROR'. OCOMM-444
--  2014-07-31  Chris Roderick
--    Added event EXCEPTION_DUE_TO_MERGE_ERROR OCOMM-274
--  2013-12-24  Chris Roderick
--    Modified to use com_updater package to register the commons parameters
--  2013-02-12  Chris Roderick
--    Updated version number to be 1.1.0, and added COM_VERSION_ERROR event.
--  2011-07-29  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

--  Register Commons parameters
begin
  com_updater.first_install(com_updater.get_latest_version);
end;
/

--  Internal Event Definitions
--  insert errors referenced in the event_mgr code into the events table
declare
  l_event_id com_event_definitions.event_id%type;

begin
--	Define the session level information which will be stored with the event definitions as part of the captured instrumentation
	dbms_application_info.set_module('COMMONS_SETUP', 'DEFINE_INTERNAL_EVENTS');
	dbms_session.set_identifier('COMMONS_SETUP');

--  com_event_mgr
  com_event_mgr.define_event('NO_ERROR_IDS', com_logger.c_error, 'There are no more event id''s in the range -20,000 to 20,999', 8, 20 ,'Y',l_event_id);
  com_event_mgr.define_event('NO_EVENT', com_logger.c_error, 'Event is not defined!',null,null ,'N',l_event_id);
  com_event_mgr.define_event('EVENT_HAS_SRC_DEPS', com_logger.c_error, 'Event has source dependencies!',null,null ,'Y',l_event_id);
  com_event_mgr.define_event('NO_NOTIFICATIONS', com_logger.c_error, 'There are no matching notifications',null,null ,'N',l_event_id);
  com_event_mgr.define_event('EVENT_EXISTS', com_logger.c_error, 'Event already exists!',null , null,'N',l_event_id);
  com_event_mgr.define_event('RECIPIENT_OF_EVENT_EXISTS', com_logger.c_error,'Recipient already receives notification', null, null ,'N',l_event_id);
  com_event_mgr.define_event('EVENT_OR_RECIPIENT_NOT_FOUND',com_logger.c_error, 'The Event or the Recipient is not found in parent table', null, null,'N',l_event_id);
  com_event_mgr.define_event('ERROR_IN_SET_DML_TIME', com_logger.c_error, 'There was an error when trying to set DML time', null, null,'Y',l_event_id);

--  com_notifier
  com_event_mgr.define_event('FAILED_NOTIFY', com_logger.c_error, 'Could not send notification', null, null ,'N',l_event_id);
  com_event_mgr.define_event('RECIPIENT_EXISTS', com_logger.c_warn, 'The given recipient already exists',null,null ,'N',l_event_id);
  
--  com_partition_mgr
  com_event_mgr.define_event('FAILED_ADD_NEW_PARTITION',com_logger.c_error,'Creation of new partition failed',8,16,'Y',l_event_id);    
  com_event_mgr.define_event('FAILED_PARTITIONED_TABLE_REG',com_logger.c_error,'Registration of time range partitioned table configuration failed',8,16,'Y',l_event_id);    
  com_event_mgr.define_event('FAILED_MANAGE_PARTITION',com_logger.c_error,'Some problem occour during the menagement of some partitions',8,16,'Y',l_event_id);
  
--  com_history_mgr
  com_event_mgr.define_event('HISTORY_TABLE_EXISTS', com_logger.c_error, 'History Table already exists!', null, null,'N',l_event_id);
  com_event_mgr.define_event('NO_PK', com_logger.c_error, 'The Table does not have a Primary Key', null, null,'Y',l_event_id);
  com_event_mgr.define_event('NO_NON_KEY_COLS', com_logger.c_error, 'The Table does not have any non-primary key columns', null, null,'N',l_event_id);
  com_event_mgr.define_event('HIST_STAMP_TRIGGER_FAILED', com_logger.c_error, 'Failed to create history stamp trigger', null, null,'N',l_event_id);
  com_event_mgr.define_event('HIST_WRITE_TRIGGER_FAILED', com_logger.c_error, 'Failed to create history write trigger', null, null,'N',l_event_id);
  com_event_mgr.define_event('HIST_TRIGGERS_FAILED', com_logger.c_error, 'Failed to create history triggers', null, null,'N',l_event_id);  
  com_event_mgr.define_event('INIT_FAILED', com_logger.c_error, 'Cannot initialise history table', null, null,'N',l_event_id);  
  com_event_mgr.define_event('ENABLE_HIST_FAILED', com_logger.c_error, 'Cannot enable history table', null, null,'N',l_event_id);  
  com_event_mgr.define_event('HISTORY_NOT_ENABLED', com_logger.c_error, 'History was not enabled', null, null,'N',l_event_id);  
  com_event_mgr.define_event('DISABLE_HIST_FAILED', com_logger.c_error, 'Cannot disable history table', null, null,'N',l_event_id); 
  com_event_mgr.define_event('HIST_TRIGGER_ERROR', com_logger.c_error, 'History Trigger Error', null, null,'N',l_event_id);
  com_event_mgr.define_event('HIST_TABLE_STRUCT_CHANGED', com_logger.c_error, 'History Table exists with a different structure. Intervention required for History to be re-enabled', null, null,'Y',l_event_id);
  com_event_mgr.define_event('EXCEPTION_DUE_TO_MERGE_ERROR', com_logger.c_error, 'Exception due to merge error ex:"ORA-30926: unable to get a stable set of rows in the source tables"', 8, 20, 'N', l_event_id) ;

--  com_object_mgr
  com_event_mgr.define_event('FAILED_REPAIR_OBJECTS',com_logger.c_error,'Exception raised by procedure on COM_OBJECT_MGR',8,16,'Y',l_event_id);
  com_event_mgr.define_event('FAILED_CHECK_OBJECTS',com_logger.c_error,'Exception raised by procedure on COM_OBJECT_MGR',8,16,'Y',l_event_id);
  com_event_mgr.define_event('FAILED_SCHEMA_ANALYSIS',com_logger.c_error,'Exception raised by procedure on COM_OBJECT_MGR',8,16,'Y',l_event_id);
  com_event_mgr.define_event('REBUILD_INDEX_ERROR',com_logger.c_error,'Rebuild the index it is not possible.'||chr(13)||'Table space name have not been found for this index',8,16,'Y',l_event_id);
  com_event_mgr.define_event('INVALID_OBJECTS_REPORT',com_logger.c_info,'Invalid objects report',8,16,'Y',l_event_id);

--	com_job_mgr
  com_event_mgr.define_event('JOB_REPORT', com_logger.c_info, 'Daily reports on jobs', null, null,'Y',l_event_id);
  com_event_mgr.define_event('JOB_MONITORING_REPORT', com_logger.c_info, 'Monitoring Report for Jobs which run too long', null, null,'Y',l_event_id);

--  com_assert
  com_event_mgr.define_event('ASSERTION_ERROR', com_logger.c_error, 'Error during assertion', null, null, 'Y', l_event_id);
  
--  com_updater
  com_event_mgr.define_event('COM_VERSION_ERROR', com_logger.c_error, 'The commons version is not recognized from the available updates', 8, 20 ,'Y',l_event_id);
  
--  General
  com_event_mgr.define_event('DDL_FAILED', com_logger.c_error, 'There was an error when trying to run ddl', null, null,'Y',l_event_id);
  com_event_mgr.define_event('TABLE_NOT_FOUND', com_logger.c_error, 'Table not found', null, null,'N',l_event_id);
  com_event_mgr.define_event('CONCAT_TOO_LONG', com_logger.c_error, 'Concatenation of values more than 4000 characters', null, null,'Y',l_event_id);
  
-- com_test_runner
	com_event_mgr.define_event('COM_TEST_RUNNER_RUN_FINISHED', com_logger.c_info, 'Test runner finished the run', null, null,'N',l_event_id); 
  com_event_mgr.define_event('COM_TEST_RUNNER_TEST_FAILED', com_logger.c_error, 'COM_TEST_RUNNER Test has failed', null, null ,'N', l_event_id);
  
  com_event_mgr.define_event('NO_DATA_FOUND_ERROR', com_logger.c_error, 'No data was found', null, null ,'N', l_event_id);

-- com_executor  
  com_event_mgr.define_event('COM_EXECUTOR_TASK_COMPLETE', com_logger.c_info, 'Com Executor task has finished', null, null,'N',l_event_id); 
  com_event_mgr.define_event('COM_EXECUTOR_TASK_FAILED', com_logger.c_error, 'Com Executor task has failed', null, null ,'N', l_event_id);
  
--  Generic events
  com_event_mgr.define_event('PROCESSING_ERROR', com_logger.c_error, 'A processing error occurred', 8, 20 ,'N', l_event_id);
  com_event_mgr.define_event('ILLEGAL_ARGUMENT', com_logger.c_error, 'A module has been passed an illegal or inappropriate argument', 8, 20 ,'N', l_event_id);
  com_event_mgr.define_event('INTERNAL_ERROR', com_logger.c_error, 'An unexpected internal error has occured', 8, 20 ,'Y', l_event_id);
end;
/

-- Register a generic Test Suite to contain all tests
begin
  com_test_mgr.add_test_suite('Complete suite'); 
end;
/

-- Register the supported notification filter rules
insert into com_event_notif_filter_rules (filter_rule) values ('=');
insert into com_event_notif_filter_rules (filter_rule) values ('!=');
insert into com_event_notif_filter_rules (filter_rule) values ('>');
insert into com_event_notif_filter_rules (filter_rule) values ('>=');
insert into com_event_notif_filter_rules (filter_rule) values ('<');
insert into com_event_notif_filter_rules (filter_rule) values ('<=');
insert into com_event_notif_filter_rules (filter_rule) values ('LIKE');
insert into com_event_notif_filter_rules (filter_rule) values ('NOT LIKE');
insert into com_event_notif_filter_rules (filter_rule) values ('IN');
insert into com_event_notif_filter_rules (filter_rule) values ('NOT IN');

commit;