------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers range partitioned tables which need to be managed, with the com_partition_mgr.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-04-25  Chris Roderick and Pascal Le Roux
--    Added com_dml_logs partition management
--  2011-09-22  Chris Roderick
--    Added com_app_sessions table partition management
--  2011-07-29  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--  Register the com_logs table, and perform the initial partition management
begin
  com_partition_mgr.reg_time_range_part_table(
      i_table_name          => 'COM_LOGS'
    , i_part_prefix         => 'COM_LOGS'
    , i_no_parts_advance    => 5
    , i_no_parts_keep       => 7
    , i_part_range_int_cnt  => 1
    , i_part_range_int_type => 'MONTH'
  );
  com_partition_mgr.manage_partitions('COM_LOGS');
  
  commit;
end;
/
------------------------------------------------------------------------------------------------------------------------
--  register the com_app_sessions table, and perform the initial partition management
begin
  com_partition_mgr.reg_time_range_part_table(
      i_table_name          => 'COM_APP_SESSIONS'
    , i_part_prefix         => 'COM_APP_SESS'
    , i_no_parts_advance    => 5
    , i_part_range_int_cnt  => 1
    , i_part_range_int_type => 'DAY'
  );
  com_partition_mgr.manage_partitions('COM_APP_SESSIONS');
end;
/


------------------------------------------------------------------------------------------------------------------------
--  register the com_dml_logs table, and perform the initial partition management
begin 
  com_partition_mgr.reg_time_range_part_table(
      i_table_name          => 'COM_DML_LOGS'
    , i_part_prefix         => 'COM_DML_LOGS'
    , i_no_parts_advance    => 5
    , i_part_range_int_cnt  => 1
    , i_part_range_int_type => 'DAY'
  );

  com_partition_mgr.manage_partitions('COM_DML_LOGS');
end;
/