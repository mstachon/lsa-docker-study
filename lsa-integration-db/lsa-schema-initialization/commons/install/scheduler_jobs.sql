-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Inserts all of the necessary configuration data, including look-up table values, and internal 
--    event definitions.
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-19 Jose Rolland
--    Replaced com_job_mgr.c_report_job_name and com_job_mgr.c_monitor_job_name by literals OCOMM-407
--  2014-07-23  Chris Roderick
--    Added schema analysis job - OCOMM-276
--  2011-07-29  Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
--  Create the com_partition_mgr scheduler jobs
begin
  dbms_scheduler.create_job(
    job_name        =>  'com_part_management',
    job_type        =>  'PLSQL_BLOCK',
    job_action      =>  'com_partition_mgr.manage_partitions;',
    start_date      =>   systimestamp + interval '1' minute,
    repeat_interval =>  'freq = hourly; byminute = 0',
    enabled         =>   true,
    comments        =>  'Create and drops partitions of tables managed by the com_partition_mgr'
  );
-- Sets the max run duration  
  dbms_scheduler.set_attribute(
     name          => 'com_part_management',
     attribute     => 'max_run_duration',
     value         => interval '5' minute
  );
  
  commit;
end;
/

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_job_mgr scheduler jobs
begin 
--  Schedules the monitoring job to be run every 5 minutes
  dbms_scheduler.create_job (
    job_name        =>  'COM_JOBS_MONITOR',
    job_type        =>  'PLSQL_BLOCK',
    job_action      =>  'com_job_mgr.monitor;',
    start_date      =>  systimestamp + interval '1' minute,
    repeat_interval =>  'freq = minutely; interval = 5',
    enabled         =>  true,
    comments        =>  'Monitors all the jobs and raise a JOB_REPORT event in case the jobs are running for too much time'
  );
-- Sets the max run duration  
  dbms_scheduler.set_attribute(
     name          => 'COM_JOBS_MONITOR',
     attribute     => 'max_run_duration',
     value         => interval '1' minute
  );
-- Schedules the daily reporting job
  dbms_scheduler.create_job (
    job_name        =>  'COM_JOBS_DAILY_REPORT',
    job_type        =>  'PLSQL_BLOCK',
    job_action      =>  'com_job_mgr.report;',
    start_date      =>  trunc(systimestamp) + interval '3' hour,
    repeat_interval =>  'freq = daily; byhour = 3',
    enabled         =>  true,
    comments        =>  'Produces reports about scheduler job execution'
  );
-- Sets the max run duration  
  dbms_scheduler.set_attribute(
     name          => 'COM_JOBS_DAILY_REPORT',
     attribute     => 'max_run_duration',
     value         => interval '1' minute
  );
  
  commit;
end;
/

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_code_analysis scheduler job
begin
  dbms_scheduler.create_job(
    job_name        =>  'com_code_analysis',
    job_type        =>  'PLSQL_BLOCK',
    job_action      =>  'com_code_analyser.analyse_code_if_changed;',
    start_date      =>   systimestamp + interval '1' minute,
    repeat_interval =>  'freq = daily; byhour = 7; byminute = 0',
    enabled         =>   true,
    comments        =>  'Analyses full code base if any code matching given criteria are changed since the last analysis'
  );
-- Sets the max run duration  
  dbms_scheduler.set_attribute(
     name          => 'com_code_analysis',
     attribute     => 'max_run_duration',
     value         => interval '5' minute
  );
  
  commit;
end;
/

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_schema_analysis scheduler job
begin
  dbms_scheduler.create_job(
    job_name        =>  'com_schema_analysis',
    job_type        =>  'PLSQL_BLOCK',
    job_action      =>  'com_schema_analyser.analyse_schema_if_changed;',
    start_date      =>   systimestamp + interval '1' minute,
    repeat_interval =>  'freq = daily; byhour = 7; byminute = 0',
    enabled         =>   true,
    comments        =>  'Analyses full schema if any object matching given criteria are changed since the last analysis'
  );
-- Sets the max run duration  
  dbms_scheduler.set_attribute(
     name          => 'com_schema_analysis',
     attribute     => 'max_run_duration',
     value         => interval '5' minute
  );
  
  commit;
end;
/
------------------------------------------------------------------------------------------------------------------------
-- OCOMM-323 Scheduler job to run test suite on a regular basis
------------------------------------------------------------------------------------------------------------------------
begin
  dbms_scheduler.create_job (
    job_name           =>  'com_test_runner_run',
    job_type           =>  'PLSQL_BLOCK',
    job_action         =>  'com_test_runner.run_test_suite(''Complete suite'', true);',
    start_date         =>   trunc(systimestamp, 'DD')+interval '7' hour,
    repeat_interval    =>  'freq = daily; byhour = 7',   
    enabled            =>   true,
    job_class          =>  'DEFAULT_JOB_CLASS',
    comments           =>  'Runs the Complete test suite');
-- Sets the max run duration  
  dbms_scheduler.set_attribute(
     name          => 'com_test_runner_run',
     attribute     => 'max_run_duration',
     value         => interval '1' minute
  );
  
  commit;
end;
/

-- OCOMM-301 Create periodic check for invalid objects
begin
  dbms_scheduler.create_job(
    job_name        =>  'COM_CHECK_INVALID_OBJECTS',
    job_type        =>  'PLSQL_BLOCK',
    job_action      =>  'com_object_mgr.check_objects;',
    start_date      =>   systimestamp + interval '1' hour,
    repeat_interval =>  'freq = hourly; byminute = 21',
    enabled         =>   true,
    comments        =>  'Check for invalid objects and try to recompile them. Notify admins if re-compilation failed'
  );

  dbms_scheduler.set_attribute ('COM_CHECK_INVALID_OBJECTS', 'max_run_duration' , interval '1' minute);
  
  commit;
end;
/



