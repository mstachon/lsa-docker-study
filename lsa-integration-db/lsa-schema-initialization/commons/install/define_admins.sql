------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Defines the initial COMMONS administrators.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-03  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

begin
--Examples
--  com_notifier.register_recipient (
--      i_recipient_name        =>  'Chris Roderick'
--    , i_recipient_email       =>  'Chris.Roderick@cern.ch'
--    , i_recipient_sms_number  =>  '+41754112084'
--    , i_recipient_is_admin    =>  'Y'
--  );
  
--  com_notifier.register_recipient (
--      i_recipient_name        =>  'Pascal Le Roux'
--    , i_recipient_email       =>  'Pascal.Le.Roux@cern.ch'
--    , i_recipient_sms_number  =>  '+41764873500'
--    , i_recipient_is_admin    =>  'Y'
--  );
  
  commit;
end;
