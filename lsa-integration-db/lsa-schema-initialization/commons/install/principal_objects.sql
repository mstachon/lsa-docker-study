-----------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2017 copyright
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Creates all of the principal objects (sequences, tables, indexes, types, etc) required by the
--    commons packages.
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-12 Steffen Pade
--    Added an additional column application_token of type varchar2(128) to com_app_sessions
--  2017-01-14 Chris Roderick
--    Modified type vc_vc_map and removed type vc_key_vc_value. OCOMM-443
--  2016-01-10 Chris Roderick
--    Added missing type table_of_integer which was created by Piotr Sowinski for com_anydata_extensions. OCOMM-355
--  2015-10-01 Chris Roderick
--    Extensions to vc_key_vc_value object - adding to_string and equals_comparator methods. OCOMM-352
--  2015-08-03 Lukasz Burdzanowski
--    Added com_test_runner OCOMM-332
--  2015-07-16 Chris Roderick
--    Added types vc_vc_map and vc_key_vc_value for OCOMM-310
--  2014-07-23  Chris Roderick
--    Added schema analysis objects
--  2014-06-24 Chris Roderick and James Menzies
--    Added grant com_parameters to commons, needed for OCOMM-266
--  2014-06-21  Chris Roderick
--    Added synonym all_source_v as per OCOMM-265
--  2014-04-25  Chris Roderick and Pascal Le Roux
--    Added com_dml_logs related objects as per OCOMM-251
--  2013-02-12  Chris Roderick
--    Modified length of com_app_sessions.client_app_name to be varchar2(48) which is consistent 
--    with the max length of a module name (as set via com_big_brother and dbms_application_info).
--  2011-09-22  Chris Roderick
--    Added com_app_sessions related objects
--  2011-07-26  Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------
--  Create a table to store some COMMONS parameters
create table com_parameters (
    parameter_name  varchar2(30)  constraint  comparams_pk primary key
  , parameter_value varchar2(100) constraint  com_params_value_nn not null
  , constraint com_params_name_uc_chk check (parameter_name = upper(parameter_name))
) organization index;

grant select on com_parameters to commons;

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_logger underlying objects

--  sequence required to generate log_ids
create sequence com_logs_log_id_seq;

--  Create a range partitioned table to store log entries, to be manged by the partition manager in 
--  order to add new partitions in advance of when they will be required, and drop older empty 
--  partitions.  An initial partition must be created, and in this case it assumes the range 
--  partition size is 1 month, and that initial partition will span the current month.
declare
--  Establish the timestamp of the start of this month
  l_start_of_this_month timestamp :=  trunc(systimestamp, 'MM');
--  Establish the timestamp of the start of the next month
  l_start_of_next_month timestamp :=  l_start_of_this_month + interval '1' month;
begin

--  Dynamically create the com_logs table with the correct partitioning structure.
  execute immediate q'#create table com_logs (
    log_id                    number(10,0)    constraint com_logs_pk primary key,
    db_user                   varchar2(30),
    utc_timestamp             timestamp,
    log_category              varchar2(10),
    log_entry                 varchar2(4000),
    log_os_user               varchar2(30) default sys_context('userenv', 'os_user'),
    log_client_info           varchar2(64) default sys_context('userenv', 'client_info'),
    log_module                varchar2(48) default sys_context('userenv', 'module'),
    log_action                varchar2(32) default sys_context('userenv', 'action'),
    log_host                  varchar2(30) default sys_context('userenv', 'host'),
    log_ip                    varchar2(15) default sys_context('userenv', 'ip_address'),
    constraint com_logs_cat_chk check (log_category in ('DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL', 'NOTIFY'))
  )#';

end;
/

--  index the log time stamps to allow rapid range searching
create index com_logs_utc_timestamp_i on com_logs(utc_timestamp);

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_app_session_mgr underlying objects

--  Range partitioned table to store application session details, to be manged by the partition 
--  manager in order to add new partitions in advance of when they will be required, and drop older 
--  empty partitions.  An initial partition must be created, and in this case it assumes the range 
--  partition size is 1 day, and that initial partition will span the current day.
declare
--  Establish the timestamp of the start of today 
  l_start_of_this_day timestamp :=  trunc(systimestamp, 'DD');
--  Establish the timestamp of the start of the next day
  l_start_of_next_day timestamp :=  l_start_of_this_day + interval '1' day;
begin

--  Dynamically create the com_app_sessions table with the correct partitioning structure.
  execute immediate q'#  create table com_app_sessions (
    com_app_session_id        number        constraint comappsess_pk primary key,
    start_utc_timestamp       timestamp     default sys_extract_utc(systimestamp) constraint comappsess_st_nn not null,
    end_utc_timestamp         timestamp,
    db_user                   varchar2(30) default sys_context('userenv', 'session_user'),  
    direct_os_user            varchar2(30) default sys_context('userenv', 'os_user'),
    direct_host               varchar2(100) default sys_context('userenv', 'host'),
    client_info               varchar2(64) default sys_context('userenv', 'client_info'),  
    client_os_user            varchar2(30), 
    client_real_user          varchar2(30), 
    client_host               varchar2(30), 
    client_app_name           varchar2(48), 
    application_token         varchar2(128), 
    db_sid                    varchar2(30) default sys_context('userenv', 'sid'),
    db_audsid                 varchar2(30) default sys_context('userenv', 'sessionid')
  )#';

end;
/

--  index the time stamps to allow rapid range searching
create index com_app_sess_st_i on com_app_sessions(start_utc_timestamp);

--  sequence to be used to generate session ids
create sequence com_app_sess_id_seq;

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_dml_logs underlying objects
--  Create a range partitioned table to store dml log entries, to be manged by the partition manager in 
--  order to add new partitions in advance of when they will be required, and drop older empty 
--  partitions.   An initial partition must be created, and in this case it assumes the range 
--  partition size is 1 day, and that initial partition will span the current day.
declare
--  Establish the timestamp of the start of today 
  l_start_of_this_day timestamp :=  trunc(systimestamp, 'DD');
--  Establish the timestamp of the start of the next day
  l_start_of_next_day timestamp :=  l_start_of_this_day + interval '1' day;
begin

--  Dynamically create the com_dml_logs table with the correct partitioning structure.
-- NOTE the com_app_session_id does not have an FK to com_app_sessions due to the need to be able to automatically drop partitions of the com_app_sessions table
  execute immediate q'#create table com_dml_logs (
    com_app_session_id  number,
    create_time_utc     timestamp(6)  constraint com_dml_logs_ctime_nn not null,
    op_type             char(1)       constraint com_dml_logs_op_type_nn not null,
    table_name          varchar2(30)  constraint com_dml_logs_table_nn not null,
    module              varchar2(48)  default sys_context('userenv', 'module'),
    action              varchar2(32)  default sys_context('userenv', 'action'),
    transaction_id      varchar2(48),
    constraint com_dml_logs_optype_chk check (op_type in ('D', 'I', 'U'))
  )#';

end;
/

--add indexes
create index com_dml_logs_app_sess_fk on com_dml_logs (com_app_session_id);
create index com_dml_logs_table_i on com_dml_logs (table_name);
create index com_dml_logs_ctime_i on com_dml_logs (create_time_utc);
create index com_dml_logs_trans_id_i on com_dml_logs (transaction_id);

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_notifier underlying objects

--  Create a sequence for generating recipient_ids.
create sequence com_recipients_ids_seq;

--  Create a table to define notification recipients
create table com_recipients (
    recipient_id              number                    constraint comrcpts_pk primary key
  , recipient_name            varchar2(100)             constraint comrcpts_name_nn not null
  , recipient_email           varchar2(100)             constraint comrcpts_email_nn not null
  , recipient_sms_number      char(12)
  , recipient_is_admin        char(1)       default 'N' constraint comrcpts_isadmin_nn not null  
  , constraint comrcpts_sms_chk check (recipient_sms_number like ('+4175411%'))
  , constraint comrcpts_isadmin_chk check (recipient_is_admin in ('N', 'Y'))
);
comment on table com_recipients is 'This table contains all recipients who can be notified via the com_notifier';
comment on column com_recipients.recipient_sms_number is 'This should be a CERN mobile number of the format ''+4175411____''';
comment on column com_recipients.recipient_is_admin is 'Indicates whether or not this recipient is an administrator';
create unique index comrcpts_email_uk on com_recipients(upper(recipient_email));

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_event_mgr underlying objects

-- Create a sequence for generating event_ids.
create sequence com_event_ids_seq;

-- Table to define user events, with a given name, and category (like log4j levels: INFO, WARN, ERROR, FATAL)
create table com_event_definitions (
    event_id              integer                                                           constraint comevntdefs_pk primary key
  , event_name            varchar2(30)                                                      constraint comevntdefs_name_nn not null
  , event_category        varchar2(30)                                                      constraint comevntdefs_cat_nn not null
  , event_description     varchar2(100)                                                     constraint comevntdefs_desc_nn not null
  , event_notify_admin    char(1)       default 'N'                                         constraint comevntdefs_notifyadmin_nn not null
  , event_sms_start_hour  integer
  , event_sms_end_hour    integer
  , event_create_time_utc timestamp     default sys_extract_utc(systimestamp)               constraint comevntdefs_ctimeutc_nn not null
  , event_creator_db_user varchar2(30)  default sys_context('USERENV', 'SESSION_USER')      constraint comevntdefs_dbusr_nn not null
  , event_creator_os_user varchar2(30)  default sys_context('USERENV', 'OS_USER')           constraint comevntdefs_osusr_nn not null
  , event_creator_client  varchar2(30)  default sys_context('USERENV', 'CLIENT_IDENTIFIER') constraint comevntdefs_client_nn not null
  , event_creator_module  varchar2(48)  default sys_context('USERENV', 'MODULE')            constraint comevntdefs_module_nn not null
  , event_creator_action  varchar2(30)  default sys_context('USERENV', 'ACTION')            constraint comevntdefs_action_nn not null
  , event_creator_host    varchar2(30)  default sys_context('USERENV', 'HOST')              constraint comevntdefs_host_nn not null
  , event_creator_ip      varchar2(30)  default sys_context('USERENV', 'IP_ADDRESS')        constraint comevntdefs_ip_nn not null
  , constraint comevntdefs_name_uk unique (event_name)
  , constraint comevntdefs_name_uc_chk check (event_name = upper(event_name))
  , constraint comevntdefs_cat_chk check (event_category in ('INFO', 'WARN', 'ERROR', 'FATAL'))
  , constraint comevntdefs_err_id_chk check (event_category not in ('ERROR', 'FATAL') or (event_id between -20999 and -20000))
  , constraint comevntdefs_smss_chk check (event_sms_start_hour between 0 and 24)
  , constraint comevntdefs_smse_chk check (event_sms_end_hour between 0 and 24)
  , constraint comevntdefs_notifyadmin_chk check (event_notify_admin in ('N', 'Y'))
);
comment on column com_event_definitions.event_id is 'This uniquely identifes all events, if the event is of the category ERROR or FATAL, then the event_if must be between -20999 and -20000 in order to be able to raise application errors';
comment on column com_event_definitions.event_notify_admin is 'Indicates whether or not administrators should be notified of this event, regardless of whether an explicit notification has been setup';

--  Create a lookup table to join Events to Recipients in a many:many relationship.
create table com_event_notifications (
    event_id			integer
  , recipient_id  integer
  , sms 				  char(1) default 'N'	constraint comevntnot_sms_nn not null
  , constraint comevntnot_pk primary key (event_id, recipient_id)
  , constraint comevntnot_id_fk foreign key (event_id) references com_event_definitions(event_id)
  , constraint comevntnot_recipient_fk foreign key (recipient_id) references com_recipients(recipient_id)
  , constraint comevntnot_sms_chk check (sms in ('Y', 'N'))
) organization index;

--  Index on FK to recipients
create index comevntnot_recipient_fk on com_event_notifications (recipient_id);

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_history_mgr underlying objects
create or replace type com_hist_ignored_cols as varray(1000) of varchar2(30);
/
--  Create a table to store the details of tables which have history enabled
create table com_history_tables (
      table_name            varchar2(30)  constraint com_hist_tabs_pk primary key
    , history_table_name    varchar2(30)  constraint com_hist_tabs_hist_tab_nn not null
    , last_enabled          timestamp(6)
    , last_change           timestamp(6)
    , ignored_columns       com_hist_ignored_cols
    , re_raise_exceptions   char(1) default 'N' constraint com_hist_tabs_reraiseexc_nn not null
    , constraint com_hist_tabs_reraiseexc_chk check (re_raise_exceptions in ('N', 'Y'))    
);
comment on table com_history_tables is 'List of tables with History enabled';
comment on column com_history_tables.table_name is 'The name of the table which has history enabled';
comment on column com_history_tables.history_table_name is 'The name of the table which holds the corresponding history data';
comment on column com_history_tables.last_enabled is 'The time that history was last enabled on this table';
comment on column com_history_tables.last_change is '';

--  Create a type to hold a collection of vachar
create or replace type table_of_varchar as table of varchar2(1000);
/

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_partition_mgr underlying objects
--  Define the table to store the metadata about how to manage the time range tablespaces
create table com_time_range_tablespaces(
  ts_name_prefix          varchar2(19)        constraint com_time_range_tablespaces_pk primary key,
  ts_datafile_size_mb     number              constraint comtrt_datafile_size_ck       check(ts_datafile_size_mb > 0),
  ts_datafile_extend_mb   number              constraint comtrt_datafile_extend_ck     check(ts_datafile_extend_mb > 0),
  ts_datafile_maxsize_mb  number              constraint comtrt_datafile_maxsize_ck    check(ts_datafile_maxsize_mb > 0),
  ts_range_int_cnt        number              constraint comtrt_range_int_cnt_nn       not null,
  ts_range_int_type       varchar2(5)         constraint comtrt_range_int_type_nn      not null,
  no_ts_advance           number              constraint comtrt_no_ts_advance_nn       not null,
  no_ts_keep_online       number              constraint comtrt_no_keep_online_ck      check(no_ts_keep_online > 0),
  no_ts_keep_total        number              constraint comtrt_no_keep_total_ck       check(no_ts_keep_total > 0),
  ts_is_bigfile           char(1) default 'N' constraint comtrt_ts_is_bigfile_ck       check(ts_is_bigfile in ('Y','N')) ,
  constraint comtrt_range_int_cnt_ck    check(ts_range_int_cnt > 0),
  constraint comtrt_range_int_type_ck   check(ts_range_int_type in ('HOUR','DAY','MONTH','YEAR')),
  constraint comtrt_no_ts_advance_ck    check(no_ts_advance > 0)
);

comment on table  com_time_range_tablespaces is 'Defines the time range tablespaces which should be managed by the com_partition_mgr';
comment on column com_time_range_tablespaces.ts_name_prefix is 'The prefix of the tablespace to be managed';
comment on column com_time_range_tablespaces.ts_datafile_size_mb is 'The initial size of a single tablespace datafile in megabytes (MB)';
comment on column com_time_range_tablespaces.ts_datafile_extend_mb is 'The size of the blocks that will extend the datafile after reaching the initial size in MB';
comment on column com_time_range_tablespaces.ts_datafile_maxsize_mb is 'The maximum size of a single tablespace datafile in MB';
comment on column com_time_range_tablespaces.ts_range_int_cnt is 'The number of intervals to be contained in a single tablespace (e.g. 1 DAY, 2 DAY, 1 MONTH)';
comment on column com_time_range_tablespaces.ts_range_int_type is 'The type of intervals to be contained in a single tablespace (one of HOUR, DAY, MONTH, YEAR)';
comment on column com_time_range_tablespaces.no_ts_advance is 'The number of tablespaces to be created in advance';
comment on column com_time_range_tablespaces.no_ts_keep_online is ' The number of tablespaces from a given definition to be kept in ONLINE mode (the mode they are created)';
comment on column com_time_range_tablespaces.no_ts_keep_total is 'The total number of tablespace from a given definition to be kept in the database';
comment on column com_time_range_tablespaces.ts_is_bigfile is 'The prefix of the tablespace to be managed';
/

--  Define the table to store the metadata about how to manage the time range partitioned tables
create table com_time_range_part_tables (
    table_name           varchar2(30)               constraint comtrpt_pk primary key
	, part_prefix          varchar2(19)               constraint comtrpt_pp_nn not null
	, no_parts_advance     number(38)     default 5   constraint comtrpt_npa_nn not null
	, no_parts_keep        number(38)
	, part_range_int_cnt   number(38)                 constraint comtrpt_pric_nn not null
	, part_range_int_type  varchar2(30)               constraint comtrpt_prit_nn not null
	, tablespace_full_name    varchar2(30)
  , dynamic_tablespace_prefix varchar2(19)
  , part_init_size_kb number
  , part_pct_free number
  , constraint comtrpt_prt_chk check (part_range_int_type in ('HOUR', 'DAY', 'MONTH', 'YEAR'))
  , constraint comtrpt_nopartadv_chk check (no_parts_advance >= 0)
  , constraint comtrpt_part_init_size_kb_ck check (part_init_size_kb >= 64)
  , constraint comtrpt_part_pct_free_ck check (part_pct_free >= 0 and part_pct_free <= 99)
  , constraint comtrpt_comtrt_fk foreign key (dynamic_tablespace_prefix) referencing com_time_range_tablespaces(ts_name_prefix)
  , constraint comtrpt_tsprefix_chk check((tablespace_full_name is null and dynamic_tablespace_prefix is null)
        or (tablespace_full_name is not null and dynamic_tablespace_prefix is null) 
        or (tablespace_full_name is null and dynamic_tablespace_prefix is not null))
);

create index comtrpt_comtrt_fk on com_time_range_part_tables(dynamic_tablespace_prefix);

comment on table  com_time_range_part_tables is 'Defines the time range partitioned tables which should be managed by the com_partition_mgr';
comment on column com_time_range_part_tables.table_name is 'The name of the time range partitioned table to be managed';
comment on column com_time_range_part_tables.part_prefix is 'The prefix to be used when generating partition names (the suffix will be _YYYYMMDDHH24)';
comment on column com_time_range_part_tables.no_parts_advance is 'The no of time range partitions to be created in advance of now';
comment on column com_time_range_part_tables.no_parts_keep is 'The no of time range partitions to be kept, with respect to now (a NULL value indicates all partitions should be kept)';
comment on column com_time_range_part_tables.part_range_int_cnt is 'Used to determine the no of intervals (combined with part_range_int_type) to be contained in a single time range partition';
comment on column com_time_range_part_tables.part_range_int_type is 'Used to determine the type of intervals (combined with part_range_int_cnt) to be contained in a single time range partition (e.g. HOUR, DAY, WEEK, MONTH, YEAR)';
/

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_code_analyser underlying objects

create synonym all_source_v for commons.all_source_v;


create table com_code_analysis_categories (
  code_analysis_category varchar2(30) constraint comcac_pk primary key
);
-- Ensure the name of the code analysis category is unique, regardless of case
create unique index comcac_uc_uk on com_code_analysis_categories(upper(code_analysis_category));

-- Sequence to generate unique ids for code analysis rules 
create sequence com_code_analysis_rules_seq;

create table com_code_analysis_rules (
  code_analysis_rule_id       number          constraint comcarules_pk primary key,
  code_analysis_rule_name	    varchar2(30)    constraint comcarules_name_nn not null,
  code_analysis_category	    varchar2(30)    constraint comcarules_cat_nn not null,  
  code_analysis_rule_exec	    varchar2(4000)  constraint comcarules_exec_nn not null, 
  should_bind_obj_owner_like  char(1)         constraint comcarules_bindown_nn not null, 
  should_bind_obj_type_like   char(1)         constraint comcarules_bindobjtype_nn not null, 
  should_bind_obj_name_like   char(1)         constraint comcarules_bindobjname_nn not null, 
  code_analysis_rule_severity integer         constraint comcarules_sev_nn not null, 
  extra_info                  varchar2(100),  
  constraint comcaarules_cat_fk foreign key (code_analysis_category) references com_code_analysis_categories (code_analysis_category),
  constraint comcarules_sev_chk check (code_analysis_rule_severity between 1 and 10),
  constraint comcarules_bindown_chk check (should_bind_obj_owner_like in ('Y', 'N')),
  constraint comcarules_bindobjtype_chk check (should_bind_obj_type_like in ('Y', 'N')),
  constraint comcarules_bindobjname_chk check (should_bind_obj_name_like in ('Y', 'N'))
);
-- Ensure the name of the code analysis case is unique, regardless of case
create unique index comcarules_uc_uk on com_code_analysis_rules(upper(code_analysis_rule_name));
-- Index the FK
create index comcarules_cat_fk on com_code_analysis_rules(code_analysis_category);
-- Populate some Code Analysis Categories
insert into com_code_analysis_categories (code_analysis_category)
values ('Syntax');
insert into com_code_analysis_categories (code_analysis_category)
values ('Formatting');
insert into com_code_analysis_categories (code_analysis_category)
values ('Size');
-- Define the types required for the code analysis
create or replace type code_analysis_result as object (
  code_analysis_rule_name	    varchar2(30),
  code_analysis_category	    varchar2(30),  
  code_analysis_rule_severity integer, 
  extra_info                  varchar2(500),
  code_owner	                varchar2(30),
  code_name	                  varchar2(30),
  code_type	                  varchar2(12),
  line_no	                    integer,
  line_text	                  varchar2(4000),
  line_column_pos             integer
);
/
create or replace type code_analysis_results as table of code_analysis_result;
/
create or replace type code_analysis_rule_result is object (
  code_owner	                varchar2(30),
  code_type	                  varchar2(12),
  code_name	                  varchar2(30),
  line_no	                    integer,
  line_text	                  varchar2(4000),
  line_column_pos             integer,
  extra_info                  varchar2(500)
);
/
create or replace type code_analysis_rule_results is table of code_analysis_rule_result;
/

create sequence com_code_analysis_run_id_seq;

create table com_code_analysis_runs  (
  code_analysis_run_id      number      constraint comcodeanalysisruns_pk primary key,
  code_analysis_start_time  timestamp,
  code_analysis_end_time    timestamp
);

create table com_code_analysis_run_results  (
  code_analysis_run_id        number        constraint comcodeanalysisrunres_fk references com_code_analysis_runs (code_analysis_run_id),
  code_analysis_rule_name     varchar2(30), 
	code_analysis_category      varchar2(30), 
	code_analysis_rule_severity number, 
	extra_info                  varchar2(500), 
	code_owner                  varchar2(30), 
	code_name                   varchar2(30), 
	code_type                   varchar2(12), 
	line_no                     number, 
	line_text                   varchar2(4000), 
	line_column_pos             number
);

grant select on com_code_analysis_runs to commons;
grant select on com_code_analysis_run_results to commons;

-----------------------------------------------------------------------------------------------------------------------
--  Create the com_schema_analyser underlying objects

create table com_schema_analysis_categories (	
  schema_analysis_category varchar2(30)	constraint comsac_pk primary key
);

create unique index comsac_uc_uk on com_schema_analysis_categories (upper(schema_analysis_category));

-- Sequence to create ids for schema analysis rules
create sequence com_schema_analysis_rules_seq; 

create table com_schema_analysis_rules (	
  schema_analysis_rule_id       number          constraint comsarules_pk primary key, 
	schema_analysis_rule_name     varchar2(30)    constraint comsarules_name_nn not null, 
	schema_analysis_category      varchar2(30)    constraint comsarules_cat_nn not null, 
	schema_analysis_rule_exec     varchar2(4000)  constraint comsarules_exec_nn not null, 
	should_bind_obj_owner_like    char(1)         constraint comsarules_bindown_nn not null, 
	should_bind_obj_type_like     char(1)         constraint comsarules_bindobjtype_nn not null, 
	should_bind_obj_name_like     char(1)         constraint comsarules_bindobjname_nn not null, 
	schema_analysis_rule_severity number(*,0)     constraint comsarules_sev_nn not null, 
	extra_info                    varchar2(100), 
   constraint comsarules_sev_chk check (schema_analysis_rule_severity between 1 and 10), 
   constraint comsarules_bindown_chk check (should_bind_obj_owner_like in ('Y', 'N')), 
   constraint comsarules_bindobjtype_chk check (should_bind_obj_type_like in ('Y', 'N')), 
   constraint comsarules_bindobjname_chk check (should_bind_obj_name_like in ('Y', 'N')), 
   constraint comsaarules_cat_fk foreign key (schema_analysis_category) references com_schema_analysis_categories (schema_analysis_category)
);

-- Index the FK
create index comsarules_cat_fk on com_schema_analysis_rules (schema_analysis_category);

-- Ensure rule names are unique regardless of case
create unique index comsarules_uc_uk on com_schema_analysis_rules (upper(schema_analysis_rule_name));

insert into com_schema_analysis_categories values ('Naming');
insert into com_schema_analysis_categories values ('Constraints');
insert into com_schema_analysis_categories values ('Indexes');
insert into com_schema_analysis_categories values ('Tables');
insert into com_schema_analysis_categories values ('Views');
insert into com_schema_analysis_categories values ('Invalid');

commit;

create or replace type schema_analysis_result as object (
  schema_analysis_rule_name	    varchar2(30),
  schema_analysis_category	    varchar2(30),  
  schema_analysis_rule_severity integer, 
  extra_info                    varchar2(500),
  schema_name	                  varchar2(30),
  object_name	                  varchar2(30),
  object_type	                  varchar2(30)
);
/
create or replace type schema_analysis_results as table of schema_analysis_result;
/
create or replace type schema_analysis_rule_result force is object (
  schema_name	                varchar2(30),
  object_name	                varchar2(30),
  object_type	                varchar2(30),
  extra_info                  varchar2(500),
  severity_factor             number
);
/
create or replace type schema_analysis_rule_results is table of schema_analysis_rule_result;
/

create sequence com_schema_analysis_run_id_seq;

create table com_schema_analysis_runs  (
  schema_analysis_run_id      number      constraint comschemaanalysisruns_pk primary key,
  schema_analysis_start_time  timestamp,
  schema_analysis_end_time    timestamp
);

create table com_schema_analysis_results  (
  schema_analysis_run_id        number        constraint comschemaanalysisrunres_fk references com_schema_analysis_runs (schema_analysis_run_id),
  schema_analysis_rule_name     varchar2(30), 
	schema_analysis_category      varchar2(30), 
	schema_analysis_rule_severity number, 
	extra_info                    varchar2(500), 
	schema_name                   varchar2(30),
  object_name	                  varchar2(30),
  object_type	                  varchar2(30)
);

grant select on com_schema_analysis_runs to commons;
grant select on com_schema_analysis_results to commons;

-----------------------------------------------------------------------------------------------------------------------
--  Create a type to hold a collection of number
create or replace type table_of_number as table of number;
/
-----------------------------------------------------------------------------------------------------------------------
--  Create a type to hold a collection of integers
create or replace type table_of_integer as table of integer;
/
-----------------------------------------------------------------------------------------------------------------------
--  Create a type to hold a collection of timestamps
create or replace type table_of_timestamp as table of timestamp;
/

-- ---------------------------------------------------------------------------------------------------------------------
-- OCOMM-319 Internal type for COM_PERIODS
create or replace type table_of_com_period force as table of com_period;
/

@/opt/oracle/oracle-data/commons/com_period/com_period.pls
@/opt/oracle/oracle-data/commons/com_period/com_period.plb

@/opt/oracle/oracle-data/commons/com_periods/com_periods.pls
@/opt/oracle/oracle-data/commons/com_periods/com_periods.plb

------------------------------------------------------------------------------------------------------------------------
-- OCOMM-329

create or replace type com_strings 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This object type is storing a collection of strings (varchar2), either constructed from a table_of_varchar 
--    or a separated string. It comes with member functions to manipulate this collection of strings (varchar2)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-03 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is object ( 
  l_string_table table_of_varchar
   
 , constructor function com_strings (self in out nocopy com_strings, i_delimited_strings in varchar2, i_delimiter in varchar2 default ',') return self as result
 , map member function sort_key return varchar2 -- MAP function implicitly used when doing select distinct, order by...
 , member function to_table_of_varchar return table_of_varchar -- return com_strings as a table_of_varchar
 , member function to_string(i_delimiter in varchar2 default ',') return varchar2 -- return com_strings as a separated (comma by default) string
 , member function order_asc return com_strings -- return a com_strings ordered ascending
 , member function order_desc return com_strings -- return a com_strings ordered descending
 , member function distincts return com_strings -- return distinct com_strings
 , member function duplicates return com_strings -- return distinct duplicated com_strings
 , member function count return number -- return the com_strings total count
 , member function count_occurrences (i_string in varchar2 default null) return number -- return the count of i_string in com_strings
 , member function contains (i_string in varchar2) return number -- Return 1 if com_strings contains i_string, otherwise  return 0 
 , member function string_at_index (i_index in integer default 1) return varchar2 -- return n th string in com_strings
 , member function length_less_than (i_number in number) return com_strings --Return com_strings with length less than i_number
 , member function length_less_than_equal (i_number in number) return com_strings --Return com_strings with length less than equal i_number
 , member function length_greater_than (i_number in number) return com_strings --Return com_strings with length greater than i_number
 , member function length_greater_than_equal (i_number in number) return com_strings --Return com_strings with length greater than equal i_number
 , member function length_betweens (i_low_number in number, i_high_number in number) return com_strings --Return com_strings with length between i_low_number and i_high_number (included)
 , member function replaces (i_old_string in varchar2, i_new_string in varchar2) return com_strings --Return com_strings where i_old_string is replaced by i_new_string in all com_strings 
 , member function substrs (i_start_position in integer, i_length in integer default null) return com_strings --Return com_strings substr from i_start_position with a length of i_length
 , member function trims (i_string in varchar2 default null) return com_strings --Return com_strings trimed on the left and right with i_string (if null, it will trim leading/trailing spaces)
 , member function ltrims (i_string in varchar2 default null) return com_strings --Return com_strings trimed on the left with i_string (if null, it will trim leading spaces)
 , member function rtrims (i_string in varchar2 default null) return com_strings --Return com_strings trimed on the right with i_string (if null, it will trim trailing spaces)
 , member function ltrim_nums return com_strings --Return com_strings with any numeric character removed on the left
 , member function rtrim_nums return com_strings --Return com_strings with any numeric character removed on the right
 , member function ltrim_chars return com_strings --Return com_strings with any alphabetic character removed on the left
 , member function rtrim_chars return com_strings --Return com_strings with any alphabetic character removed on the right
 , member function alphabetic_only return com_strings --Return com_strings with only alphabetic letters (removing control characters, punctuation or numbers) 
 , member function numeric_only return com_strings --Return com_strings with only numbers (removing control characters, punctuation or letters) 
 , member function alphanumeric_only return com_strings --Return com_strings with only alpha numeric characters (removing control characters and punctuation) 
 , member function lpads (i_padded_length in integer, i_pad_string in varchar2 default ' ') return com_strings --Return com_strings padded on the left with i_padded_length of i_pad_string
 , member function rpads (i_padded_length in integer, i_pad_string in varchar2 default ' ') return com_strings --Return com_strings padded on the right with i_padded_length of i_pad_string
 , member function lowers return com_strings --Return com_strings in lower case
 , member function uppers return com_strings --Return com_strings in upper case
 , member function initcaps return com_strings --Return com_strings in initcap case
 , member function unions (i_com_strings in com_strings) return com_strings --Return com_strings union distinct i_com_strings
 , member function unions_all (i_com_strings in com_strings) return com_strings --Return com_strings union all i_com_strings
 , member function intersects (i_com_strings in com_strings) return com_strings --Return com_strings intersect distinct i_com_strings
 , member function intersects_all (i_com_strings in com_strings) return com_strings --Return com_strings intersect all i_com_strings
 , member function excepts (i_com_strings in com_strings) return com_strings --Return com_strings except distinct i_com_strings
 , member function excepts_all (i_com_strings in com_strings) return com_strings --Return com_strings except all i_com_strings
);
/

------------------------------------------------------------------------------------------------------------------------

create or replace type body com_strings 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-27 Pascal Le Roux
--    Added exception handling for subscript_beyond_count in string_at_index
--  2015-06-04 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  
  constructor function com_strings (self in out nocopy com_strings, i_delimited_strings in varchar2, i_delimiter in varchar2 default ',') return self as result
  is
  l_pattern varchar2(104); 
  begin
    com_assert.is_true(length(i_delimiter) <= 100 , 'The length of the delimiter '||i_delimiter||' should not exceed 100 characters');
    
    l_pattern :='[^'||i_delimiter||']+';
  
    select individual_string bulk collect into self.l_string_table
    from (
      select regexp_substr(i_delimited_strings, l_pattern, 1, level) as individual_string
      from dual
      connect by level <= length(regexp_replace(i_delimited_strings, l_pattern)) + 1
    ) where individual_string is not null;

    return;
  end com_strings;
  
------------------------------------------------------------------------------------------------------------------------

 map member function sort_key return varchar2
 is
 begin
  return self.to_string();
 end sort_key;

------------------------------------------------------------------------------------------------------------------------

  member function to_table_of_varchar return table_of_varchar
  is
  begin
  return self.l_string_table;
  end to_table_of_varchar;

------------------------------------------------------------------------------------------------------------------------

  member function to_string(i_delimiter in varchar2 default ',') return varchar2
  is
  l_result   varchar2(32767);
  begin
  
  select listagg(column_value, i_delimiter) within group (order by rownum) into l_result
  from table(self.l_string_table);
  
  return l_result;
  
  end to_string; 
------------------------------------------------------------------------------------------------------------------------

  member function order_asc return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  order by column_value asc;
  
  return com_strings(l_result);
  
  end order_asc;  

------------------------------------------------------------------------------------------------------------------------

  member function order_desc return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  order by column_value desc;
  
  return com_strings(l_result);
  
  end order_desc;    
  
------------------------------------------------------------------------------------------------------------------------

  member function distincts return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select distinct column_value bulk collect into l_result
  from table(self.l_string_table)
  order by column_value asc;
  
  return com_strings(l_result);
  
  end distincts;

------------------------------------------------------------------------------------------------------------------------

  member function duplicates return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  group by column_value having count(*) > 1;
  
  return com_strings(l_result);
  
  end duplicates;
  
------------------------------------------------------------------------------------------------------------------------
  
  member function count return number
  is
    l_result number;
  begin
    return self.l_string_table.count;
  end count;
  
------------------------------------------------------------------------------------------------------------------------
  
  member function count_occurrences (i_string in varchar2 default null) return number
  is
    l_result number;
  begin
      select count(*) into l_result
      from table(self.l_string_table)
      where column_value = i_string;
    return l_result;
  end count_occurrences;

------------------------------------------------------------------------------------------------------------------------
  
  member function contains (i_string in varchar2) return number
  is
    l_result number;
  begin
  return case when self.count_occurrences(i_string) > 0 then 1 else 0 end;
  end contains;

 
------------------------------------------------------------------------------------------------------------------------
  
  member function string_at_index (i_index in integer default 1) return varchar2
  is
  begin
    return self.l_string_table(i_index);
    exception 
    when subscript_beyond_count then
    return null;
  end string_at_index; 
  
------------------------------------------------------------------------------------------------------------------------
  
  member function length_less_than (i_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) < i_number;
  
  return com_strings(l_result);
  
  end length_less_than;  

------------------------------------------------------------------------------------------------------------------------
  
  member function length_less_than_equal (i_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) <= i_number;
  
  return com_strings(l_result);
  
  end length_less_than_equal;  

------------------------------------------------------------------------------------------------------------------------
  
  member function length_greater_than (i_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select distinct column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) > i_number;
  
  return com_strings(l_result);
  
  end length_greater_than;    

------------------------------------------------------------------------------------------------------------------------
  
  member function length_greater_than_equal (i_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) >= i_number;
  
  return com_strings(l_result);
  
  end length_greater_than_equal; 
  
------------------------------------------------------------------------------------------------------------------------
  
  member function length_betweens (i_low_number in number, i_high_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) between i_low_number and i_high_number;
  
  return com_strings(l_result);
  
  end length_betweens;    

------------------------------------------------------------------------------------------------------------------------
  
  member function replaces (i_old_string in varchar2, i_new_string in varchar2) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select replace(column_value, i_old_string, i_new_string)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end replaces;    

------------------------------------------------------------------------------------------------------------------------
  
  member function substrs (i_start_position in integer, i_length in integer default null) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select substr(column_value, i_start_position, coalesce(i_length, length(column_value)))  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end substrs;    
------------------------------------------------------------------------------------------------------------------------
  
  member function trims (i_string in varchar2 default null) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select rtrim(ltrim(column_value, coalesce(i_string,' ')), coalesce(i_string,' '))   bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end trims;    
------------------------------------------------------------------------------------------------------------------------
  
  member function ltrims (i_string in varchar2 default null) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select ltrim(column_value, coalesce(i_string,' '))  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end ltrims;    

------------------------------------------------------------------------------------------------------------------------
  
  member function rtrims (i_string in varchar2 default null) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select rtrim(column_value, coalesce(i_string,' '))  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end rtrims;    


------------------------------------------------------------------------------------------------------------------------
  
  member function ltrim_nums return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select ltrim(column_value, '0123456789')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end ltrim_nums;    

------------------------------------------------------------------------------------------------------------------------
  
  member function rtrim_nums return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select rtrim(column_value, '0123456789')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end rtrim_nums;    

------------------------------------------------------------------------------------------------------------------------
  
  member function ltrim_chars return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select regexp_replace(column_value,'^[a-zA-Z]*','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end ltrim_chars;    

------------------------------------------------------------------------------------------------------------------------
  
  member function rtrim_chars return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select regexp_replace(column_value,'[a-zA-Z]*$','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end rtrim_chars;    

------------------------------------------------------------------------------------------------------------------------
  
  member function alphabetic_only return com_strings
  is
  l_result  table_of_varchar;
  begin
  select regexp_replace(column_value,'[^a-zA-Z]','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end alphabetic_only;    

------------------------------------------------------------------------------------------------------------------------
  
  member function numeric_only return com_strings
  is
  l_result  table_of_varchar;
  begin
  select regexp_replace(column_value,'[^0123456789]','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end numeric_only;    

------------------------------------------------------------------------------------------------------------------------
  
  member function alphanumeric_only return com_strings
  is
  l_result  table_of_varchar;
  begin
  select regexp_replace(column_value,'[^0123456789a-zA-Z]','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end alphanumeric_only;    
------------------------------------------------------------------------------------------------------------------------
  
  member function lpads (i_padded_length in integer, i_pad_string in varchar2 default ' ') return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select lpad(column_value, i_padded_length, i_pad_string)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end lpads;   

------------------------------------------------------------------------------------------------------------------------
  
  member function rpads (i_padded_length in integer, i_pad_string in varchar2 default ' ') return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select rpad(column_value, i_padded_length, i_pad_string)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end rpads;   

------------------------------------------------------------------------------------------------------------------------
  
  member function lowers return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select lower(column_value)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end lowers;   

------------------------------------------------------------------------------------------------------------------------
  
  member function uppers return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select upper(column_value)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end uppers;  

------------------------------------------------------------------------------------------------------------------------
  
  member function initcaps return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select initcap(column_value)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end initcaps; 

------------------------------------------------------------------------------------------------------------------------
  
  member function unions (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset union distinct i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end unions;      

------------------------------------------------------------------------------------------------------------------------
  
  member function unions_all (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset union all i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end unions_all;      

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset intersect distinct i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end intersects; 

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects_all (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset intersect all i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end intersects_all; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset except distinct i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end excepts; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts_all (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset except all i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end excepts_all; 
  
end;
/

------------------------------------------------------------------------------------------------------------------------
-- OCOMM-328 

create or replace type com_numbers 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This object type is storing a collection of numbers, either constructed from a table_of_number 
--    or a separated string of numbers. It comes with member functions to maipulating this collection of numbers
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is object (
   l_number_table table_of_number
   
 , constructor function com_numbers (self in out nocopy com_numbers, i_delimited_numbers in varchar2, i_delimiter in varchar2 default ',') return self as result
 , map member function sort_key return varchar2 -- MAP function implicitly used when doing select distinct, order by...
 , member function to_table_of_number return table_of_number -- return com_numbers as a table_of_number
 , member function to_string(i_delimiter in varchar2 default ',') return varchar2 -- return com_numbers as a separated (comma by default) string
 , member function order_asc return com_numbers -- return a com_numbers ordered ascending
 , member function order_desc return com_numbers -- return a com_numbers ordered descending
 , member function distincts return com_numbers -- return distinct com_numbers
 , member function duplicates return com_numbers -- return the distinct duplicated com_numbers
 , member function count return number -- -- return the com_numbers total count
 , member function count_occurrences (i_number in number default null) return number -- return the count of i_number in com_numbers
 , member function contains (i_number in number) return number -- Return 1 if com_numbers contains i_number, otherwise  return 0 
 , member function maxi return number -- return biggest number
 , member function mini return number -- return smallest number
 , member function average return number -- return the average of the com_numbers
 , member function less_than (i_number in number) return com_numbers --Return com_numbers less than i_number
 , member function less_than_equal (i_number in number) return com_numbers --Return com_numbers less than equal i_number
 , member function greater_than (i_number in number) return com_numbers --Return com_numbers greater than i_number
 , member function greater_than_equal (i_number in number) return com_numbers --Return com_numbers greater than equal i_number
 , member function betweens (i_low_number in number, i_high_number in number) return com_numbers --Return com_numbers between i_low_number and i_high_number (included)
 , member function unions (i_com_numbers in com_numbers) return com_numbers --Return com_numbers union distinct i_com_numbers
 , member function unions_all (i_com_numbers in com_numbers) return com_numbers --Return com_numbers union all i_com_numbers
 , member function intersects (i_com_numbers in com_numbers) return com_numbers --Return com_numbers intersect distinct i_com_numbers
 , member function intersects_all (i_com_numbers in com_numbers) return com_numbers --Return com_numbers intersect all i_com_numbers
 , member function excepts (i_com_numbers in com_numbers) return com_numbers --Return com_numbers except distinct i_com_numbers
 , member function excepts_all (i_com_numbers in com_numbers) return com_numbers --Return com_numbers except all i_com_numbers
);
/

------------------------------------------------------------------------------------------------------------------------

create or replace type body com_numbers
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  
  constructor function com_numbers (self in out nocopy com_numbers, i_delimited_numbers in varchar2, i_delimiter in varchar2 default ',') return self as result
  is
  l_pattern varchar2(104); 

  begin
    com_assert.is_true(length(i_delimiter) <= 100 , 'The length of the delimiter '||i_delimiter||' should not exceed 100 characters');
    
    l_pattern :='[^'||i_delimiter||']+';
  
    select individual_number bulk collect into self.l_number_table
    from (
      select to_number(regexp_substr(i_delimited_numbers, l_pattern, 1, level)) as individual_number
      from dual
      connect by level <= length(regexp_replace(i_delimited_numbers, l_pattern)) + 1
    ) where individual_number is not null;

    return;
  end com_numbers;
  
------------------------------------------------------------------------------------------------------------------------
 
 map member function sort_key return varchar2
 is
 begin
 return self.to_string();
 end sort_key;
 
------------------------------------------------------------------------------------------------------------------------

  member function to_table_of_number return table_of_number
  is
  begin
  
  return self.l_number_table;
  
  end to_table_of_number;

------------------------------------------------------------------------------------------------------------------------

  member function to_string(i_delimiter in varchar2 default ',') return varchar2
  is
  l_result   varchar2(32767);
  begin
  
  select listagg(column_value, i_delimiter) within group (order by rownum) into l_result
  from table(self.l_number_table);
  
  return l_result;
  
  end to_string;
  
------------------------------------------------------------------------------------------------------------------------

  member function order_asc return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  order by column_value asc;
  
  return com_numbers(l_result);
  
  end order_asc;  

------------------------------------------------------------------------------------------------------------------------

  member function order_desc return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  order by column_value desc;
  
  return com_numbers(l_result);
  
  end order_desc;  

------------------------------------------------------------------------------------------------------------------------

  member function duplicates return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  group by column_value having count(*) > 1;
  
  return com_numbers(l_result);
  
  end duplicates; 

------------------------------------------------------------------------------------------------------------------------

  member function distincts return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select distinct column_value bulk collect into l_result
  from table(self.l_number_table)
  order by column_value asc;
  
  return com_numbers(l_result);
  
  end distincts;

------------------------------------------------------------------------------------------------------------------------

  member function count return number
  is
    l_result number;
  begin
    return self.l_number_table.count;
  end count;

------------------------------------------------------------------------------------------------------------------------
  
  member function count_occurrences(i_number in number default null) return number
  is
    l_result number;
  begin
      select count(*) into l_result
      from table(self.l_number_table)
      where column_value = i_number;

    return l_result;
  end count_occurrences;

------------------------------------------------------------------------------------------------------------------------
  
  member function contains (i_number in number) return number
  is
    l_result number;
  begin
      select unique 1 into l_result
      from table(self.l_number_table)
      where column_value = i_number;
   
    return l_result;
    exception
      when no_data_found then
      return 0;
  end contains;
  
------------------------------------------------------------------------------------------------------------------------

  member function maxi return number
  is
  l_result number;
  begin
  
  select max(column_value) into l_result
  from table(self.l_number_table);
  
  return l_result;
  
  end maxi;

------------------------------------------------------------------------------------------------------------------------
  
  member function mini return number
  is
  l_result number;
  begin
  
  select min(column_value) into l_result
  from table(self.l_number_table);
  
  return l_result;
  
  end mini; 

------------------------------------------------------------------------------------------------------------------------
  
  member function average return number
  is
  l_result number;
  begin
  
  select avg(column_value) into l_result
  from table(self.l_number_table);
  
  return l_result;
  
  end average;  

------------------------------------------------------------------------------------------------------------------------
  
  member function less_than (i_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value < i_number;
  
  return com_numbers(l_result);
  
  end less_than;  

------------------------------------------------------------------------------------------------------------------------
  
  member function less_than_equal (i_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value <= i_number;
  
  return com_numbers(l_result);
  
  end less_than_equal;  

------------------------------------------------------------------------------------------------------------------------
  
  member function greater_than (i_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select distinct column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value > i_number;
  
  return com_numbers(l_result);
  
  end greater_than;  

------------------------------------------------------------------------------------------------------------------------
  
  member function greater_than_equal (i_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value >= i_number;
  
  return com_numbers(l_result);
  
  end greater_than_equal;  

------------------------------------------------------------------------------------------------------------------------
  
  member function betweens (i_low_number in number, i_high_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value between i_low_number and i_high_number;
  
  return com_numbers(l_result);
  
  end betweens;    

------------------------------------------------------------------------------------------------------------------------
  
  member function unions (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset union distinct i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end unions;      

------------------------------------------------------------------------------------------------------------------------
  
  member function unions_all (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset union all i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end unions_all;      

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset intersect distinct i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end intersects; 

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects_all (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset intersect all i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end intersects_all; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset except distinct i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end excepts; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts_all (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset except all i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end excepts_all; 
end;
/

------------------------------------------------------------------------------------------------------------------------

create table com_event_notif_filters (
  filter_id               number constraint cenf_pk primary key,
  filter_name             varchar2(30)  constraint cenf_filter_name_nn not null,
  filter_description      varchar2(200) constraint cenf_description_nn not null,
  filter_combination_rule varchar2(3)   default 'AND' constraint cenf_combine_rule_nn not null,
  constraint cenf_combine_rule_chk check (filter_combination_rule in ('AND', 'OR'))
);


create unique index cenf_filter_name_idx on com_event_notif_filters(UPPER(filter_name));


create table com_event_notif_filter_rules(
filter_rule varchar2(10) constraint cenfr_pk primary key,
constraint cenfr_filter_rule_chk check (filter_rule = UPPER(filter_rule))
)
organization index;


create table com_event_notif_filter_values (
  filter_id       number         constraint cenfv_cenf_fk references com_event_notif_filters (filter_id),
  filter_key      varchar2(30)   constraint cenfv_key_chk check (filter_key = UPPER(filter_key)),
  filter_values   varchar2(4000) constraint cenfv_values_nn not null,
  filter_rule     varchar2(10)   constraint cenfv_rule_nn not null,
  constraint cenfv_pk primary key (filter_id, filter_key),
  constraint cenfv_cenfr_fk foreign key (filter_rule) referencing com_event_notif_filter_rules(filter_rule)
);


alter table com_event_notifications add (
  filter_id       number        constraint comevntnot_cenf_fk references com_event_notif_filters (filter_id)
);


create index comevntnot_cenf_fk on com_event_notifications(filter_id);


create sequence com_event_notif_filters_seq nocycle;


create table com_event_domains (
  domain_id number constraint com_event_domains_pk primary key,
  domain_name varchar2(30) constraint com_event_domains_name_nn not null,
  domain_description varchar2(100),
  category_id number
);


create unique index com_event_domain_name_idx on com_event_domains(UPPER(domain_name));


create sequence com_event_domains_seq nocycle;

alter table com_event_definitions add (
  domain_id number constraint com_evnt_def_com_evnt_dom_fk references com_event_domains (domain_id),
  event_filter_view_name varchar2(30)
);


------------------------------------------------------------------------------------------------------------------------
-- OCOMM-443 vc_vc_map
------------------------------------------------------------------------------------------------------------------------
@/opt/oracle/oracle-data/commons/vc_vc_map/vc_vc_map.pls
@/opt/oracle/oracle-data/commons/vc_vc_map/vc_vc_map.plb
/

------------------------------------------------------------------------------------------------------------------------
-- OCOMM-323 Create tables and views holding configuration and results
------------------------------------------------------------------------------------------------------------------------
create table com_test_suites (
  suite_id number constraint com_test_suite_pk primary key,
  suite_name varchar2(128) constraint com_test_suites_name_nn not null,
  parent_suite_id number,
  constraint com_test_suites_name_uk unique (suite_name),  
  constraint com_test_suites_parent_fk foreign key (parent_suite_id) references com_test_suites(suite_id)
);
create index com_test_suites_parent_fk on com_test_suites(parent_suite_id);

create table com_test_packages (
  package_id number constraint com_test_packages_pk primary key,
  package_name varchar2(30) constraint com_test_packages_name_nn not null,
  constraint com_test_packages_pkg_name_uk unique (package_name),
  constraint com_test_packages_chk check (package_name = upper(package_name))  
);

create table com_test_suite_packages (
  suite_id number,
  package_id number,
  constraint com_test_sp_suites_fk foreign key (suite_id) references com_test_suites(suite_id),
  constraint com_test_sp_packages_fk foreign key (package_id) references com_test_packages(package_id),
  constraint com_test_suite_packages_pk primary key (suite_id, package_id)
);
create index com_test_sp_packages_fk on com_test_suite_packages(package_id);

create table com_test_runs (
  run_id number constraint com_test_runs_pk primary key,
  utc_timestamp timestamp default sys_extract_utc(systimestamp) constraint com_test_runs_timestamp_nn not null,
  suite_id number,
  constraint com_test_runs_suites_fk foreign key (suite_id) references com_test_suites(suite_id)
);
create index com_test_runs_suites_fk on com_test_runs(suite_id);

create table com_test_run_results (
  run_result_id number constraint com_test_runs_results_pk primary key,
  run_id number,
  suite_id number,
  package_id number,
  test_name varchar2(30) constraint com_test_runs_res_name_nn not null,
  test_result varchar2(10) 
  constraint com_test_runs_res_result_chk check (test_result in ('SUCCESS', 'FAILURE'))
  constraint com_test_runs_res_result_nn not null,
  utc_timestamp_start timestamp constraint com_test_runs_res_start_nn not null,
  utc_timestamp_end timestamp constraint com_test_runs_res_end_nn not null,
  test_result_message varchar2(4000),    
  constraint com_test_runs_res_run_fk foreign key (run_id) references com_test_runs(run_id),
  constraint com_test_runs_res_suite_fk foreign key (suite_id) references com_test_suites(suite_id),
  constraint com_test_runs_res_package_fk foreign key (package_id) references com_test_packages(package_id)
);
create index com_test_runs_res_run_fk on com_test_run_results(run_id);
create index com_test_runs_res_suite_fk on com_test_run_results(suite_id);
create index com_test_runs_res_package_fk on com_test_run_results(package_id);

create sequence com_test_runner_seq start with 1 increment by 1 nocache nocycle;
create sequence com_test_suites_seq start with 1 increment by 1 nocache nocycle;
create sequence com_test_packages_seq start with 1 increment by 1 nocache nocycle;

------------------------------------------------------------------------------------------------------------------------
-- OCOMM-336 Extend com_event_domains with categories
------------------------------------------------------------------------------------------------------------------------
create table com_event_domain_categories (
  category_id number constraint com_event_domain_categories_pk primary key,
  category_name varchar2(30) constraint cedc_category_name_nn not null, 
	category_description varchar2(100)
);

create unique index cedc_category_name_idx on com_event_domain_categories (upper(category_name));


alter table com_event_domains add constraint ced_category_id_fk foreign key (category_id) references com_event_domain_categories(category_id);
create index ced_category_id_fk on com_event_domains(category_id);


create sequence com_event_domain_categorie_seq;
