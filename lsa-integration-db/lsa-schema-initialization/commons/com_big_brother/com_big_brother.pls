create or replace package com_big_brother authid definer
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Instrumentation tasks, including optional logging
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-13 Jose Rolland
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-394
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of modules/actions
--  2013-09-26  Gkergki Strakosa
--    Added an overloaded version of set_client_app_info to simplify client calls
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  procedure begin_action(
    i_action_name   in  varchar2,
    i_log           in  boolean   default false
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Flags the start of the given action, and thus adds it to the stack within the session.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_action_name
--      The name of the action which has begun. Names longer than 32 bytes are truncated.
--    i_log
--      Flag to specify if the action name should be logged. False by default.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of action names
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure end_action(
    i_log in boolean default false
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Flags the end of the current action, and thus removes it from the stack, and sets the prior 
--    action/null within the session.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log
--      Flag to specify if the end of the action should be logged. False by default.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of action names
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure set_client_app_info(
    i_app_name      in  varchar2,
    i_curr_action   in  varchar2 default null,
    i_log           in  boolean  default false
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Calls set_client_app_info using information from SYS_CONTEXT in order to simplify instrumentation
--    from PL/SQL modules.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_app_name
--      The name of the application which the end user is using in order to interact with the database.
--      Names longer than 48 bytes are truncated.
--    i_curr_action
--      The name of the current action being performed by the client application (if any).  When 
--      provided this will be used to set the session level action, and make a call to begin_action.
--      Names longer than 32 bytes are truncated.
--    i_log
--      Flag to specify if the start of the module/action should be logged. False by default.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of module/action names
--  2013-09-26  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------  

  procedure set_client_app_info (
    i_username    in varchar2, 
    i_app_name    in varchar2, 
    i_host_name   in varchar2, 
    i_ip_address  in varchar2,
    i_curr_action in varchar2 default null,
    i_log         in boolean  default false
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets the com_big_brother_ctx to hold the given fields, and combines some of them to set the
--    module, action and the client_info.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_username
--      The real username of the end user of the database, which may be different to the name of the OS user,
--      especially in a multi-tier environment.
--    i_app_name
--      The name of the application which the end user is using in order to interact with the database.
--      Names longer than 48 bytes are truncated.
--    i_host_name
--      The name of the host on which the end user is running the application, which may be different
--      to the name of the host connected directly to the database in a multi-tier environment.
--    i_ip_address
--      The IP address of the host on which the end user is running the application, which may be 
--      different to the name of the host connected directly to the database in a multi-tier 
--      environment.
--    i_curr_action
--      The name of the current action being performed by the client application (if any).  When 
--      provided this will be used to set the session level action, and make a call to begin_action.
--      Names longer than 32 bytes are truncated.
--    i_log
--      Flag to specify if the start of the module/action should be logged. False by default.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of module/action names
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure clear_client_app_info(
    i_log in boolean default false
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Clears the com_big_brother_ctx information for this session, as well as the module, action 
--    and the client_info.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log
--      Flag to specify if the end of the module/action should be logged. False by default.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of module/action names
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_username 
  return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the value of the username which is currently set in the context com_big_brother_ctx 
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_app_name 
  return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the value of the app_name which is currently set in the context com_big_brother_ctx 
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_host_name
  return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the value of the host_name which is currently set in the context com_big_brother_ctx 
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_ip_address 
  return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the value of the IP address which is currently set in the context com_big_brother_ctx 
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
    
end com_big_brother;