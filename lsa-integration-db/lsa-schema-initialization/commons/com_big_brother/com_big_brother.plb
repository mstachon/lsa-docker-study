create or replace package body com_big_brother 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Instrumentation tasks, including optional logging
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-13 Jose Rolland
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-394
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of modules/actions
--  2013-09-26  Gkergki Strakosa
--    Added an overloaded version of set_client_app_info to simplify client calls
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  type t_stack_rec is record (
    module      varchar2(48),
    action      varchar2(32),
    client_info varchar2(64)  
  );
  
  type t_stack is table of t_stack_rec;
  
  g_stack t_stack := t_stack();
  
  subtype t_module       is  varchar2(48);
  subtype t_action       is  varchar2(32);
  subtype t_client_info  is  varchar2(64);
  
  g_default_name  varchar2(10) := 'UNKNOWN';
  
  function get_action(
    i_action_name in  varchar2
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the action name in the expected length by Oracle.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_action_name
--      The name of the action.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return substr(i_action_name, 1, 32);
  end get_action;
  
  function get_module(
    i_module  in  varchar2
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the module name in the expected length by Oracle.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_module
--      The name of the module.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return  substr(i_module, 1, 48);
  end get_module;
  
  function get_client_info(
    i_client_info  in  varchar2
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the client info in the expected length by Oracle.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_client_info
--      The client info.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return  substr(i_client_info, 1, 48);
  end get_client_info;  
  
  procedure push_on_stack(
    i_module      in  t_module      default null,
    i_client_info in  t_client_info default null,
    i_action      in  t_action
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Pushes instrumentation information in the stack.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_module
--      The module name.
--    i_client_info
--      The client info.
--    i_action
--      The action name.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_module      t_module := get_module(i_module);
    l_client_info t_client_info := get_client_info(i_client_info);
    l_action      t_action := get_action(i_action);
  begin
    if (l_module is null and l_client_info is null and g_stack.count > 0) then
--    add action for current module
      l_module := g_stack(g_stack.last).module;
      l_client_info := g_stack(g_stack.last).client_info;    
    end if;
    
    g_stack.extend;
    g_stack (g_stack.last).module := l_module;
    g_stack (g_stack.last).client_info := l_client_info;
    g_stack (g_stack.last).action := l_action;   
  end push_on_stack;

  procedure begin_action(
    i_action_name   in  varchar2,
    i_log           in  boolean   default false
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of action names
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_action  t_action := coalesce(get_action(i_action_name), g_default_name);
  begin
    push_on_stack(i_action => l_action);
    dbms_application_info.set_action(l_action);
    
    if (i_log) then
      com_logger.log_entry(com_logger.c_info, 'Start action: ' || l_action, true);
    end if;
  end begin_action;
  
  procedure set_oracle_dbms_info(
    i_module      in  varchar2,
    i_client_info in  varchar2,
    i_action      in  varchar2    
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Set Oracle's session level instrumentation by calling dbms_application_info.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_module
--      The module name.
--    i_client_info
--      The client info.
--    i_action
--      The action name.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin

    dbms_application_info.set_module(get_module(i_module), get_action(i_action));
    dbms_application_info.set_client_info(get_client_info(i_client_info));  
  end set_oracle_dbms_info;

  procedure end_action(
    i_log in boolean default false
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of action names
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if(g_stack.count = 0) then
      return;
    elsif g_stack.count = 1 then
      if(i_log) then
        com_logger.log_entry(com_logger.c_info, 'End action: ' || g_stack(g_stack.last).action, true);
        com_logger.log_entry(com_logger.c_info, 'End module: ' || g_stack(g_stack.last).module, true);
      end if;
      g_stack.trim;
      clear_client_app_info;
    else
      if(i_log) then
        com_logger.log_entry(com_logger.c_info, 'End action: ' || g_stack(g_stack.last).action, true);
      end if;
      g_stack.trim;
      set_oracle_dbms_info(g_stack(g_stack.last).module, 
                           g_stack(g_stack.last).client_info, 
                           g_stack(g_stack.last).action
                           );      
    end if;    
  end end_action;  
  
  procedure set_client_app_info(
    i_app_name      in  varchar2,
    i_curr_action   in  varchar2  default null,
    i_log           in  boolean   default false
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of module/action names
--  2013-09-26  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------  
  is
  begin
    set_client_app_info(sys_context('USERENV', 'SESSION_USER'),
                        i_app_name,
                        sys_context('USERENV', 'HOST'),
                        sys_context('USERENV', 'IP_ADDRESS'),
                        i_curr_action,
                        i_log); 
  end set_client_app_info; 
  
  procedure set_client_app_info (
    i_username      in  varchar2, 
    i_app_name      in  varchar2, 
    i_host_name     in  varchar2, 
    i_ip_address    in  varchar2,
    i_curr_action   in  varchar2  default null,
    i_log           in  boolean   default false
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of module/action names
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_username    varchar2(4000) := coalesce(i_username, g_default_name);
    l_app_name    t_module       := coalesce(get_module(i_app_name), g_default_name);
    l_host_name   varchar2(4000) := coalesce(i_host_name, g_default_name);
    l_ip_address  varchar2(4000) := coalesce(i_ip_address, g_default_name);
    l_curr_action t_action       := get_action(i_curr_action);
    
    l_client_info t_client_info := 
      get_client_info(l_username||'/'||sys_context('USERENV', 'OS_USER')||'@'||l_host_name);
  begin   
    commons.com_ctx_mgr.set_client_app_info(l_username, l_app_name, l_host_name, l_ip_address);
    push_on_stack(l_app_name, l_client_info, l_curr_action);
    set_oracle_dbms_info(l_app_name, l_client_info, l_curr_action);
    
    if (i_log) then
      com_logger.log_entry(com_logger.c_info, 'Start module: ' || l_app_name, true);
      if(i_curr_action is not null) then
        com_logger.log_entry(com_logger.c_info, 'Start action: ' || l_curr_action, true);
      end if;
    end if;    
  end set_client_app_info;

  procedure clear_client_app_info(
    i_log in boolean default false
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-16  Gkergki Strakosa
--    Added support for nested actions and logging of module/action names
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if(g_stack.count = 0) then
      set_oracle_dbms_info(null, null, null);
      commons.com_ctx_mgr.clear_client_app_info;
    elsif g_stack.count = 1 then
      if(i_log) then
        com_logger.log_entry(com_logger.c_info, 'End action: ' || g_stack(g_stack.last).action, true);
        com_logger.log_entry(com_logger.c_info, 'End module: ' || g_stack(g_stack.last).module, true);
      end if;
      g_stack.trim;
      set_oracle_dbms_info(null, null, null);
      commons.com_ctx_mgr.clear_client_app_info;
    else
      if(i_log) then
        com_logger.log_entry(com_logger.c_info, 'End action: ' || g_stack(g_stack.last).action, true);
      end if;
      g_stack.trim;
      set_oracle_dbms_info(g_stack(g_stack.last).module, 
                           g_stack(g_stack.last).client_info, 
                           g_stack(g_stack.last).action
                           );
    end if;
  end clear_client_app_info;
  
  function get_username 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return commons.com_ctx_mgr.get_username;
  end get_username;
  
  function get_app_name 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return commons.com_ctx_mgr.get_app_name;
  end get_app_name;
  
  function get_host_name
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return commons.com_ctx_mgr.get_host_name;
  end get_host_name;
  
  function get_ip_address 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return commons.com_ctx_mgr.get_ip_address;
  end get_ip_address;
    
end com_big_brother;