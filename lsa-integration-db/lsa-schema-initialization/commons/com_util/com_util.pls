create or replace package com_util authid definer
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Provides general purpose utility modules.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-04-21  Piotr Sowinski 
--    Modified function get_table_of_timestamps OCOMM-387
--  2016-02-23 Lukasz Burdzanowski
--    Added boolean_to_literal. OCOMM-375
--  2015-11-23  Chris Roderick
--    Modified functions char_to_boolean, boolean_to_char, integer_to_boolean and boolean_to_integer to be 
--    deterministic. OCOMM-363
--  2015-11-15 Chris Roderick
--    Added functions integer_to_boolean and boolean_to_integer. OCOMM-363
--  2015-10-01 Chris Roderick
--    Removed some of the sub_types and constants which can now be found in dedicated packages. OCOMM-348, OCOMM-347
--  2015-07-28  Chris Roderick
--    OCOMM-331 - changed function get_table_of_timestamps to be pipelined
--  2015-05-25 Chris Roderick
--    Added procedure close_cursor_if_open - OCOMM-314
--  2014-06-03  Chris Roderick
--    Added get_table_of_timestamps
--  2014-05-07 Piotr Sowinski
--    Added get_number_array
--  2014-05-02 Gkergki Strakosa
--    Added get_token, char_to_boolean, boolean_to_char.
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  subtype t_timestamp_millis  is  timestamp(3);
  subtype t_timestamp_micros  is  timestamp(6);
  subtype t_timestamp_nanos   is  timestamp(9);
    
  function get_token(
     i_list  in varchar2,
     i_index in number,
     i_delim in varchar2 := ','
  )
  return  varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Tokenizes the given i_list using the i_delim and returns the i_index token
--    Borrowed from: http://glosoli.blogspot.ch/2006/07/oracle-plsql-function-to-split-strings.html
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_list  - The string to tokenize
--    i_index - The number of the token to return
--    i_delim - The delimiter used for tokenizing the string
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-02: Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  function char_to_boolean (
    i_boolean_char in com_types.t_boolean_as_char
  ) 
  return boolean deterministic;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the given char (Y or N) to boolean
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_boolean_char - The char value to be converted to a boolean
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - when the given char value is not Y or N
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-01 Chris Roderick
--    changed input parameter to reference the subtype now defined in com_types. OCOMM-348
--  2014-04-30: Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------  
  
  function boolean_to_char (
    i_boolean in boolean
  ) 
  return com_types.t_boolean_as_char deterministic;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the given boolean inputs to a char Y or N
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_boolean     - The boolean value to be converted to a char
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-01 Chris Roderick
--    changed input parameter to reference the subtype now defined in com_types. OCOMM-348
--  2014-04-30: Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function integer_to_boolean (
    i_boolean_char in integer
  ) 
  return boolean deterministic;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the given integer (1 or 0) to boolean
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_boolean_char - The integer value to be converted to a boolean
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - when the given integer is not 1 or 0
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-15 Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------  
  
  function boolean_to_integer (
    i_boolean in boolean
  ) 
  return com_types.t_boolean_as_char deterministic;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the given boolean inputs to an integer 1 or 0
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_boolean     - The boolean value to be converted to an integer
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-15 Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  
  function interval_to_seconds(
    i_interval in interval day to second
  ) 
  return number;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the provided interval to seconds.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_interval
--      The interval
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  function get_current_timestamp
  return timestamp;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the current timestamp, which is systimestamp if it is not set to a different value 
--    by the current session.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------  
  
  function get_current_utc_timestamp
  return timestamp;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the current timestamp in UTC.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  procedure set_current_timestamp(
    i_current_timestamp in timestamp
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets the current timestamp. This is useful for testing purposes.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_current_timestamp
--      The timestamp to be set as current
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  procedure set_sys_timestamp;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets the current timestamp back to systimestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  function get_param(
    i_param_name in varchar2
  )
  return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the text value of the provided parameter name.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  procedure set_param(
    i_param_name  in  varchar2,
    i_param_value in  varchar2
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Stores the provided text parameter value of the provided parameter name, so that it can be 
--    retrieved later in the session with get_param.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
--    i_param_value
--      The parameter value
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  procedure set_param(
    i_param_name  in  varchar2,
    i_param_value in  number
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Stores the provided numeric parameter value of the provided parameter name, so that it can be 
--    retrieved later in the session with get_num_param.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
--    i_param_value
--      The parameter value
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 

  procedure set_ts_nanos_param(
    i_param_name  in  varchar2,
    i_param_value in  t_timestamp_nanos
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Stores the provided timestamp with nanoseconds precision value of the provided parameter name, 
--    so that it can be retrieved later in the session with get_ts_nanos_param.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
--    i_param_value
--      The timestamp with nanoseconds precision
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  procedure set_ts_millis_param(
    i_param_name  in  varchar2,
    i_param_value in  t_timestamp_millis
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Stores the provided timestamp with milliseconds precision value of the provided parameter name, 
--    so that it can be retrieved later in the session with get_ts_millis_param.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
--    i_param_value
--      The timestamp with milliseconds precision
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  procedure set_ts_micros_param(
    i_param_name  in  varchar2,
    i_param_value in  t_timestamp_micros
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Stores the provided timestamp with microseconds precision value of the provided parameter name, 
--    so that it can be retrieved later in the session with get_ts_micros_param.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
--    i_param_value
--      The timestamp with microseconds precision
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  function get_ts_nanos_param(
    i_param_name  in varchar2
  )
  return t_timestamp_nanos;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the timestamp with nanoseconds precision of the provided parameter name.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  function get_ts_millis_param(
    i_param_name  in varchar2
  )
  return t_timestamp_millis;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the timestamp with milliseconds precision of the provided parameter name.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  function get_ts_micros_param(
    i_param_name  in varchar2
  )
  return t_timestamp_micros;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the timestamp with microseconds precision of the provided parameter name.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  
  function get_num_param(
    i_param_name  in varchar2
  )
  return number;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the numeric value of the provided parameter name.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_param_name
--      The parameter name
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 

  function get_number_array(i_array_size in  number)
	return table_of_number pipelined;        
------------------------------------------------------------------------------------------------------------------------
--    Generates a table of numbers from 1 up to i_array_size (inclusive).
--    Example usage:
--    select * from table(get_number_array(3));
--    Gives the output:
--    1
--    2
--    3
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-06-03  Chris Roderick
--    Formatted to commons conventions
--  2014-05-07  Piotr Sowinski
--    Moved out from time_scale package
--  2004-12-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------ 

  function get_table_of_timestamps (
    i_start_time            in  timestamp,
    i_end_time              in  timestamp,
    i_window_interval_size  in  interval day to second
  )
	return table_of_timestamp pipelined;        
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Generates a table of timestamps in a given time window based on the predefined time interval 
--    Example usage:
--    select * from table(com_util.get_table_of_timestamps(timestamp '2014-06-03 00:00:00', timestamp '2014-06-03 01:00:00', interval '15' minute));
--    Gives the output:
--      2014-06-03 00:00:00.000000000
--      2014-06-03 00:15:00.000000000
--      2014-06-03 00:30:00.000000000
--      2014-06-03 00:45:00.000000000
--    The end of the time window will be never added to the list. 
--    The last value in the list is always the closest calculated intermediate point before the i_end_time value.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_start_time  
--      Start time of the interval. That will be the first timestamp added to the list. 
--    i_end_time    
--      The timestamp is used for determining the end of the time window.
--    i_window_interval_size 
--      The size of the interval to be used for generating the intermediate points.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-28  Chris Roderick
--    OCOMM-331 - changed to pipelined function
--  2014-06-03  Chris Roderick
--    Creation - see OCOMM-263
------------------------------------------------------------------------------------------------------------------------

  procedure close_cursor_if_open(i_cursor_to_close in sys_refcursor);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks if the given cursor is open, and if so, closes it. This was developed out of the need to handle the special
--    exception NO_DATA_NEEDED which may get raised when using pipelined functions 
--    for more info see: http://tkyte.blogspot.fr/2010/04/nodataneeded-something-i-learned.html 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_cursor_to_close - The cursor to be closed if open
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-05-25 Chris Roderick
--    Creation - as per OCOMM-314
------------------------------------------------------------------------------------------------------------------------

  function boolean_to_literal (
    i_boolean in boolean
  ) 
  return varchar2 deterministic;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the given boolean into a literal string: "true" or "false" 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_boolean - The boolean value to convert
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-23 Lukasz Burdzanowski
--    Creation. OCOMM-375
------------------------------------------------------------------------------------------------------------------------

end com_util;