create or replace package body com_util 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Provides general purpose utility modules
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-03-29  Nikolay Tsvetkov
--    Modified function get_table_of_timestamps to handle zero(empty) window intervals - OCOMM-453 
--  2016-04-21  Piotr Sowinski 
--    Modified function get_table_of_timestamps OCOMM-387
--  2016-02-23 Lukasz Burdzanowski
--    Added boolean_to_literal. OCOMM-375
--  2015-11-23  Chris Roderick
--    Modified functions char_to_boolean, boolean_to_char, integer_to_boolean and boolean_to_integer to be 
--    deterministic. OCOMM-363
--  2015-11-15 Chris Roderick
--    Added functions integer_to_boolean and boolean_to_integer. OCOMM-363
--  2015-10-01 Chris Roderick
--    changed argument types and constants in functions char_to_boolean and boolean_to_char.  OCOMM-348
--  2015-07-28  Chris Roderick
--    OCOMM-331 - changed function get_table_of_timestamps to be pipelined
--  2015-05-25 Chris Roderick
--    Added procedure close_cursor_if_open - OCOMM-314
--  2014-06-03  Chris Roderick
--    Added get_table_of_timestamps
--  2014-05-07 Piotr Sowinski
--    Added get_number_array
--  2014-05-02 Gkergki Strakosa
--    Added get_token, char_to_boolean, boolean_to_char.
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  g_current_timestamp timestamp;
  type t_param        is table of varchar2(4000) index by varchar2(4000);
  g_param             t_param;
  g_ts_millis_format  varchar2(30) := 'YYYY-MM-DD HH24:MI:SS.FF3';
  g_ts_micros_format  varchar2(30) := 'YYYY-MM-DD HH24:MI:SS.FF6';
  g_ts_nanos_format   varchar2(30) := 'YYYY-MM-DD HH24:MI:SS.FF9';
  
  function get_token(
     i_list  in varchar2,
     i_index in number,
     i_delim in varchar2 := ','
  )
  return  varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-02: Gkergki Strakosa
--    Creation (Borrowed from: http://glosoli.blogspot.ch/2006/07/oracle-plsql-function-to-split-strings.html)
------------------------------------------------------------------------------------------------------------------------
  is
    l_start_pos number;
    l_end_pos   number;
  begin
     if i_index = 1 then
       l_start_pos := 1;
     else
       l_start_pos := instr(i_list, i_delim, 1, i_index - 1);
       if l_start_pos = 0 then
         return null;
       else
         l_start_pos := l_start_pos + length(i_delim);
       end if;
     end if;
  
     l_end_pos := instr(i_list, i_delim, l_start_pos, 1);
  
     if l_end_pos = 0 then
       return substr(i_list, l_start_pos);
     else
       return substr(i_list, l_start_pos, l_end_pos - l_start_pos);
     end if;
  
  end get_token;
  
  function char_to_boolean (
    i_boolean_char in com_types.t_boolean_as_char
  ) 
  return boolean deterministic
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-15 Chris Roderick
--    added assertion that passed values are in com_constants.c_true, com_constants.c_false
--  2015-10-01 Chris Roderick
--    Simplified code, changed input parameter to reference the subtype now defined in com_types, changed comparison of 
--    input parameter to be against constant in com_constants. OCOMM-348
--  2014-04-30: Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.is_true(i_boolean_char in (com_constants.c_true, com_constants.c_false), 
      'Boolean values as char must be '||com_constants.c_true||' or '||com_constants.c_false||
      ' you passed '||i_boolean_char);
    return i_boolean_char = com_constants.c_true;
  end char_to_boolean;
  
  function boolean_to_char (
    i_boolean in boolean
  ) 
  return com_types.t_boolean_as_char deterministic
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    Simplified code, changed return parameter to reference the subtype now defined in com_types, changed comparison of
--    input parameter to be against constant in com_constants. OCOMM-348
--  2014-04-30: Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return (case when i_boolean then com_constants.c_true else com_constants.c_false end);
  end boolean_to_char;

  function integer_to_boolean (
    i_boolean_char in integer
  ) 
  return boolean deterministic
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-15 Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.is_true(i_boolean_char in (com_constants.c_true, com_constants.c_false), 
      'Boolean values as integers must be '||com_constants.c_true_as_integer||' or '||com_constants.c_false_as_integer||
      ' you passed '||i_boolean_char);
    return i_boolean_char = com_constants.c_true_as_integer;
  end integer_to_boolean;
  
  function boolean_to_integer (
    i_boolean in boolean
  ) 
  return com_types.t_boolean_as_char deterministic
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-15 Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return (case when i_boolean then com_constants.c_true_as_integer else com_constants.c_false_as_integer end);
  end boolean_to_integer;
  
  procedure set_ts_nanos_param(
    i_param_name  in  varchar2,
    i_param_value in  t_timestamp_nanos
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    set_param(i_param_name, to_char(i_param_value, g_ts_nanos_format));  
  end set_ts_nanos_param;
  
  procedure set_ts_micros_param(
    i_param_name  in  varchar2,
    i_param_value in  t_timestamp_micros
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    set_param(i_param_name, to_char(i_param_value, g_ts_micros_format));  
  end set_ts_micros_param;
  
  procedure set_ts_millis_param(
    i_param_name  in  varchar2,
    i_param_value in  t_timestamp_millis
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  is
  begin
    set_param(i_param_name, to_char(i_param_value, g_ts_millis_format));  
  end set_ts_millis_param;
  
  function get_ts_nanos_param(
    i_param_name  in varchar2
  )
  return t_timestamp_nanos
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  is
  begin
    return to_timestamp(g_param(i_param_name), g_ts_nanos_format);
  end get_ts_nanos_param;
  
  function get_ts_millis_param(
    i_param_name  in varchar2
  )
  return t_timestamp_millis
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return to_timestamp(g_param(i_param_name), g_ts_millis_format);
  end get_ts_millis_param;  
  
  function get_ts_micros_param(
    i_param_name  in varchar2
  )
  return t_timestamp_micros
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-29  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  is
  begin
    return to_timestamp(g_param(i_param_name), g_ts_micros_format);
  end get_ts_micros_param;
  
  procedure set_param(
    i_param_name  in  varchar2,
    i_param_value in  varchar2
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    g_param(i_param_name) := i_param_value;
  end set_param;  
  
  procedure set_param(
    i_param_name  in  varchar2,
    i_param_value in  number
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is 
  begin
    set_param(i_param_name, to_char(i_param_value));
  end set_param;
  
  function get_num_param(
    i_param_name in varchar2
  )
  return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return to_number(g_param(i_param_name));
  end get_num_param;  
  
  function get_param(
    i_param_name in varchar2
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return g_param(i_param_name);
  end get_param;

  function interval_to_seconds(
    i_interval in interval day to second
  ) 
  return number 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_no_of_seconds number;
  begin
    select avg(
      extract( day from (i_interval) )*24*60*60+
      extract( hour from (i_interval) )*60*60+
      extract( minute from (i_interval) )*60+
      extract( second from (i_interval))
    ) into l_no_of_seconds 
    from dual;

    return l_no_of_seconds;
  end interval_to_seconds;
  
  function get_current_timestamp
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if(g_current_timestamp is null) then
      return systimestamp;
    else
      return g_current_timestamp;
    end if;
  end get_current_timestamp;
  
  function get_current_utc_timestamp
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return sys_extract_utc(get_current_timestamp);
  end get_current_utc_timestamp;
  
  procedure set_sys_timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    g_current_timestamp := null;
  end set_sys_timestamp;
  
  procedure set_current_timestamp(
    i_current_timestamp in timestamp
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-11  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    g_current_timestamp := i_current_timestamp;
  end set_current_timestamp;
  
  function get_number_array(i_array_size in  number)
  return table_of_number pipelined
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-06-03  Chris Roderick
--    Formatted to commons conventions
--  2014-05-07  Piotr Sowinski
--    Moved out from time_scale package
--  2004-12-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    for i in 1 .. coalesce(i_array_size, 0) loop
      pipe row (i);
    end loop;
  
    return;
  end get_number_array;   
  
  function get_table_of_timestamps (
    i_start_time            in  timestamp,
    i_end_time              in  timestamp,
    i_window_interval_size  in  interval day to second
  )
  return table_of_timestamp pipelined 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-03-29  Nikolay Tsvetkov
--    Handle zero or null window interval size and return empty table in such case - OCOMM-453 
--  2016-04-21  Piotr Sowinski 
--    Replaced floor() with ceil() function as we were missing the last timestamp in certain cases - OCOMM-387
--  2015-07-28  Chris Roderick
--    Changed to pipelined function OCOMM-331
--  2014-06-03  Chris Roderick
--    Creation - see OCOMM-263
------------------------------------------------------------------------------------------------------------------------
  is
    l_timestamps  table_of_timestamp;
  begin  
    for r_ts in (
    with ref_data as (
    select 
      case 
        when com_util.interval_to_seconds(i_window_interval_size) > 0 
        then ceil(com_util.interval_to_seconds(i_end_time - i_start_time) 
              / com_util.interval_to_seconds(i_window_interval_size)) 
        else 0
      end no_intervals_between_st_et
    from dual
    )
    select i_start_time + (rownum-1) * i_window_interval_size ts
    from ref_data
    where no_intervals_between_st_et > 0
    connect by 1=1 and rownum <=no_intervals_between_st_et
    ) loop
    
    pipe row (r_ts.ts);
    
    end loop;
  
  end get_table_of_timestamps;

  procedure close_cursor_if_open(i_cursor_to_close in sys_refcursor)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-05-25 Chris Roderick
--    Creation - as per OCOMM-314
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if i_cursor_to_close%isopen then
      close i_cursor_to_close;
    end if;
  end close_cursor_if_open;
  
  function boolean_to_literal (
    i_boolean in boolean
  ) 
  return varchar2 deterministic
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-23 Lukasz Burdzanowski
--    Creation. OCOMM-375
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return case when i_boolean then 'true' else 'false' end; 
  end boolean_to_literal;  
end com_util;