-- Register a recipient
begin 
  com_notifier.register_recipient('Chris Roderick', 'Chris.Roderick@cern.ch', '+41764872084');
  commit;
end;
/

-- Send a notification by email and SMS to the recipient
declare
  l_recipients      com_notifier.table_of_recipients;
  l_subject	        varchar2(100)                       :=  'Chris'' Com Notifier Test';
  l_message	        varchar2(100)                       :=  'If your reading this, it isn''t failing - it works ;-)';
  l_sender	        varchar2(100)                       :=  'no-reply-commons@devdb10.cern.ch';
  l_from_alias      varchar2(100)                       :=  'Chris Roderick';
  l_importance      com_notifier.t_email_importance	    :=	com_notifier.c_low;
  l_email_req       boolean                             :=  true;
  l_sms_req         boolean                             :=  true;
  l_sms_start_hour  pls_integer;                        
  l_sms_end_hour    pls_integer;
begin
  
-- Gather the recipient data
  select cr.recipient_id, cr.recipient_name, cr.recipient_email, cr.recipient_sms_number
  bulk collect into l_recipients
  from com_recipients cr
  where cr.recipient_name = 'Chris Roderick';
  
  com_notifier.notify(l_recipients, l_subject, l_message, l_sender, l_from_alias, l_importance, l_email_req, l_sms_req);
  commit;
end;
/