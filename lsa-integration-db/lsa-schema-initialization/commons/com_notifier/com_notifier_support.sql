-- Define user events, with a given name, and category (like log4j levels: INFO, WARN, ERROR, FATAL) and store them in a table 
create table com_recipients (
  recipient_id              number        constraint comrcpts_pk primary key,
  recipient_name            varchar2(100) constraint comrcpts_name_nn not null,
  recipient_email           varchar2(100) constraint comrcpts_email_nn not null,
  recipient_sms_number      char(12),
  constraint comrcpts_email_chk check (recipient_sms_number like ('+4176487%'))
);

comment on table com_recipients is 'This table contains all recipients who can be notified via the com_notifier';
comment on column com_recipients.recipient_sms_number is 'This should be a CERN mobile number of the format ''+4176487____''';
create unique index comrcpts_email_uk on com_recipients(upper(recipient_email));

-- Create a sequence for generating recipient_ids.
create sequence com_recipients_ids_seq;

--insert events referenced in the notifier code into the events table
declare
  l_event_id com_event_definitions.event_id%type;

begin
--	Define the session level information which will be stored with the event definitions as part of the captured instrumentation
	dbms_application_info.set_module('COMMONS_SETUP', 'DEFINE_INTERNAL_EVENTS');
	dbms_session.set_identifier('COMMONS_SETUP');

  com_event_mgr.define_event('FAILED_NOTIFY', 'ERROR', 'Could not send notification', null, null ,l_event_id);
  com_event_mgr.define_event('RECIPIENT_EXISTS', 'WARN', 'The given recipient already exists',null,null ,l_event_id);
end;
/