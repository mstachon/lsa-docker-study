create or replace package body com_notifier is
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to manage recipients of notifications ,and send email based 
--    notifications to one or more recipients either immediately ,or within a specificed time window.
--    The notifier should also avoid and warn about attempts to send the same notification in rapid 
--    succession.
--  Scope:
--    This module is intended to be used at the database level - exposed via a grant execute 
--    privilege to each registered commons user (See com_admin package for registration).
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-09 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-410
--  2015-09-09  E. Fortescue-Beck & C. Roderick
--    Updated procedure send_email_as_clob OCCOM-336
--  2015-06-04  Nikolay Tsvetkov
--    Added procedures register_notif_filters, register_notif_filter_values and function get_notif_filter_id.
--    Added procedure add_recpient_to_list and function build_new_recipient_for OCOMM-310
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Added procedure add_recipient and function combine_recipients OCOMM-308
--  2014-05-16  Pascal Le Roux
--    Added i_content_type parameter in notify_with_clob to either send mail as HTML or plain text
--  2014-04-04  Chris Roderick
--   Updated send_email
--  2011-08-01  Chris Roderick
--    Added attribute to register_recipient procedure, which indicates if the recipient is an 
--    administrator or not.
--  2011-03-24  Pascal Le Roux
--    Updated the call to the com_logger.clear_log due to changes in the com_logger package
--  2011-03-11  Chris Roderick
--    Bug fix for sitiuation when provided notification message or subject contained quotes.
--  2011-01-31  Chris Roderick
--    Changed i_sender and i_from_alias defaults in procedure notify
--  2010-12-07  Chris Roderick
--    Added i_is_auton flag to notify and send_email procedures.
--  2010-11-11  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function resolve_recipient_email(i_recipient in com_recipients%rowtype) return varchar2 is
  
  begin
    return i_recipient.recipient_name || ' <' || i_recipient.recipient_email || '>';
  
  end resolve_recipient_email;

  procedure register_recipient(
    i_recipient_name       in com_recipients.recipient_name%type,
    i_recipient_email      in com_recipients.recipient_email%type,
    i_recipient_sms_number in com_recipients.recipient_sms_number%type default null,
    i_recipient_is_admin   in com_recipients.recipient_is_admin%type default 'N'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-01  Chris Roderick
--    Added boolean attribute to indicates if the recipient is an administrator or not.
--  2010-11-11  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is  
  begin
    insert into com_recipients
      (recipient_id, recipient_name, recipient_email, recipient_sms_number, recipient_is_admin)
    values
      (com_recipients_ids_seq.nextval,
       i_recipient_name,
       i_recipient_email,
       i_recipient_sms_number,
       i_recipient_is_admin);  
  exception
    when dup_val_on_index then
      com_event_mgr.raise_event('RECIPIENT_EXISTS',
                                'Recipient already exists with email address ' || i_recipient_email,
                                true);   
  end register_recipient;
 
  procedure add_recipient(
    i_recipients           in out table_of_recipients,
    i_recipient_name       in com_recipients.recipient_name%type,
    i_recipient_email      in com_recipients.recipient_email%type
  )
----------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Creation OCOMM-308
----------------------------------------------------------------------------------------------------
  is
    l_recipient com_recipients%rowtype;
  begin
    
    select null, i_recipient_name, i_recipient_email, null, 'N'
    into l_recipient
    from dual;
    
    i_recipients(i_recipients.count+1) := l_recipient;
    
  end add_recipient; 
  
  procedure add_recipient_to_list(
    i_recipient_list    in out table_of_recipients,
    i_recipient_to_add  in     com_recipients%rowtype
  )
----------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-30  Nikolay Tsvetkov
--    Creation OCOMM-310
----------------------------------------------------------------------------------------------------
  is
  begin
    i_recipient_list(i_recipient_list.count+1) := i_recipient_to_add;
  end add_recipient_to_list;
  
  function build_new_recipient_for(
    i_recipient_name       in com_recipients.recipient_name%type,
    i_recipient_email      in com_recipients.recipient_email%type,
    i_recipient_sms_number in com_recipients.recipient_sms_number%type default null,
    i_recipient_is_admin   in com_recipients.recipient_is_admin%type   default com_constants.c_false,
    i_recipient_id         in com_recipients.recipient_id%type         default null
  )
  return com_recipients%rowtype
----------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-30  Nikolay Tsvetkov
--    Creation OCOMM-310
----------------------------------------------------------------------------------------------------
  is
    l_recipient com_recipients%rowtype;
    l_recipient_is_admin com_recipients.recipient_is_admin%type;
  begin 
      l_recipient_is_admin := i_recipient_is_admin;
      
      select i_recipient_id, i_recipient_name, i_recipient_email, i_recipient_sms_number, l_recipient_is_admin
      into l_recipient
      from dual;
  
    return l_recipient;
  
  end build_new_recipient_for;
     
  function combine_recipients(
    i_recipients_1  in  table_of_recipients,
    i_recipients_2  in  table_of_recipients
  )
  return table_of_recipients
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Creation - OCOMM-308
------------------------------------------------------------------------------------------------------------------------
  is
    l_combined_recipients  table_of_recipients;
  begin

    for i in 1..i_recipients_1.count loop
      l_combined_recipients(l_combined_recipients.count+1) := i_recipients_1(i);
    end loop;
    
    for i in 1..i_recipients_2.count loop
      l_combined_recipients(l_combined_recipients.count+1) := i_recipients_2(i);
    end loop;
    
    return l_combined_recipients;
    
  end combine_recipients;

  procedure send_email_as_clob(i_recipients   in table_of_recipients,
                               i_subject      in varchar2,
                               i_message      in clob,
                               i_sender       in varchar2,
                               i_from_alias   in varchar2 default sys_context('USERENV', 'CURRENT_SCHEMA') || ' Database',
                               i_importance   in t_email_importance default c_normal,
                               i_content_type in t_content_type default c_html)
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sends an email larger then 32kb based on the given parameters to the given recipients 
--    using utl_smtp.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_recipients
--      Recipients to be emailed.
--    i_subject
--      The subject of the notification.
--    i_message
--      The message body.
--    i_sender
--      The email address of the sender.
--    i_importance
--      The importance of the notification. Possible values include c_low ,c_normal ,and c_high.
--    i_content_type
--      The content type of the notification. Possible values include c_html, c_text.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No Known Exceptions
------------------------------------------------------------------------------------------------------------------------
--  2015-09-09  E. Fortescue-Beck & C. Roderick
--    OCCOM-336: Replaced write_data with write_raw_data to avoid double dots appearing in emails. See issue for details
--  2014-05-16  Pascal Le Roux
--    Added content_type parameter to either send mail as HTML or plain text
--  2014-02-11  James Menzies
--    Creation
------------------------------------------------------------------------------------------------------------------------
   is
    c_boundary        constant varchar2(255) default 'a1b2c3d4e3f2g1';
    c_interval        constant number(10) := 20;
    l_amount          number(10);
    c_smtp_server     constant varchar2(200) := 'cernmx.cern.ch';
    l_smtp_conn       utl_smtp.connection;
    l_mail_msg_index  number := 1;
  
    procedure add_mail_attribute(i_header_attribute_name in varchar2, i_header_attribute_value in varchar2) is
    
    begin
      utl_smtp.write_data(l_smtp_conn, i_header_attribute_name || ': ' || i_header_attribute_value || utl_tcp.crlf);
    end add_mail_attribute;
  
  begin
    l_smtp_conn := utl_smtp.open_connection(c_smtp_server);
    utl_smtp.helo(l_smtp_conn, c_smtp_server);
    utl_smtp.mail(l_smtp_conn, i_sender);
    for i in 1 .. i_recipients.count loop
      utl_smtp.rcpt(l_smtp_conn, i_recipients(i).recipient_email);
    end loop;
    
    utl_smtp.open_data(l_smtp_conn);    
    
    for i in 1 .. i_recipients.count loop
      add_mail_attribute('To', resolve_recipient_email(i_recipients(i)));
    end loop;
    add_mail_attribute('From', '"' || i_from_alias || '" <' || i_sender || '>');
    add_mail_attribute('Subject', i_subject);
    add_mail_attribute('X-Priority', i_importance);
    add_mail_attribute('MIME-Version', '1.0');
    utl_smtp.write_data(l_smtp_conn, 'Content-Type: multipart/alternative; boundary="' || c_boundary || ' " ' || utl_tcp.crlf || utl_tcp.crlf);
    utl_smtp.write_data(l_smtp_conn, '--' || c_boundary || utl_tcp.crlf);
    utl_smtp.write_data(l_smtp_conn, 'Content-Type: '||i_content_type||'; charset="iso-8859-1"' || utl_tcp.crlf || utl_tcp.crlf);  
    
    if length(i_message) > c_interval then
    loop
      if l_mail_msg_index + c_interval <= length(i_message) + 1 then
      utl_smtp.write_raw_data(l_smtp_conn, utl_raw.cast_to_raw( substr(i_message, l_mail_msg_index,c_interval)));
      end if;
      l_mail_msg_index := l_mail_msg_index + c_interval;
      exit when l_mail_msg_index + c_interval > length(i_message);
    end loop;
    utl_smtp.write_raw_data(l_smtp_conn, utl_raw.cast_to_raw( substr(i_message, l_mail_msg_index, length(i_message) - l_mail_msg_index +1)));
    else
      utl_smtp.write_raw_data(l_smtp_conn, utl_raw.cast_to_raw( i_message));
    end if;
  
    utl_smtp.write_data(l_smtp_conn, utl_tcp.crlf || utl_tcp.crlf);
    utl_smtp.write_data(l_smtp_conn, '--' || c_boundary || '--' || utl_tcp.crlf);
    
    utl_smtp.close_data(l_smtp_conn);
    utl_smtp.quit(l_smtp_conn);
  
  exception
    when utl_smtp.transient_error or utl_smtp.permanent_error then
      begin
        utl_smtp.quit(l_smtp_conn);
      exception
        when utl_smtp.transient_error or utl_smtp.permanent_error then
          null; -- When the SMTP server is down or unavailable, we don't have a connection to the server. The QUIT call will raise an exception that we can ignore.
      end;
    when others then
      raise;
  end send_email_as_clob;

  procedure send_email(i_recipients        in varchar2,
                       i_subject           in varchar2,
                       i_message           in varchar2,
                       i_sender            in varchar2,
                       i_importance        in t_email_importance default c_high,
                       i_delayed_send_time in timestamp default null,
                       i_is_auton          in boolean default false,
                       i_content_type      in t_content_type default c_html)
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sends an email based on the given parameters to the given recipients using utl_mail via a
--    job submission.  This implies that the sending process is transactional.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_recipients
--      The comma delimited list of recipients (including aliases) to be emailed.
--    i_subject
--      The subject of the notification.
--    i_message
--      The message body.
--    i_sender
--      The email address (including alias if required) of the sender.
--    i_importance
--      The importance of the notification. Possible values include c_low ,c_normal ,and c_high.
--    i_delayed_send_time
--      The timestamp in the future (in local time) at which the email should be sent. If not 
--      provided then the email will be sent immediately.
--    i_is_auton
--      Boolean flag to set the autonomous transaction mode: 
--        'True' meaning that the email will be sent immediately within the current transaction.
--        'False' meaning that the email will only be made if the current transaction is commited.
--    i_content_type
--      The content type of the e-mail. Possible values include c_html, c_text.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No Known Exceptions
------------------------------------------------------------------------------------------------------------------------
--  2016-08-09 Jose Rolland
--    Added i_content_type OCOMM-410
--  2015-11-12  Chris Roderick, Pascal LE Roux
--    Added text/html in utl_mail.send
--  2014-04-04  Chris Roderick
--    Changed call to utl_mail to use HTML instead of plain text
--  2010-12-07  Chris Roderick
--    Bug fix for sitiuation when provided message or subject contain quotes. The fix relies on   
--    these fields not containing the ^ character.
--  2010-12-07  Chris Roderick
--    Added i_is_auton flag
--  2010-11-17  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
   is
    l_job_no   pls_integer;
    l_job_what varchar2(32767);
  
  begin
  
    --  If not autonomous then use a dbms_job to ensure that the email is only sent if transaction is
    --  committed, and at the right time    
    if not i_is_auton or i_delayed_send_time is not null then
    
      --    establish what is to be submitted as a job
      l_job_what := 'utl_mail.send(mime_type=>'''|| i_content_type ||'; charset="us-ascii"'', sender=>''' || i_sender || 
        ''',recipients=>''' || i_recipients || ''',subject=>q''^' || i_subject || '^'',message=>q''^' || i_message || 
        '^'',priority=>' || i_importance || ');';
    
      --    ensure that there is not a pending job for the same thing
      begin
        select job 
        into l_job_no 
        from user_jobs 
        where what = l_job_what;
      
      exception
        when no_data_found then
        
          --        the job does not exist - submit it        
          dbms_job.submit(job => l_job_no, what => l_job_what, next_date => coalesce(i_delayed_send_time, sysdate));
        when others then
          raise;
      end;
    
    else
      utl_mail.send(sender     => i_sender,
                    recipients => i_recipients,
                    subject    => i_subject,
                    message    => i_message,
                    priority   => i_importance,
                    mime_type  => i_content_type ||'; charset="us-ascii"');
    
    end if;  
  end send_email;

  function establish_delayed_send_time(i_start_hour in pls_integer default 0, i_end_hour in pls_integer default 24)
    return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the next possible timestamp when a notification can be sent, given that it can only be 
--    sent between the given hours of the day.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_start_hour
--      Defines the hour of the 24 hour day (in local time) from which a notification can be sent.
--    i_end_hour
--      Defines the hour of the 24 hour day (in local time) up until which a notification can be sent.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    None.
------------------------------------------------------------------------------------------------------------------------
--  2010-12-07  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
   is
    l_delayed_send_time timestamp;
    l_current_hour      pls_integer;
  
  begin
  
    --  Establish the current hour of the day
    l_current_hour := extract(hour from systimestamp);
  
    case
    
      when l_current_hour < i_start_hour then
        l_delayed_send_time := trunc(systimestamp, 'DD') + numtodsinterval(i_start_hour, 'hour');
      
      when l_current_hour > i_end_hour then
        l_delayed_send_time := trunc(systimestamp, 'DD') + numtodsinterval(1, 'day') +
                               numtodsinterval(i_start_hour, 'hour');
      
      else
        l_delayed_send_time := null;
      
    end case;
  
    return l_delayed_send_time;
  
  end establish_delayed_send_time;

  procedure notify(
    i_recipients     in table_of_recipients,
    i_subject        in varchar2,
    i_message        in clob,
    i_sender         in varchar2 default null,
    i_from_alias     in varchar2 default null,
    i_importance     in t_email_importance default c_high,
    i_email_req      in boolean default true,
    i_sms_req        in boolean default true,
    i_sms_start_hour in pls_integer default null,
    i_sms_end_hour   in pls_integer default null,
    i_is_auton       in boolean default false,
    i_content_type   in t_content_type default c_html
  )
------------------------------------------------------------------------------------------------------------------------
--  2016-08-09 Jose Rolland
--    Added i_content_type, defaults of i_sender and i_from_alias handled by private functions - OCOMM-410
--  2014-07-12  Chris Roderick
--    Modified to take a clob as input for the event message instead of varchar2, and send_email_as_clob if
--    message length is > c_max_non_clob_email_length
--  2011-01-31  Chris Roderick
--    Changed i_sender and i_from_alias defaults to be dynamic based on current user and database
--  2010-12-07  Chris Roderick
--    Added i_is_auton flag
--  2010-11-16  Chris Roderick
--    Added implementation based on utl_mail
--  2010-11-11  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
   is
    --  Place holder for a concatenated string of email recipients
    l_email_recipients varchar2(32767);
    --  Place holder for a concatenated string of sms recipients
    l_sms_recipients varchar2(32767);
    l_sms_recipient  varchar2(200);
    l_sender         varchar2(200);
    
    c_max_non_clob_email_length constant  pls_integer :=  3500; -- leaves 500 chars for recipients plus procedure signature etc.
  
    function resolve_recipient_sms_email(i_recipient in com_recipients%rowtype) return varchar2 is   
      c_cern_email_sms_domain constant char(16) := 'mail2sms.cern.ch';    
    begin
      case
        when i_recipient.recipient_sms_number is not null then
          return i_recipient.recipient_name || ' <' || i_recipient.recipient_sms_number || '@' || c_cern_email_sms_domain || '>';
        else
          return null;
      end case;   
    end resolve_recipient_sms_email;
    
    function get_default_sender return varchar2 
    is
    begin
      return 'no-reply-' || sys_context('USERENV', 'DB_NAME') || '@' || sys_context('USERENV', 'DB_DOMAIN');
    end get_default_sender;
    
    function get_default_sender_alias return varchar2
    is
    begin
      return sys_context('USERENV', 'CURRENT_SCHEMA') || ' Database';
    end get_default_sender_alias;
    
  
  begin
    --  Prepare the email sender
    l_sender := coalesce(i_from_alias,get_default_sender_alias) || ' <' || coalesce(i_sender,get_default_sender) || '>';
  
    --  Prepare the email recipients in a comma seperated list
    if i_email_req then  
      if length(i_message) < c_max_non_clob_email_length then    
        for i in 1 .. i_recipients.count loop      
          if i = 1 then
            l_email_recipients := resolve_recipient_email(i_recipients(i));
          else
            l_email_recipients := l_email_recipients || ',' || resolve_recipient_email(i_recipients(i));
          end if;      
        end loop;
      
        send_email(i_recipients => l_email_recipients,
                   i_subject    => i_subject,
                   i_message    => i_message,
                   i_sender     => l_sender,
                   i_importance => i_importance,
                   i_is_auton   => i_is_auton,
                   i_content_type => i_content_type);                  
      else 
        send_email_as_clob(i_recipients, i_subject, i_message, coalesce(i_sender,get_default_sender), 
          coalesce(i_from_alias,get_default_sender_alias), i_importance, i_content_type);       
      end if;    
    end if;
  
    --  Prepare the sms recipients in a comma seperated list
    if i_sms_req then
    
      for i in 1 .. i_recipients.count loop
      
        l_sms_recipient := resolve_recipient_sms_email(i_recipients(i));
        if l_sms_recipient is not null then
        
          if l_sms_recipients is null then
            l_sms_recipients := resolve_recipient_sms_email(i_recipients(i));
          else
            l_sms_recipients := l_sms_recipients || ',' || resolve_recipient_sms_email(i_recipients(i));
          end if;
        
        end if;
      
      end loop;
    
      --    Send an SMS via email to all recipients ,ensuring that the message length does not exceed 160
      if l_sms_recipients is not null then
      
        com_logger.log_entry(com_logger.c_info, 'SMS: ' || l_sms_recipients);
      
        send_email(i_recipients        => l_sms_recipients,
                   i_subject           => i_subject,
                   i_message           => substr(i_message, 1, 160),
                   i_sender            => l_sender,
                   i_importance        => i_importance,
                   i_delayed_send_time => establish_delayed_send_time(i_sms_start_hour, i_sms_end_hour),
                   i_is_auton          => i_is_auton);
      end if;
    
    end if;
  
  exception
    when others then
      com_event_mgr.raise_error_event('FAILED_NOTIFY',
                                'Failed send of subject ' || i_subject || ' to ' || l_email_recipients || ' Cause: ' ||
                                dbms_utility.format_error_stack);
  end notify;
  
  procedure create_notif_filters (
    i_filter_name             in com_event_notif_filters.filter_name%type,
    i_filter_description      in com_event_notif_filters.filter_description%type,
    i_filter_combination_rule in com_event_notif_filters.filter_combination_rule%type  default 'AND'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04  Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_filter_combination_rule   com_event_notif_filters.filter_combination_rule%type;
  begin
    com_assert.not_null(i_filter_name,'The argument i_filter_name is required and it must not be null !');
    com_assert.not_null(i_filter_description,'The argument i_filter_description is required and it must not be null !');
    
    l_filter_combination_rule := i_filter_combination_rule;
    
    insert into com_event_notif_filters (filter_id, filter_name, filter_description, filter_combination_rule)
    values (com_event_notif_filters_seq.nextval, i_filter_name, i_filter_description, l_filter_combination_rule);
  end create_notif_filters;

  function get_notif_filter_id (i_filter_name  in com_event_notif_filters.filter_name%type)
  return com_event_notif_filters.filter_id%type
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04  Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------ 
  is
    l_filter_id   com_event_notif_filters.filter_id%type;
  begin
    select filter_id into l_filter_id
    from com_event_notif_filters
    where filter_name = i_filter_name;
  
    return l_filter_id;
  exception
    when no_data_found then
      com_event_mgr.raise_error_event_no_stack('ASSERTION_ERROR','There is no filter defined with name: '|| i_filter_name ||' !');
  end get_notif_filter_id;
  
  procedure register_notif_filter_values (
    i_filter_id       in com_event_notif_filters.filter_id%type,
    i_filter_key      in com_event_notif_filter_values.filter_key%type,
    i_filter_values   in com_event_notif_filter_values.filter_values%type,
    i_filter_rule     in com_event_notif_filter_values.filter_rule%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04  Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.not_null(i_filter_id,'The argument i_filter_id is required and it must not be null !');
    com_assert.not_null(i_filter_key,'The argument i_filter_key is required and it must not be null !');
    com_assert.not_null(i_filter_values,'The argument i_filter_values is required and it must not be null !');
    com_assert.not_null(i_filter_rule,'The argument i_filter_rule is required and it must not be null !');
    
    insert into com_event_notif_filter_values (filter_id, filter_key, filter_values, filter_rule)
    values (i_filter_id, i_filter_key, i_filter_values, i_filter_rule);
  end register_notif_filter_values;
  

end com_notifier;
