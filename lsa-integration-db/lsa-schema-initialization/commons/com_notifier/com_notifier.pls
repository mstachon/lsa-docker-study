create or replace package com_notifier
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to manage recipients of notifications, and send email based 
--    notifications to one or more recipients either immediately, or within a specificed time window.
--    The notifier should also avoid and warn about attempts to send the same notification in rapid 
--    succession.
--  Scope:
--    This module is intended to be used at the database level - exposed via a grant execute 
--    privilege to each registered commons user (See com_admin package for registration).
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-09 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-410
--  2015-07-16 Chris Roderick
--    Added t_notification_command as part of OCOMM-310
--  2015-06-04  Nikolay Tsvetkov
--    Added procedures register_notif_filters, register_notif_filter_values and function get_notif_filter_id.
--    Added procedure add_recpient_to_list and function build_new_recipient_for OCOMM-310
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Added procedure add_recipient and function combine_recipients OCOMM-308
--  2014-07-12  Chris Roderick
--    Modified the notify procedure to take a clob as input for the message instead of varchar2
--  2014-05-16  Pascal Le Roux
--    Added i_content_type parameter in notify_with_clob to either send mail as HTML or plain text
--  2011-08-01  Chris Roderick
--    Added attribute to register_recipient procedure, which indicates if the recipient is an 
--    administrator or not.
--  2011-03-24  Pascal Le Roux
--    Updated the call to the com_logger.clear_log due to changes in the com_logger package
--  2011-03-11  Chris Roderick
--    Bug fix for sitiuation when provided notification message or subject contained quotes.
--  2011-01-31  Chris Roderick
--    Changed i_sender and i_from_alias defaults in procedure notify
--  2010-12-07  Chris Roderick
--    Added i_is_auton flag to notify and send_email procedures.
--  2010-11-11  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  subtype t_email_importance is pls_integer;

  c_low    constant t_email_importance := 5;
  c_normal constant t_email_importance := 3;
  c_high   constant t_email_importance := 1;

  subtype t_content_type is varchar2(10);
  c_text    constant t_content_type := 'text/plain';
  c_html    constant t_content_type := 'text/html';

  type table_of_recipients is table of com_recipients%rowtype index by pls_integer;
  
  type t_notification_command is record (
    event_name          com_event_definitions.event_name%type,
    event_msg           clob,
    raise_exception     boolean := false,
    is_auton            boolean := false,
    dynamic_recipients  com_notifier.table_of_recipients,
    attach_stack        boolean := true,
    filter_payload      vc_vc_map
  );

  procedure register_recipient(
    i_recipient_name       in com_recipients.recipient_name%type,
    i_recipient_email      in com_recipients.recipient_email%type,
    i_recipient_sms_number in com_recipients.recipient_sms_number%type default null,
    i_recipient_is_admin   in com_recipients.recipient_is_admin%type default 'N'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Takes the given recipient data and inserts into the com_recipients table
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_recipient_name
--      The name of the recipient to be registered.
--    i_recipient_email
--      The email address of the recipient to be registered.
--    i_recipient_sms_number
--      The sms number of the recipient to be registered. Optional.
--    i_recipient_is_admin
--      'N' means the recipient is not an administrator, 'Y' means the recipient is an administrator
--      If the recipient is an administrator, then they will be notified of certain events, 
--      regardless of whether or not notifications have been explicitly configured for the recipient
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    RECIPIENT_EXISTS - When the given recipients email address is already registered
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-01  Chris Roderick
--    Added boolean attribute to indicate if the recipient is an administrator or not.
--  2010-11-11  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure add_recipient(
    i_recipients           in out table_of_recipients,
    i_recipient_name       in com_recipients.recipient_name%type,
    i_recipient_email      in com_recipients.recipient_email%type
  );
------------------------------------------------------------------------------------------------------------------------
-- @DEPRECATED
--  Description:
--    Takes the given recipient data and adds it to the given collection of recipients
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_recipients
--      The collection of recipients to be added to.
--    i_recipient_name
--      The name of the recipient to be added.
--    i_recipient_email
--      The email address of the recipient to be added.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Creation OCOMM-308
------------------------------------------------------------------------------------------------------------------------

  procedure add_recipient_to_list(
    i_recipient_list    in out table_of_recipients,
    i_recipient_to_add  in     com_recipients%rowtype
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Extends a given collection of recipients by adding an additional recipient
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_recipient_list   - The collection of recipients to be added to.
--    i_recipient_to_add - Recipient to be added to the collection
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-30  Nikolay Tsvetkov
--    Creation OCOMM-310
------------------------------------------------------------------------------------------------------------------------
  
  function build_new_recipient_for(
    i_recipient_name       in com_recipients.recipient_name%type,
    i_recipient_email      in com_recipients.recipient_email%type,
    i_recipient_sms_number in com_recipients.recipient_sms_number%type default null,
    i_recipient_is_admin   in com_recipients.recipient_is_admin%type   default com_constants.c_false,
    i_recipient_id         in com_recipients.recipient_id%type         default null
  )
  return com_recipients%rowtype;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Takes the given recipient data and adds it to the given collection of recipients
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_recipient_name - The name of the recipient to be added.
--    i_recipient_email - The email address of the recipient to be added.
--    i_recipient_sms_number - The SMS number of the recipient to be added
--    i_recipient_is_admin - The information whether the recipient is administrator to be added.
--    i_recipient_id - The recipient ID to be added
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-30  Nikolay Tsvetkov
--    Creation OCOMM-310
------------------------------------------------------------------------------------------------------------------------

  function combine_recipients(
    i_recipients_1  in  table_of_recipients,
    i_recipients_2  in  table_of_recipients
  )
  return table_of_recipients;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns a single collection of recipients that contains all recipients from two given collections
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_recipients_1 - The first collection of recipients to be combined.
--    i_recipients_2 - The second collection of recipients to be combined.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions  
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Creation - OCOMM-308
------------------------------------------------------------------------------------------------------------------------


  procedure notify(
    i_recipients     in table_of_recipients,
    i_subject        in varchar2,
    i_message        in clob,
    i_sender         in varchar2 default null,
    i_from_alias     in varchar2 default null,
    i_importance     in t_email_importance default c_high,
    i_email_req      in boolean default true,
    i_sms_req        in boolean default true,
    i_sms_start_hour in pls_integer default null,
    i_sms_end_hour   in pls_integer default null,
    i_is_auton       in boolean default false,
    i_content_type   in t_content_type default c_html
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Send a notification based on the given parameters to the given recipients.
--    Avoid rapid repetitions.
--    Specify a time window within which to send notifications (useful for SMS cases).
--    Log the notifications which have been sent in a dedicated table, using the com_logger package.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_recipients
--      The recipients to be notified.
--    i_subject
--      The subject of the notification.
--    i_message
--      The message body.
--    i_sender
--      The email address of the sender.
--    i_from_alias
--      The alias of the sender.
--    i_importance
--      The importance of the notification. Possible values include c_low, c_normal, and c_high.
--    i_email_req
--      Indicates if an email is required to be sent.
--    i_sms_req  
--      Indicates if an SMS is required to be sent to recipients having a value defined for their 
--      recipient_sms_number attribute.
--    i_sms_start_hour
--      Defines the hour of the 24 hour day (in local time) from which an SMS message can be sent.  
--      Combined with i_sms_end_hour to define a time window within which an SMS message can be sent. 
--      If not defined then an SMS will be sent immediately (providing i_sms_req = true).
--    i_sms_end_hour
--      Defines the hour of the 24 hour day (in local time) up until which an SMS message can be sent. 
--      Combined with i_sms_start_hour to define a time window within which an SMS message can be sent.
--    i_is_auton
--      Boolean flag to set the autonomous transaction mode: 
--        'True' meaning that the notification will be sent immediately within the current transaction.
--        'False' meaning that the notification will only be made if the current transaction is committed.
--    i_content_type
--      The content type of the notification. Possible values include c_html, c_text.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    FAILED_NOTIFY - When a notification could not be sent for any reason
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-09 Jose Rolland
--    Added i_content_type, modify defaults of i_sender and i_from_alias - OCOMM-410
--  2014-07-12  Chris Roderick
--    Modified to take a clob as input for the event message instead of varchar2
--  2011-01-31  Chris Roderick
--    Changed i_sender and i_from_alias defaults to be dynamic based on current user and database
--  2010-12-07  Chris Roderick
--    Added i_is_auton flag
--  2010-11-11  Chris Roderick
--    Specification completed
--  2010-10-05  Zory Zaharieva, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure create_notif_filters (
    i_filter_name             in com_event_notif_filters.filter_name%type,
    i_filter_description      in com_event_notif_filters.filter_description%type,
    i_filter_combination_rule in com_event_notif_filters.filter_combination_rule%type  default 'AND'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Create new notification filter 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_filter_name             - notification filter name (Unique)
--    i_filter_description      - filter description
--    i_filter_combination_rule - rule to specify the logic to be used to combine multiple filter conditions 
--                                (e.g. 'AND' or 'OR')
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04  Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_notif_filter_id (i_filter_name  in com_event_notif_filters.filter_name%type)
  return com_event_notif_filters.filter_id%type;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns filter_id for a given filter name
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_filter_name - notification filter name (Unique)
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises ASSERTION_ERROR when there is no existing filter with this name
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04  Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure register_notif_filter_values (
    i_filter_id       in com_event_notif_filters.filter_id%type,
    i_filter_key      in com_event_notif_filter_values.filter_key%type,
    i_filter_values   in com_event_notif_filter_values.filter_values%type,
    i_filter_rule     in com_event_notif_filter_values.filter_rule%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Adds keys,values and rule operators in order to define predicates to existing notification filter
------------------------------------------------------------------------------------------------------------------------
-- Input Arguments:
--  i_filter_id     - notification filter ID
--  i_filter_key    - column name we want to check for 
--  i_filter_values - value we are looking for in the column
--  i_filter_rule   - operator to define what is the key-value criterion (=,>,<,like etc.)
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04  Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------

end com_notifier;