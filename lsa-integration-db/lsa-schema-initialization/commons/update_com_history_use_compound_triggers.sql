------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Removes old style commons history triggers, and replaces them with compound triggers
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  Chris Roderick  2014-08-06
--    Switched to use procedure to replace history triggers 
--  Chris Roderick  2013-02-07
--    Updated to ensure that only Commons history triggers are removed, and that History columns 
--    which were ignored in the history tables remain ignored.
--  Pascal Le Roux  2013-02-04
--    Creation
------------------------------------------------------------------------------------------------------------------------
declare
  type varchar_list is table of varchar2(61) index by binary_integer;
  l_hist_trgs		    varchar_list;
  l_ignore_cols     table_of_varchar;
begin

--Drop the _HISTSTMP and _WHIST triggers associated only with COMMOMNS history management
  select trigger_name bulk collect into l_hist_trgs
  from user_triggers ut
  join com_history_tables cht on (ut.table_name = cht.table_name)
  where (ut.trigger_name like '%_HISTSTMP' or trigger_name like '%_WHIST')
  order by ut.table_name, ut.trigger_name;
  
  if l_hist_trgs is not null and l_hist_trgs.count > 0 then
    
    for i in l_hist_trgs.first .. l_hist_trgs.last loop
      execute immediate ('drop trigger '||l_hist_trgs(i));
    end loop;	
  
    for r_table in (
      select table_name 
      from com_history_tables
    ) loop
      com_history_mgr.replace_history_trigger (r_table.table_name);                
    end loop;
    	
  end if;
end;
/