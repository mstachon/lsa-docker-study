whenever sqlerror continue

-- OCOMM-318
create or replace type com_period force
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This object type is intended to provide constructor and function to work with period 
--    A period is a pair of timestamp: period_start, period_end. period_start <= period_end
--    The relationship checking operators return the integer value 1 if the relationship between the two periods exists
--    otherwise 0 if the relationship does not exist.
--    Convention used for period: lower boundary inclusive, upper boundary exclusive
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation, see OCOMM-318
------------------------------------------------------------------------------------------------------------------------
is object ( 
   l_period_start timestamp(9)
 , l_period_end   timestamp(9)
 , l_period_id    number
 , l_period_label varchar2(4000)
   
 , constructor function com_period (self in out nocopy com_period,i_period_start in timestamp, i_period_end in timestamp) return self as result
 , constructor function com_period (self in out nocopy com_period,l_period_start in timestamp, l_period_end in timestamp, l_period_id in number, l_period_label in varchar2) return self as result
 , map member function sort_key return varchar2 -- MAP function implicitly used when doing select distinct, order by, multiset operation with com_periods...
 , member function to_string(i_delimiter in varchar2 default ',') return varchar2 --Return period_id, period_label, period_start, period_end as a separated (comma by default) string
 , member function get_number_of_days return number --Return the number of days of this period
 , member function equal (i_period in com_period) return number --Checks if the two periods are equal (that is, their start and end times are the same)
 , member function contain (i_period in com_period) return number --Checks if the first period contains the second period passed as parameter
 , member function meet (i_period in com_period) return number --Checks if the end of the first period is the start of the second period passed as parameter
 , member function overlap (i_period in com_period) return number --checks if two periods overlap
 , member function less_than (i_period in com_period) return number --Checks if the end of the first period is less than (that is, earlier than) the start of the second period
 , member function greater_than (i_period in com_period) return number --Checks if the start of the first period is greater than (that is, later than) the end of the second period
 , member function intersection (i_period in com_period) return com_period --Returns the intersection of the two periods, that is, the time range common to both periods
 , member function ldiff (i_period in com_period) return com_period --Returns the difference between the two periods on the left (that is, earlier in time)
 , member function rdiff (i_period in com_period) return com_period --Returns the difference between the two periods on the right (that is, later in time)
 , member function pack (i_period in com_period, i_overlapping_period_only char default 'Y') return com_period --returns the merge period between the two periods if they overlap (or optionally meet)
 , member function is_not_yet_valid (i_date in timestamp default systimestamp) return number --checks if the period (lower boundary inclusive, upper boundary exclusive) is not yet valid at the date passed as parameter (it will be valid after the date)
 , member function is_valid (i_date in timestamp default systimestamp) return number --checks if the period (lower boundary inclusive, upper boundary exclusive) is valid at the date passed as parameter
 , member function is_expired (i_date in timestamp default systimestamp) return number --checks if the period (lower boundary inclusive, upper boundary exclusive) is expired at the date passed as parameter
) not final;
/

create or replace type body com_period 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  constructor function com_period (self in out nocopy com_period, i_period_start in timestamp, i_period_end in timestamp) return self as result
  is
  begin
    com_assert.is_true(i_period_start <= i_period_end , 'period start '||i_period_start||' should be before the period end '||i_period_end);

    self.l_period_start := i_period_start;
    self.l_period_end   := i_period_end;
    return;
  end com_period;

------------------------------------------------------------------------------------------------------------------------

  constructor function com_period (self in out nocopy com_period, l_period_start in timestamp, l_period_end in timestamp, l_period_id in number, l_period_label in varchar2) return self as result
  is
  begin
    com_assert.is_true(l_period_start<= l_period_end , 'period start '||l_period_start||' should be before the period end '||l_period_end);

    self.l_period_start := l_period_start;
    self.l_period_end   := l_period_end;
    self.l_period_id    := l_period_id;
    self.l_period_label   := l_period_label;
    return;
  end com_period;

------------------------------------------------------------------------------------------------------------------------

  map member function sort_key return varchar2
   -- MAP function implicitly used when doing select distinct, order by, multiset operation with com_periods...
  is
  begin 
  return rpad (to_char(self.l_period_start,'YYYY-MM-DD HH24:MI:SSXFF'), 30) ||rpad (to_char(self.l_period_end,'YYYY-MM-DD HH24:MI:SSXFF'), 30)||coalesce (self.l_period_id, 'X')||coalesce (self.l_period_label, 'X');
  end sort_key;
 
------------------------------------------------------------------------------------------------------------------------
    
  member function to_string(i_delimiter in varchar2 default ',') return varchar2
  --Return period_start and period_end as a separated (comma by default) string
  is
  begin
    return ltrim(rtrim((self.l_period_id||i_delimiter||self.l_period_label||i_delimiter||self.l_period_start||i_delimiter||self.l_period_end), i_delimiter), i_delimiter);
  end to_string;

------------------------------------------------------------------------------------------------------------------------
    
  member function get_number_of_days return number
  --Return the number of days of this period
  is
  begin 
   return (extract (day from (self.l_period_end -self.l_period_start))*24*60*60
               + extract (hour   from (self.l_period_end -self.l_period_start))*60*60
               + extract (minute from (self.l_period_end -self.l_period_start))*60
               +extract (second from (self.l_period_end -self.l_period_start)))/(60*60*24);
  end get_number_of_days;

------------------------------------------------------------------------------------------------------------------------
    
  member function equal (i_period in com_period) return number
  --checks if the two periods are equal (that is, their start and end times are the same)
  is
  begin 
    if self.l_period_start = i_period.l_period_start and self.l_period_end = i_period.l_period_end then
      return 1;
    else 
      return 0;
    end if;
  end equal;

------------------------------------------------------------------------------------------------------------------------
    
  member function contain (i_period in com_period) return number
  --checks if the first period contains the second period passed as parameter
  is
  begin 
    if self.l_period_start <= i_period.l_period_start and i_period.l_period_end <= self.l_period_end then
      return 1;
    else 
      return 0;
    end if;
  end contain;
    
------------------------------------------------------------------------------------------------------------------------
    
  member function meet (i_period in com_period) return number
  --checks if the end of the first period is the start of the second period passed as parameter
  is
  begin 
    if self.l_period_end = i_period.l_period_start then
      return 1;
    else 
      return 0;
    end if;
  end meet;    

------------------------------------------------------------------------------------------------------------------------
    
  member function overlap (i_period in com_period) return number
  --checks if two periods overlap
  is 
  begin 
    if self.l_period_start < i_period.l_period_end and self.l_period_end > i_period.l_period_start then
      return 1;
    else 
      return 0;
    end if;
  end overlap;    

------------------------------------------------------------------------------------------------------------------------
    
  member function less_than  (i_period in com_period) return number
  --checks if the end of the first period is less than (that is, earlier than) the start of the second period
  is 
  begin 
    if  self.l_period_end < i_period.l_period_start then
      return 1;
    else 
      return 0;
    end if;
  end less_than;    

------------------------------------------------------------------------------------------------------------------------
    
    member function greater_than  (i_period in com_period) return number
    --checks if the start of the first period is greater than (that is, later than) the end of the second period
    is 
    begin 
      if  self.l_period_start > i_period.l_period_end then
        return 1;
      else 
        return 0;
      end if;
    end greater_than;    

------------------------------------------------------------------------------------------------------------------------

  member function intersection (i_period in com_period) return com_period 
  --returns the intersection of the two periods, that is, the time range common to both periods.
  is 
  l_period  com_period := com_period(i_period_start => null, i_period_end => null);
  begin 
    if self.l_period_start < i_period.l_period_end and self.l_period_end > i_period.l_period_start then --Checking if they overlap
      l_period := com_period (greatest(self.l_period_start, i_period.l_period_start), least(self.l_period_end,i_period.l_period_end));
    end if;    
    
    return l_period;    
  end intersection; 

------------------------------------------------------------------------------------------------------------------------

  member function ldiff (i_period in com_period) return com_period 
  --returns the difference between the two periods on the left (that is, earlier in time)
  is 
  l_period  com_period := com_period(i_period_start => null, i_period_end => null);
  begin 
    if not (self.l_period_start = i_period.l_period_start and self.l_period_end = i_period.l_period_end) then --Checking if the two periods are not equal
      if self.l_period_start < i_period.l_period_end and self.l_period_end > i_period.l_period_start then --Checking if they overlap
        l_period := com_period (least(self.l_period_start, i_period.l_period_start), greatest(self.l_period_start, i_period.l_period_start));
      end if;    
    end if; 
    return l_period;    
  end ldiff; 

------------------------------------------------------------------------------------------------------------------------

  member function rdiff (i_period in com_period) return com_period 
  --returns the difference between the two periods on the right (that is, later in time)
  is 
  l_period  com_period := com_period(i_period_start => null, i_period_end => null);
  begin 
    if not (self.l_period_start = i_period.l_period_start and self.l_period_end = i_period.l_period_end) then --Checking if the two periods are not equal
      if self.l_period_start < i_period.l_period_end and self.l_period_end > i_period.l_period_start then --Checking if they overlap
        l_period := com_period (least(self.l_period_end, i_period.l_period_end), greatest(self.l_period_end, i_period.l_period_end));
      end if;    
    end if; 
    return l_period;    
  end rdiff; 

------------------------------------------------------------------------------------------------------------------------

  member function pack (i_period in com_period, i_overlapping_period_only char default 'Y') return com_period
  --returns the merge period between the two periods if they overlap (or optionally meet)
  is 
  l_period  com_period := com_period(i_period_start => null, i_period_end => null);
  begin

    --Checking if they overlap, meet (both sides)
    if (self.l_period_start < i_period.l_period_end and self.l_period_end > i_period.l_period_start) 
    or (i_overlapping_period_only != 'Y' and ((self.l_period_end = i_period.l_period_start) or (i_period.l_period_end = self.l_period_start))) then 
      l_period := com_period (least(self.l_period_start, i_period.l_period_start), greatest(self.l_period_end, i_period.l_period_end));
    end if;
      
    return l_period;    
  end pack; 

------------------------------------------------------------------------------------------------------------------------

  member function is_not_yet_valid (i_date in timestamp default systimestamp) return number 
  --checks if the period (lower boundary inclusive, upper boundary exclusive) is not yet valid at the date passed as parameter (it will be valid after the date)
  is 
  begin 
  if i_date < self.l_period_start then
    return 1;
  else
    return 0;    
  end if;
  end is_not_yet_valid;         
    
------------------------------------------------------------------------------------------------------------------------

  member function is_valid (i_date in timestamp default systimestamp) return number 
  --checks if the period (lower boundary inclusive, upper boundary exclusive) is valid at the date passed as parameter
  is 
  begin 
  if (self.l_period_start <= i_date and i_date < self.l_period_end) then
    return 1;
  else
    return 0;    
  end if;
  end is_valid; 
    
------------------------------------------------------------------------------------------------------------------------

  member function is_expired (i_date in timestamp default systimestamp) return number 
  --checks if the period (lower boundary inclusive, upper boundary exclusive) is expired at the date passed as parameter
  is 
  begin 
  if i_date >= self.l_period_end  then
    return 1;
  else
    return 0;    
  end if;
  end is_expired;        
end;
/

-- ---------------------------------------------------------------------------------------------------------------------
-- OCOMM-319 Internal type for COM_PERIODS

create or replace type table_of_com_period force as table of com_period;
/

------------------------------------------------------------------------------------------------------------------------

-- OCOMM-319
create or replace type com_periods force 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This object type is intended to provide constructor and functions to work a table of com_period 
--    Convention used for period: lower boundary inclusive, upper boundary exclusive
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation see OCOMM-319
------------------------------------------------------------------------------------------------------------------------
is object (
   l_com_period_table table_of_com_period
   
 , member function pack_periods  (i_overlapping_period_only char default 'Y') return com_periods -- Pack overlapping periods (and optionally meeting periods), then return the resulting periods
 , member function gantt_periods (i_timestamp_format in varchar2 default 'YYYY-MM-DD HH24:MI:SS', i_width in integer default 150) return table_of_varchar -- Return a simplistic gantt chart of the periods
 , member function max_overlapping_period  (i_com_period in com_period default com_period(null, null)) return com_period deterministic --Return the greatest overlapping of the periods and an optional time windows given as a com_period
 , member function order_asc return com_periods -- Return a com_periods ordered ascending
 , member function order_desc return com_periods -- Return a com_periods ordered descending
 , member function unions (i_com_periods in com_periods) return com_periods --Return com_periods union distinct i_com_periods
 , member function unions_all (i_com_periods in com_periods) return com_periods --Return com_periods union all i_com_periods
 , member function intersects (i_com_periods in com_periods) return com_periods --Return com_periods intersect distinct i_com_periods
 , member function intersects_all (i_com_periods in com_periods) return com_periods --Return com_periods intersect all i_com_periods
 , member function excepts (i_com_periods in com_periods) return com_periods --Return com_periods except distinct i_com_periods
 , member function excepts_all (i_com_periods in com_periods) return com_periods --Return com_periods except all i_com_periods
 , member function count return number -- return the com_periods total count
 , member function count_occurrences(i_com_period in com_period default null) return number -- return the count of i_com_period in com_periods
 , member function distincts return com_periods -- return distinct com_periods
 , member function duplicates return com_periods -- return distinct duplicated com_periods
 , member function contains (i_com_period in com_period) return number -- Return 1 if com_periods contains i_com_period, otherwise  return 0 
) not final;
/

create or replace type body com_periods
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  
  member function pack_periods  (i_overlapping_period_only char default 'Y') return com_periods
  -- Pack overlapping periods (and optionally meeting periods), then return the resulting periods
  is
    l_periods       table_of_com_period;
    c_eternity_date constant timestamp(9) := to_timestamp ( '9999-12-31 00:00:00','YYYY-MM-DD HH24:MI:SSXFF');
    c_yes           constant char (1) := 'Y';
  begin
  if self.l_com_period_table.count > 0 then

    with periods as 
      (select row_number() over(order by l_period_start, l_period_end) as pid, l_period_start as period_start, l_period_end as period_end
       from (select distinct pe.l_period_start, pe.l_period_end --remove duplicate periods
             from table(self.l_com_period_table) pe
            )
      ) 
    , removed_internal_periods as --remove internal periods
      (select pa.pid, pa.period_start, pa.period_end 
       from periods pa
       where  not exists(select null  
                         from periods pin
                         where pa.pid != pin.pid and pa.period_start >= pin.period_start and pa.period_end <= pin.period_end) 
      ) 
    , startofgroup as 
      (select pid, period_start, period_end
      , case when (period_start < coalesce(lag(period_end) over(order by period_start), c_eternity_date)
               and period_end > coalesce(lag(period_start) over(order by period_start), c_eternity_date)) --overlappings
             or (i_overlapping_period_only != c_yes and ((period_end = coalesce(lag(period_start) over(order by period_start), c_eternity_date)) 
                                                                    or (coalesce(lag(period_end) over(order by period_start), c_eternity_date) = period_start))) --meetings
             then
                                                       0 else 1 end new_group
    
        from removed_internal_periods 
      )   
    , grouped_periods as 
      (select pid, period_start, period_end, new_group
            , sum(new_group) over(order by period_start) as group_no
       from startofgroup
      )
    , packed_by_group as
      (select min(period_start) period_start, max(period_end) period_end
       from grouped_periods
       group by group_no  
      ) select com_period(period_start, period_end) bulk collect into l_periods
        from packed_by_group;
   
  end if;
  return com_periods(l_periods);
  end pack_periods;

------------------------------------------------------------------------------------------------------------------------

  member function gantt_periods (i_timestamp_format in varchar2 default 'YYYY-MM-DD HH24:MI:SS', i_width in integer default 150) return table_of_varchar 
  -- Return a simplistic gantt chart of the periods
  is
  l_result      table_of_varchar := table_of_varchar();
  l_width       integer := coalesce(i_width, 150);  
  l_left_margin integer := 10;  
  begin
    if self.l_com_period_table.count > 0 then
      with periods as
      (select rownum as rowindex
            , coalesce(t.l_period_label, to_char(t.l_period_id), to_char(rownum)) as label
            , t.l_period_start  start_date
            , t.l_period_end    end_date
      from table(self.l_com_period_table) t
      )
      , limits as
        (select min(start_date) period_start
              , max(end_date)   period_end
              , l_width width
         from   periods
        )
      , bars as
        ( select lpad(label, l_left_margin)||'|' activity
               , (cast(start_date as date) - cast(period_start as date))/(cast(period_end as date) - cast(period_start as date)) * width from_pos
               , (cast(end_date as date) - cast(period_start as date))/(cast(period_end as date) - cast(period_start as date)) * width to_pos
          from periods
          cross join limits
        )
      select  activity||lpad('|',from_pos)||rpad('=', to_pos - from_pos-1, '=')||'|' gantt bulk collect into l_result
      from bars;
     
    end if;
    return l_result;
  end gantt_periods;    

------------------------------------------------------------------------------------------------------------------------

  member function max_overlapping_period  (i_com_period in com_period default com_period(null, null)) return com_period deterministic 
  --Return the greatest overlapping of the periods and an optional time windows given as a com_period    
  is
  l_count                 integer :=0;
  l_from_date             timestamp(9) := null;   
  l_to_date               timestamp(9) := null;  
  l_com_period            com_period := i_com_period;
  c_default_period_start  constant timestamp(9) := to_timestamp ( '1900-01-01 00:00:00','YYYY-MM-DD HH24:MI:SSXFF'); 
  c_default_period_end    constant timestamp(9) := to_timestamp ( '9999-12-31 00:00:00','YYYY-MM-DD HH24:MI:SSXFF');

  begin
    if self.l_com_period_table.count > 0 then
      begin
        --if we do not get a no_data_found error then there are periods which don't overlap...
        select unique 1 into l_count
        from table(self.l_com_period_table) t1
           , table(self.l_com_period_table) t2
        where not (t1.l_period_start,t1.l_period_end) overlaps (t2.l_period_start,t2.l_period_end);
      exception
      when no_data_found then
        l_com_period.l_period_start := coalesce(l_com_period.l_period_start, c_default_period_start);
        l_com_period.l_period_end := coalesce(l_com_period.l_period_end, c_default_period_end);

        select max(l_start), min(l_end) into l_from_date, l_to_date
        from
        (select greatest (one.l_period_start,two.l_period_start) l_start
              , least(one.l_period_end,two.l_period_end) l_end
        from table(self.l_com_period_table) one
           , table(self.l_com_period_table) two
        where (one.l_period_start,one.l_period_end) overlaps (two.l_period_start,two.l_period_end));

        select greatest (l_from_date, l_com_period.l_period_start) 
             , least(l_to_date, l_com_period.l_period_end) into l_com_period.l_period_start, l_com_period.l_period_end
        from dual
        where  (l_from_date, l_to_date)  overlaps (l_com_period.l_period_start, l_com_period.l_period_end);
      end;
    else
      l_com_period := com_period(null,null); 
    end if;
    return l_com_period;
  exception
    when others then
      return com_period(null,null);
  end max_overlapping_period;

------------------------------------------------------------------------------------------------------------------------

  member function order_asc return com_periods
  is
  l_result  table_of_com_period;
  begin
  
  select com_period(l_period_start, l_period_end, l_period_id, l_period_label) bulk collect into l_result
  from table(self.l_com_period_table)
  order by l_period_start, l_period_end, l_period_id nulls last, l_period_label nulls last;
  
  return com_periods(l_result);
   
  end order_asc;  

------------------------------------------------------------------------------------------------------------------------

  member function order_desc return com_periods
  is
  l_result  table_of_com_period;
  begin
  
  select com_period(l_period_start, l_period_end, l_period_id, l_period_label) bulk collect into l_result
  from table(self.l_com_period_table)
  order by l_period_start desc, l_period_end desc, l_period_id desc nulls last, l_period_label desc nulls last;
  
  return com_periods(l_result);
  
  end order_desc; 

------------------------------------------------------------------------------------------------------------------------

  member function unions (i_com_periods in com_periods) return com_periods
  --Return com_periods union distinct i_com_periods
  --Require a map member function in com_period object type  
  is
  l_result  table_of_com_period;
  begin
  
  l_result := self.l_com_period_table multiset union distinct i_com_periods.l_com_period_table; 
  
  return com_periods(l_result);
  
  end unions;

------------------------------------------------------------------------------------------------------------------------

  member function unions_all (i_com_periods in com_periods) return com_periods
  --Return com_periods union distinct i_com_periods
  --Require a map member function in com_period object type  
  is
  l_result  table_of_com_period;
  begin
  
  l_result := self.l_com_period_table multiset union all i_com_periods.l_com_period_table; 
  
  return com_periods(l_result);
  
  end unions_all;  

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects (i_com_periods in com_periods) return com_periods
  is
  l_result  table_of_com_period;
  begin
  
  l_result := self.l_com_period_table multiset intersect distinct i_com_periods.l_com_period_table; 
  
  return com_periods(l_result);
  
  end intersects; 

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects_all (i_com_periods in com_periods) return com_periods
  is
  l_result  table_of_com_period;
  begin
  
  l_result := self.l_com_period_table multiset intersect all i_com_periods.l_com_period_table; 
  
  return com_periods(l_result);
  
  end intersects_all; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts (i_com_periods in com_periods) return com_periods
  is
  l_result  table_of_com_period;
  begin
  
  l_result := self.l_com_period_table multiset except distinct i_com_periods.l_com_period_table; 
  
  return com_periods(l_result);
  
  end excepts; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts_all (i_com_periods in com_periods) return com_periods
  is
  l_result  table_of_com_period;
  begin
  
  l_result := self.l_com_period_table multiset except all i_com_periods.l_com_period_table; 
  
  return com_periods(l_result);
  
  end excepts_all;   

------------------------------------------------------------------------------------------------------------------------

  member function count return number
  is
  begin
  return self.l_com_period_table.count;
  end count;
------------------------------------------------------------------------------------------------------------------------

  member function count_occurrences(i_com_period in com_period default null) return number
  is
    l_result number;
    c_default_period_start   constant timestamp(9) := to_timestamp ( '1900-01-01 00:00:00','YYYY-MM-DD HH24:MI:SSXFF'); 
  begin
      select count(*) into l_result
      from table(self.l_com_period_table) pe
      where coalesce(pe.l_period_start, c_default_period_start) = coalesce(i_com_period.l_period_start, c_default_period_start) 
        and coalesce(pe.l_period_end, c_default_period_start) = coalesce(i_com_period.l_period_end, c_default_period_start)  
        and coalesce(pe.l_period_id, -9999)   = coalesce(i_com_period.l_period_id, -9999)  
        and coalesce(pe.l_period_label, '-xXx')   = coalesce(i_com_period.l_period_label, '-xXx');  
    return l_result;
  end count_occurrences;  

------------------------------------------------------------------------------------------------------------------------

  member function distincts return com_periods
  is
  l_result  table_of_com_period;
  begin
  
  select com_period(l_period_start, l_period_end, l_period_id, l_period_label) bulk collect into l_result
  from(select distinct l_period_start, l_period_end, l_period_id, l_period_label from table(self.l_com_period_table)) 
  order by l_period_start, l_period_end, l_period_id nulls last, l_period_label nulls last;
  
  return com_periods(l_result);
  
  end distincts;

------------------------------------------------------------------------------------------------------------------------

  member function duplicates return com_periods
  is
  l_result  table_of_com_period;
  begin
  
  select com_period(l_period_start, l_period_end, l_period_id, l_period_label) bulk collect into l_result
  from table(self.l_com_period_table)
  group by l_period_start, l_period_end, l_period_id, l_period_label having count(*) > 1;
  
  return com_periods(l_result);
  
  end duplicates;  

------------------------------------------------------------------------------------------------------------------------

  member function contains (i_com_period in com_period) return number
  is
    l_result number;
    c_default_period_start   constant timestamp(9) := to_timestamp ( '1900-01-01 00:00:00','YYYY-MM-DD HH24:MI:SSXFF');
  begin
      select unique 1 into l_result
      from table(self.l_com_period_table) pe
      where coalesce(pe.l_period_start, c_default_period_start) = coalesce(i_com_period.l_period_start, c_default_period_start) 
        and coalesce(pe.l_period_end, c_default_period_start) = coalesce(i_com_period.l_period_end, c_default_period_start)  
        and coalesce(pe.l_period_id, -9999)   = coalesce(i_com_period.l_period_id, -9999)  
        and coalesce(pe.l_period_label, '-xXx')   = coalesce(i_com_period.l_period_label, '-xXx');  
   
    return l_result;
    exception
      when no_data_found then
      return 0;
  end contains;
end;
/

------------------------------------------------------------------------------------------------------------------------
-- OCOMM-329

create or replace type com_strings 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This object type is storing a collection of strings (varchar2), either constructed from a table_of_varchar 
--    or a separated string. It comes with member functions to manipulate this collection of strings (varchar2)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-03 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is object ( 
  l_string_table table_of_varchar
   
 , constructor function com_strings (self in out nocopy com_strings, i_delimited_strings in varchar2, i_delimiter in varchar2 default ',') return self as result
 , map member function sort_key return varchar2 -- MAP function implicitly used when doing select distinct, order by...
 , member function to_table_of_varchar return table_of_varchar -- return com_strings as a table_of_varchar
 , member function to_string(i_delimiter in varchar2 default ',') return varchar2 -- return com_strings as a separated (comma by default) string
 , member function order_asc return com_strings -- return a com_strings ordered ascending
 , member function order_desc return com_strings -- return a com_strings ordered descending
 , member function distincts return com_strings -- return distinct com_strings
 , member function duplicates return com_strings -- return distinct duplicated com_strings
 , member function count return number -- return the com_strings total count
 , member function count_occurrences (i_string in varchar2 default null) return number -- return the count of i_string in com_strings
 , member function contains (i_string in varchar2) return number -- Return 1 if com_strings contains i_string, otherwise  return 0 
 , member function string_at_index (i_index in integer default 1) return varchar2 -- return n th string in com_strings
 , member function length_less_than (i_number in number) return com_strings --Return com_strings with length less than i_number
 , member function length_less_than_equal (i_number in number) return com_strings --Return com_strings with length less than equal i_number
 , member function length_greater_than (i_number in number) return com_strings --Return com_strings with length greater than i_number
 , member function length_greater_than_equal (i_number in number) return com_strings --Return com_strings with length greater than equal i_number
 , member function length_betweens (i_low_number in number, i_high_number in number) return com_strings --Return com_strings with length between i_low_number and i_high_number (included)
 , member function replaces (i_old_string in varchar2, i_new_string in varchar2) return com_strings --Return com_strings where i_old_string is replaced by i_new_string in all com_strings 
 , member function substrs (i_start_position in integer, i_length in integer default null) return com_strings --Return com_strings substr from i_start_position with a length of i_length
 , member function trims (i_string in varchar2 default null) return com_strings --Return com_strings trimed on the left and right with i_string (if null, it will trim leading/trailing spaces)
 , member function ltrims (i_string in varchar2 default null) return com_strings --Return com_strings trimed on the left with i_string (if null, it will trim leading spaces)
 , member function rtrims (i_string in varchar2 default null) return com_strings --Return com_strings trimed on the right with i_string (if null, it will trim trailing spaces)
 , member function ltrim_nums return com_strings --Return com_strings with any numeric character removed on the left
 , member function rtrim_nums return com_strings --Return com_strings with any numeric character removed on the right
 , member function ltrim_chars return com_strings --Return com_strings with any alphabetic character removed on the left
 , member function rtrim_chars return com_strings --Return com_strings with any alphabetic character removed on the right
 , member function alphabetic_only return com_strings --Return com_strings with only alphabetic letters (removing control characters, punctuation or numbers) 
 , member function numeric_only return com_strings --Return com_strings with only numbers (removing control characters, punctuation or letters) 
 , member function alphanumeric_only return com_strings --Return com_strings with only alpha numeric characters (removing control characters and punctuation) 
 , member function lpads (i_padded_length in integer, i_pad_string in varchar2 default ' ') return com_strings --Return com_strings padded on the left with i_padded_length of i_pad_string
 , member function rpads (i_padded_length in integer, i_pad_string in varchar2 default ' ') return com_strings --Return com_strings padded on the right with i_padded_length of i_pad_string
 , member function lowers return com_strings --Return com_strings in lower case
 , member function uppers return com_strings --Return com_strings in upper case
 , member function initcaps return com_strings --Return com_strings in initcap case
 , member function unions (i_com_strings in com_strings) return com_strings --Return com_strings union distinct i_com_strings
 , member function unions_all (i_com_strings in com_strings) return com_strings --Return com_strings union all i_com_strings
 , member function intersects (i_com_strings in com_strings) return com_strings --Return com_strings intersect distinct i_com_strings
 , member function intersects_all (i_com_strings in com_strings) return com_strings --Return com_strings intersect all i_com_strings
 , member function excepts (i_com_strings in com_strings) return com_strings --Return com_strings except distinct i_com_strings
 , member function excepts_all (i_com_strings in com_strings) return com_strings --Return com_strings except all i_com_strings
);
/

------------------------------------------------------------------------------------------------------------------------

create or replace type body com_strings 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-27 Pascal Le Roux
--    Added exception handling for subscript_beyond_count in string_at_index
--  2015-06-04 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  
  constructor function com_strings (self in out nocopy com_strings, i_delimited_strings in varchar2, i_delimiter in varchar2 default ',') return self as result
  is
  l_pattern varchar2(104); 
  begin
    com_assert.is_true(length(i_delimiter) <= 100 , 'The length of the delimiter '||i_delimiter||' should not exceed 100 characters');
    
    l_pattern :='[^'||i_delimiter||']+';
  
    select individual_string bulk collect into self.l_string_table
    from (
      select regexp_substr(i_delimited_strings, l_pattern, 1, level) as individual_string
      from dual
      connect by level <= length(regexp_replace(i_delimited_strings, l_pattern)) + 1
    ) where individual_string is not null;

    return;
  end com_strings;
  
------------------------------------------------------------------------------------------------------------------------

 map member function sort_key return varchar2
 is
 begin
  return self.to_string();
 end sort_key;

------------------------------------------------------------------------------------------------------------------------

  member function to_table_of_varchar return table_of_varchar
  is
  begin
  return self.l_string_table;
  end to_table_of_varchar;

------------------------------------------------------------------------------------------------------------------------

  member function to_string(i_delimiter in varchar2 default ',') return varchar2
  is
  l_result   varchar2(32767);
  begin
  
  select listagg(column_value, i_delimiter) within group (order by rownum) into l_result
  from table(self.l_string_table);
  
  return l_result;
  
  end to_string; 
------------------------------------------------------------------------------------------------------------------------

  member function order_asc return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  order by column_value asc;
  
  return com_strings(l_result);
  
  end order_asc;  

------------------------------------------------------------------------------------------------------------------------

  member function order_desc return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  order by column_value desc;
  
  return com_strings(l_result);
  
  end order_desc;    
  
------------------------------------------------------------------------------------------------------------------------

  member function distincts return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select distinct column_value bulk collect into l_result
  from table(self.l_string_table)
  order by column_value asc;
  
  return com_strings(l_result);
  
  end distincts;

------------------------------------------------------------------------------------------------------------------------

  member function duplicates return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  group by column_value having count(*) > 1;
  
  return com_strings(l_result);
  
  end duplicates;
  
------------------------------------------------------------------------------------------------------------------------
  
  member function count return number
  is
    l_result number;
  begin
    return self.l_string_table.count;
  end count;
  
------------------------------------------------------------------------------------------------------------------------
  
  member function count_occurrences (i_string in varchar2 default null) return number
  is
    l_result number;
  begin
      select count(*) into l_result
      from table(self.l_string_table)
      where column_value = i_string;
    return l_result;
  end count_occurrences;

------------------------------------------------------------------------------------------------------------------------
  
  member function contains (i_string in varchar2) return number
  is
    l_result number;
  begin
  return case when self.count_occurrences(i_string) > 0 then 1 else 0 end;
  end contains;

 
------------------------------------------------------------------------------------------------------------------------
  
  member function string_at_index (i_index in integer default 1) return varchar2
  is
  begin
    return self.l_string_table(i_index);
    exception 
    when subscript_beyond_count then
    return null;
  end string_at_index; 
  
------------------------------------------------------------------------------------------------------------------------
  
  member function length_less_than (i_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) < i_number;
  
  return com_strings(l_result);
  
  end length_less_than;  

------------------------------------------------------------------------------------------------------------------------
  
  member function length_less_than_equal (i_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) <= i_number;
  
  return com_strings(l_result);
  
  end length_less_than_equal;  

------------------------------------------------------------------------------------------------------------------------
  
  member function length_greater_than (i_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select distinct column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) > i_number;
  
  return com_strings(l_result);
  
  end length_greater_than;    

------------------------------------------------------------------------------------------------------------------------
  
  member function length_greater_than_equal (i_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) >= i_number;
  
  return com_strings(l_result);
  
  end length_greater_than_equal; 
  
------------------------------------------------------------------------------------------------------------------------
  
  member function length_betweens (i_low_number in number, i_high_number in number) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_string_table)
  where length(column_value) between i_low_number and i_high_number;
  
  return com_strings(l_result);
  
  end length_betweens;    

------------------------------------------------------------------------------------------------------------------------
  
  member function replaces (i_old_string in varchar2, i_new_string in varchar2) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select replace(column_value, i_old_string, i_new_string)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end replaces;    

------------------------------------------------------------------------------------------------------------------------
  
  member function substrs (i_start_position in integer, i_length in integer default null) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select substr(column_value, i_start_position, coalesce(i_length, length(column_value)))  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end substrs;    
------------------------------------------------------------------------------------------------------------------------
  
  member function trims (i_string in varchar2 default null) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select rtrim(ltrim(column_value, coalesce(i_string,' ')), coalesce(i_string,' '))   bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end trims;    
------------------------------------------------------------------------------------------------------------------------
  
  member function ltrims (i_string in varchar2 default null) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select ltrim(column_value, coalesce(i_string,' '))  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end ltrims;    

------------------------------------------------------------------------------------------------------------------------
  
  member function rtrims (i_string in varchar2 default null) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select rtrim(column_value, coalesce(i_string,' '))  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end rtrims;    


------------------------------------------------------------------------------------------------------------------------
  
  member function ltrim_nums return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select ltrim(column_value, '0123456789')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end ltrim_nums;    

------------------------------------------------------------------------------------------------------------------------
  
  member function rtrim_nums return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select rtrim(column_value, '0123456789')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end rtrim_nums;    

------------------------------------------------------------------------------------------------------------------------
  
  member function ltrim_chars return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select regexp_replace(column_value,'^[a-zA-Z]*','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end ltrim_chars;    

------------------------------------------------------------------------------------------------------------------------
  
  member function rtrim_chars return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  select regexp_replace(column_value,'[a-zA-Z]*$','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end rtrim_chars;    

------------------------------------------------------------------------------------------------------------------------
  
  member function alphabetic_only return com_strings
  is
  l_result  table_of_varchar;
  begin
  select regexp_replace(column_value,'[^a-zA-Z]','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end alphabetic_only;    

------------------------------------------------------------------------------------------------------------------------
  
  member function numeric_only return com_strings
  is
  l_result  table_of_varchar;
  begin
  select regexp_replace(column_value,'[^0123456789]','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end numeric_only;    

------------------------------------------------------------------------------------------------------------------------
  
  member function alphanumeric_only return com_strings
  is
  l_result  table_of_varchar;
  begin
  select regexp_replace(column_value,'[^0123456789a-zA-Z]','')  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end alphanumeric_only;    
------------------------------------------------------------------------------------------------------------------------
  
  member function lpads (i_padded_length in integer, i_pad_string in varchar2 default ' ') return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select lpad(column_value, i_padded_length, i_pad_string)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end lpads;   

------------------------------------------------------------------------------------------------------------------------
  
  member function rpads (i_padded_length in integer, i_pad_string in varchar2 default ' ') return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select rpad(column_value, i_padded_length, i_pad_string)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end rpads;   

------------------------------------------------------------------------------------------------------------------------
  
  member function lowers return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select lower(column_value)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end lowers;   

------------------------------------------------------------------------------------------------------------------------
  
  member function uppers return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select upper(column_value)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end uppers;  

------------------------------------------------------------------------------------------------------------------------
  
  member function initcaps return com_strings 
  is
  l_result  table_of_varchar;
  begin
  
  select initcap(column_value)  bulk collect into l_result
  from table(self.l_string_table);
  
  return com_strings(l_result);
  
  end initcaps; 

------------------------------------------------------------------------------------------------------------------------
  
  member function unions (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset union distinct i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end unions;      

------------------------------------------------------------------------------------------------------------------------
  
  member function unions_all (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset union all i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end unions_all;      

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset intersect distinct i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end intersects; 

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects_all (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset intersect all i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end intersects_all; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset except distinct i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end excepts; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts_all (i_com_strings in com_strings) return com_strings
  is
  l_result  table_of_varchar;
  begin
  
  l_result := self.l_string_table multiset except all i_com_strings.to_table_of_varchar; 
  
  return com_strings(l_result);
  
  end excepts_all; 
  
end;
/

------------------------------------------------------------------------------------------------------------------------
-- OCOMM-328 

create or replace type com_numbers 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This object type is storing a collection of numbers, either constructed from a table_of_number 
--    or a separated string of numbers. It comes with member functions to maipulating this collection of numbers
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is object (
   l_number_table table_of_number
   
 , constructor function com_numbers (self in out nocopy com_numbers, i_delimited_numbers in varchar2, i_delimiter in varchar2 default ',') return self as result
 , map member function sort_key return varchar2 -- MAP function implicitly used when doing select distinct, order by...
 , member function to_table_of_number return table_of_number -- return com_numbers as a table_of_number
 , member function to_string(i_delimiter in varchar2 default ',') return varchar2 -- return com_numbers as a separated (comma by default) string
 , member function order_asc return com_numbers -- return a com_numbers ordered ascending
 , member function order_desc return com_numbers -- return a com_numbers ordered descending
 , member function distincts return com_numbers -- return distinct com_numbers
 , member function duplicates return com_numbers -- return the distinct duplicated com_numbers
 , member function count return number -- -- return the com_numbers total count
 , member function count_occurrences (i_number in number default null) return number -- return the count of i_number in com_numbers
 , member function contains (i_number in number) return number -- Return 1 if com_numbers contains i_number, otherwise  return 0 
 , member function maxi return number -- return biggest number
 , member function mini return number -- return smallest number
 , member function average return number -- return the average of the com_numbers
 , member function less_than (i_number in number) return com_numbers --Return com_numbers less than i_number
 , member function less_than_equal (i_number in number) return com_numbers --Return com_numbers less than equal i_number
 , member function greater_than (i_number in number) return com_numbers --Return com_numbers greater than i_number
 , member function greater_than_equal (i_number in number) return com_numbers --Return com_numbers greater than equal i_number
 , member function betweens (i_low_number in number, i_high_number in number) return com_numbers --Return com_numbers between i_low_number and i_high_number (included)
 , member function unions (i_com_numbers in com_numbers) return com_numbers --Return com_numbers union distinct i_com_numbers
 , member function unions_all (i_com_numbers in com_numbers) return com_numbers --Return com_numbers union all i_com_numbers
 , member function intersects (i_com_numbers in com_numbers) return com_numbers --Return com_numbers intersect distinct i_com_numbers
 , member function intersects_all (i_com_numbers in com_numbers) return com_numbers --Return com_numbers intersect all i_com_numbers
 , member function excepts (i_com_numbers in com_numbers) return com_numbers --Return com_numbers except distinct i_com_numbers
 , member function excepts_all (i_com_numbers in com_numbers) return com_numbers --Return com_numbers except all i_com_numbers
);
/

------------------------------------------------------------------------------------------------------------------------

create or replace type body com_numbers
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  
  constructor function com_numbers (self in out nocopy com_numbers, i_delimited_numbers in varchar2, i_delimiter in varchar2 default ',') return self as result
  is
  l_pattern varchar2(104); 

  begin
    com_assert.is_true(length(i_delimiter) <= 100 , 'The length of the delimiter '||i_delimiter||' should not exceed 100 characters');
    
    l_pattern :='[^'||i_delimiter||']+';
  
    select individual_number bulk collect into self.l_number_table
    from (
      select to_number(regexp_substr(i_delimited_numbers, l_pattern, 1, level)) as individual_number
      from dual
      connect by level <= length(regexp_replace(i_delimited_numbers, l_pattern)) + 1
    ) where individual_number is not null;

    return;
  end com_numbers;
  
------------------------------------------------------------------------------------------------------------------------
 
 map member function sort_key return varchar2
 is
 begin
 return self.to_string();
 end sort_key;
 
------------------------------------------------------------------------------------------------------------------------

  member function to_table_of_number return table_of_number
  is
  begin
  
  return self.l_number_table;
  
  end to_table_of_number;

------------------------------------------------------------------------------------------------------------------------

  member function to_string(i_delimiter in varchar2 default ',') return varchar2
  is
  l_result   varchar2(32767);
  begin
  
  select listagg(column_value, i_delimiter) within group (order by rownum) into l_result
  from table(self.l_number_table);
  
  return l_result;
  
  end to_string;
  
------------------------------------------------------------------------------------------------------------------------

  member function order_asc return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  order by column_value asc;
  
  return com_numbers(l_result);
  
  end order_asc;  

------------------------------------------------------------------------------------------------------------------------

  member function order_desc return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  order by column_value desc;
  
  return com_numbers(l_result);
  
  end order_desc;  

------------------------------------------------------------------------------------------------------------------------

  member function duplicates return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  group by column_value having count(*) > 1;
  
  return com_numbers(l_result);
  
  end duplicates; 

------------------------------------------------------------------------------------------------------------------------

  member function distincts return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select distinct column_value bulk collect into l_result
  from table(self.l_number_table)
  order by column_value asc;
  
  return com_numbers(l_result);
  
  end distincts;

------------------------------------------------------------------------------------------------------------------------

  member function count return number
  is
    l_result number;
  begin
    return self.l_number_table.count;
  end count;

------------------------------------------------------------------------------------------------------------------------
  
  member function count_occurrences(i_number in number default null) return number
  is
    l_result number;
  begin
      select count(*) into l_result
      from table(self.l_number_table)
      where column_value = i_number;

    return l_result;
  end count_occurrences;

------------------------------------------------------------------------------------------------------------------------
  
  member function contains (i_number in number) return number
  is
    l_result number;
  begin
      select unique 1 into l_result
      from table(self.l_number_table)
      where column_value = i_number;
   
    return l_result;
    exception
      when no_data_found then
      return 0;
  end contains;
  
------------------------------------------------------------------------------------------------------------------------

  member function maxi return number
  is
  l_result number;
  begin
  
  select max(column_value) into l_result
  from table(self.l_number_table);
  
  return l_result;
  
  end maxi;

------------------------------------------------------------------------------------------------------------------------
  
  member function mini return number
  is
  l_result number;
  begin
  
  select min(column_value) into l_result
  from table(self.l_number_table);
  
  return l_result;
  
  end mini; 

------------------------------------------------------------------------------------------------------------------------
  
  member function average return number
  is
  l_result number;
  begin
  
  select avg(column_value) into l_result
  from table(self.l_number_table);
  
  return l_result;
  
  end average;  

------------------------------------------------------------------------------------------------------------------------
  
  member function less_than (i_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value < i_number;
  
  return com_numbers(l_result);
  
  end less_than;  

------------------------------------------------------------------------------------------------------------------------
  
  member function less_than_equal (i_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value <= i_number;
  
  return com_numbers(l_result);
  
  end less_than_equal;  

------------------------------------------------------------------------------------------------------------------------
  
  member function greater_than (i_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select distinct column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value > i_number;
  
  return com_numbers(l_result);
  
  end greater_than;  

------------------------------------------------------------------------------------------------------------------------
  
  member function greater_than_equal (i_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value >= i_number;
  
  return com_numbers(l_result);
  
  end greater_than_equal;  

------------------------------------------------------------------------------------------------------------------------
  
  member function betweens (i_low_number in number, i_high_number in number) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  select column_value bulk collect into l_result
  from table(self.l_number_table)
  where column_value between i_low_number and i_high_number;
  
  return com_numbers(l_result);
  
  end betweens;    

------------------------------------------------------------------------------------------------------------------------
  
  member function unions (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset union distinct i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end unions;      

------------------------------------------------------------------------------------------------------------------------
  
  member function unions_all (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset union all i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end unions_all;      

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset intersect distinct i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end intersects; 

------------------------------------------------------------------------------------------------------------------------
  
  member function intersects_all (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset intersect all i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end intersects_all; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset except distinct i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end excepts; 

------------------------------------------------------------------------------------------------------------------------
  
  member function excepts_all (i_com_numbers in com_numbers) return com_numbers
  is
  l_result  table_of_number;
  begin
  
  l_result := self.l_number_table multiset except all i_com_numbers.to_table_of_number; 
  
  return com_numbers(l_result);
  
  end excepts_all; 
end;
/
------------------------------------------------------------------------------------------------------------------------
-- OCOMM-323 Create tables and views holding configuration and results
------------------------------------------------------------------------------------------------------------------------
create table com_test_suites (
  suite_id number constraint com_test_suite_pk primary key,
  suite_name varchar2(128) constraint com_test_suites_name_nn not null,
  parent_suite_id number,
  constraint com_test_suites_name_uk unique (suite_name),  
  constraint com_test_suites_parent_fk foreign key (parent_suite_id) references com_test_suites(suite_id)
);
create index com_test_suites_parent_fk on com_test_suites(parent_suite_id);

create table com_test_packages (
  package_id number constraint com_test_packages_pk primary key,
  package_name varchar2(30) constraint com_test_packages_name_nn not null,
  constraint com_test_packages_pkg_name_uk unique (package_name),
  constraint com_test_packages_chk check (package_name = upper(package_name))  
);

create table com_test_suite_packages (
  suite_id number,
  package_id number,
  constraint com_test_sp_suites_fk foreign key (suite_id) references com_test_suites(suite_id),
  constraint com_test_sp_packages_fk foreign key (package_id) references com_test_packages(package_id),
  constraint com_test_suite_packages_pk primary key (suite_id, package_id)
);
create index com_test_sp_packages_fk on com_test_suite_packages(package_id);

create table com_test_runs (
  run_id number constraint com_test_runs_pk primary key,
  utc_timestamp timestamp default sys_extract_utc(systimestamp) constraint com_test_runs_timestamp_nn not null,
  suite_id number,
  constraint com_test_runs_suites_fk foreign key (suite_id) references com_test_suites(suite_id)
);
create index com_test_runs_suites_fk on com_test_runs(suite_id);

create table com_test_run_results (
  run_result_id number constraint com_test_runs_results_pk primary key,
  run_id number,
  suite_id number,
  package_id number,
  test_name varchar2(30) constraint com_test_runs_res_name_nn not null,
  test_result varchar2(10) 
  constraint com_test_runs_res_result_chk check (test_result in ('SUCCESS', 'FAILURE'))
  constraint com_test_runs_res_result_nn not null,
  utc_timestamp_start timestamp constraint com_test_runs_res_start_nn not null,
  utc_timestamp_end timestamp constraint com_test_runs_res_end_nn not null,
  test_result_message varchar2(4000),    
  constraint com_test_runs_res_run_fk foreign key (run_id) references com_test_runs(run_id),
  constraint com_test_runs_res_suite_fk foreign key (suite_id) references com_test_suites(suite_id),
  constraint com_test_runs_res_package_fk foreign key (package_id) references com_test_packages(package_id)
);
create index com_test_runs_res_run_fk on com_test_run_results(run_id);
create index com_test_runs_res_suite_fk on com_test_run_results(suite_id);
create index com_test_runs_res_package_fk on com_test_run_results(package_id);

create sequence com_test_runner_seq start with 1 increment by 1 nocache nocycle;
create sequence com_test_suites_seq start with 1 increment by 1 nocache nocycle;
create sequence com_test_packages_seq start with 1 increment by 1 nocache nocycle;

-- OCOMM-323 Register events
declare
  l_event_id com_event_definitions.event_id%type;
begin
  dbms_application_info.set_module('EVENT_SETUP', 'Define COM_TEST_RUNNER events') ;
  dbms_session.set_identifier('EVENT_SETUP') ;
  com_event_mgr.define_event('COM_TEST_RUNNER_RUN_FINISHED', com_logger.c_info, 'Test runner finished the run', 8, 20, 'N', l_event_id) ;
  com_event_mgr.define_event('COM_TEST_RUNNER_TEST_FAILED', com_logger.c_error, 'COM_TEST_RUNNER Test has failed', 8, 20 ,'N', l_event_id);
end;
/

-- OCOMM-323 Install packages
@@com_test_mgr/com_test_mgr.pls
@@com_test_generator/com_test_generator.pls
@@com_test_runner/com_test_runner.pls

@@com_test_mgr/com_test_mgr.plb
@@com_test_generator/com_test_generator.plb
@@com_test_runner/com_test_runner.plb

-- OCOMM-326, OCOMM-327
@@com_formatter/com_formatter.pls
@@com_formatter/com_formatter.plb

-- Other formatting which was applied
@@com_assert/com_assert.pls
@@com_assert/com_assert.plb

-- OCOMM-331
@@com_util/com_util.pls
@@com_util/com_util.plb

------------------------------------------------------------------------------------------------------------------------
-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/

-- OCOMM-323 Register a generic suite to contain all tests
begin
  com_test_mgr.add_test_suite('Complete suite'); 
end;
/
-- OCOMM-323 Scheduler job to run test suite on a regular basis
begin
  dbms_scheduler.create_job (
    job_name           =>  'com_test_runner_run',
    job_type           =>  'PLSQL_BLOCK',
    job_action         =>  'com_test_runner.run_test_suite(''Complete suite'', true);',
    start_date         =>   trunc(systimestamp, 'DD')+interval '7' hour,
    repeat_interval    =>  'freq = daily; byhour = 7',   
    enabled            =>   true,
    job_class          =>  'DEFAULT_JOB_CLASS',
    comments           =>  'Runs the Complete test suite');
end;
/


begin
  com_updater.register_applied_changes('1.12.0');
  commit;
end;
/
