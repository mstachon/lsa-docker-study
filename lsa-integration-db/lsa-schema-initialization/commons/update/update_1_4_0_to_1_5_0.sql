whenever sqlerror exit

create sequence com_code_analysis_run_id_seq;

create table com_code_analysis_runs  (
  code_analysis_run_id      number      constraint comcodeanalysisruns_pk primary key,
  code_analysis_start_time  timestamp,
  code_analysis_end_time    timestamp
);

create table com_code_analysis_run_results  (
  code_analysis_run_id        number        constraint comcodeanalysisrunres_fk references com_code_analysis_runs (code_analysis_run_id),
  code_analysis_rule_name     varchar2(30), 
	code_analysis_category      varchar2(30), 
	code_analysis_rule_severity number, 
	extra_info                  varchar2(500), 
	code_owner                  varchar2(30), 
	code_name                   varchar2(30), 
	code_type                   varchar2(12), 
	line_no                     number, 
	line_text                   varchar2(4000), 
	line_column_pos             number
);

@@com_code_analyser/com_code_analyser.pls
@@com_code_analyser/com_code_analyser.plb
/

begin
  dbms_scheduler.create_job(
    job_name        =>  'com_code_analysis',
    job_type        =>  'PLSQL_BLOCK',
    job_action      =>  'com_code_analyser.analyse_code_if_changed;',
    start_date      =>   systimestamp + interval '1' minute,
    repeat_interval =>  'freq = daily; byhour = 7; byminute = 0',
    enabled         =>   true,
    comments        =>  'Analyses full code base if any code matching given criteria are changed since the last analysis'
  );
-- Sets the max run duration  
  dbms_scheduler.set_attribute(
     name          => 'com_code_analysis',
     attribute     => 'max_run_duration',
     value         => interval '5' minute
  );
  
  commit;
end;
/


begin
  com_updater.register_applied_changes('1.5.0');
  commit;
end;
/