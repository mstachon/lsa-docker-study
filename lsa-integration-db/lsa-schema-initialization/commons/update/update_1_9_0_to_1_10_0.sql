whenever sqlerror continue

-- OCOMM-289
@@com_schema_analysis_tables/com_schema_analysis_tables.pls
@@com_schema_analysis_tables/com_schema_analysis_tables.plb

-- OCOMM-290
@@com_schema_analysis_naming/com_schema_analysis_naming.pls
@@com_schema_analysis_naming/com_schema_analysis_naming.plb

-- OCOMM-311
@@com_event_mgr/com_event_mgr.pls
@@com_event_mgr/com_event_mgr.plb

-- OCOMM-289, OCOMM-290
begin
  com_schema_analyser.reg_schema_analysis_rule (
	'Singular tables names', com_schema_analyser.c_naming, 'com_schema_analysis_naming.chk_singular_tabs', true, true, true, 5, 'Tables names are in singular form'
  );
  com_schema_analyser.reg_schema_analysis_rule (
	'Table without any relations', com_schema_analyser.c_tables, 'com_schema_analysis_tables.chk_for_no_relations', true, true, true, 5, 'Table has no incoming or outgoing relations'
  );
  commit;
end;
/

-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/

begin
  com_updater.register_applied_changes('1.10.0');
  commit;
end;
/

