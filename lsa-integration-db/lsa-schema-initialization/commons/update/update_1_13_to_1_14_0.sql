whenever sqlerror continue

-- OCOMM-355
create or replace type table_of_integer as table of integer;
/
@@com_anydata_extensions/com_anydata_extensions.pls
@@com_anydata_extensions/com_anydata_extensions.plb

-- OCOMM-288
@@com_schema_analysis_indexes/com_schema_analysis_indexes.pls
@@com_schema_analysis_indexes/com_schema_analysis_indexes.plb

-- OCOMM-359
@@com_string_utils/com_string_utils.pls
@@com_string_utils/com_string_utils.plb

-- OCOMM-359
@@com_test_generator/com_test_generator.pls
@@com_test_generator/com_test_generator.plb

-- OCOMM-337, OCOMM-367
@@com_template_util/com_template_util.pls
@@com_template/com_template.pls
@@com_template_util/com_template_util.plb
@@com_template/com_template.plb

-- OCOMM-368
@@com_constants/com_constants.pls
@@com_action_trigger/com_action_trigger.pls
@@com_action_trigger/com_action_trigger.plb

-- OCOMM-288 Check for IOTs with Secondary Indexes with com_schema_analysis_indexes.chk_iot_2ndary_indexes
------------------------------------------------------------------------------------------------------------------------
begin
  com_schema_analyser.reg_schema_analysis_rule (
    'IOTs with Secondary Indexes', com_schema_analyser.c_indexes, 'com_schema_analysis_indexes.chk_iot_secondary_indexes',true, true, true, 5, 'IOTs should not contain secondary indexes'
  ); 
exception
  when dup_val_on_index then null;
  when others then raise;
end;
/

-- OCOMM-301 Create periodic check for invalid objects
@@com_object_mgr/com_object_mgr.plb
begin
  dbms_scheduler.create_job(
    job_name        =>  'COM_CHECK_INVALID_OBJECTS',
    job_type        =>  'PLSQL_BLOCK',
    job_action      =>  'com_object_mgr.check_objects;',
    start_date      =>   systimestamp + interval '1' hour,
    repeat_interval =>  'freq = daily; byhour = 14',
    enabled         =>   true,
    comments        =>  'Check for invalid objects and try to recompile them. Notify admins if re-compilation failed'
  );

  dbms_scheduler.set_attribute ('COM_CHECK_INVALID_OBJECTS', 'max_run_duration' , interval '1' minute);
end;
/

-- Ensure generic error events are defined
declare
  l_event_id com_event_definitions.event_id%type;
begin
	dbms_application_info.set_module('EVENT_SETUP', 'DEFINE_GENERIC_EVENT');
	dbms_session.set_identifier('EVENT_SETUP');  
  com_event_mgr.define_event('PROCESSING_ERROR', com_logger.c_error, 'A processing error occurred', 8, 20 ,'N', l_event_id);
  com_event_mgr.define_event('ILLEGAL_ARGUMENT', com_logger.c_error, 'A module has been passed an illegal or inappropriate argument', 8, 20 ,'N', l_event_id);
  com_event_mgr.define_event('INTERNAL_ERROR', com_logger.c_error, 'An unexpected internal error has occured', 8, 20 ,'Y', l_event_id);
exception
  when others then
    if not com_event_mgr.event_equals('EVENT_EXISTS', sqlcode) then
      raise;
    end if;
end;
/
commit;

-- OCOMM-363
@@com_util/com_util.pls
@@com_util/com_util.plb

@@com_period/com_period.pls
@@com_period/com_period.plb

@@com_periods/com_periods.pls
@@com_periods/com_periods.plb


-- OCOMM-336 Extend com_event_domains with categories
create table com_event_domain_categories (
  category_id number constraint com_event_domain_categories_pk primary key,
  category_name varchar2(30) constraint cedc_category_name_nn not null, 
	category_description varchar2(100)
);
create unique index cedc_category_name_idx on com_event_domain_categories (upper(category_name));

alter table com_event_domains add category_id number;
alter table com_event_domains add constraint ced_category_id_fk foreign key (category_id) references com_event_domain_categories(category_id);
create index ced_category_id_fk on com_event_domains(category_id);

create sequence com_event_domain_categorie_seq;

------------------------------------------------------------------------------------------------------------------------
-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/
alter package com_updater compile body; 
begin
  com_updater.register_applied_changes('1.14.0');
  commit;
end;
/