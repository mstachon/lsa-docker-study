whenever sqlerror continue

-- OCOMM-340
@@com_test_mgr/com_test_mgr.plb

------------------------------------------------------------------------------------------------------------------------
-- OCOMM-323 Scheduler job to run test suite on a regular basis
------------------------------------------------------------------------------------------------------------------------
begin
-- Sets the max run duration  
  dbms_scheduler.set_attribute(
     name          => 'com_test_runner_run',
     attribute     => 'max_run_duration',
     value         => interval '1' minute
  );
  
  commit;
end;
/

------------------------------------------------------------------------------------------------------------------------
-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/
begin
  com_updater.register_applied_changes('1.12.2');
  commit;
end;
/