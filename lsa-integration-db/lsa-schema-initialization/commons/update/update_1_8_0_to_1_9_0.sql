whenever sqlerror continue

-- OCOMM-308
@@com_event_mgr/com_event_mgr.pls
@@com_event_mgr/com_event_mgr.plb
@@com_notifier/com_notifier.pls
@@com_notifier/com_notifier.plb

-- OCOMM-303,OCOMM-304,OCOMM-305 
@@com_assert/com_assert.pls
@@com_assert/com_assert.plb
@@com_history_mgr/com_history_mgr.pls
@@com_history_mgr/com_history_mgr.plb

-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/

begin
  com_updater.register_applied_changes('1.9.0');
  commit;
end;
/

