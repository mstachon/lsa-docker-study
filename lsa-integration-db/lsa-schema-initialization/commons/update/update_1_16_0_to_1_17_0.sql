whenever sqlerror continue

--OCOMM-465
@@com_periods/com_periods.pls
@@com_periods/com_periods.plb

-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/
begin
  com_updater.register_applied_changes('1.17.0');
  commit;
end;
/