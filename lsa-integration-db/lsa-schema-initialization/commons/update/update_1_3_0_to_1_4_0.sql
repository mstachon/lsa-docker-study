whenever sqlerror exit

--  Check that DBMS_XMLGEN is executable (required by com_formatter) 
declare
  l_privilege_object  varchar2(30);
  l_privilege         varchar2(30);
begin    
  l_privilege_object  :=  'DBMS_XMLGEN';
  l_privilege         :=  'EXECUTE';

  select distinct privilege
  into l_privilege
  from all_tab_privs_recd 
  where table_name = l_privilege_object 
  and privilege = l_privilege 
  and grantee in ('PUBLIC', user);
  
exception
  when no_data_found then  
    raise_application_error(-20000, 'Required privilege not found: '||l_privilege||' on '||l_privilege_object||' for PUBLIC user or '||user);   
  when others then
    raise;

end;
/

whenever sqlerror continue
--  Create a type to hold a collection of number need by com_util.get_number_array
create or replace type table_of_number as table of number;
/

@@com_util/com_util.pls
@@com_util/com_util.plb
/

--Added i_content_type parameter in notify_with_clob to either send mail as HTML or plain text
@@com_notifier/com_notifier.pls
@@com_notifier/com_notifier.plb
/
--New package coming with functions to transform ref_cursor to HTML table or JIRA table format
set define off;
/
@@com_formatter/com_formatter.pls
@@com_formatter/com_formatter.plb
/
set define on;
/

--Improved com_job_mgr.report to send HTML mails using com_formatter
@@com_job_mgr/com_job_mgr.pls
@@com_job_mgr/com_job_mgr.plb
/

begin
  com_updater.register_applied_changes('1.4.0');
  commit;
end;
/