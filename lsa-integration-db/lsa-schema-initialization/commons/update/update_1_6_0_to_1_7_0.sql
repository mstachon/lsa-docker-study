whenever sqlerror continue

-- New event for OCOMM-274
declare
  l_event_id com_event_definitions.event_id%type;
begin
  dbms_application_info.set_module('EVENT_SETUP', 'DEFINE_COM_HISTORY_EVENTS') ;
  dbms_session.set_identifier('EVENT_SETUP') ;
  com_event_mgr.define_event('EXCEPTION_DUE_TO_MERGE_ERROR', com_logger.c_error, 'Exception due to merge error ex:"ORA-30926: unable to get a stable set of rows in the source tables"', 8, 20, 'N', l_event_id) ;

end;
/

-- New tables and types for schema analysis
create table com_schema_analysis_categories (	
  schema_analysis_category varchar2(30)	constraint comsac_pk primary key
);

create unique index comsac_uc_uk on com_schema_analysis_categories (upper(schema_analysis_category));

-- Sequence to create ids for schema analysis rules
create sequence com_schema_analysis_rules_seq; 

create table com_schema_analysis_rules (	
  schema_analysis_rule_id       number          constraint comsarules_pk primary key, 
	schema_analysis_rule_name     varchar2(30)    constraint comsarules_name_nn not null, 
	schema_analysis_category      varchar2(30)    constraint comsarules_cat_nn not null, 
	schema_analysis_rule_exec     varchar2(4000)  constraint comsarules_exec_nn not null, 
	should_bind_obj_owner_like    char(1)         constraint comsarules_bindown_nn not null, 
	should_bind_obj_type_like     char(1)         constraint comsarules_bindobjtype_nn not null, 
	should_bind_obj_name_like     char(1)         constraint comsarules_bindobjname_nn not null, 
	schema_analysis_rule_severity number(*,0)     constraint comsarules_sev_nn not null, 
	extra_info                    varchar2(100), 
   constraint comsarules_sev_chk check (schema_analysis_rule_severity between 1 and 10), 
   constraint comsarules_bindown_chk check (should_bind_obj_owner_like in ('Y', 'N')), 
   constraint comsarules_bindobjtype_chk check (should_bind_obj_type_like in ('Y', 'N')), 
   constraint comsarules_bindobjname_chk check (should_bind_obj_name_like in ('Y', 'N')), 
   constraint comsaarules_cat_fk foreign key (schema_analysis_category) references com_schema_analysis_categories (schema_analysis_category)
);

-- Index the FK
create index comsarules_cat_fk on com_schema_analysis_rules (schema_analysis_category);

-- Ensure rule names are unique regardless of case
create unique index comsarules_uc_uk on com_schema_analysis_rules (upper(schema_analysis_rule_name));

insert into com_schema_analysis_categories values ('Naming');
insert into com_schema_analysis_categories values ('Constraints');
insert into com_schema_analysis_categories values ('Indexes');
insert into com_schema_analysis_categories values ('Tables');
insert into com_schema_analysis_categories values ('Views');
insert into com_schema_analysis_categories values ('Invalid');

commit;

create or replace type schema_analysis_result as object (
  schema_analysis_rule_name	    varchar2(30),
  schema_analysis_category	    varchar2(30),  
  schema_analysis_rule_severity integer, 
  extra_info                    varchar2(500),
  schema_name	                  varchar2(30),
  object_name	                  varchar2(30),
  object_type	                  varchar2(30)
);
/
create or replace type schema_analysis_results as table of schema_analysis_result;
/
create or replace type schema_analysis_rule_result force is object (
  schema_name	                varchar2(30),
  object_name	                varchar2(30),
  object_type	                varchar2(30),
  extra_info                  varchar2(500),
  severity_factor             number
);
/
create or replace type schema_analysis_rule_results is table of schema_analysis_rule_result;
/

create sequence com_schema_analysis_run_id_seq;

create table com_schema_analysis_runs  (
  schema_analysis_run_id      number      constraint comschemaanalysisruns_pk primary key,
  schema_analysis_start_time  timestamp,
  schema_analysis_end_time    timestamp
);

-- Remove the original tables for schema analysis (using the com_object_mgr) and the corresponding sequence
drop table com_schema_analysis_results;
drop table com_schema_analysis_types;
drop sequence com_schema_analysis_type_seq;

create table com_schema_analysis_results  (
  schema_analysis_run_id        number        constraint comschemaanalysisrunres_fk references com_schema_analysis_runs (schema_analysis_run_id),
  schema_analysis_rule_name     varchar2(30), 
	schema_analysis_category      varchar2(30), 
	schema_analysis_rule_severity number, 
	extra_info                    varchar2(500), 
	schema_name                   varchar2(30),
  object_name	                  varchar2(30),
  object_type	                  varchar2(30)
);

grant select on com_schema_analysis_runs to commons;
grant select on com_schema_analysis_results to commons;

-- many packages have been changed / re-factored
@@install/packages.sql

declare
  l_dependent_users dbms_sql.varchar2_table;
begin
  com_object_mgr.repair_objects(l_dependent_users) ;
end;
/

-- Register some schema analysis rules
begin
  com_schema_analyser.reg_schema_analysis_rule (
    'Invalid Objects', com_schema_analyser.c_invalid, 'com_schema_analysis_invalid.analyse_invalid_objects',true, true, true, 10, 'Compile / Re-factor / Remove Invalid Objects'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Missing FK Indexes', com_schema_analyser.c_indexes, 'com_schema_analysis_indexes.chk_missing_fk_indexes',true, true, true, 7, 'Ensure FK columns are indexed to avoid performance problems'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Unusable Indexes', com_schema_analyser.c_indexes, 'com_schema_analysis_indexes.chk_unusable_indexes',true, true, true, 7, 'Consider  to rebuild the index'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Missing PK Constraints', com_schema_analyser.c_constraints, 'com_schema_analysis_constraint.chk_missing_pk_constraints',true, true, true, 9, 'Tables should normally have a Primary Key constraint'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Disabled Constraints', com_schema_analyser.c_constraints, 'com_schema_analysis_constraint.chk_disabled_constraints',true, true, true, 6, 'Tables should normally not have disabled constraints, consider enabling or removing the constraint'
  );
  com_schema_analyser.reg_schema_analysis_rule (
    'Nested Views', com_schema_analyser.c_views, 'com_schema_analysis_views.chk_view_nesting',true, true, true, 6, 'Views should not be built on top of other views, consider flattening the View'
  );
  commit;
end;
/

begin
  com_updater.register_applied_changes('1.7.0');
  commit;
end;
/

--  Create the com_schema_analysis scheduler job
begin
  dbms_scheduler.create_job(
    job_name        =>  'com_schema_analysis',
    job_type        =>  'PLSQL_BLOCK',
    job_action      =>  'com_schema_analyser.analyse_schema_if_changed;',
    start_date      =>   systimestamp + interval '1' minute,
    repeat_interval =>  'freq = daily; byhour = 7; byminute = 0',
    enabled         =>   true,
    comments        =>  'Analyses full schema if any object matching given criteria are changed since the last analysis'
  );
-- Sets the max run duration  
  dbms_scheduler.set_attribute(
     name          => 'com_schema_analysis',
     attribute     => 'max_run_duration',
     value         => interval '5' minute
  );
  
  commit;
end;
/
