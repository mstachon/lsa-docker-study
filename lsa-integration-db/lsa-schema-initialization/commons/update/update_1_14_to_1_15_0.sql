whenever sqlerror continue

-- OCOMM-287
@@com_schema_analysis_views/com_schema_analysis_views.pls
@@com_schema_analysis_views/com_schema_analysis_views.plb
begin
  com_schema_analyser.reg_schema_analysis_rule (
    'Views with rownum predicates', com_schema_analyser.c_views, 'com_schema_analysis_views.chk_rownum_predicates', true, true, true, 5, 'Views should not contain rownum predicates'
  );
  commit;
exception
  when dup_val_on_index then null;
  when others then raise;  
end;
/

-- OCOMM-358, OCOMM-388, OCOMM-389
@@com_types/com_types.pls
@@com_constants/com_constants.pls
@@com_assert/com_assert.pls
@@com_assert/com_assert.plb

-- OCOMM-364
@@com_rest/com_rest.pls
@@com_rest/com_rest.plb

-- OCOMM-373
@@com_test_generator/com_test_generator.plb

-- OCOMM-375, OCOMM-387
@@com_util/com_util.pls
@@com_util/com_util.plb
@@com_executor/com_executor.pls
@@com_executor/com_executor.plb

-- OCOMM-376
@@com_object_mgr/com_object_mgr.plb

-- OCOMM-378
@@com_template_util/com_template_util.pls
@@com_template_util/com_template_util.plb
@@com_template/com_template.pls
@@com_template/com_template.plb

declare
  l_event_id com_event_definitions.event_id%type;
begin
  dbms_application_info.set_module('EVENT_SETUP', 'DEFINE_INTERNAL_EVENTS');
  dbms_session.set_identifier('EVENT_SETUP');  
  begin
    com_event_mgr.define_event('COM_EXECUTOR_TASK_COMPLETE', com_logger.c_info, 'Com Executor task has finished', null, null,'N',l_event_id);
  exception
    when others then
      if not com_event_mgr.event_equals('EVENT_EXISTS', sqlcode) then
        raise;
      end if;
  end;
  
  begin
    com_event_mgr.define_event('COM_EXECUTOR_TASK_FAILED', com_logger.c_error, 'Com Executor task has failed', null, null ,'N', l_event_id);
  exception
    when others then
      if not com_event_mgr.event_equals('EVENT_EXISTS', sqlcode) then
        raise;
      end if;
  end;
end;
/
commit;
/

-- OCOMM-365, OCOMM-299
create table com_time_range_tablespaces(
  ts_name_prefix          varchar2(19)        constraint com_time_range_tablespaces_pk primary key,
  ts_datafile_size_mb     number              constraint comtrt_datafile_size_ck       check(ts_datafile_size_mb > 0),
  ts_datafile_extend_mb   number              constraint comtrt_datafile_extend_ck     check(ts_datafile_extend_mb > 0),
  ts_datafile_maxsize_mb  number              constraint comtrt_datafile_maxsize_ck    check(ts_datafile_maxsize_mb > 0),
  ts_range_int_cnt        number              constraint comtrt_range_int_cnt_nn       not null,
  ts_range_int_type       varchar2(5)         constraint comtrt_range_int_type_nn      not null,
  no_ts_advance           number              constraint comtrt_no_ts_advance_nn       not null,
  no_ts_keep_online       number              constraint comtrt_no_keep_online_ck      check(no_ts_keep_online > 0),
  no_ts_keep_total        number              constraint comtrt_no_keep_total_ck       check(no_ts_keep_total > 0),
  ts_is_bigfile           char(1) default 'N' constraint comtrt_ts_is_bigfile_ck       check(ts_is_bigfile in ('Y','N')) ,
  constraint comtrt_range_int_cnt_ck    check(ts_range_int_cnt > 0),
  constraint comtrt_range_int_type_ck   check(ts_range_int_type in ('HOUR','DAY','MONTH','YEAR')),
  constraint comtrt_no_ts_advance_ck    check(no_ts_advance > 0)
);

comment on table  com_time_range_tablespaces is 'Defines the time range tablespaces which should be managed by the com_partition_mgr';
comment on column com_time_range_tablespaces.ts_name_prefix is 'The prefix of the tablespace to be managed';
comment on column com_time_range_tablespaces.ts_datafile_size_mb is 'The initial size of a single tablespace datafile in megabytes (MB)';
comment on column com_time_range_tablespaces.ts_datafile_extend_mb is 'The size of the blocks that will extend the datafile after reaching the initial size in MB';
comment on column com_time_range_tablespaces.ts_datafile_maxsize_mb is 'The maximum size of a single tablespace datafile in MB';
comment on column com_time_range_tablespaces.ts_range_int_cnt is 'The number of intervals to be contained in a single tablespace (e.g. 1 DAY, 2 DAY, 1 MONTH)';
comment on column com_time_range_tablespaces.ts_range_int_type is 'The type of intervals to be contained in a single tablespace (one of HOUR, DAY, MONTH, YEAR)';
comment on column com_time_range_tablespaces.no_ts_advance is 'The number of tablespaces to be created in advance';
comment on column com_time_range_tablespaces.no_ts_keep_online is ' The number of tablespaces from a given definition to be kept in ONLINE mode (the mode they are created)';
comment on column com_time_range_tablespaces.no_ts_keep_total is 'The total number of tablespace from a given definition to be kept in the database';
comment on column com_time_range_tablespaces.ts_is_bigfile is 'The prefix of the tablespace to be managed';
/

alter table com_time_range_part_tables add (
  dynamic_tablespace_prefix varchar2(19),
  part_init_size_kb number,
  part_pct_free number
);
/

alter table com_time_range_part_tables add (
  constraint comtrpt_part_init_size_kb_ck check (part_init_size_kb >= 64)
);
/

alter table com_time_range_part_tables add (
  constraint comtrpt_part_pct_free_ck check (part_pct_free >= 0 and part_pct_free <= 99)
);
/

alter table com_time_range_part_tables add (
  constraint comtrpt_comtrt_fk foreign key (dynamic_tablespace_prefix) referencing  com_time_range_tablespaces(ts_name_prefix)
);
/

create index comtrpt_comtrt_fk on com_time_range_part_tables(dynamic_tablespace_prefix);
/

alter table com_time_range_part_tables rename column tablespace_prefix to tablespace_full_name;
/

alter table com_time_range_part_tables drop constraint comtrpt_tsprefix_chk;
/

alter table com_time_range_part_tables drop column dynamic_tablespace;
/

alter table com_time_range_part_tables add (
  constraint comtrpt_tsprefix_chk check(
    (tablespace_full_name is null and dynamic_tablespace_prefix is null)
    or (tablespace_full_name is not null and dynamic_tablespace_prefix is null) 
    or (tablespace_full_name is null and dynamic_tablespace_prefix is not null)
  )
);
/
@@com_partition_mgr/com_partition_mgr.pls
@@com_partition_mgr/com_partition_mgr.plb

--OCOMM-372
@@com_action_trigger/com_action_trigger.plb

-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/
begin
  com_updater.register_applied_changes('1.15.0');
  commit;
end;
/

--The procedure com_schema_analysis_naming.chk_singular_tabs has been renamed com_schema_analysis_naming.chk_singular_table_names
--but the data hasn't been updated, causing the failure of the job com_schema_analyser.analyse_schema_if_changed
update com_schema_analysis_rules set schema_analysis_rule_exec ='com_schema_analysis_naming.chk_singular_table_names'
where schema_analysis_rule_exec ='com_schema_analysis_naming.chk_singular_tabs';
/