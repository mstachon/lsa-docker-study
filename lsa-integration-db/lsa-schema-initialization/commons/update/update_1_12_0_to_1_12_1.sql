whenever sqlerror continue

-- OCOMM-338
@@com_test_generator/com_test_generator.plb
-- Missing event used in com_test_mgr
declare
  l_event_id com_event_definitions.event_id%type;
begin
	dbms_application_info.set_module('EVENT_SETUP', 'DEFINE_GENERIC_EVENT');
	dbms_session.set_identifier('EVENT_SETUP');  
  com_event_mgr.define_event('NO_DATA_FOUND_ERROR', com_logger.c_error, 'No data was found', null, null ,'N', l_event_id);
end;
/
-- Badly defined scheduler job - missing ';'
begin
  dbms_scheduler.set_attribute(
    name =>       'com_test_runner_run', 
    attribute =>  'job_action', 
    value =>      'com_test_runner.run_test_suite(''Complete suite'', true);'
  );
end; 
/

------------------------------------------------------------------------------------------------------------------------
-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/
begin
  com_updater.register_applied_changes('1.12.1');
  commit;
end;
/