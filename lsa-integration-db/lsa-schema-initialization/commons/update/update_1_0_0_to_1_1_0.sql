declare
  l_event_id com_event_definitions.event_id%type;  
begin
  dbms_application_info.set_module('COMMONS_UPGRADE', 'DEFINE_INTERNAL_EVENTS');
  dbms_session.set_identifier('COMMONS_UPGRADE');

  com_event_mgr.define_event('COM_VERSION_ERROR', com_logger.c_error, 'The commons version is not recognized from the available updates', 8, 20 ,'Y',l_event_id);
  execute immediate 'alter table com_app_sessions modify client_app_name varchar2(48)';
  com_updater.register_applied_changes('1.1.0');
  commit;
end;
/