whenever sqlerror exit

-- global context used by com_logger for cross-session communication
create or replace context log_ctx using commons.com_ctx_mgr accessed globally;
/

-- view needed by the code analysis packages
create or replace view all_source_v as
select owner, name, type, line, 
  case 
    when not regexp_like(text, '''.*''') then
      text
    else
      regexp_replace(text, '''.*''', '')
  end text
 from
 (select owner, name, type, line,
    case 
      when text not like '%--%' then
        text
      else
        substr(text, 1, instr(trim(text), '--') - 1)
    end text
  from all_source
 )
;
/

grant select on all_source_v to public;
/

@@../com_ctx_mgr/com_ctx_mgr.pls
@@../com_ctx_mgr/com_ctx_mgr.plb