@backwards_incompatible_update_change.sql

whenever sqlerror continue

set define off

-- OCOMM-420
@@com_schema_analysis_naming/com_schema_analysis_naming.pls
@@com_schema_analysis_naming/com_schema_analysis_naming.plb

update com_schema_analysis_rules
set schema_analysis_rule_exec = 'com_schema_analysis_naming.chk_singular_table_names'
where schema_analysis_rule_exec = 'com_schema_analysis_naming.chk_singular_tabs';

commit;


-- OCOMM-418
@@com_schema_analysis_indexes/com_schema_analysis_indexes.pls
@@com_schema_analysis_indexes/com_schema_analysis_indexes.plb

update com_schema_analysis_rules
set schema_analysis_rule_exec = 'com_schema_analysis_indexes.chk_iot_secondary_indexes'
where schema_analysis_rule_exec = 'com_schema_analysis_indexes.chk_iot_2ndary_indexes';

commit;


-- OCOMM-390
@@com_action_trigger/com_action_trigger.pls
@@com_action_trigger/com_action_trigger.plb


-- OCOMM-392
@@com_app_session_mgr/com_app_session_mgr.pls
@@com_app_session_mgr/com_app_session_mgr.plb

-- OCOMM-394
@@com_big_brother/com_big_brother.pls
@@com_big_brother/com_big_brother.plb

-- OCOMM-397
@@com_code_analysis_format/com_code_analysis_format.pls
@@com_code_analysis_format/com_code_analysis_format.plb

-- OCOMM-398
@@com_code_analysis_size/com_code_analysis_size.pls
@@com_code_analysis_size/com_code_analysis_size.plb

-- OCOMM-399
@@com_code_analysis_syntax/com_code_analysis_syntax.pls
@@com_code_analysis_syntax/com_code_analysis_syntax.plb

-- OCOMM-402
@@com_error_mgr/com_error_mgr.pls

-- OCOMM-403 | OCOMM-439
@@com_event_mgr/com_event_mgr.pls
@@com_event_mgr/com_event_mgr.plb

-- OCOMM-404
@@com_executor/com_executor.pls

-- OCOMM-406 | OCOMM-383 | OCOMM-380
@@com_history_mgr/com_history_mgr.pls
@@com_history_mgr/com_history_mgr.plb

-- OCOMM-407
@@com_job_mgr/com_job_mgr.pls

-- OCOMM-408
@@com_logger/com_logger.pls
@@com_logger/com_logger.plb

-- OCOMM-409
@@com_math/com_math.pls
@@com_math/com_math.plb

-- OCOMM-412 | OCOMM-436
@@com_partition_mgr/com_partition_mgr.pls
@@com_partition_mgr/com_partition_mgr.plb

-- OCOMM-413
@@com_period/com_period.pls

-- OCOMM-414
@@com_periods/com_periods.pls

-- OCOMM-415
@@com_rest/com_rest.pls

-- OCOMM-416
@@com_schema_analyser/com_schema_analyser.pls

-- OCOMM-417
@@com_schema_analysis_constraint/com_schema_analysis_constraint.pls

-- OCOMM-417
@@com_schema_analysis_invalid/com_schema_analysis_invalid.pls

-- OCOMM-421
@@com_schema_analysis_tables/com_schema_analysis_tables.pls

-- OCOMM-422
@@com_schema_analysis_views/com_schema_analysis_views.pls

-- OCOMM-423
@@com_string_utils/com_string_utils.pls
@@com_string_utils/com_string_utils.plb

-- OCOMM-424
@@com_template/com_template.pls

-- OCOMM-425
@@com_template_util/com_template_util.pls

-- OCOMM-426
@@com_test_generator/com_test_generator.pls

-- OCOMM-428
@@com_test_runner/com_test_runner.pls

-- OCOMM-430
@@com_updater/com_updater.pls
@@com_updater/com_updater.plb

-- OCOMM-453
@@com_util/com_util.pls
@@com_util/com_util.plb

------------------------------------------------------------------------------------------------------------------------
-- Backwards incompatible changes
------------------------------------------------------------------------------------------------------------------------
-- OCOMM-391: functions in this package have been renamed from access_xxx to get_xxx
@@com_anydata_extensions/com_anydata_extensions.pls
@@com_anydata_extensions/com_anydata_extensions.plb

-- OCOMM-395: Removed function contains_string
@@com_code_analyser/com_code_analyser.pls
@@com_code_analyser/com_code_analyser.plb

-- OCOMM-400 (and OCOMM-434): 
--   get_vc_vc_map(i_keys in table_of_varchar, i_values in table_of_varchar ) renamed to key_val_varchars_to_vc_vc_map
--   get_vc_vc_map(i_cu_key_values in sys_refcursor) renamed to cursor_to_vc_vc_map
--   to_table_of_number renamed to varchars_to_numbers
@@com_collection_utils/com_collection_utils.pls
@@com_collection_utils/com_collection_utils.plb

-- OCOMM-405: 
--   to_string(i_numbers in table_of_number ... ) removed
--   to_string(i_strings in table_of_varchar ... ) removed
--   to_string(i_timestamps in table_of_timestamp ... ) removed
--   com_formatter.c_yes and com_formatter.c_no moved to com_constants package
@@com_types/com_types.pls
@@com_constants/com_constants.pls
@@com_formatter/com_formatter.pls
@@com_formatter/com_formatter.plb

-- OCOMM-410: 
--   notify_with_clob removed
--   clear_logs removed
@@com_notifier/com_notifier.pls
@@com_notifier/com_notifier.plb

-- OCOMM-411: 
--   repair_objects function removed
@@com_object_mgr/com_object_mgr.pls
@@com_object_mgr/com_object_mgr.plb

-- OCOMM-443
-- Make vc_vc_map have real map like behaviour and functions 
drop type vc_key_vc_value force;
@@vc_vc_map/vc_vc_map.pls
@@vc_vc_map/vc_vc_map.plb
@@vc_vc_map/tests/vc_vc_map_test.pls
@@vc_vc_map/tests/vc_vc_map_test.plb
@@com_types/com_types.pls
@@com_rest/com_rest.plb
@@com_job_mgr/com_job_mgr.plb
@@com_collection_utils/com_collection_utils.plb
@@com_event_mgr/com_event_mgr.plb

-- OCOMM-455
alter table com_app_sessions add application_token varchar2(128);
-- needed for some machines with longer host names:
alter table com_app_sessions modify (direct_host varchar2(100));

@@com_app_session_mgr/com_app_session_mgr.pls
@@com_app_session_mgr/com_app_session_mgr.plb

-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/
-- Add the test package to a new testing suite, and attach that suite to the Complete suite which is executed daily 
/*
begin
  com_test_mgr.add_test_package ('VC_VC_MAP_TEST', 'COMMONS Tests');
  commit;
end;
/
*/

begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/
begin
  com_updater.register_applied_changes('1.16.0');
  commit;
end;
/

set define on