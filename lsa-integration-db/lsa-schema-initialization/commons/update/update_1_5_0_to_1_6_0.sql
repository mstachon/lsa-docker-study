whenever sqlerror exit

grant select on com_parameters to commons;
grant select on com_code_analysis_runs to commons;
grant select on com_code_analysis_run_results to commons;

begin
  com_updater.register_applied_changes('1.6.0');
  commit;
end;
/