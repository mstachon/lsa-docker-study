whenever sqlerror continue

-- OCOMM-286
@@com_schema_analysis_views/com_schema_analysis_views.pls
@@com_schema_analysis_views/com_schema_analysis_views.plb

-- OCOMM-334
@@com_formatter/com_formatter.plb

-- OCOMM-286 Check for Views Containing "order by" with com_schema_analysis_views.chk_order_by
------------------------------------------------------------------------------------------------------------------------
begin
  com_schema_analyser.reg_schema_analysis_rule (
    'Views with order by clauses', com_schema_analyser.c_views, 'com_schema_analysis_views.chk_order_by', true, true, true, 6, 'Views should not contain order by clauses'
  );  
exception
  when dup_val_on_index then null;
  when others then raise;
end;
/

-- OCOMM-352 - Adding member functions to support native comparison of map entries
create or replace type vc_key_vc_value force as object (
  vc_key        varchar2(30),
  vc_value      varchar2(4000),
  member function to_string return varchar2,
  map member function equals_comparator return varchar2
);
/

create or replace type body vc_key_vc_value is

  member function to_string return varchar2
  is
  begin
    return 'key='||self.vc_key||',value='||self.vc_value;
  end to_string;
  
  map member function equals_comparator return varchar2
  is
  begin
    return self.to_string();
  end equals_comparator;
 
end;
/

-- OCOMM-248
@@com_math/com_math.pls
@@com_math/com_math.plb

-- OCOMM-347
@@com_types/com_types.pls
@@com_constants/com_constants.pls
@@com_util/com_util.pls
@@com_util/com_util.plb
@@com_history_mgr/com_history_mgr.plb
@@com_schema_analyser/com_schema_analyser.plb

-- OCOMM-351
@@com_test_generator/com_test_generator.plb

-- OCOMM-346, OCOMM-349
@@com_collection_utils/com_collection_utils.pls
@@com_collection_utils/com_collection_utils.plb

-- OCOMM-343
@@com_string_utils/com_string_utils.pls
@@com_string_utils/com_string_utils.plb


-- Missing from OCOMM-310 and OCOMM-313 (Version 1.11.0), and needed for OCOMM-354
begin
  insert into com_event_notif_filter_rules (filter_rule) values ('=');
  insert into com_event_notif_filter_rules (filter_rule) values ('!=');
  insert into com_event_notif_filter_rules (filter_rule) values ('>');
  insert into com_event_notif_filter_rules (filter_rule) values ('>=');
  insert into com_event_notif_filter_rules (filter_rule) values ('<');
  insert into com_event_notif_filter_rules (filter_rule) values ('<=');
  insert into com_event_notif_filter_rules (filter_rule) values ('LIKE');
  insert into com_event_notif_filter_rules (filter_rule) values ('NOT LIKE');
  insert into com_event_notif_filter_rules (filter_rule) values ('IN');
  insert into com_event_notif_filter_rules (filter_rule) values ('NOT IN');
exception
  when dup_val_on_index then null;
  when others then raise;
end;
/

-- OCOMM-354
declare
  l_event_id com_event_definitions.event_id%type;
begin
  dbms_application_info.set_module('COMMONS_SETUP', 'DEFINE_INTERNAL_EVENTS');
	dbms_session.set_identifier('COMMONS_SETUP');
  begin
    com_event_mgr.define_event('JOB_MONITORING_REPORT', com_logger.c_info, 'Monitoring Report for Jobs which run too long', null, null,'Y',l_event_id);
  exception
    when others then
      if not com_event_mgr.event_equals('EVENT_EXISTS', sqlcode) then
        raise;
      end if;
  end;
end;
/
@@com_job_mgr/com_job_mgr.pls
@@com_job_mgr/com_job_mgr.plb

------------------------------------------------------------------------------------------------------------------------
-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/
begin
  com_updater.register_applied_changes('1.13.0');
  commit;
end;
/