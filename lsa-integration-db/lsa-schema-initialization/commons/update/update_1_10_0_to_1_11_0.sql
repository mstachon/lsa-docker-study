whenever sqlerror continue

-- OCOMM-314
@@com_util/com_util.pls
@@com_util/com_util.plb

-- OCOMM-315
@@com_schema_analysis_views/com_schema_analysis_views.plb

-- OCOMM-316
@@com_assert/com_assert.pls
@@com_assert/com_assert.plb

-- OCOMM-317
comment on column com_recipients.recipient_sms_number is 'This should be a CERN mobile number of the format ''+4175411____''';
alter table com_recipients drop constraint "COMRCPTS_EMAIL_CHK";
update com_recipients set recipient_sms_number = replace (recipient_sms_number,'+4176487','+4175411') where recipient_sms_number is not null  ;
alter table com_recipients add constraint comrcpts_sms_chk check (recipient_sms_number like ('+4175411%')) enable;
/

-- OCOMM-320
@@com_logger/com_logger.plb
@@com_job_mgr/com_job_mgr.plb

-- OCOMM-310 and OCOMM-313
create table com_event_notif_filters (
  filter_id               number constraint cenf_pk primary key,
  filter_name             varchar2(30)  constraint cenf_filter_name_nn not null,
  filter_description      varchar2(200) constraint cenf_description_nn not null,
  filter_combination_rule varchar2(3)   default 'AND' constraint cenf_combine_rule_nn not null,
  constraint cenf_combine_rule_chk check (filter_combination_rule in ('AND', 'OR'))
);
/

create unique index cenf_filter_name_idx on com_event_notif_filters(UPPER(filter_name));
/

create table com_event_notif_filter_rules(
filter_rule varchar2(10) constraint cenfr_pk primary key,
constraint cenfr_filter_rule_chk check (filter_rule = UPPER(filter_rule))
)
organization index;
/

create table com_event_notif_filter_values (
  filter_id       number         constraint cenfv_cenf_fk references com_event_notif_filters (filter_id),
  filter_key      varchar2(30)   constraint cenfv_key_chk check (filter_key = UPPER(filter_key)),
  filter_values   varchar2(4000) constraint cenfv_values_nn not null,
  filter_rule     varchar2(10)   constraint cenfv_rule_nn not null,
  constraint cenfv_pk primary key (filter_id, filter_key),
  constraint cenfv_cenfr_fk foreign key (filter_rule) referencing com_event_notif_filter_rules(filter_rule)
);
/

alter table com_event_notifications add (
  filter_id       number        constraint comevntnot_cenf_fk references com_event_notif_filters (filter_id)
);
/

create index comevntnot_cenf_fk on com_event_notifications(filter_id);
/

create sequence com_event_notif_filters_seq nocycle;
/

create table com_event_domains (
  domain_id number constraint com_event_domains_pk primary key,
  domain_name varchar2(30) constraint com_event_domains_name_nn not null,
  domain_description varchar2(100)
);
/

create unique index com_event_domain_name_idx on com_event_domains(UPPER(domain_name));
/

create sequence com_event_domains_seq nocycle;

alter table com_event_definitions add (
  domain_id number constraint com_evnt_def_com_evnt_dom_fk references com_event_domains (domain_id),
  event_filter_view_name varchar2(30)
);
/

create or replace type vc_key_vc_value force as object (
  vc_key        varchar2(30),
  vc_value      varchar2(4000)
);
/

create or replace type vc_vc_map as table of vc_key_vc_value;
/

@@com_collection_utils/com_collection_utils.pls
@@com_collection_utils/com_collection_utils.plb

@@com_notifier/com_notifier.pls
@@com_notifier/com_notifier.plb

@@com_event_mgr/com_event_mgr.pls
@@com_event_mgr/com_event_mgr.plb

-- Compile invalid objects
begin
  dbms_utility.compile_schema(sys_context('userenv','current_schema'), compile_all => false);
end;
/

begin
  com_updater.register_applied_changes('1.11.0');
  commit;
end;
/
