whenever sqlerror exit

declare
  l_privilege_object  varchar2(30)  :=  'ALL_SOURCE_V';
  l_privilege         varchar2(30)  :=  'SELECT';
begin

--Check that ALL_SOURCE_V is visible      
  select distinct privilege
  into l_privilege
  from all_tab_privs_recd 
  where table_name = l_privilege_object 
  and privilege = l_privilege 
  and grantee in ('PUBLIC', user);
  
exception
  when no_data_found then  
    raise_application_error(-20000, 'Required privilege not found: '||l_privilege||' on '||l_privilege_object||' for PUBLIC user or '||user);   
  when others then
    raise;

end;
/

whenever sqlerror continue

declare
  l_event_id com_event_definitions.event_id%type;  
begin
  dbms_application_info.set_module('COMMONS_UPGRADE', 'DEFINE_INTERNAL_EVENTS');
  dbms_session.set_identifier('COMMONS_UPGRADE');

  com_event_mgr.define_event('ASSERTION_ERROR', com_logger.c_error, 'Error during assertion', null, null, 'Y', l_event_id);
  commit;
end;
/

create synonym all_source_v for commons.all_source_v;
/


alter table com_logs add (log_client_id varchar2(30) default sys_context('userenv', 'client_identifier'));
/

create table com_code_analysis_categories (
  code_analysis_category varchar2(30) constraint comcac_pk primary key
);
-- Ensure the name of the code analysis category is unique, regardless of case
create unique index comcac_uc_uk on com_code_analysis_categories(upper(code_analysis_category));

-- Sequence to generate unique ids for code analysis rules 
create sequence com_code_analysis_rules_seq;

create table com_code_analysis_rules (
  code_analysis_rule_id       number          constraint comcarules_pk primary key,
  code_analysis_rule_name	    varchar2(30)    constraint comcarules_name_nn not null,
  code_analysis_category	    varchar2(30)    constraint comcarules_cat_nn not null,  
  code_analysis_rule_exec	    varchar2(4000)  constraint comcarules_exec_nn not null, 
  should_bind_obj_owner_like  char(1)         constraint comcarules_bindown_nn not null, 
  should_bind_obj_type_like   char(1)         constraint comcarules_bindobjtype_nn not null, 
  should_bind_obj_name_like   char(1)         constraint comcarules_bindobjname_nn not null, 
  code_analysis_rule_severity integer         constraint comcarules_sev_nn not null, 
  extra_info                  varchar2(100),  
  constraint comcaarules_cat_fk foreign key (code_analysis_category) references com_code_analysis_categories (code_analysis_category),
  constraint comcarules_sev_chk check (code_analysis_rule_severity between 1 and 10),
  constraint comcarules_bindown_chk check (should_bind_obj_owner_like in ('Y', 'N')),
  constraint comcarules_bindobjtype_chk check (should_bind_obj_type_like in ('Y', 'N')),
  constraint comcarules_bindobjname_chk check (should_bind_obj_name_like in ('Y', 'N'))
);
-- Ensure the name of the code analysis case is unique, regardless of case
create unique index comcarules_uc_uk on com_code_analysis_rules(upper(code_analysis_rule_name));
-- Index the FK
create index comcarules_cat_fk on com_code_analysis_rules(code_analysis_category);
-- Populate some Code Analysis Categories
insert into com_code_analysis_categories (code_analysis_category)
values ('Syntax');
insert into com_code_analysis_categories (code_analysis_category)
values ('Formatting');
insert into com_code_analysis_categories (code_analysis_category)
values ('Size');
-- Define the types required for the code analysis
create or replace type code_analysis_result as object (
  code_analysis_rule_name	    varchar2(30),
  code_analysis_category	    varchar2(30),  
  code_analysis_rule_severity integer, 
  extra_info                  varchar2(500),
  code_owner	                varchar2(30),
  code_name	                  varchar2(30),
  code_type	                  varchar2(12),
  line_no	                    integer,
  line_text	                  varchar2(4000),
  line_column_pos             integer
);
/
create or replace type code_analysis_results as table of code_analysis_result;
/
create or replace type code_analysis_rule_result is object (
  code_owner	                varchar2(30),
  code_type	                  varchar2(12),
  code_name	                  varchar2(30),
  line_no	                    integer,
  line_text	                  varchar2(4000),
  line_column_pos             integer,
  extra_info                  varchar2(100)
);
/
create or replace type code_analysis_rule_results is table of code_analysis_rule_result;
/

@@com_util/com_util.pls
@@com_assert/com_assert.pls
@@com_event_mgr/com_event_mgr.pls
@@com_big_brother/com_big_brother.pls
@@com_logger/com_logger.pls
@@com_code_analyser/com_code_analyser.pls
@@com_code_analysis_format/com_code_analysis_format.pls
@@com_code_analysis_size/com_code_analysis_size.pls
@@com_code_analysis_syntax/com_code_analysis_syntax.pls

@@com_util/com_util.plb
@@com_assert/com_assert.plb
@@com_event_mgr/com_event_mgr.plb
@@com_big_brother/com_big_brother.plb
@@com_logger/com_logger.plb
@@com_code_analyser/com_code_analyser.plb
@@com_code_analysis_format/com_code_analysis_format.plb
@@com_code_analysis_size/com_code_analysis_size.plb
@@com_code_analysis_syntax/com_code_analysis_syntax.plb

@@install/define_analysis_rules.sql
begin
  com_updater.register_applied_changes('1.2.0');
  commit;
end;
/