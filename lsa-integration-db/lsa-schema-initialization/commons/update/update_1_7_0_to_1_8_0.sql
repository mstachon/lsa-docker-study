whenever sqlerror continue

-- OCOMM-300
alter table com_history_tables add (
  re_raise_exceptions char(1) default 'N' constraint com_hist_tabs_reraiseexc_nn not null,
  constraint com_hist_tabs_reraiseexc_chk check (re_raise_exceptions in ('N', 'Y'))
);
@@com_history_mgr/com_history_mgr.pls
@@com_history_mgr/com_history_mgr.plb

-- Some packages for schema analysis have been changed / re-factored
-- OCOMM-297 and OCOMM-298
@@com_schema_analysis_constraint/com_schema_analysis_constraint.pls
@@com_schema_analysis_constraint/com_schema_analysis_constraint.plb

-- Register some new schema analysis rules
begin  
   com_schema_analyser.reg_schema_analysis_rule (
	'Deffered Constraints', com_schema_analyser.c_constraints, 'com_schema_analysis_constraint.chk_deferred_constraints', true, true, true, 3,'Table is using DEFERRABLE or DEFRRED constraints, consider revieweing data and the constraints');
  com_schema_analyser.reg_schema_analysis_rule (
	'Not Validated Constraints', com_schema_analyser.c_constraints, 'com_schema_analysis_constraint.chk_not_validated_constraints', true, true, true, 5, 'Table is using NOT VALIDATED constraint, consider revieweing data and the constraints');
  commit;
end;
/

begin
  com_updater.register_applied_changes('1.8.0');
  commit;
end;
/

