whenever sqlerror exit
prompt starting backwards incompatible pre-update check 
begin
  if upper(&force_back_incompatible_Y_N) != 'Y' then
    raise_application_error(-20000, 
      'This version of commons contains backwards incompatible changes, and you have chosen not to force such an upgrade. Exiting...');   
  end if;
end;
/
