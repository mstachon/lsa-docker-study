whenever sqlerror exit

------------------------------------------------------------------------------------------------------------------------
--  Create the com_dml_logs underlying objects
--  Create a range partitioned table to store dml log entries, to be manged by the partition manager in 
--  order to add new partitions in advance of when they will be required, and drop older empty 
--  partitions.   An initial partition must be created, and in this case it assumes the range 
--  partition size is 1 day, and that initial partition will span the current day.
declare
--  Establish the timestamp of the start of today 
  l_start_of_this_day timestamp :=  trunc(systimestamp, 'DD');
--  Establish the timestamp of the start of the next day
  l_start_of_next_day timestamp :=  l_start_of_this_day + interval '1' day;
begin

--  Dynamically create the com_dml_logs table with the correct partitioning structure.
-- NOTE the com_app_session_id does not have an FK to com_app_sessions due to the need to be able to automatically drop partitions of the com_app_sessions table
  execute immediate q'#create table com_dml_logs (
    com_app_session_id  number,
    create_time_utc     timestamp(6)  constraint com_dml_logs_ctime_nn not null,
    op_type             char(1)       constraint com_dml_logs_op_type_nn not null,
    table_name          varchar2(30)  constraint com_dml_logs_table_nn not null,
    module              varchar2(48)  default sys_context('userenv', 'module'),
    action              varchar2(32)  default sys_context('userenv', 'action'),
    transaction_id      varchar2(48),
    constraint com_dml_logs_optype_chk check (op_type in ('D', 'I', 'U'))
  ) partition by range (create_time_utc) (
    partition com_dml_logs_#'||to_char(l_start_of_this_day, 'YYYYMMDD')||q'# values less than (timestamp '#'||to_char(l_start_of_next_day, 'YYYY-MM-DD HH24:MI:SS')||q'#')
  )#';

end;
/

--add indexes
create index com_dml_logs_app_sess_fk on com_dml_logs (com_app_session_id);
create index com_dml_logs_table_i on com_dml_logs (table_name);
create index com_dml_logs_ctime_i on com_dml_logs (create_time_utc);
create index com_dml_logs_trans_id_i on com_dml_logs (transaction_id);

------------------------------------------------------------------------------------------------------------------------
--  register the com_dml_logs table, and perform the initial partition management
begin 
  com_partition_mgr.reg_time_range_part_table(
      i_table_name          => 'COM_DML_LOGS'
    , i_part_prefix         => 'COM_DML_LOGS'
    , i_no_parts_advance    => 5
    , i_part_range_int_cnt  => 1
    , i_part_range_int_type => 'DAY'
  );

  com_partition_mgr.manage_partitions('COM_DML_LOGS');
end;
/
create or replace type com_hist_ignored_cols force as varray(1000) of varchar2(30);
/
alter table com_history_tables add ignored_columns com_hist_ignored_cols;
/

@@com_history_mgr/com_history_mgr.pls
@@com_history_mgr/com_history_mgr.plb

begin
  com_updater.register_applied_changes('1.3.0');
  commit;
end;
/

