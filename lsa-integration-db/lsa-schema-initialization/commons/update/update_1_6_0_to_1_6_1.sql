whenever sqlerror exit

-- many package have been changed as a result of issue OCOMM-267
@@install/packages.sql

declare
  l_dependent_users dbms_sql.varchar2_table;
begin
  com_object_mgr.repair_objects(l_dependent_users) ;
end;
/

begin
  com_updater.register_applied_changes('1.6.1');
  commit;
end;
/