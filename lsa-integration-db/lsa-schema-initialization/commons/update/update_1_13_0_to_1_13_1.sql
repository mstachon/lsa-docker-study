whenever sqlerror continue

--  In response to comment from Marcin on OCOMM-346
@@com_collection_utils/com_collection_utils.plb

begin
  com_updater.register_applied_changes('1.13.1');
  commit;
end;
/