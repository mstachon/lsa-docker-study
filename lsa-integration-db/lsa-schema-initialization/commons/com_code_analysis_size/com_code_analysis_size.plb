create or replace package body com_code_analysis_size
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains code analysis rules related to size / length checks.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-22 : Chris Roderick, Mateusz Polnik
--    Updated analyse_pkg_body_mod_length
--  2013-09-12 : Chris Roderick, Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  function analyse_pkg_body_length (
    i_obj_owner_like  in  all_source.owner%type default user, 
    i_obj_type_like   in  all_source.type%type  default '%', 
    i_obj_name_like   in  all_source.name%type  default '%'
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks package body length to see if size exceeds recommended limit
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-01 : Gkergki Strakosa
--    Ignore commented and empty lines (OCOMM-275)
--  2013-09-12 : Chris Roderick, Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result              code_analysis_rule_results;
    -- FIXME - can we modify the infrastructure to pass such parameters from the rule definitions in a generic way
    l_max_pkg_body_length number :=  1000;
  begin
      
    select code_analysis_rule_result(code_owner, code_type, code_name, 1, code_name, 1, 
      'Package Length is '||package_length||' lines, Max recommended length is '||l_max_pkg_body_length||' lines')
    bulk collect into l_result
    from (
      select owner code_owner, type code_type, name code_name, count(line) package_length
      from all_source_v 
      where type like i_obj_type_like
      and owner like i_obj_owner_like
      and name like i_obj_name_like
      and regexp_replace(text, '[[:space:]]*','') is not null 
      and (length(regexp_replace(text, '[[:space:]]*','')) != 1 and ascii(regexp_replace(text, '[[:space:]]*','')) not in (10, 13)) 
      group by name, owner, type
      having count(line) > l_max_pkg_body_length
    )
    order by package_length desc;
    
    return l_result;     
  end analyse_pkg_body_length;

  function analyse_pkg_body_mod_length (
    i_obj_owner_like  in  all_source.owner%type default user, 
    i_obj_type_like   in  all_source.type%type  default '%', 
    i_obj_name_like   in  all_source.name%type  default '%'
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks package body module (procedure and function) length to see if size exceeds recommended limit
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-01 : Gkergki Strakosa
--    Ignore commented and empty lines (OCOMM-275)
--  2013-11-22 : Chris Roderick, Mateusz Polnik
--    OCMOMM-247 - Excluded whitespace and comments from the module count.
--  2013-09-19 : Chris Roderick, Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result              code_analysis_rule_results;
    -- FIXME - can we modify the infrastructure to pass such parameters from the rule definitions in a generic way
    l_max_module_length number :=  50;
  begin

    select code_analysis_rule_result(code_owner, 'PACKAGE BODY', object_name, line, '  '||code_type||' '||code_name, col, 
      code_type ||' '||object_name||'.'||code_name ||' length is '||type_length||' lines, Max recommended length is '||l_max_module_length||' lines')
    bulk collect into l_result
    from (
      select owner code_owner, name code_name, type code_type, object_name, object_type, (
        select count(*) 
        from all_source_v asrc 
        where asrc.type = 'PACKAGE BODY'
        and asrc.owner = src.owner
        and asrc.name = src.object_name
        and line between type_def_start and type_def_end
        and regexp_replace(text, '[[:space:]]*','') is not null 
        and (length(regexp_replace(text, '[[:space:]]*','')) != 1 and ascii(regexp_replace(text, '[[:space:]]*','')) not in (10, 13))
        ) type_length, type_def_start line, col      
      from (
        select owner, name, type, object_name, object_type, line type_def_start, col, 
          coalesce(lead(line) over (partition by object_name, object_type, usage order by line), (
            select max(line) 
            from all_source_v 
            where type = ai.object_type 
            and owner = ai.owner 
            and name = ai.object_name)
          ) - 2 type_def_end
        from all_identifiers ai
        where object_type = 'PACKAGE BODY' 
        and usage = 'DEFINITION' 
        and type in ('PROCEDURE','FUNCTION')
        and owner like i_obj_owner_like
        and object_name like i_obj_name_like
      ) src
    )
    where type_length > l_max_module_length
    order by type_length desc;

    return l_result;      
  end analyse_pkg_body_mod_length;
  
end com_code_analysis_size;