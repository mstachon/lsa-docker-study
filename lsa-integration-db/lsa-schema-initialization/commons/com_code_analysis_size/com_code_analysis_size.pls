create or replace package com_code_analysis_size
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains code analysis rules related to size / length checks.
--  Scope:
--    Operates on the database level, but depends on individual code analysis rule implementations.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-15 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-398
--  2013-09-12 : Chris Roderick, Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  function analyse_pkg_body_length (
    i_obj_owner_like  in  all_source.owner%type default user, 
    i_obj_type_like   in  all_source.type%type  default '%', 
    i_obj_name_like   in  all_source.name%type  default '%'
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks package body length to see if size exceeds recommended limit
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-09-12 : Chris Roderick, Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function analyse_pkg_body_mod_length (
    i_obj_owner_like  in  all_source.owner%type default user, 
    i_obj_type_like   in  all_source.type%type  default '%', 
    i_obj_name_like   in  all_source.name%type  default '%'
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks package body module (procedure and function) length to see if size exceeds recommended limit
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-09-19 : Chris Roderick, Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------

end com_code_analysis_size;