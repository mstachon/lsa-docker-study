create or replace type com_template
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Facilitates code generation by binding placeholders in the provided template. 
--    Supported placeholders are: 
--      #name - single value placeholder; all the occurrences will be bound. The name can't contain regex wildcards  
--      $name[...] - collection placeholder; content in the brackets will be repeated n-times based on collection input;
--    Single value placeholders can be used within the collection, i.e.:
--      $procedures[
--        #procedure_name 
--         is
--          begin
--            null;
--          end #procedure_name;
--      ]
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-14 Jose Rolland
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 -  OCOMM-424
--  2016-03-04 Lukasz Burdzanowski
--    Added bind(i_template_text in clob, i_collection in table_of_varchar) OCOMM-378
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------
is object (
  l_template_text clob,  

  constructor function com_template (self in out nocopy com_template, i_template_text in varchar2) return self as result,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Creates new instance of the template based on provided text.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_template_text - template text 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------

  member function bind(i_key in varchar2, i_value in varchar2) return com_template,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Searches template for all occurrences of the binding key (starting from #), replaces them with the passed 
--    value and returns modified template text. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_key - binding key to look for
--    i_value - value replacing all occurrences of i_key  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INVALID_ARGUMENT assertion on unknown binding
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------  
  
  member function bind(i_collection_key in varchar2, i_collection in table_of_varchar) return com_template,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Searches template for first occurence of the collection binding key (starting from $) and replaces it 
--    n-times based on provided table. Returns modified template text. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_collection_key - binding key to look for
--    i_collection - collection of values replacing the i_key   
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INVALID_ARGUMENT assertion on unknown binding
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------
  
  member function bind(i_collection_key in varchar2, i_cursor in sys_refcursor, i_close_cursor in boolean default false)
  return com_template,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Searches template for first occurence of the collection binding key (starting from $) and replaces it 
--    n-times based on provided cursor. Returns modified template text. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_collection_key - binding key to look for
--    i_collection - collection of values replacing the i_key 
--    i_close_cursor - optionally closes the passed cursor 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INVALID_ARGUMENT assertion on unknown binding
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------    

  member function bind(i_collection in table_of_varchar)
  return clob,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Binds all occurrences of # with consecutive values from the passed array.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_collection - collection of values replacing consecutive # anchors 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INVALID_ARGUMENT when collection size does not match count of # anchors
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-03-04 Lukasz Burdzanowski
--    Creation OCOMM-378
------------------------------------------------------------------------------------------------------------------------

  member function generate return clob
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the template text with all keys bound.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------  
);