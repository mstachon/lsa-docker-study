create or replace type body com_template 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-14 Pascal le Roux
--  When calling com_template constructor in the bind functions, needed to specify explicitly the parameter name i_template_text
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------ 
is
  constructor function com_template (self in out nocopy com_template, i_template_text in varchar2) return self as result
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------     
  is
  begin
    com_assert.arg_not_null(i_template_text, 'i_template');    
    self.l_template_text := i_template_text;    
    return;
  end com_template;  
  
  member function bind(i_key in varchar2, i_value in varchar2) return com_template
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-14 Pascal le Roux
--    Removed unused variable l_out clob;
--    Need to specify explicitly the parameter name i_template_text, otherwise, get
--    ORA-03113: end-of-file on communication channel
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------   
  is
  begin  
    return com_template( i_template_text => com_template_util.bind(self.l_template_text, i_key, i_value));  
  end bind;
  
  member function bind(i_collection_key in varchar2, i_collection in table_of_varchar) return com_template
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-14 Pascal le Roux
--    Need to specify explicitly the parameter name i_template_text, otherwise, get
--    ORA-03113: end-of-file on communication channel
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------   
  is
  begin
    return com_template(i_template_text => com_template_util.bind(self.l_template_text, i_collection_key, i_collection));   
  end bind;
  
  member function bind(i_collection_key in varchar2, i_cursor in sys_refcursor, i_close_cursor in boolean default false)
  return com_template
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-14 Pascal le Roux
--    Need to specify explicitly the parameter name i_template_text, otherwise, get
--    ORA-03113: end-of-file on communication channel
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------   
  is    
  begin
    return com_template(i_template_text => com_template_util.bind(self.l_template_text, i_collection_key, i_cursor));  
  end bind;
  
  member function bind(i_collection in table_of_varchar)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-03-04 Lukasz Burdzanowski
--    Creation OCOMM-378
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return com_template_util.bind(self.l_template_text, i_collection);
  end bind;
    
  member function generate return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-09 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------   
  is
  begin
    return self.l_template_text;
  end generate;  
end;