create or replace package com_math
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   This package contains methods for performing basic numeric operations such as linear interpolation ...
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Added binary_to_decimal, decimal_to_binary, decimal_to_hexadecimal and hexadecimal_to_decimal. OCOMM-409
--  2015-10-02  Chris Roderick
--    Some formatting and documentation prior to inclusion in Commons. OCOMM-248
--  2014-02-06  Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  function get_linear_interpolation_for(
    i_x0  in number,
    i_y0  in number,
    i_x1  in number,
    i_y1  in number,
    i_x   in number
  ) 
  return number;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This function calculates a linear interpolation for the given variable and coordinates.
--     (see https://en.wikipedia.org/wiki/Linear_interpolation)
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_x        - the x coordinate at which to calculate and return the corresponding value of y
--   i_x0       - the x coordinate of the first point
--   i_x1       - the x coordinate of the second point
--   i_y0       - the y coordinate of the first point
--   i_y1       - the y coordinate of the second point
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-02  Chris Roderick
--    Some formatting and documentation prior to inclusion in Commons. OCOMM-248
--  2014-02-06  Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function decimal_to_binary (i_decimal_number in number) return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Converts a decimal number to its binary representation
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_decimal_number - decimal number to translate
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR when i_decimal_number is null or is not a positive integer
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Refactored and added to Commons4Oracle. OCOMM-409
--  2015-11-16 Ana
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function binary_to_decimal (i_binary_number in varchar2) return number;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Converts a binary number to its decimal representation
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_binary_number - binary number to translate
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR when i_binary_number is null or is not a binary number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Refactored and added to Commons4Oracle. OCOMM-409
--  2015-11-16 Ana
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function decimal_to_hexadecimal (
    i_decimal_number in number,
    i_include_prefix in com_types.t_boolean_as_char default com_constants.c_true
  ) 
  return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Converts a decimal number to its hexadecimal representation
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_decimal_number - decimal number to translate
--   i_include_prefix - indicates if the result includes the hexadecimal preffix '0X'
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR when i_decimal_number is null or is not a positive integer
--    ASSERTION_ERROR when i_include_prefix is not Y or N
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Refactored and added to Commons4Oracle. OCOMM-409
------------------------------------------------------------------------------------------------------------------------

  function hexadecimal_to_decimal (i_hexadecimal_number in varchar2) return number;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Converts an hexadecimal number to its decimal representation
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_hexadecimal_number - binary number to translate
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR when i_hexadecimal_number is null or is not an hexadecimal number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Refactored and added to Commons4Oracle. OCOMM-409
------------------------------------------------------------------------------------------------------------------------
  
end com_math;