create or replace package body com_math
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Added binary_to_decimal, decimal_to_binary, decimal_to_hexadecimal and hexadecimal_to_decimal. OCOMM-409
--  2015-10-02  Chris Roderick
--    Some formatting and documentation prior to inclusion in Commons. OCOMM-248
--  2014-02-06  Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  function get_linear_interpolation_for(
    i_x0  in number,
    i_y0  in number,
    i_x1  in number,
    i_y1  in number,
    i_x   in number
  ) 
  return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-02  Chris Roderick
--    Some formatting, documentation, and assertions prior to inclusion in Commons. OCOMM-248
--  2014-02-06  Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.arg_not_null(i_x0, 'i_x0');
    com_assert.arg_not_null(i_y0, 'i_y0');
    com_assert.arg_not_null(i_x1, 'i_x1');
    com_assert.arg_not_null(i_y1, 'i_y1');
    com_assert.arg_not_null(i_x, 'i_x');
    
    return i_y0 + ((i_x - i_x0) / (i_x1 - i_x0) * (i_y1 - i_y0));    
  end get_linear_interpolation_for;
  
  function decimal_to_binary (i_decimal_number in number) return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Refactored and added to Commons4Oracle. OCOMM-409
--  2015-11-16 Ana
--    Creation
------------------------------------------------------------------------------------------------------------------------  
  is
    l_binary_number com_types.t_varchar2_max_size_plsql;
    l_decimal_number_to_iterate number := i_decimal_number;
  begin
    com_assert.arg_not_null(i_decimal_number, 'i_decimal_number');
    com_assert.is_true(i_decimal_number >= 0 and i_decimal_number = trunc(i_decimal_number), i_decimal_number||
      ' must be a positive integer.');
    
    while (l_decimal_number_to_iterate > 0 ) loop
       l_binary_number := mod(l_decimal_number_to_iterate, 2) || l_binary_number;
       l_decimal_number_to_iterate := trunc(l_decimal_number_to_iterate / 2);
    end loop;
    
    return coalesce(l_binary_number,'0');
  end decimal_to_binary;

  function binary_to_decimal (i_binary_number in varchar2) return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Refactored and added to Commons4Oracle. OCOMM-409
--  2015-11-16 Ana
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_current_digit varchar2(1);
    l_decimal_number number := 0;
  begin
    com_assert.arg_not_null(i_binary_number, 'i_binary_number');
    com_assert.is_true(regexp_like(i_binary_number, '^[01]{1,}$'), i_binary_number||' is not a binary number.');
  
    for i in 1..length(i_binary_number) loop
       l_current_digit := substr(i_binary_number, i, 1);
       l_decimal_number := (l_decimal_number * 2) + to_number(l_current_digit);
    end loop;
    
    return l_decimal_number;
  end binary_to_decimal;
  
  function decimal_to_hexadecimal (
    i_decimal_number in number,
    i_include_prefix in com_types.t_boolean_as_char default com_constants.c_true
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Refactored and added to Commons4Oracle. OCOMM-409
------------------------------------------------------------------------------------------------------------------------  
  is
    l_hexadecimal_number com_types.t_varchar2_max_size_plsql;
    l_decimal_number_to_iterate number := i_decimal_number;
    l_decimal_remainder pls_integer;
    l_hexadecimal_remainder varchar2(1);
  begin
    com_assert.arg_not_null(i_decimal_number, 'i_decimal_number');
    com_assert.is_true(i_include_prefix in ('Y','N'), 'Parameter i_include_prefix must have value Y or N'); 
    com_assert.is_true(i_decimal_number >= 0 and i_decimal_number = trunc(i_decimal_number), i_decimal_number||
      ' must be a positive integer.');
    
    while (l_decimal_number_to_iterate > 0 ) loop
      l_decimal_remainder := mod(l_decimal_number_to_iterate, 16);          
      l_hexadecimal_remainder :=  
        case 
          when l_decimal_remainder < 10 then chr(ascii('0') + l_decimal_remainder)
          else chr(ascii('A') + l_decimal_remainder - 10)
        end;
    
      l_hexadecimal_number := l_hexadecimal_remainder || l_hexadecimal_number;
      l_decimal_number_to_iterate := trunc(l_decimal_number_to_iterate / 16);
    end loop;
    
    return case when i_include_prefix = com_constants.c_true then '0X' end || coalesce(l_hexadecimal_number,'0');
  end decimal_to_hexadecimal;

  function hexadecimal_to_decimal (i_hexadecimal_number in varchar2) return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-27  Jose Rolland
--    Refactored and added to Commons4Oracle. OCOMM-409
------------------------------------------------------------------------------------------------------------------------
  is
    l_current_digit varchar2(1);
    l_current_digit_as_decimal pls_integer;
    l_decimal_number number := 0;
    l_hex_number_without_prefix com_types.t_varchar2_max_size_plsql;
  begin
    com_assert.arg_not_null(i_hexadecimal_number, 'i_hexadecimal_number');
    
    l_hex_number_without_prefix := replace(upper(i_hexadecimal_number),'0X','0');
    com_assert.is_true(regexp_like(l_hex_number_without_prefix, '^[[:xdigit:]]{1,}$'), i_hexadecimal_number||
      ' is not an hexadecimal number.');
    
    for i in 1..length(l_hex_number_without_prefix) loop
       l_current_digit := substr(l_hex_number_without_prefix, i, 1);
       l_current_digit_as_decimal :=
        case
          when l_current_digit >= 'A' and l_current_digit <= 'F' then 10 + ascii(l_current_digit) - ascii('A')
          else ascii(l_current_digit) - ascii('0')
        end;
      
       l_decimal_number := (l_decimal_number * 16) + l_current_digit_as_decimal;
    end loop;
    
    return l_decimal_number;
  end hexadecimal_to_decimal;
    
end com_math;