create or replace package body com_math_test
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--  	Auto-generated test package for com_math
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-30 Jose Rolland
--    Added tests OCOMM-409
--  2015-10-02 Chris
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------
is

  procedure test_linear_interpolation
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-02 Chris
--    Auto-generated test for get_linear_interpolation_for
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  number;
    i_x0                             number;
    i_y0                             number;
    i_x1                             number;
    i_y1                             number;
    i_x                              number;
    l_expected_test_result           number;

  procedure with_given_conditions
  is
  begin
    i_x0                    := 35;
    i_y0                    := 4.3313;
    i_x1                    := 64;
    i_y1                    := 4.3944;
    i_x                     := 45;
    l_expected_test_result  := 4.353;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := round(com_math.get_linear_interpolation_for(i_x0, i_y0, i_x1, i_y1, i_x), 3);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end test_linear_interpolation;

  procedure test_binary_to_decimal
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-30 jrolland
--    Auto-generated test for binary_to_decimal
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  number;
    i_binary_number                  varchar2(4000);
    l_expected_test_result           number;

  procedure with_given_conditions
  is
  begin
    i_binary_number := '111001001';
    l_expected_test_result := 457;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_math.binary_to_decimal(i_binary_number);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||
      ''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('com_test_runner_test_failed', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_binary_to_decimal;

  procedure test_decimal_to_binary
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-30 jrolland
--    Auto-generated test for decimal_to_binary
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  varchar2(4000);
    i_decimal_number                 number;
    l_expected_test_result           varchar2(4000);

  procedure with_given_conditions
  is
  begin
    i_decimal_number := 675;
    l_expected_test_result := '1010100011';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_math.decimal_to_binary(i_decimal_number);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||
      ''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('com_test_runner_test_failed', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_decimal_to_binary;

  procedure test_decimal_to_hexadecimal
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-30 jrolland
--    Auto-generated test for decimal_to_hexadecimal
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  varchar2(4000);
    i_decimal_number                 number;
    i_include_prefix                 com_types.t_boolean_as_char;
    l_expected_test_result           varchar2(4000);

  procedure with_given_conditions
  is
  begin
    i_decimal_number := 4526523;
    i_include_prefix := com_constants.c_true;
    l_expected_test_result := '0X4511BB';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_math.decimal_to_hexadecimal(i_decimal_number, i_include_prefix);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||
      ''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('com_test_runner_test_failed', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_decimal_to_hexadecimal;

  procedure test_dec_to_hex_without_prefix
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-30 jrolland
--    Auto-generated test for decimal_to_hexadecimal
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  varchar2(4000);
    i_decimal_number                 number;
    i_include_prefix                 com_types.t_boolean_as_char;
    l_expected_test_result           varchar2(4000);

  procedure with_given_conditions
  is
  begin
    i_decimal_number := 4526523;
    i_include_prefix := com_constants.c_false;
    l_expected_test_result := '4511BB';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_math.decimal_to_hexadecimal(i_decimal_number, i_include_prefix);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||
      ''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('com_test_runner_test_failed', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_dec_to_hex_without_prefix;
  
  procedure test_hexadecimal_to_decimal
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-30 jrolland
--    Auto-generated test for hexadecimal_to_decimal
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  number;
    i_hexadecimal_number             varchar2(4000);
    l_expected_test_result           number;

  procedure with_given_conditions
  is
  begin
    i_hexadecimal_number := '4F6456FDA2';
    l_expected_test_result := 340985839010;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_math.hexadecimal_to_decimal(i_hexadecimal_number);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||
      ''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('com_test_runner_test_failed', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_hexadecimal_to_decimal;
  
end com_math_test;