create or replace package com_app_session_mgr 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2010-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Stores application session information related to the current DB session and its SID 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-12  Steffen Pade
--    Modified procedure start_app_session OCOMM-455
--  2016-05-10 Eve Fortescue-Beck
--    Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-392
--  2011-09-21 Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  procedure start_app_session (
      i_client_os_user    in  com_app_sessions.client_os_user%type    default null, 
      i_client_real_user  in  com_app_sessions.client_real_user%type  default commons.com_ctx_mgr.get_username,
      i_client_host       in  com_app_sessions.client_host%type       default commons.com_ctx_mgr.get_host_name,
      i_client_app_name   in  com_app_sessions.client_app_name%type   default commons.com_ctx_mgr.get_app_name,
      i_client_app_token  in  com_app_sessions.application_token%type default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Starts an application session, which implies inserting a record into the table com_app_sessions
--    and setting the com_app_session_id in an application context.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_client_os_user
--      Optional value to specify the OS user on the client machine (which may differ from the OS 
--      user of the machine connected to the database e.g. in a 3-tier environment).
--    i_client_real_user
--      Optional value to specify the real user on the client machine (which may differ from the OS 
--      user on the client machine).
--    i_client_host
--      Optional value to specify the host name the client machine (which may differ from the host 
--      name of the machine connected to the database e.g. in a 3-tier environment).
--    i_client_app_name
--      Optional value to specify the name of the application.
--    i_client_app_token
--      Optional value to identify the client's application session for future retrieval of 
--      com_app_sessions.com_app_session_id which interlinks with other commons logging and tracing
--      mechanisms.

------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-12  Steffen Pade
--    Added parameter i_client_app_token (OCOMM-455)
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure start_and_get_app_session (
      o_app_session_id    out com_app_sessions.com_app_session_id%type,
      i_client_os_user    in  com_app_sessions.client_os_user%type    default null, 
      i_client_real_user  in  com_app_sessions.client_real_user%type  default commons.com_ctx_mgr.get_username,
      i_client_host       in  com_app_sessions.client_host%type       default commons.com_ctx_mgr.get_host_name,
      i_client_app_name   in  com_app_sessions.client_app_name%type   default commons.com_ctx_mgr.get_app_name,
      i_client_app_token  in  com_app_sessions.application_token%type default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Starts an application session, which implies inserting a record into the table com_app_sessions
--    and setting the com_app_session_id in an application context.  The com_app_session_id is also set in
--    the corresponding output parameter.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_client_os_user
--      Optional value to specify the OS user on the client machine (which may differ from the OS 
--      user of the machine connected to the database e.g. in a 3-tier environment).
--    i_client_real_user
--      Optional value to specify the real user on the client machine (which may differ from the OS 
--      user on the client machine).
--    i_client_host
--      Optional value to specify the host name the client machine (which may differ from the host 
--      name of the machine connected to the database e.g. in a 3-tier environment).
--    i_client_app_name
--      Optional value to specify the name of the application).
--    i_client_app_token
--      Optional value to identify the client's application session for future retrieval of 
--      com_app_sessions.com_app_session_id which refers to other commons logging and tracing mechanisms.
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--    o_app_session_id
--      The com_app_session_id value currently set in the relevant application context.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-12  Steffen Pade
--    Added parameter i_client_app_token (OCOMM-455)
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure end_app_session;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Ends the current application session, which implies updating a record in the table 
--    com_app_sessions which has the corresponding com_app_session_id set in an application context. The
--    update operation sets the end time of the session.
--    The application context value is also cleared. 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure set_app_session_id (
    i_app_session_id  in  com_app_sessions.com_app_session_id%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Directly sets the com_app_session_id in an application context without starting an application session.
--    The sole purpose of this procedure is to facilitate integration with COMMONS for 
--    infrastructures that have existing session tracking mechanisms.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_app_session_id
--      The id to be set in the application context, which will in turn be accessible via calls to
--      the function get_com_app_session_id().
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure clear_app_session_id;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Directly clears the com_app_session_id currently set in an application context without trying to update an 
--    application session.
--    The sole purpose of this procedure is to facilitate integration with COMMONS for infrastructures that have 
--    existing session tracking mechanisms.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_app_session_id return com_app_sessions.com_app_session_id%type;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the com_app_session_id currently set in an application context (if any) and returns it as
--    a NUMBER value.  Returns null if no value is currently set in the application context.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------  
  
end com_app_session_mgr;