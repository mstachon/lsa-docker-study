create or replace package body com_app_session_mgr 
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2010-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Author:       Chris Roderick
--  Description:
--    Provides a means to read and write information about application sessions, e.g. os user, 
--    IP address, host name, start time, end time, and the database auditing session id, and SID 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-12  Steffen Pade
--    Added parameter i_client_app_token to start_app_session and start_and_get_app_session (OCOMM-455)
--  2011-09-21 Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_app_session_id return com_app_sessions.com_app_session_id%type
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the com_app_session_id currently set in an application context (if any) and returns it as
--    a NUMBER value.  Returns null if no value is currently set in the application context.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  
  begin    
    return commons.com_ctx_mgr.get_app_session_id;    
    
  end get_app_session_id;
  
  procedure start_app_session (
      i_client_os_user    in  com_app_sessions.client_os_user%type    default null, 
      i_client_real_user  in  com_app_sessions.client_real_user%type  default commons.com_ctx_mgr.get_username,
      i_client_host       in  com_app_sessions.client_host%type       default commons.com_ctx_mgr.get_host_name,
      i_client_app_name   in  com_app_sessions.client_app_name%type   default commons.com_ctx_mgr.get_app_name,
      i_client_app_token  in  com_app_sessions.application_token%type default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Starts an application session, which implies inserting a record into the table com_app_sessions
--    and setting the com_app_session_id in an application context.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_client_os_user
--      Optional value to specify the OS user on the client machine (which may differ from the OS 
--      user of the machine connected to the database e.g. in a 3-tier environment).
--    i_client_real_user
--      Optional value to specify the real user on the client machine (which may differ from the OS 
--      user on the client machine e.g. RBAC identified user on a CCC console with lhcop OS user).
--    i_client_host
--      Optional value to specify the host name the client machine (which may differ from the host 
--      name of the machine connected to the database e.g. in a 3-tier environment).
--    i_client_app_name
--      Optional value to specify the name of the application (e.g. TIMBER, TRIM Controller, etc.).
--    i_client_app_token
--      Optional value to identify the client's application session for future retrieval of 
--      com_app_sessions.com_app_session_id which interlinks with other commons logging and tracing
--      mechanisms.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-12  Steffen Pade
--    Added parameter i_client_app_token (OCOMM-455)
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_app_session_id  com_app_sessions.com_app_session_id%type;
  begin
--  Insert a new record into the com_app_sessions table
    insert into com_app_sessions (com_app_session_id, client_os_user, client_real_user, client_host, client_app_name, application_token)
    values (com_app_sess_id_seq.nextval, i_client_os_user, i_client_real_user, i_client_host, i_client_app_name, i_client_app_token)
    returning com_app_session_id into l_app_session_id;

--  Set a new session id in the relevant COMMONS application context
    commons.com_ctx_mgr.set_app_session_id(l_app_session_id);
    
  end start_app_session;
  
  procedure start_and_get_app_session (
      o_app_session_id    out com_app_sessions.com_app_session_id%type,
      i_client_os_user    in  com_app_sessions.client_os_user%type    default null, 
      i_client_real_user  in  com_app_sessions.client_real_user%type  default commons.com_ctx_mgr.get_username,
      i_client_host       in  com_app_sessions.client_host%type       default commons.com_ctx_mgr.get_host_name,
      i_client_app_name   in  com_app_sessions.client_app_name%type   default commons.com_ctx_mgr.get_app_name,
      i_client_app_token  in  com_app_sessions.application_token%type default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Starts an application session, which implies inserting a record into the table com_app_sessions
--    and setting the com_app_session_id in an application context.  The com_app_session_id is also 
--    set in the corresponding output parameter.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_client_os_user
--      Optional value to specify the OS user on the client machine (which may differ from the OS 
--      user of the machine connected to the database e.g. in a 3-tier environment).
--    i_client_real_user
--      Optional value to specify the real user on the client machine (which may differ from the OS 
--      user on the client machine e.g. RBAC identified user on a CCC console with lhcop OS user).
--    i_client_host
--      Optional value to specify the host name the client machine (which may differ from the host 
--      name of the machine connected to the database e.g. in a 3-tier environment).
--    i_client_app_name
--      Optional value to specify the name of the application (e.g. TIMBER, TRIM Controller, etc.).
--    i_client_app_token
--      Optional value to identify the client's application session for future retrieval of 
--      com_app_sessions.com_app_session_id which interlinks with other commons logging and tracing
--      mechanisms.
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--    o_app_session_id
--      The com_app_session_id value currently set in the relevant application context.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-12  Steffen Pade
--    Added parameter i_client_app_token (OCOMM-455)
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    
  begin
    start_app_session(i_client_os_user, i_client_real_user, i_client_host, i_client_app_name, i_client_app_token);
    o_app_session_id := get_app_session_id;
    
  end start_and_get_app_session;
  
  procedure end_app_session
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Ends the current application session, which implies updating a record in the table 
--    com_app_sessions which has the corresponding com_app_session_id set in an application context.
--    The update operation sets the end time of the session.
--    The application context value is also cleared. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    <argument_name> - <argument description>
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--    <argument_name> - <argument description>
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    <error_code> - <exception description>
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  <Change date (YYYY-MM-DD) + Change Author>
--    <Change description>
------------------------------------------------------------------------------------------------------------------------
  is
    
  begin
--  Update the end time of the current session (if any) in the table com_app_sessions
    update com_app_sessions set end_utc_timestamp = sys_extract_utc(systimestamp)
    where com_app_session_id = get_app_session_id;

--  Clear the app session id set in the application context    
    commons.com_ctx_mgr.clear_app_session_id;
    
  end end_app_session;
  
  procedure set_app_session_id (
    i_app_session_id  in  com_app_sessions.com_app_session_id%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Directly sets the com_app_session_id in an application context without starting an application
--    session.
--    The sole purpose of this procedure is to facilitate integration with COMMONS for 
--    infrastructures that have existing session tracking mechanisms (e.g. CCDB usage).
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_app_session_id
--      The id to be set in the application context, which will in turn be accessible via calls to
--      the function get_app_session_id().
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    
  begin
--  Set the given session id in the relevant COMMONS application context
    commons.com_ctx_mgr.set_app_session_id(i_app_session_id);
  end set_app_session_id;
  
  procedure clear_app_session_id
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Directly clears the com_app_session_id currently set in an application context without trying 
--    to update an application session.
--    The sole purpose of this procedure is to facilitate integration with COMMONS for 
--    infrastructures that have existing session tracking mechanisms (e.g. CCDB usage).
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    
  begin
    commons.com_ctx_mgr.clear_app_session_id;
    
  end clear_app_session_id;
  
end com_app_session_mgr;