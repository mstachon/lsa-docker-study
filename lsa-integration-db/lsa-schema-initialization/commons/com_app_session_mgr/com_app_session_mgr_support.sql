--  Range partitioned table to store application session details, to be manged by the partition 
--  manager in order to add new partitions in advance of when they will be required, and drop older 
--  empty partitions
create table com_app_sessions (
  com_app_session_id        number        constraint comappsess_pk primary key,
  start_utc_timestamp       timestamp     default sys_extract_utc(systimestamp) constraint comappsess_st_nn not null,
  end_utc_timestamp         timestamp,
  db_user                   varchar2(30) default sys_context('userenv', 'session_user'),  
  direct_os_user            varchar2(30) default sys_context('userenv', 'os_user'),
  direct_host               varchar2(100) default sys_context('userenv', 'host'),
  client_info               varchar2(64) default sys_context('userenv', 'client_info'),  
  client_os_user            varchar2(30), 
  client_real_user          varchar2(30), 
  client_host               varchar2(30), 
  client_app_name           varchar2(30), 
  application_token         varchar2(128), 
  db_sid                    varchar2(30) default sys_context('userenv', 'sid'),
  db_audsid                 varchar2(30) default sys_context('userenv', 'sessionid')
) partition by range (start_utc_timestamp) (
  partition com_app_sess_20110921 values less than (timestamp '2011-09-22 00:00:00')
);

--  index the time stamps to allow rapid range searching
create index com_app_sess_st_i on com_app_sessions(start_utc_timestamp);

--  sequence to be used to generate session ids
create sequence com_app_sess_id_seq;

--  register for usage with the partition manager, and perform the initial partition management
begin
  com_partition_mgr.reg_time_range_part_table(
      i_table_name          => 'COM_APP_SESSIONS'
    , i_part_prefix         => 'COM_APP_SESS'
    , i_no_parts_advance    => 5
    , i_part_range_int_cnt  => 1
    , i_part_range_int_type => 'DAY'
  );
  com_partition_mgr.manage_partitions('COM_APP_SESSIONS');
end;
/

--  Some tests of the various modules
begin
  com_app_session_mgr.start_app_session();
end;
/
select * from com_app_sessions;
commit;

select com_app_session_mgr.get_app_session_id from dual;

begin
  com_app_session_mgr.end_app_session;
end;
/

select * from com_app_sessions;
commit;

select com_app_session_mgr.get_app_session_id from dual;

begin
  com_app_session_mgr.set_app_session_id(999);
end;
/

select com_app_session_mgr.get_app_session_id from dual;

begin
  com_app_session_mgr.clear_app_session_id;
end;
/

select com_app_session_mgr.get_app_session_id from dual;

DECLARE
  l_app_session_id  com_app_sessions.com_app_session_id%type;
begin
  com_app_session_mgr.start_and_get_app_session(l_app_session_id);
  dbms_output.put_line('Session Id: '||l_app_session_id);
end;
/

begin
  com_big_brother.set_client_app_info(
    i_username    =>  'Chris', 
    i_app_name    =>  'SQL Developer', 
    i_host_name   =>  'VPC', 
    i_ip_address  =>  null,
    i_curr_action =>  'Commons Test'
  );
  
  com_app_session_mgr.start_app_session();
end;
/

select * from user_objects where status = 'INVALID';
begin compile_invalid_objects; end;
/