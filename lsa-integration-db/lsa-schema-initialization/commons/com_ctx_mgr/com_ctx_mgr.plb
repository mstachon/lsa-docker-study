create or replace package body com_ctx_mgr 
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Author:       Chris Roderick
--  Description:
--    This package is intended to manage various Commons application contexts which are system wide.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-17  Gkergki Strakosa
--    Added a wrapper on top of dbms_session.set_context/clear_context
--  2011-09-22  Chris Roderick
--    Added procedure clear_app_session_id
--  2011-03-15  Eve Fortescue-Beck, Chris Roderick
--    Extended to manage values for history context keys
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

--  The name of the context used to store Commons session data
  c_commons_ctx                   constant varchar2(30) := 'COMMONS_CTX';

--  Big Brother Context Keys  
  c_bb_user_ctx_key               constant varchar2(30) := 'CLIENT_USERNAME';
  c_bb_app_ctx_key                constant varchar2(30) := 'CLIENT_APP_NAME';
  c_bb_host_ctx_key               constant varchar2(30) := 'CLIENT_HOSTNAME';
  c_bb_ip_ctx_key                 constant varchar2(30) := 'CLIENT_IP';
  
--  History context keys
  c_hist_dml_stmp_ctx_key         constant varchar2(30) := 'DML_STAMP';
  c_hist_dml_stmp_format_ctx_key  constant varchar2(30) := 'DML_STAMP_FORMAT';

--  Application session keys
  c_app_session_id_ctx_key        constant varchar2(30) := 'APP_SESSION_ID';
  
  procedure set_context(
    i_context_name  in  varchar2,
    i_attribute     in  varchar2,
    i_value         in  varchar2,
    i_username      in  varchar2,
    i_client_id     in  varchar2
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Wrapper on top of dbms_session.set_context so that the context can be set from other procedures.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_context_name
--      The namespace of the application context to be set
--    i_attribute
--      The attribute of the application context to be set
--    i_value
--      The value of the application context to be set
--    i_username
--      The database username attribute of the application context.
--    i_client_id
--      The application-specific client_id attribute of the application context
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-17  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    dbms_session.set_context (i_context_name, i_attribute, i_value, i_username, i_client_id);  
  end set_context;
  
  procedure clear_context(
    i_context_name  in  varchar2,
    i_client_id     in  varchar2,
    i_attribute     in  varchar2
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Wrapper on top of dbms_session.clear_context so that the context can be cleared from other procedures.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_context_name
--      The namespace of the application context to be set
--    i_client_id
--      The application-specific client_id attribute of the application context
--    i_attribute
--      The attribute of the application context to be set
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-17  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    dbms_session.clear_context(i_context_name, i_client_id, i_attribute);
  end clear_context;
  
  procedure set_client_app_info (
    i_username    in varchar2, 
    i_app_name    in varchar2, 
    i_host_name   in varchar2, 
    i_ip_address  in varchar2
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets the Commons context to hold the given fields.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_username
--      The real username of the end user of the database, which may be different than the OS user,
--      especially in a multi-tier environment.
--    i_app_name
--      The name of the application which the end user is using in order to interact with the database.
--    i_host_name
--      The name of the host on which the end user is running the application, which may be different
--      to the name of the host connected directly to the database in a multi-tier environment.
--    i_ip_address
--      The IP address of the host on which the end user is running the application, which may be 
--      different to the name of the host connected directly to the database in a multi-tier 
--      environment.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is

  begin
  
--  Set the c_bb_ctx context information
    dbms_session.set_context(c_commons_ctx, c_bb_user_ctx_key, i_username);
    dbms_session.set_context(c_commons_ctx, c_bb_app_ctx_key, i_app_name);
    dbms_session.set_context(c_commons_ctx, c_bb_host_ctx_key, i_host_name);
    dbms_session.set_context(c_commons_ctx, c_bb_ip_ctx_key, i_ip_address);
    
  end set_client_app_info;

  procedure clear_client_app_info
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Clears any application info from the Commons context for this session.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is

  begin

--  Clear the client application info from the Commons context information
    dbms_session.set_context(c_commons_ctx, c_bb_user_ctx_key, null);
    dbms_session.set_context(c_commons_ctx, c_bb_app_ctx_key, null);
    dbms_session.set_context(c_commons_ctx, c_bb_host_ctx_key, null);
    dbms_session.set_context(c_commons_ctx, c_bb_ip_ctx_key, null);

  end clear_client_app_info;
  
  
  
  function get_username 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the value of the username which is currently set in the Commons context 
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is

  begin
    return sys_context(c_commons_ctx, c_bb_user_ctx_key);
  end get_username;
  
  function get_app_name 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the value of the app_name which is currently set in the Commons context  
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is

  begin
    return sys_context(c_commons_ctx, c_bb_app_ctx_key);
  end get_app_name;
  
  function get_host_name
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the value of the host_name which is currently set in the Commons context  
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is

  begin
    return sys_context(c_commons_ctx, c_bb_host_ctx_key);
  end get_host_name;
  
  function get_ip_address 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the value of the IP address which is currently set in the Commons context  
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-14  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is

  begin
    return sys_context(c_commons_ctx, c_bb_ip_ctx_key);
  end get_ip_address;
  
  procedure set_dml_time(
    i_dml_stamp     in timestamp,
    i_stamp_format  in varchar2 default 'YYYY-MM-DD HH24:MI:SS.FF3'
  ) is
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets a timestamp associated with a DML Time in the Commons context (for use by the com_history_mgr).
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_dml_stamp
--       The UTC timestamp to be set.
--    i_stamp_format
--      The format to be applied to the timestamp when storing it in the Commons context.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-15  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin
  
    dbms_session.set_context(c_commons_ctx, c_hist_dml_stmp_ctx_key, to_char(i_dml_stamp, i_stamp_format));
    dbms_session.set_context(c_commons_ctx, c_hist_dml_stmp_format_ctx_key, i_stamp_format);

  end set_dml_time;
  
  function get_dml_time
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the timestamp associated with a DML Time which is currently set in the Commons context 
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-15  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------     
  is

  begin
    return to_timestamp(sys_context(c_commons_ctx, c_hist_dml_stmp_ctx_key), sys_context(c_commons_ctx, c_hist_dml_stmp_format_ctx_key));
  end get_dml_time;


  procedure set_app_session_id (
    i_app_session_id  in  number
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets the given id as the application session id in the Commons context within the session.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_app_session_id
--      A unique identifier to trace this application session, typically a value from a sequence.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-08  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is

  begin
    dbms_session.set_context(c_commons_ctx, c_app_session_id_ctx_key, to_char(i_app_session_id));
  end set_app_session_id;

  function get_app_session_id
  return number
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the id of the application session which is currently set in the Commons context 
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-08  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is

  begin
    return to_number(sys_context(c_commons_ctx, c_app_session_id_ctx_key));
  end get_app_session_id;

  procedure clear_app_session_id
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Clears the app_session_id currently set in an application context.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-09-22  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is

  begin
    dbms_session.set_context(c_commons_ctx, c_app_session_id_ctx_key, null);
  end clear_app_session_id;  
  
end com_ctx_mgr;
/
