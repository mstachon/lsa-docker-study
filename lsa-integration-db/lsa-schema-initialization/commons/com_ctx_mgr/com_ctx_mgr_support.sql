--  Create a context for storing Commons related information
--  REMEMBER, there context names are unique per database, and can only be managed by one package
--  This means that it is necessary to grant execute to public and possibly create a public synonym  
--  for the com_ctx_mgr package.
create or replace context commons_ctx using commons.com_ctx_mgr;
/

-- Grant execute to public on com_ctx_mgr package
grant execute on com_ctx_mgr to public;

--  Option to create a public synonyms for the commons.com_ctx_mgr requires additional privileges
--create or replace public synonym com_ctx_mgr for commons.com_ctx_mgr;