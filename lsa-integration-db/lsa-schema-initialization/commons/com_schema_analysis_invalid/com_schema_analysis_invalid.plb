create or replace package body com_schema_analysis_invalid
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to invalid objects.
--  Scope:
--    Operates on the database level, but depends on individual schema analysis rule implementations.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-03  Chris Roderick and Vasilis Giannoutsos
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function analyse_invalid_objects (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-03  Chris Roderick and Vasilis Giannoutsos
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin
      
    select schema_analysis_rule_result(owner, object_name, object_type, 'Object is INVALID consider to Compile, Re-factor, or Remove', 1)
    bulk collect into l_result
    from all_objects
    where object_type like i_obj_type_like
    and owner like i_obj_owner_like
    and object_name like i_obj_name_like
    and status = 'INVALID';
    
    return l_result;
    
  end analyse_invalid_objects;  

end com_schema_analysis_invalid;