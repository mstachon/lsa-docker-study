create or replace package com_collection_utils
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
-- Description:
--  This package is intended to manage data collections 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-12 Jose Rolland
--   Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-400
--  2016-05-03  Pascal Le Roux
--    added a new get_vc_vc_map with 2 collections as input
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    added functions to_table_of_number (OCOMM-346) and to_string (OCOMM-349)
--  2015-06-30 Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  function key_val_varchars_to_vc_vc_map(i_keys in table_of_varchar, i_values in table_of_varchar )
  return vc_vc_map;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns table of keys and value pairs passing as parameters 2 collections of table_of_varchar
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_keys    - table_of_varchar providing keys
--    i_values  - table_of_varchar providing matching values
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INTERNAL_ERROR :  when the collections are not of the same size
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-12 Jose Rolland
--   Changed name from get_vc_vc_map to key_val_varchars_to_vc_vc_map OCOMM-REVIEWS-10 - OCOMM-400
--  2016-05-03  Pascal Le Roux
--    Creation - OCOMM-434
------------------------------------------------------------------------------------------------------------------------

  function cursor_to_vc_vc_map (i_cu_key_values in sys_refcursor)
  return vc_vc_map;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns table of keys and value pairs
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_cu_filt_key_values  - ref cursor providing column(key) value pairs
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-12 Jose Rolland
--   Changed name from get_vc_vc_map to cursor_to_vc_vc_map OCOMM-REVIEWS-10 - OCOMM-400
--  2015-06-30  Nikolay Tsvetkov
--    Creation - OCOMM-310
------------------------------------------------------------------------------------------------------------------------

  function varchars_to_numbers (i_table_of_varchar in table_of_varchar)
  return table_of_number;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the given table_of_varchar to a table_of_number
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_of_varchar - The i_table_of_varchar to convert
------------------------------------------------------------------------------------------------------------------------
--  Returns:
--    return a table_of_number
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ILLEGAL_ARGUMENT - when any of the elements of the given table_of_varchar cannot be converted to a number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-12 Jose Rolland
--   Changed name from to_table_of_number to varchars_to_numbers OCOMM-REVIEWS-10 - OCOMM-400
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    creation - OCOMM-346
------------------------------------------------------------------------------------------------------------------------

  function varchars_to_string (
    i_table_of_varchar  in  table_of_varchar,
    i_delimiter         in  varchar2 default ','
  ) 
  return varchar2; 
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the given table_of_varchar to a string delimited by the given delimiter
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_of_varchar - The i_table_of_varchar to convert
--    i_delimiter - the delimiter to be used
------------------------------------------------------------------------------------------------------------------------
--  Returns:
--    return a delimited string
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    creation - OCOMM-349
------------------------------------------------------------------------------------------------------------------------

  function numbers_to_string (
    i_table_of_number  in  table_of_number,
    i_delimiter         in  varchar2 default ','
  ) 
  return varchar2; 
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the given table_of_number to a string delimited by the given delimiter
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_of_number - The i_table_of_number to convert
--    i_delimiter - the delimiter to be used
------------------------------------------------------------------------------------------------------------------------
--  Returns:
--    return a delimited string
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    creation - OCOMM-349
------------------------------------------------------------------------------------------------------------------------

  function timestamps_to_string (
    i_table_of_timestamp  in  table_of_timestamp,
    i_delimiter           in  varchar2 default ',',
    i_timestamp_format    in  varchar2  default com_constants.c_iso_timestamp_format_ff
  ) 
  return varchar2; 
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the given table_of_timestamp to a string delimited by the given delimiter
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_of_timestamp - The i_table_of_timestamp to convert
--    i_delimiter - the delimiter to be used
--    i_timestamp_format - the timestamp format to be used for conversion
------------------------------------------------------------------------------------------------------------------------
--  Returns:
--    return a delimited string
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    creation - OCOMM-349
------------------------------------------------------------------------------------------------------------------------

end com_collection_utils;