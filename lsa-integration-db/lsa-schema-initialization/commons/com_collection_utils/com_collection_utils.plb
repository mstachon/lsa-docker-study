create or replace package body com_collection_utils
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2017 copyright
------------------------------------------------------------------------------------------------------------------------
-- Description:
--  This package is intended to manage data collections 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Modified procedures key_val_varchars_to_vc_vc_map and cursor_to_vc_vc_map OCOMM-443
--  2016-07-12 Jose Rolland
--   Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-400
--  2016-05-03  Pascal Le Roux
--    added a new get_vc_vc_map with 2 collections as input
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    added functions to_table_of_number (OCOMM-346) and X_to_string (OCOMM-349)
--  2015-06-30 Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  function key_val_varchars_to_vc_vc_map(i_keys in table_of_varchar, i_values in table_of_varchar )
-- DEPRECATED - should just use constructor vc_vc_map(i_keys, i_values)
  return vc_vc_map
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    adapted to changes in vc_vc_map definition OCOMM-443
--  2016-07-12 Jose Rolland
--   Changed name from get_vc_vc_map to key_val_varchars_to_vc_vc_map OCOMM-REVIEWS-10 - OCOMM-400
--  2016-05-03  Pascal Le Roux
--    Creation - OCOMM-434
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return vc_vc_map(i_keys, i_values);
  end key_val_varchars_to_vc_vc_map;

  function cursor_to_vc_vc_map (i_cu_key_values in sys_refcursor)
  return vc_vc_map
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    adapted to changes in vc_vc_map definition OCOMM-443
--  2016-07-12 Jose Rolland
--   Changed name from get_vc_vc_map to cursor_to_vc_vc_map OCOMM-REVIEWS-10 - OCOMM-400
--  2015-06-30  Nikolay Tsvetkov
--    Creation - OCOMM-310
------------------------------------------------------------------------------------------------------------------------
  is
    l_keys    table_of_varchar;
    l_values  table_of_varchar;
  begin
    select vc_key, vc_value bulk collect into l_keys, l_values
    from(
      select tt.column_value.getrootelement() as vc_key,
            extractvalue (tt.column_value, 'node()') as vc_value
       from table (xmlsequence (i_cu_key_values)) t,
            table (xmlsequence (extract (column_value, '/ROW/node()'))) tt);
            
    return vc_vc_map(l_keys, l_values);
  end cursor_to_vc_vc_map;
  
  function varchars_to_numbers (i_table_of_varchar in table_of_varchar)
  return table_of_number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-12 Jose Rolland
--   Changed name from to_table_of_number to varchars_to_numbers OCOMM-REVIEWS-10 - OCOMM-400
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    creation - OCOMM-346
------------------------------------------------------------------------------------------------------------------------
  is
    l_table_of_number table_of_number;
  begin
    com_assert.not_null(i_table_of_varchar, 'Cannot convert null table_of_varchar to table_of_number');
    
    l_table_of_number := table_of_number();  
    l_table_of_number.extend(i_table_of_varchar.count);
  
    for r_varchar_value in 1 .. i_table_of_varchar.count
    loop      
      begin
        l_table_of_number(r_varchar_value) := to_number(i_table_of_varchar(r_varchar_value));
      exception
        when others then
          com_event_mgr.raise_error_event('ILLEGAL_ARGUMENT', 'Cannot convert '||i_table_of_varchar(r_varchar_value)||
            ' to number, Err: '||dbms_utility.format_error_stack);
      end;
    end loop;
    
    return l_table_of_number;
  end varchars_to_numbers;  
  
  function varchars_to_string (
    i_table_of_varchar  in  table_of_varchar,
    i_delimiter         in  varchar2 default ','
  ) 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    creation - OCOMM-349
------------------------------------------------------------------------------------------------------------------------
  is
  begin  
    return com_strings(i_table_of_varchar).to_string(i_delimiter);  
  end varchars_to_string;
  
  function numbers_to_string (
    i_table_of_number   in  table_of_number,
    i_delimiter         in  varchar2 default ','
  ) 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    creation - OCOMM-349
------------------------------------------------------------------------------------------------------------------------
  is
  begin  
    return com_numbers(i_table_of_number).to_string(i_delimiter);
  end numbers_to_string;
  
  function timestamps_to_string (
    i_table_of_timestamp  in  table_of_timestamp,
    i_delimiter           in  varchar2  default ',',
    i_timestamp_format    in  varchar2  default com_constants.c_iso_timestamp_format_ff
  ) 
  return varchar2 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    creation - OCOMM-349
------------------------------------------------------------------------------------------------------------------------  
  is
    l_delimited_string   varchar2(32767);
  begin  
    select listagg(to_char(column_value, i_timestamp_format), i_delimiter) within group (order by rownum) into l_delimited_string
    from table(i_table_of_timestamp);
    
    return l_delimited_string;
  
  end timestamps_to_string;
  
end com_collection_utils;