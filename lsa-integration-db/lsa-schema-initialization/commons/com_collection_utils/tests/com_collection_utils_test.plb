create or replace package body com_collection_utils_test
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--  	Auto-generated test package for com_collection_utils
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Modified procedure test_cursor_to_vc_vc_map OCOMM-443
--  2015-09-29 Chris
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------
is

  procedure test_cursor_to_vc_vc_map
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    adapted to changes in vc_vc_map definition OCOMM-443
--  2015-09-29 Chris
--    Auto-generated test for get_vc_vc_map
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  vc_vc_map;
    i_cu_key_values                  sys_refcursor;
    l_expected_test_result           vc_vc_map;

  procedure with_given_conditions
  is
  begin
    open i_cu_key_values for 'select 1 as COL1, 2 as COL2, 3 as COL3 from dual';
      l_expected_test_result  :=  vc_vc_map();
      l_expected_test_result.put('COL1', '1');
      l_expected_test_result.put('COL2', '2');
      l_expected_test_result.put('COL3', '3');
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_collection_utils.cursor_to_vc_vc_map (i_cu_key_values);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'expected and actual vc_vc_map are not equal!');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    com_util.close_cursor_if_open(i_cu_key_values);
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end test_cursor_to_vc_vc_map;

  procedure test_numbers_to_string
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris
--    Auto-generated test for numbers_to_string
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  varchar2(32767);
    i_table_of_number                table_of_number;
    i_delimiter                      varchar2(1);
    l_expected_test_result           varchar2(32767); 

  procedure with_given_conditions
  is
  begin
    l_expected_test_result  :=  '1,2,3';
    i_table_of_number       :=  table_of_number(1,2,3);
    i_delimiter             :=  ',';
  end with_given_conditions;
  
  procedure when_executing
  is
  begin
    function_result := com_collection_utils.numbers_to_string (i_table_of_number, i_delimiter);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end test_numbers_to_string;

  procedure test_timestamps_to_string
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris
--    Auto-generated test for timestamps_to_string
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  varchar2(32767);
    i_table_of_timestamp             table_of_timestamp;
    i_delimiter                      varchar2(1);
    i_timestamp_format               varchar2(30);
    l_expected_test_result           varchar2(32767); 

  procedure with_given_conditions
  is
  begin
    l_expected_test_result  := '2015-01-01 00:00:00.000,2015-01-01 01:01:01.000';
    i_table_of_timestamp    :=  table_of_timestamp (timestamp '2015-01-01 00:00:00', timestamp '2015-01-01 01:01:01');
    i_delimiter             :=  ',';
    i_timestamp_format      :=  'YYYY-MM-DD HH24:MI:SS.ff3';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_collection_utils.timestamps_to_string (i_table_of_timestamp, i_delimiter, i_timestamp_format);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end test_timestamps_to_string;

  procedure varchars_to_nums_should_fail
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  table_of_number;
    i_table_of_varchar               table_of_varchar;

  procedure with_given_conditions
  is
  begin
    i_table_of_varchar      :=  table_of_varchar('A','B','C');
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_collection_utils.varchars_to_numbers (i_table_of_varchar);
  end when_executing;

  procedure should_result_in
  is
  begin
    null;
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_assert.is_true(com_event_mgr.event_equals('ILLEGAL_ARGUMENT', i_error_code));
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end varchars_to_nums_should_fail;
  
  procedure varchars_to_numbers_empty_tab
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-23 Chris
--    Creation in response to comment from Marcin on OCOMM-346
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  table_of_number;
    i_empty_table_of_varchar         table_of_varchar;

  procedure with_given_conditions
  is
  begin
    i_empty_table_of_varchar      :=  table_of_varchar();
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_collection_utils.varchars_to_numbers (i_empty_table_of_varchar);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.not_null(function_result, 'Expected to receive a non null table_of_number when given an empty table_of_varchar');
    com_assert.is_empty(function_result, 'Expected to receive a non null, but empty table_of_number');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end varchars_to_numbers_empty_tab;
  
  procedure test_varchars_to_numbers
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris
--    Auto-generated test for varchars_to_numbers
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  table_of_number;
    i_table_of_varchar               table_of_varchar;
    l_expected_test_result           table_of_number; 

  procedure with_given_conditions
  is
  begin
    l_expected_test_result  :=  table_of_number(1,2,3);
    i_table_of_varchar      :=  table_of_varchar('1','2','3');
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result         := com_collection_utils.varchars_to_numbers (i_table_of_varchar);
  end when_executing;

  procedure should_result_in
  is
  begin
--  This does not check the order!
    com_assert.is_true(function_result = l_expected_test_result, 'Expected result does not match actual result');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end test_varchars_to_numbers;

  procedure test_varchars_to_string
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-29 Chris
--    Auto-generated test for varchars_to_string
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  varchar2(32767);
    i_table_of_varchar               table_of_varchar;
    i_delimiter                      varchar2(1);
    l_expected_test_result           varchar2(32767); 

  procedure with_given_conditions
  is
  begin
    l_expected_test_result  :=  'A,B,C';
    i_table_of_varchar      :=  table_of_varchar('A','B','C');
    i_delimiter             :=  ',';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_collection_utils.varchars_to_string (i_table_of_varchar, i_delimiter);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end test_varchars_to_string;

end com_collection_utils_test;

