create or replace package body com_object_mgr
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to compile invalid objects and notify of problematic objects. 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-11-24 Jose Rolland
--   Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-411
--  2016-02-12 Jose Rolland
--    Modified check_objects OCOMM-376
--  2015-11-17 Jose Rolland
--    Refactored check_objects and package format, removed get_invalid_objects procedure OCOMM-301
--  2014-07-23  Chris Roderick
--    Bug fix in repair_objects
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Removed schema_analysis, and related procedures - now in com_schema_analyser as per OCOMM-277
--  2010-10-01  Zereyakob Makonnen, Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
is
	procedure check_objects
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This module is based on JIRA issue OCOMM-1.
--    Check and notify administrators in case of INVALID objects,
--    UNUSABLE indexes, modified objects (with respect to the last checked time).
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Not known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-12 Jose Rolland
--    Modified has_invalid_objects, to count MVs that are invalid only due to compilation error OCOMM-376
--  2015-11-17 Jose Rolland
--    Refactored OCCOM-301
--  2010-10-01  Zereyakob Makonnen
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    c_invalid_object_notif  constant  com_event_definitions.event_name%type := 'INVALID_OBJECTS_REPORT';
    
    procedure compile_objects
    is
    begin
      dbms_utility.compile_schema(sys_context('userenv','current_schema'),compile_all=>false); 
    end compile_objects;
  
    function has_invalid_objects
    return boolean	
    is
      l_invalid_objects_cnt pls_integer;
    begin
      select count(*) into l_invalid_objects_cnt 
      from user_objects ao
      where ao.status = 'INVALID'
      and (
        ao.object_type != 'MATERIALIZED VIEW' 
        or 
        exists (
          select null 
          from user_mviews umv
          where umv.mview_name = ao.object_name
          and umv.compile_state = 'COMPILATION_ERROR'
        )
      );     
      
      return l_invalid_objects_cnt > 0;
    end has_invalid_objects;
    
    procedure notify_invalid_objects
    is
      l_message                    clob;
      cu_list_of_invalid_objects   sys_refcursor;
    begin
      open cu_list_of_invalid_objects for
        select ao.object_name, ao.object_type
        from user_objects ao
        where ao.status = 'INVALID'
        and (
          ao.object_type != 'MATERIALIZED VIEW' 
          or exists (
            select null 
            from user_mviews umv
            where umv.mview_name = ao.object_name
            and umv.compile_state = 'COMPILATION_ERROR'
          )
        )
        order by ao.object_name;
      l_message := com_formatter.ref_cursor_to_html_table(cu_list_of_invalid_objects, com_constants.c_yes); 
      close cu_list_of_invalid_objects;
      
      l_message := '<html>
      <head><title>List of invalid objects in '||user||'</title></head>
      <style>
      table {border: 1px solid #2854a1;border-collapse:collapse; background:#ffffff;padding:5px;}
      th {border: 1px solid #2854a1;background-color: #2854a1;color: #ffffff;font-family: Arial, Helvetica, Verdana, '
      ||'Lucida, Sans-Serif;font-size: 0.8em;font-weight: bold;text-align: center;}
      td {border: 1px solid #2854a1;background-color: #ffffff;font-family: Arial, Helvetica, Verdana, Lucida, '
      ||'Sans-Serif;font-size: 0.8em;text-align: center;}
      </style>
      <body style="font-family:Arial, Helvetica, Verdana, Lucida, Sans-Serif;font-size: 0.8em;">'
      || '<p>List of invalid objects in '||user||' that could not be re-compiled:</p>'                               
      || l_message
      || '</body></html>';
               
      com_event_mgr.raise_event(c_invalid_object_notif, l_message);
    end notify_invalid_objects;
	begin
    compile_objects;
    if has_invalid_objects then
      notify_invalid_objects;
    end if;
  exception
    when others then
      com_event_mgr.raise_error_event('INTERNAL_ERROR', dbms_utility.format_error_stack);  
	end check_objects;
  
end com_object_mgr;