--------------------------------------------------------------------------------
--Test missing PK on table
--------------------------------------------------------------------------------

create table test_com_obj_mgr_table2 
   (	column1 varchar2(20 byte) not null , 
	    column2 number not null ,
      column4 varchar2(20 byte) not null, 
	    constraint test_com_obj_mgr_tab2_pk primary key (column2, column1)
   );  
create table test_com_obj_mgr_table1 
   (	column1 varchar2(20 byte) not null , 
	    column2 number not null ,
      column3 varchar2(2000 byte),
      column4 varchar2(20 byte) not null
   );
create table test_com_obj_mgr_table3 
   (	column1 varchar2(20 byte) not null , 
	    column2 number not null ,
      column3 varchar2(2000 byte)
   );   
   
--grant all on test_com_obj_mgr_table2 to commons;
--grant all on test_com_obj_mgr_table1 to commons;

set serveroutput on
/
--------------------------------------------------------------------------------
-- Run the test
--------------------------------------------------------------------------------
declare
  
  l_result_cnt number;
    
begin
  
  com_object_mgr.schema_analysis();

-- MISSING_PRIMARY_KEY_CHECK

  select count(*) into l_result_cnt
  from com_schema_analysis_results
  where object_name = upper('test_com_obj_mgr_table1')
  and analysis_type_id in (select analysis_type_id
                           from com_schema_analysis_types
                           where analysis_type_name='MISSING_PRIMARY_KEY_CHECK');
-- clean table
--  delete from commons.com_schema_analysis_results
--  where object_name in (upper('test_com_obj_mgr_table1'),upper('test_com_obj_mgr_table2'));
  commit;

  if l_result_cnt = 1 then 
    dbms_output.put_line('TEST_MISSING_PRIMARY_KEY passed');
  else
    dbms_output.put_line('TEST_MISSING_PRIMARY_KEY failed!!');
  end if;
  
end;
/
--------------------------------------------------------------------------------
--Test missing FK on table
--------------------------------------------------------------------------------

alter table test_com_obj_mgr_table1
add constraint test_com_obj_mgr_tab1_pk primary key (column1,column2);

alter table test_com_obj_mgr_table1
add constraint test_tab1_tab2_fk1 foreign key (column1,column2)
	  references test_com_obj_mgr_table2 (column1,column2);

--alter table test_com_obj_mgr_table1 drop constraint test_tab2_uk;
--create unique index test_tab1_uk on test_com_obj_mgr_table1 (column4);
--create unique index test_tab2_uk on test_com_obj_mgr_table2 (column4);
alter table test_com_obj_mgr_table2 add  constraint test_tab2_uk unique (column4);

alter table test_com_obj_mgr_table1
add constraint test_tab1_tab2_fk2 foreign key (column4)
	  references test_com_obj_mgr_table2 (column4);

/

--------------------------------------------------------------------------------
-- Run the test
--------------------------------------------------------------------------------
declare
  
  l_result_cnt number;
    
begin
  
  commons.com_object_mgr.schema_analysis();

-- MISSING_FK_INDEX_CHECK

  select count(*) into l_result_cnt
  from commons.com_schema_analysis_results
  where object_name = upper('test_com_obj_mgr_table1')
  and analysis_type_id in (select analysis_type_id
                           from commons.com_schema_analysis_types
                           where analysis_type_name='MISSING_FK_INDEX_CHECK');
   
  if l_result_cnt = 1 then 
    dbms_output.put_line('TEST_MISSING_FK_INDEX_CHECK passed');
  else
    dbms_output.put_line('TEST_MISSING_FK_INDEX_CHECK failed!!');
  end if;

-- clean table
--  delete from commons.com_schema_analysis_results
--  where object_name in (upper('test_com_obj_mgr_table1'),upper('test_com_obj_mgr_table2'));
  commit;
end;
/
--------------------------------------------------------------------------------
--Test invalid view
--------------------------------------------------------------------------------
create view test_com_obj_invalid_view as
  select column1,column2,column3
  from test_com_obj_mgr_table1;
  
-- COMMONS grants
-- grant all on test_com_obj_invalid_view to commons;

alter table test_com_obj_mgr_table1
drop column column3;

/
--------------------------------------------------------------------------------
-- Run the test
--------------------------------------------------------------------------------
declare
  
  l_result_cnt number;
    
begin
  
  commons.com_object_mgr.schema_analysis();

-- INVALID_VIEW_CHECK

  select count(*) into l_result_cnt
  from commons.com_schema_analysis_results
  where object_name = upper('test_com_obj_invalid_view')
  and analysis_type_id in (select analysis_type_id
                           from commons.com_schema_analysis_types
                           where analysis_type_name='INVALID_VIEW_CHECK');
   
  if l_result_cnt = 1 then 
    dbms_output.put_line('TEST_INVALID_VIEW_CHECK passed');
  else
    dbms_output.put_line('TEST_INVALID_VIEW_CHECK failed!!');
  end if;

-- clean table
--  delete from commons.com_schema_analysis_results
--  where object_name in (upper('test_com_obj_mgr_table1')
--                       ,upper('test_com_obj_mgr_table2')
--                       ,upper('test_com_obj_invalid_view'));
  commit;
end;
/
--------------------------------------------------------------------------------
-- Test Check Object
--------------------------------------------------------------------------------

-- registration notification events
declare
  l_recipient_id   commons.com_recipients.recipient_id%type;
  l_event_id       commons.com_event_definitions.event_id%type;
begin

  begin
  <<add_recipient>>
  com_notifier.register_recipient('Zere','Zereyakob.Makonnen@cern.ch');
  
  exception
    when others then
--  TODO pending function com_event_mgr.event_equals   
--      if com_event_mgr.event_equals('RECIPIENT_EXISTS', sqlcode) then 
--        null;
--      else 
--        raise;
--      end if;
      null;
  
  end add_recipient;
  
  select recipient_id into l_recipient_id 
  from com_recipients
  where RECIPIENT_NAME = 'Zere'
    and RECIPIENT_EMAIL = 'Zereyakob.Makonnen@cern.ch';
    
  --commons.com_event_mgr.define_event(com_object_mgr.c_invalid_object_notif,com_logger.c_info,'Invalid objects report',l_event_id);
  begin
  com_event_mgr.register_event_notification('INVALID_OBJECTS_REPORT',l_recipient_id,'N');
  exception
    when others then
    --RECIPIENT_OF_EVENT_EXISTS
    null;
  end;
  
  commit;
  
end;
/
--------------------------------------------------------------------------------
-- Run the test
--------------------------------------------------------------------------------
begin
  commons.com_object_mgr.check_objects;
  -- Zere should receive an E-mail with the invalid objects
  commit;
end;
/
--------------------------------------------------------------------------------
-- Clean up Test Environment
--------------------------------------------------------------------------------

drop table test_com_obj_mgr_table1;
drop table test_com_obj_mgr_table3;
drop table test_com_obj_mgr_table2;
drop view  test_com_obj_invalid_view;

--  clean table com_schema_analysis_results
delete from commons.com_schema_analysis_results
  where object_name in (upper('test_com_obj_mgr_table1'),upper('test_com_obj_mgr_table3'),upper('test_com_obj_mgr_table2'),upper('test_com_obj_invalid_view'));