create or replace package com_object_mgr
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to compile invalid objects and notify of problematic objects. 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-11-24 Jose Rolland
--   Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-411
--  2015-11-17 Jose Rolland
--    Package format OCCOM-301
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Removed schema_analysis - now in com_schema_analyser as per OCOMM-277
--  2010-09-28  Zereyakob Makonnen, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  procedure check_objects;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Check and notify administrators in case of invalid objects that cannot be re-compiled.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Not known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-10-01  Zereyakob Makonnen
--    Creation
------------------------------------------------------------------------------------------------------------------------
end com_object_mgr;