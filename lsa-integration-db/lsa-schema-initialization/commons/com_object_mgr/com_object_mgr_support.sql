-- -----------------------------------------------------------------------------
-- Description:
--    This module is based on JIRA issue OCOMM-1.
--    It is intended to create the objects to support the com_object_mgr package.
-- Dependencies:
--      packages header : com_event_mgr
-- -----------------------------------------------------------------------------
-- Change Log (descending):
-- 2010-10-01 Zereyakob Makonnen
-- creation
-- -----------------------------------------------------------------------------

-- sequence
create sequence com_schema_analysis_type_seq;
/
-- tables
create table com_schema_analysis_types(
  analysis_type_id            number  constraint comschemanalyt_pk primary key
  ,analysis_type_name         varchar2(30)
  ,analysis_type_description  varchar2(70)
);

comment on table com_schema_analysis_types is 'Defines the different types of schema analyses that can be performed by the com_object_mgr package';

create table com_schema_analysis_results(
  owner                 varchar2(30)
  ,analysis_time_utc    timestamp(3)  default sys_extract_utc(systimestamp)
  ,object_name          varchar2(30)
  ,object_type          varchar2(30)
  ,analysis_type_id     number        constraint comschemanaly_analt_fk references com_schema_analysis_types (analysis_type_id)
  ,analysis_result      varchar2(4000)
  , constraint comschemanaly_pk primary key (owner, analysis_time_utc, object_name, object_type, analysis_type_id)
);

comment on table com_schema_analysis_results is 'Stores the results of the schema analyis which have been performed';

-- Index the FK
create index comschemanaly_analt_fk on com_schema_analysis_results(analysis_type_id);
/

-- -----------------------------------------------------------------------------
-- Description:
--   To compile this second part the common object manager header is needed.
--   It is intended to create the objects to support the com_object_mgr package.
-- Dependencies:
--      packages header: com_event_mgr
--                       com_object_mgr
-- -----------------------------------------------------------------------------

-- Examples of possible analysis_description values
--'UN_INDEXED_FK for FK <fk_name> on table <table_name> for columns <column_list>'
--'UN_INDEXED_FK repair option - create index <fk_name> on table <table_name> (<column_list>)'


-- data entry for com_schema_analysis_types table
insert into com_schema_analysis_types(analysis_type_id,analysis_type_name,analysis_type_description)
values(com_schema_analysis_type_seq.nextval,'MISSING_FK_INDEX_CHECK','Check if there are foreign keys not indexed.');

insert into com_schema_analysis_types(analysis_type_id,analysis_type_name,analysis_type_description)
values (com_schema_analysis_type_seq.nextval,'MISSING_PRIMARY_KEY_CHECK','Check if there are tables without primary key defined yet');

--insert into com_schema_analysis_types(analysis_type_id,analysis_type_name,analysis_type_description)
--values(com_schema_analysis_type_seq.nextval,'DISABLED_INDEX_CHECK','Check if there are indexes not used.');

insert into com_schema_analysis_types(analysis_type_id,analysis_type_name,analysis_type_description)
values(com_schema_analysis_type_seq.nextval,'INVALID_CONSTRAINTS_CHECK','Check if there are invalid constraints.');

insert into com_schema_analysis_types(analysis_type_id,analysis_type_name,analysis_type_description)
values(com_schema_analysis_type_seq.nextval,'DISABLED_CONSTRAINTS_CHECK','Check if there are disabled constraints.');

insert into com_schema_analysis_types(analysis_type_id,analysis_type_name,analysis_type_description)
values(com_schema_analysis_type_seq.nextval,'INVALID_INDEX_CHECK','Check if there are invalid indexes.');

insert into com_schema_analysis_types(analysis_type_id,analysis_type_name,analysis_type_description)
values(com_schema_analysis_type_seq.nextval,'INVALID_VIEW_CHECK','Check if there are invalid views.');

/
-- registration exception and notification events
declare
  l_event_id   commons.com_event_definitions.event_id%type;
begin
  commons.com_event_mgr.define_event('FAILED_REPAIR_OBJECTS',com_logger.c_error,'Exception raised by procedure on COM_OBJECT_MGR',8,16,l_event_id);
  commons.com_event_mgr.define_event('FAILED_CHECK_OBJECTS',com_logger.c_error,'Exception raised by procedure on COM_OBJECT_MGR',8,16,l_event_id);
  commons.com_event_mgr.define_event('FAILED_SCHEMA_ANALYSIS',com_logger.c_error,'Exception raised by procedure on COM_OBJECT_MGR',8,16,l_event_id);
  commons.com_event_mgr.define_event('REBUILD_INDEX_ERROR',com_logger.c_error,'Rebuild the index it is not possible.'||Chr(13)||'Table space name have not been found for this index',8,16,l_event_id);
  commons.com_event_mgr.define_event('INVALID_OBJECTS_REPORT',com_logger.c_info,'Invalid objects report',8,16,l_event_id);
  commit;
end;
/
commit;