create or replace package com_schema_analysis_indexes
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis checks related to Indexes.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-11-24 Jose Rolland
--   Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-418
--  2015-10-06 Jose Rolland
--    Added chk_iot_secondary_indexes - OCOMM-288
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  function chk_missing_fk_indexes (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for Missing Indexes on table columns which have FK constraints
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function chk_unusable_indexes (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for Unusable Indexes on tables or table partitions
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-14 Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function chk_iot_secondary_indexes (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for IOTs with Secondary Indexes
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Jose Rolland
--    Creation - OCOMM-288
------------------------------------------------------------------------------------------------------------------------

end com_schema_analysis_indexes;