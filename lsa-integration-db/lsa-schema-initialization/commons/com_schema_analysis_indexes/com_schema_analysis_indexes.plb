create or replace package body com_schema_analysis_indexes
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to Indexes.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Jose Rolland
--    Added chk_iot_secondary_indexes - OCOMM-288
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function chk_missing_fk_indexes (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin    
    select schema_analysis_rule_result(owner, table_name, 'INDEX', 'Suggested DDL for fix: create index '||constraint_name||' on '||owner||'.'||table_name||'('||fk_columns||')', 1)
    bulk collect into l_result
    from (
      select ac.owner, ac.table_name, acc.constraint_name, listagg(acc.column_name, ', ') within group (order by position) fk_columns
      from all_cons_columns acc 
      join all_constraints ac on (acc.constraint_name = ac.constraint_name and acc.owner = ac.owner)
      where ac.owner like i_obj_owner_like  
      and ac.table_name like i_obj_name_like
      and ac.constraint_type = 'R'
      and not exists (
        select null 
        from all_ind_columns uic
        where uic.index_owner = acc.owner 
        and uic.table_name = acc.table_name
        and uic.column_name = acc.column_name
        and uic.column_position = acc.position
      ) 
      group by ac.owner, ac.table_name, acc.constraint_name
    );
    
    return l_result;
    
  end chk_missing_fk_indexes;
  
  function chk_unusable_indexes (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-14 Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin    
    select schema_analysis_rule_result(owner, index_name, 'INDEX', extra_info, 1)
    bulk collect into l_result
    from (
      select owner, index_name, 'Index '||owner||'.'||index_name||' is '||status||', consider to rebuild: '||'alter index '||index_name||' rebuild' extra_info 
      from all_indexes 
      where status not in ('VALID', 'N/A')
      and owner like i_obj_owner_like
      and index_name like i_obj_name_like
      union all
      select index_owner owner, index_name, 'Index '||index_owner||'.'||index_name||' is '||status||', consider to rebuild: '||'alter index '||index_name||' rebuild partition '||partition_name extra_info
      from all_ind_partitions 
      where status not in ('VALID', 'N/A')
      and index_owner like i_obj_owner_like
      and index_name like i_obj_name_like
    );
    
    return l_result;
    
  end chk_unusable_indexes;

  function chk_iot_secondary_indexes (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06 Jose Rolland
--    Creation - OCOMM-288
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin    
    select schema_analysis_rule_result(owner, index_name, 'INDEX', extra_info, 1)
    bulk collect into l_result
    from (
      select ai.owner, ai.index_name, 'Index '||ai.owner||'.'||ai.index_name||' is secondary in IOT '||ai.table_name extra_info 
      from all_indexes ai
      where ai.owner like i_obj_owner_like
      and ai.index_name like i_obj_name_like
      and ai.index_type = 'NORMAL'
      and exists (
        select atab.table_name 
        from all_tables atab
        where atab.owner = ai.owner
        and atab.table_name = ai.table_name
        and atab.iot_type is not null
      )
    );
    
    return l_result;
  end chk_iot_secondary_indexes;

end com_schema_analysis_indexes;