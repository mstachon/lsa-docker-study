-- create a table to bulk insert data which can be used to generate some bulk exceptions
create table test_bulk_errors (
  t_id    number(2) constraint tbulkerrors_pk primary key,
  t_name  varchar2(30),
  constraint tbulkerrors_name_uc_chk check (t_name = upper(t_name)) 
);

-- Generate some bulk exceptions, and use the com_error_mgr to log them
declare
  l_tab_to_test_bulk_err_ids    dbms_sql.number_table;
  l_tab_to_test_bulk_err_names  dbms_sql.varchar2_table;
begin

-- populate the collection  
  select rownum t_id, (case rownum when 50 then 'test' else 'TEST' end)||rownum t_name
  bulk collect into l_tab_to_test_bulk_err_ids, l_tab_to_test_bulk_err_names
  from dual connect by 1=1 and rownum <=100;

-- insert into the destination table
  forall i in l_tab_to_test_bulk_err_ids.first .. l_tab_to_test_bulk_err_ids.count save exceptions
    insert into test_bulk_errors(t_id, t_name)
    values (l_tab_to_test_bulk_err_ids(i), l_tab_to_test_bulk_err_names(i));

exception  
  when others then    
    com_error_mgr.log_bulk_errors;
end;
/