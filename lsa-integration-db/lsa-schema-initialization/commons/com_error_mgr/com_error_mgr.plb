create or replace package body com_error_mgr 
is
-----------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to raise errors, and log bulk errors.
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-10-13  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------

  procedure raise_error(
    i_error_id    in  com_event_definitions.event_id%type,
    i_error_msg   in  com_event_definitions.event_description%type
  ) 
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
----------------------------------------------------------------------------------------------------------------------- 
  is
  begin  
    raise_application_error(i_error_id, i_error_msg);   
  end raise_error;

  procedure log_bulk_errors
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-12-09  Chris Roderick
--    Updated to use com_logger.log_entry instead of dbms_output.putline
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
----------------------------------------------------------------------------------------------------------------------- 
  is
    l_bulk_error_count  pls_integer;
  begin
    l_bulk_error_count  :=  sql%bulk_exceptions.count;
    com_logger.log_entry(com_logger.c_error, 'Bulk Error Count: '||l_bulk_error_count||' Message: '||dbms_utility.format_error_stack);

--  Loop through the individual errors and log them
    for i in 1 .. l_bulk_error_count loop
      com_logger.log_entry(com_logger.c_error, 'Error #'||i||' at iteration #'||sql%bulk_exceptions(i).error_index||': '||sqlerrm(-sql%bulk_exceptions(i).error_code));
    end loop;
  
  end log_bulk_errors;

end com_error_mgr;