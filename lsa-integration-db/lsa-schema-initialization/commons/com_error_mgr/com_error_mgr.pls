create or replace package com_error_mgr
-----------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to raise errors, and log bulk errors.
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-13 Eve Fortescue-Beck
--    Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-402
--  2010-10-13  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
is
  procedure raise_error(
    i_error_id    in  com_event_definitions.event_id%type,    
    i_error_msg   in  com_event_definitions.event_description%type
  );
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Procedure to raise application_error, where the error_code is given by the value of the i_error_id which is 
--    statically defined in the column com_event_definitions.event_id
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_error_id - Error code
--    i_error_msg - Error message
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions   
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-12 Chris Roderick
--    Modified to take a clob as input for the event message instead of 
--    com_event_definitions.event_description%type - OCOMM-278
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------

  procedure log_bulk_errors;
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Means to log bulk errors (means to log the individual elements of the sql%bulk_exceptions) using com_logger.
--    After performing a a bulk DML operation with the FORALL statement using the SAVE EXCEPTIONS clause, you can use
--    this procedure to write entries to the COM_LOGS table indicating the total number of exceptions encountered 
--    followed by the details of each individual exception)
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions   
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
end com_error_mgr;