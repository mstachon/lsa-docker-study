create or replace package com_job_mgr 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to monitor and report on user job activity
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-19 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-407
--  2015-10-05  Chris Roderick
--    Added some constants to support notification filters. OCOMM-354 
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions
--  2011-01-11  Daniel Teixeira
--    Creation
------------------------------------------------------------------------------------------------------------------------
is 
  c_max_run_duration_warn constant  com_event_notif_filter_values.filter_key%type :=  'MAX_RUN_DURATION_WARN';
  c_disabled_jobs         constant  com_event_notif_filter_values.filter_key%type :=  'DISABLED_JOBS';
  c_non_scheduler_jobs    constant  com_event_notif_filter_values.filter_key%type :=  'NON_SCHEDULER_JOBS';
  c_failure_count         constant  com_event_notif_filter_values.filter_key%type :=  'FAILURE_COUNT';
 
  procedure report;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Reports on jobs statistics and raises a 'JOB_REPORT' event with statistics summary. 
--    The statistics include, for each job, the number of executions, min, max, avg run times and 
--    the number of failures. 
--    In addition, the report includes a list of jobs that don't have the max_duration_time property
--    set, and jobs which are still scheduled using DBMS_JOB (excluding jobs created via 
--    dbms_refresh refresh groups - used for refreshing materialized views).
--    This procedure should be scheduled to run daily in the user account where COMMONS is deployed.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-01-11  Daniel Teixeira
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure monitor;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Monitors all the scheduler jobs and raises a JOB_REPORT event when there 
--    is at least one job running for longer than its max_run_duration property.
--    This procedure should be scheduled to be executed every 5 minutes.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-01-11 Daniel Teixeira
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
end com_job_mgr;