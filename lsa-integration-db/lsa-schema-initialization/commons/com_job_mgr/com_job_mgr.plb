create or replace package body com_job_mgr 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Modified procedure report OCOMM-443
--  2015-10-03  Chris Roderick
--    Refactoring. OCOMM-354
--  2015-07-16 Chris Roderick
--    Replaced calls to com_logger.log_entry with com_logger.debug etc. for OCOMM-320
--  2011-08-04  Chris Roderick
--    Cast intervals in reports to interval day(1) to second(1) to improve the formatting - only 
--    showing a reasonable amount of digits.
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions
--  2011-01-11  Daniel Teixeira
--    Creation
------------------------------------------------------------------------------------------------------------------------
is  
  procedure report 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    adapted to changes in vc_vc_map definition OCOMM-443
--  2015-10-03  Chris Roderick
--    Refactoring, particularly to add a payload about warnings and errors when reporting, such that report 
--    notifications can be filtered to ony send reports when there are errors or warnings present. OCOMM-354
--  2014-05-21  Pascal le Roux
--    Profit from com_notifier.notify_with_clob and com_formatter.ref_cursor_to_html_table
--    Now sending HTML formatted mail. see OCOMM-262 
--  2011-08-04  Chris Roderick
--    Cast intervals in reports to interval day(1) to second(1) to improve the formatting - only 
--    showing a reasonable amount of digits.
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions.
--    Added extra predicate for excluding system generated DBMS_JOB entries for Materialized Views 
--    and refresh groups.
--    Changed the type of l_monitoring_window_start and l_monitoring_window_end to reference the type of the relevant 
--    columns in the user_scheduler_job_run_details view. This avoids potential bind variable type mismatch.
--    Added final exception block which raises a 'JOB_REPORT' event in an autonomous transacion 
--    which indicates the failure that occurred.
--  2011-01-11  Daniel Teixeira
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    c_report_event_name     constant  com_event_definitions.event_name%type :=  'JOB_REPORT';
    c_report_job_name       constant  user_scheduler_jobs.job_name%type := 'COM_JOBS_DAILY_REPORT';
    c_report_interval       constant  interval day to second  := numtodsinterval(1, 'DAY');
    l_report_window_start             user_scheduler_job_run_details.actual_start_date%type;
    l_report_window_end               user_scheduler_job_run_details.actual_start_date%type;
    l_job_failure_count               pls_integer;
    l_report_msg                      clob;
    l_return_msg                      clob;
    l_refcursor                       sys_refcursor; 
    l_report_event_filter_payload     vc_vc_map;
    l_report_notification_command     com_notifier.t_notification_command;

    procedure establish_report_window
    is
    begin
--    NOTE: next_run_date for the job executing this code will not have been updated until this job completes, thus the 
--    value is equivalent to NOW, but precise with repesect to the exact time the code was last run.
      select j.next_run_date - c_report_interval, j.next_run_date
      into l_report_window_start, l_report_window_end
      from user_scheduler_jobs j
      where job_name = c_report_job_name
      and upper(enabled) = 'TRUE';  
    end establish_report_window;
    
    procedure add_report_event_filter (
      i_event_filter_key    in  com_event_notif_filter_values.filter_key%type,
      i_event_filter_values in  com_event_notif_filter_values.filter_values%type
    )
    is
    begin
      l_report_event_filter_payload.put(i_event_filter_key, i_event_filter_values);
    end add_report_event_filter;
    
    procedure check_max_run_duration
    is
    begin
      open l_refcursor for      
        select job_name from user_scheduler_jobs where upper(enabled) = 'TRUE' and max_run_duration is null;
        l_return_msg := com_formatter.ref_cursor_to_html_table(l_refcursor, com_constants.c_yes);
      close l_refcursor;      
      
      if l_return_msg is not null then
        l_report_msg := l_report_msg||q'[
          <p>The following jobs don''t have the MAX_RUN_DURATION property set!
          <br/>You can set the MAX_RUN_DURATION issuing the command:
          <br/>dbms_scheduler.set_attribute (''$JOB_NAME'', ''max_run_duration'' , interval ''5'' minute)
          </p>]'||
          l_return_msg;
        
        add_report_event_filter(c_max_run_duration_warn, 'Y'); 
      end if;
    end check_max_run_duration;
    
    procedure check_disabled
    is
    begin
      open l_refcursor for      
        select job_name from user_scheduler_jobs where upper(enabled) = 'FALSE';
        l_return_msg := com_formatter.ref_cursor_to_html_table(l_refcursor, com_constants.c_yes);
      close l_refcursor;      
      
      if l_return_msg is not null then
        l_report_msg := l_report_msg||q'[<p>The following jobs are disabled!]</p>]'||l_return_msg;
        add_report_event_filter(c_disabled_jobs, 'Y'); 
      end if;
    end check_disabled;
    
    procedure check_non_scheduler_jobs
    is
    begin
      open l_refcursor for      
        select uj.job
        from user_jobs uj
        where uj.interval is not null
        and (lower(uj.what) not like 'dbms_mview.refresh%'
          or lower(uj.what) not like 'dbms_refresh.refresh%');
        l_return_msg := com_formatter.ref_cursor_to_html_table(l_refcursor, com_constants.c_yes);
      close l_refcursor;      
      
      if l_return_msg is not null then
        l_report_msg := l_report_msg||q'[<p>The following jobs should be converted to use DBMS_SCHEDULER!</p>]'||l_return_msg;
        add_report_event_filter(c_non_scheduler_jobs, 'Y'); 
      end if;
    end check_non_scheduler_jobs;
    
    procedure compute_job_stats
    is
    begin
      open l_refcursor for      
        select job_name, max_duration, min_duration, avg_duration, run_count, 
          '<div style="left:0px; top:0px; width:100%; height:100%; background-color:'||(case 
          when failure_count > 0 then '#ff3700' else '#ffffff' end)||'">'||failure_count||'</div>' failure_count 
        from (
          select job_name, cast(max(run_duration) as interval day (1) to second(1)) max_duration, 
            cast(min(run_duration) as interval day (1) to second(1)) min_duration, 
            cast(numtodsinterval(round(avg((
              extract (second from run_duration)  +            
              extract (minute from run_duration) * 60 +
              extract (hour   from run_duration) * 60 * 60  + 
              extract (day    from run_duration) * 24 * 60 * 60 )
            )),'SECOND') as interval day (1) to second(1)) avg_duration, 
            count(*) run_count, sum(case status when 'SUCCEEDED' then 0 else 1 end) failure_count
          from user_scheduler_job_run_details j
          where j.actual_start_date between l_report_window_start and l_report_window_end
          group by job_name
          order by job_name
        );  
        l_return_msg := com_formatter.ref_cursor_to_html_table(l_refcursor, com_constants.c_yes);    
      close l_refcursor;      
      
      if l_return_msg is not null then
        l_report_msg := l_report_msg||'<p>Job statistics between '||to_char(l_report_window_start, 'YYYY-MM-DD HH24:MI')||
          ' and '||to_char(l_report_window_end, 'YYYY-MM-DD HH24:MI TZD')||'</p>'||l_return_msg;
          
        select sum(case status when 'SUCCEEDED' then 0 else 1 end) into l_job_failure_count
        from user_scheduler_job_run_details j
        where j.actual_start_date between l_report_window_start and l_report_window_end;
        
        add_report_event_filter(c_failure_count, l_job_failure_count);
      end if;
    end compute_job_stats;
    
  begin
    l_report_event_filter_payload := vc_vc_map();
    l_report_msg :=
        q'[
        <html>
          <head><title>Daily Job Report</title></head>
          <style>
            table {border: 1px solid #2854a1;border-collapse:collapse; background:#ffffff;padding:5px;}
            th {border: 1px solid #2854a1;background-color: #2854a1;color: #ffffff;font-family: Arial, Helvetica, Verdana, Lucida, Sans-Serif;font-size: 0.8em;font-weight: bold;text-align: center;}
            td {border: 1px solid #2854a1;background-color: #ffffff;font-family: Arial, Helvetica, Verdana, Lucida, Sans-Serif;font-size: 0.8em;text-align: center;}
          </style>
          <body style="font-family:Arial, Helvetica, Verdana, Lucida, Sans-Serif;font-size: 0.8em;">
          <p>
            <b>Daily Job Report:<b>
          </p>
        ]';
        
    establish_report_window;
    check_max_run_duration;
    check_disabled;
    check_non_scheduler_jobs;
    compute_job_stats;

    l_report_msg:= l_report_msg ||'</body></html>';
    
    l_report_notification_command.event_name      :=  c_report_event_name;
    l_report_notification_command.event_msg       :=  l_report_msg;
    l_report_notification_command.filter_payload  :=  l_report_event_filter_payload;
    
    com_event_mgr.raise_event(l_report_notification_command);
    com_logger.debug('Sent job report', $$plunit);
  exception
    when others then
      com_event_mgr.raise_error_event('INTERNAL_ERROR', 'Failed to report on jobs: '||dbms_utility.format_error_stack);  
  end report;
  
  procedure monitor 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-03  Chris Roderick
--    Refactoring. OCOMM-354
--  2011-08-04  Chris Roderick
--    Cast intervals in reports to interval day(1) to second(0) to improve the formatting - only 
--    showing a reasonable amount of digits.
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions.
--    Changed the type of l_monitoring_window_start and l_monitoring_window_end to reference the type of the relevant columns
--    in the user_scheduler_job_run_details view. This avoids potential bind variable type mismatch.
--    Added final exception block which raises a 'JOB_REPORT' event in an autonomous transacion 
--    which indicates the failure that occurred.
--  2011-01-11 Daniel Teixeira
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    c_report_event_name       constant    com_event_definitions.event_name%type :=  'JOB_MONITORING_REPORT';
    c_monitor_job_name        constant    user_scheduler_jobs.job_name%type  := 'COM_JOBS_MONITOR';
    l_monitoring_window_start             user_scheduler_job_run_details.actual_start_date%type;
    l_monitoring_window_end               user_scheduler_job_run_details.actual_start_date%type;
    l_report_msg                          com_types.t_varchar2_max_size_plsql; 
    l_jobs_ran_too_long                   com_types.t_varchar2_max_size_plsql;
    l_jobs_running_too_long               com_types.t_varchar2_max_size_plsql;
    l_something_to_report                 boolean := false;
    
    procedure establish_monitoring_window
    is
    begin
      l_monitoring_window_end := systimestamp;
      select max(usjrd.req_start_date) 
      into l_monitoring_window_start
      from user_scheduler_job_run_details usjrd
      where job_name = c_monitor_job_name
      and usjrd.req_start_date > systimestamp - interval '1' day;
    exception      
      when others then 
        com_logger.error('Failed to establish start time as expected: '||dbms_utility.format_error_stack, $$plsql_unit);
        l_monitoring_window_start := l_monitoring_window_end - interval '5' minute;      
    end establish_monitoring_window;
    
    procedure check_jobs_ran_over_limit
    is
    begin 
--    Find completed job executions that actually ran for more than their pre-defined execution time limit
      select substr (sys_connect_by_path (description , ','), 2) csv 
      into l_jobs_ran_too_long
      from (
        select description , row_number() over (order by job_name) rn, count(*) over () cnt
        from (
          select usj.job_name job_name, com_constants.c_line_break||chr(13)||com_constants.c_tab||usj.job_name||' started at '||usjrd.actual_start_date||' ran for '||cast(usjrd.run_duration as interval day (1) to second(0))||' when max duration is '||cast(usj.max_run_duration as interval day (1) to second(0)) description
          from user_scheduler_jobs usj 
          join user_scheduler_job_run_details usjrd on (usj.job_name = usjrd.job_name)
          where (usjrd.run_duration > usj.max_run_duration)
          and usjrd.actual_start_date between l_monitoring_window_start and l_monitoring_window_end 
        )
      )
      where rn = cnt
      start with rn = 1
      connect by rn = prior rn + 1;
    
      if l_jobs_ran_too_long is not null then 
        l_report_msg := l_report_msg||'Finished jobs that have run for more than their limit between '||l_monitoring_window_start||' and '||l_monitoring_window_end||':'||com_constants.c_line_break||l_jobs_ran_too_long||com_constants.c_line_break;
        l_something_to_report := true;
      end if;  
  
    exception 
      when no_data_found then null;
      when others then raise;
    end check_jobs_ran_over_limit;
    
    procedure check_jobs_running_over_limit
    is
    begin 
--    Check running jobs that are already running for more than their pre-defined execution time limit
      select substr (sys_connect_by_path (description , ','), 2) csv 
      into l_jobs_running_too_long
      from (
        select description , row_number() over (order by job_name) rn, count(*) over () cnt
        from ( 
          select usj.job_name job_name, com_constants.c_line_break||com_constants.c_tab||usj.job_name||' is running for '||cast(elapsed_time as interval day (1) to second(0))||' when its max run duration is set to '||cast(max_run_duration as interval day (1) to second(0)) description
          from user_scheduler_jobs usj 
          join user_scheduler_running_jobs usrj on (usj.job_name = usrj.job_name)
          where usrj.elapsed_time > usj.max_run_duration
        ) 
      )
      where rn = cnt
      start with rn = 1
      connect by rn = prior rn + 1;
      
      if l_jobs_running_too_long is not null then 
        l_report_msg := l_report_msg||'Jobs that are currently running for more than their max duration:'||com_constants.c_line_break||l_jobs_running_too_long;
        l_something_to_report := true;
      end if;  
  
    exception 
      when no_data_found then null;
      when others then raise;
    end check_jobs_running_over_limit;
    
  begin
    l_report_msg := '--------------------------  Monitoring jobs ---------------------------- '||com_constants.c_line_break;    
    establish_monitoring_window;
    check_jobs_ran_over_limit;
    check_jobs_running_over_limit;
    
    if l_something_to_report then 
      com_event_mgr.raise_event(c_report_event_name, l_report_msg);
    end if;
    
  exception 
    when others then
      com_event_mgr.raise_error_event('INTERNAL_ERROR', 'Failed to monitor jobs: '||dbms_utility.format_error_stack);
  end monitor;

end com_job_mgr;