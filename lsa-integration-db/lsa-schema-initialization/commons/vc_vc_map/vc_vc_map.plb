create or replace type body vc_vc_map 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2016-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-19  Chris Roderick
--    Added functions get_timestamp_tz_value and get_timestamp_tz_value_at OCOMM-461
--  2017-01-10  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
is
  constructor function vc_vc_map(
    self      in out nocopy vc_vc_map 
  ) 
  return self as result
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------  
  is
  begin
    l_keys   := table_of_varchar();
    l_values := table_of_varchar();
    return;
  end vc_vc_map;
  
  constructor function vc_vc_map (
    self      in out nocopy vc_vc_map, 
    l_keys    in table_of_varchar, 
    l_values  in table_of_varchar 
  ) 
  return self as result
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-05-19  Pascal Le Roux
--    Force uniqueness of the keys in the vc_vc_map [OCOMM-463]
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.is_true(l_keys.count = l_values.count, 'collections of keys and values must be of the same size');
    com_assert.is_true(l_keys.count = cardinality(set(l_keys)), 'collections of keys and values must not have duplicated key');
    
    self.l_keys :=  l_keys;
    self.l_values :=  l_values;
    return;
  end vc_vc_map;
  
  map member function equals_comparator 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return self.to_string();
  end equals_comparator;
  
  member function to_string (i_delimiter in varchar2 default ',') 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
--  FIXME - Chris Roderick - this is quite fragile, and needs re-thinking how to avoid running out of chars. Oracle does not allow to use a CLOB here.  
    l_vc_vc_map_as_string com_types.t_varchar2_max_size_plsql;
  begin 
    for r_entry in 1..l_keys.count loop
      l_vc_vc_map_as_string :=  l_vc_vc_map_as_string||(case when r_entry != 1 then i_delimiter end)||
        '['||l_keys(r_entry)||','||l_values(r_entry)||']';
    end loop;
    
    return l_vc_vc_map_as_string;
  end to_string;
  
  member function is_empty
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return l_keys is empty;
  end is_empty;

  member function contains_key(i_key in varchar2)
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return i_key member of l_keys;
  end contains_key;
  
  member function index_of(i_key in varchar2)
  return integer
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-13  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    for r_index in 1..l_keys.count loop
      
      if l_keys(r_index) = i_key then
        return r_index;
      end if;
    end loop;
    
    return -1;
  end index_of;

  member function get_varchar_value(i_key in varchar2)
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if self.contains_key(i_key) then
      return l_values(self.index_of(i_key));
    end if;
    
    return null;
  end get_varchar_value;
  
  member function get_varchar_value_at(i_index in integer)
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.is_true(i_index > 0, 'Map entry cannot be accessed by index < 1');
    if self.entry_count() >= i_index then
      return l_values(i_index);
    end if;
    
    return null;
  end get_varchar_value_at;  

  member function get_number_value(i_key in varchar2)
  return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
    l_varchar_value  com_types.t_varchar2_max_size_db;
  begin
    l_varchar_value  := self.get_varchar_value(i_key);
    if  l_varchar_value is null then
      return null;
    end if;
    
    return to_number(l_varchar_value);
  exception
    when others then 
      com_event_mgr.raise_error_event('PROCESSING_ERROR', 
        'Unable to convert value '''||l_varchar_value||''' to a number');
  end get_number_value;
  
  member function get_number_value_at(i_index in integer)
  return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
    l_varchar_value  com_types.t_varchar2_max_size_db;
  begin
    l_varchar_value  := self.get_varchar_value_at(i_index);    
    if l_varchar_value is null then
      return null;
    end if;
    
    return to_number(l_varchar_value);
  exception
    when others then 
      com_event_mgr.raise_error_event('PROCESSING_ERROR', 
        'Unable to convert value '''||l_varchar_value||''' to a number');
  end get_number_value_at;

  member function get_timestamp_value(
    i_key               in varchar2,
    i_timestamp_format  in varchar2 default 'YYYY-MM-DD HH24:MI:SS'
  )
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
    l_varchar_value  com_types.t_varchar2_max_size_db;
  begin
    l_varchar_value  := self.get_varchar_value(i_key);
    if  l_varchar_value is null then
      return null;
    end if;
    
    return to_timestamp(l_varchar_value, i_timestamp_format);
  exception
    when others then 
      com_event_mgr.raise_error_event('PROCESSING_ERROR', 
        'Unable to convert value '''||l_values(self.index_of(i_key))||''' to a timestamp');
  end get_timestamp_value;
  
  member function get_timestamp_tz_value(
    i_key               in varchar2,
    i_timestamp_format  in varchar2 default 'YYYY-MM-DD"T"HH24:MI:SSTZH:TZM'
  )
  return timestamp with time zone
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-19  Chris Roderick
--    Creation OCOMM-461
------------------------------------------------------------------------------------------------------------------------
  is
    l_varchar_value  com_types.t_varchar2_max_size_db;
  begin
    l_varchar_value  := self.get_varchar_value(i_key);
    if  l_varchar_value is null then
      return null;
    end if;
    
    return to_timestamp_tz(l_varchar_value, i_timestamp_format);
  exception
    when others then 
      com_event_mgr.raise_error_event('PROCESSING_ERROR', 
        'Unable to convert value '''||l_values(self.index_of(i_key))||''' to a timestamp with time zone');
  end get_timestamp_tz_value;

  member function get_timestamp_value_at(
    i_index             in integer,
    i_timestamp_format  in varchar2 default 'YYYY-MM-DD HH24:MI:SS'
  )
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
    l_varchar_value  com_types.t_varchar2_max_size_db;
  begin
    l_varchar_value  := self.get_varchar_value_at(i_index);    
    if l_varchar_value is null then
      return null;
    end if;
    
    return to_timestamp(l_varchar_value, i_timestamp_format);
  exception
    when others then 
      com_event_mgr.raise_error_event('PROCESSING_ERROR', 
        'Unable to convert value '''||l_varchar_value||''' to a timestamp');
  end get_timestamp_value_at;
  
  member function get_timestamp_tz_value_at(
    i_index             in integer,
    i_timestamp_format  in varchar2 default 'YYYY-MM-DD"T"HH24:MI:SSTZH:TZM'
  )
  return timestamp with time zone
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
    l_varchar_value  com_types.t_varchar2_max_size_db;
  begin
    l_varchar_value  := self.get_varchar_value_at(i_index);    
    if l_varchar_value is null then
      return null;
    end if;
    
    return to_timestamp_tz(l_varchar_value, i_timestamp_format);
  exception
    when others then 
      com_event_mgr.raise_error_event('PROCESSING_ERROR', 
        'Unable to convert value '''||l_varchar_value||''' to a timestamp');
  end get_timestamp_tz_value_at;  

  member function key_set
  return table_of_varchar
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return l_keys;
  end key_set;

  member function value_set
  return table_of_varchar
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return l_values;
  end value_set;
  
  member function entry_count
  return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return l_keys.count();
  end entry_count;
  
  member procedure put(
    i_key   in  varchar2,
    i_value in  varchar2
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.not_null(i_key, 'Cannot put a map entry with a null key');
    if not self.contains_key(i_key) then
      l_keys.extend();
      l_keys(l_keys.count)  :=  i_key;
      l_values.extend();
      l_values(l_values.count)  :=  i_value;
    else
      l_values(self.index_of(i_key))  :=  i_value;
    end if;
  end put;

  member procedure remove (
    i_key   in  varchar2
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Removes the mapping for a key from this map if it is present. 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
    l_index_of_key  integer;
  begin
    l_index_of_key  :=  index_of(i_key);
    self.l_keys.delete(l_index_of_key);
    self.l_values.delete(l_index_of_key);
  end remove;

  member procedure put_if_absent (
    i_key   in  varchar2,
    i_value in  varchar2
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    If the specified key is not already contained in the map, puts the given key value pair into the map. Otherwise 
--    does nothing.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if not self.contains_key(i_key) then
      self.put(i_key, i_value);
    end if;
  end put_if_absent;
  
  member procedure clear_entries
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Removes all entries from the map.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    l_keys.delete();
    l_values.delete();
  end clear_entries;
  
end;