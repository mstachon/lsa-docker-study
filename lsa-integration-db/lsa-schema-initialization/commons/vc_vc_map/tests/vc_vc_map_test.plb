create or replace package body vc_vc_map_test
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--  	Auto-generated test package for vc_vc_map
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--  	 Auto-generated OCOMM-443
------------------------------------------------------------------------------------------------------------------------
is

  procedure test_clear_entries
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for clear_entries
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map vc_vc_map;
  
    procedure with_given_conditions
    is
    begin
      l_vc_vc_map :=  vc_vc_map(
        table_of_varchar('id', 'startTime', 'maxRows'), 
        table_of_varchar('123', '2017-01-14 09:00:00', '100')
      );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_vc_vc_map.clear_entries();
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_vc_vc_map.entry_count() = 0, 
        'Expected Map to have 0 elements but got '||l_vc_vc_map.entry_count());
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_clear_entries;

  procedure test_contains_key_true
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Test for contains_key expecting to be true
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    i_key             com_types.t_varchar2_max_size_db;
    l_function_result boolean;
    l_expected_result boolean;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      i_key             :=  'id';
      l_expected_result :=  true;
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.contains_key(i_key);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result, 'Expected Map to contain key '''||i_key||'''');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_contains_key_true;

  procedure test_contains_key_false
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-15 Chris
--    Test for contains_key expecting to be fasle
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    i_key             com_types.t_varchar2_max_size_db;
    l_function_result boolean;
    l_expected_result boolean;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      i_key             :=  'blah';
      l_expected_result :=  true;
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.contains_key(i_key);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_false(l_function_result, 'Expected Map to NOT contain key '''||i_key||'''');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_contains_key_false;
  
  procedure test_entry_count
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for entry_count
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result number;
    l_expected_result number;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      l_expected_result :=  3;
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.entry_count();
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected Map to contain '||l_expected_result||' entries, but found '||l_function_result);
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_entry_count;

  procedure test_equals_comparator_true
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for equals_comparator
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map1      vc_vc_map;
    l_vc_vc_map2      vc_vc_map;
    l_function_result boolean;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map1      :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      l_vc_vc_map2      :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map1 = l_vc_vc_map2;
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result, 'Expected the 2 maps to be equal');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_equals_comparator_true;
  
  procedure test_equals_comparator_false
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for equals_comparator
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map1      vc_vc_map;
    l_vc_vc_map2      vc_vc_map;
    l_function_result boolean;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map1      :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      l_vc_vc_map2      :=  vc_vc_map(
                              table_of_varchar('differentid', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map1 = l_vc_vc_map2;
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_false(l_function_result, 'Expected the 2 maps NOT to be equal');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_equals_comparator_false;

  procedure test_get_varchar_value
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for get_varchar_value
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result com_types.t_varchar2_max_size_db;
    l_expected_result com_types.t_varchar2_max_size_db;
    i_key             com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      i_key             :=  'maxRows';
      l_expected_result :=  '100';
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.get_varchar_value(i_key);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected to get varchar value '''||l_expected_result||
        ''' for key '''||i_key||''' but got '''||l_function_result||'''');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;

    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_get_varchar_value;
  
  procedure test_get_varchar_value_at
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result com_types.t_varchar2_max_size_db;
    l_expected_result com_types.t_varchar2_max_size_db;
    i_index           integer;

    procedure with_given_conditions
    is
    begin
      l_expected_result :=  '2017-01-14 09:00:00';
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', l_expected_result, '100')
                            );
      i_index           :=  2;
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.get_varchar_value_at(i_index);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected to get varchar value '''||l_expected_result||
        ''' at index '''||i_index||''' but got '''||l_function_result||'''');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;

    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_get_varchar_value_at;  

  procedure test_get_number_value
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-15 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result number;
    l_expected_result number;
    i_key             com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      i_key             :=  'maxRows';
      l_expected_result :=  100.1;
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', i_key), 
                              table_of_varchar('123', '2017-01-14 09:00:00', l_expected_result)
                            );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.get_number_value(i_key);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected to get number value '||l_expected_result||
        ' for key '''||i_key||''' but got '||l_function_result);
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;

    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_get_number_value;

  procedure test_get_number_value_fail
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-16 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result number;
    i_key             com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      i_key             :=  'maxRows';
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', i_key), 
                              table_of_varchar('123', '2017-01-14 09:00:00', 'non_numeric_value')
                            );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.get_number_value(i_key);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      null;
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_assert.is_true(com_event_mgr.event_equals('PROCESSING_ERROR', i_error_code));
    end should_raise;

    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_get_number_value_fail;
  
  procedure test_get_number_value_at
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result number;
    l_expected_result number;
    i_index           integer;

    procedure with_given_conditions
    is
    begin
      i_index           :=  '3';
      l_expected_result :=  100.1;
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', l_expected_result)
                            );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.get_number_value_at(i_index);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected to get number value '||l_expected_result||
        ' at index '''||i_index||''' but got '||l_function_result);
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;

    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_get_number_value_at;  
  
  procedure test_get_timestamp_value
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-15 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map             vc_vc_map;
    l_function_result       timestamp;
    l_expected_result       timestamp;
    i_key                   com_types.t_varchar2_max_size_db;
    l_timestamp_as_varchar  com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      i_key                   :=  'startTime';
      l_timestamp_as_varchar := '2017-01-14 09:00:00';
      l_vc_vc_map             :=  vc_vc_map(
                                    table_of_varchar('id', i_key, 'maxRows'), 
                                    table_of_varchar('123', l_timestamp_as_varchar, '100')
                                  );
      l_expected_result       :=  timestamp '2017-01-14 09:00:00';
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.get_timestamp_value(i_key);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected to get timestamp value '||l_expected_result||
        ' for key '''||i_key||''' but got '||l_function_result);
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;

    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_get_timestamp_value;
  
  procedure test_get_timestamp_value_at
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map             vc_vc_map;
    l_function_result       timestamp;
    l_expected_result       timestamp;
    i_index                 integer;
    l_timestamp_as_varchar  com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      i_index                 :=  2;
      l_timestamp_as_varchar  := '2017-01-14 09:00:00';
      l_vc_vc_map             :=  vc_vc_map(
                                    table_of_varchar('id', 'startTime', 'maxRows'), 
                                    table_of_varchar('123', l_timestamp_as_varchar, '100')
                                  );
      l_expected_result       :=  timestamp '2017-01-14 09:00:00';
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.get_timestamp_value_at(i_index);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected to get timestamp value '||l_expected_result||
        ' at index '''||i_index||''' but got '||l_function_result);
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;

    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_get_timestamp_value_at;  

  procedure test_get_timestamp_value_fail
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-16 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map             vc_vc_map;
    l_function_result       timestamp;
    i_key                   com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      i_key                   :=  'startTime';
      l_vc_vc_map             :=  vc_vc_map(
                                    table_of_varchar('id', i_key, 'maxRows'), 
                                    table_of_varchar('123', 'non_timestamp_value', '100')
                                  );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.get_timestamp_value(i_key);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      null;
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_assert.is_true(com_event_mgr.event_equals('PROCESSING_ERROR', i_error_code));
    end should_raise;

    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_get_timestamp_value_fail;
  
  procedure test_index_of
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for index_of
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map         vc_vc_map;
    l_function_result   integer;
    l_expected_result   integer;
    i_key               com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      i_key             :=  'maxRows';
      l_expected_result :=  3;
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.index_of(i_key);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected index of key '''||i_key||''' to be '||l_expected_result||' but got '||l_function_result);
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_index_of;

  procedure test_is_empty_true
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Test for is_empty expecting to be false
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result boolean;
    l_expected_result boolean;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map();
      l_expected_result :=  true;
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.is_empty();
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 'Expected Map to be empty');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_is_empty_true;
  
  procedure test_is_empty_false
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Test for is_empty expecting to be false
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result boolean;
    l_expected_result boolean;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      l_expected_result :=  false;
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.is_empty();
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 'Expected Map Not to be empty');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_is_empty_false;

  procedure test_key_set
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for key_set
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result table_of_varchar;
    l_expected_result table_of_varchar;

    procedure with_given_conditions
    is
    begin
      l_expected_result :=  table_of_varchar('id', 'startTime', 'maxRows');
      l_vc_vc_map       :=  vc_vc_map(
                              l_expected_result, 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.key_set();
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected to get keySet '||com_collection_utils.varchars_to_string(l_expected_result)||
        ' but got '||com_collection_utils.varchars_to_string(l_function_result));
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;

  procedure teardown
  is
  begin
    null; -- no state and no transaction here so nothing to teardown.
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_key_set;

  procedure test_put_new
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map vc_vc_map;
    i_key       com_types.t_varchar2_max_size_db;
    i_value     com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      i_key             :=  'endTime';
      i_value           :=  '2017-01-14 10:00:00';
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_vc_vc_map.put(i_key, i_value);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_vc_vc_map.contains_key(i_key), 'Expected to put new key '''||i_key||'''');
      com_assert.is_true(l_vc_vc_map.get_varchar_value(i_key) = i_value, 
        'Expected to put new varchar value '''||i_value||''' for key '''||i_key||'''');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_put_new;
  
  procedure test_put_update
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map vc_vc_map;
    i_key       com_types.t_varchar2_max_size_db;
    i_value     com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      i_key             :=  'startTime';
      i_value           :=  '2017-01-14 10:00:00';
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_vc_vc_map.put(i_key, i_value);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_vc_vc_map.contains_key(i_key), 'Expected to update existing key '''||i_key||'''');
      com_assert.is_true(l_vc_vc_map.get_varchar_value(i_key) = i_value, 
        'Expected to update varchar value to '''||i_value||''' for key '''||i_key||'''');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_put_update;

  procedure test_put_if_absent_absent
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for put_if_absent
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map vc_vc_map;
    i_key                            com_types.t_varchar2_max_size_db;
    i_value                          com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
      i_key             :=  'endTime';
      i_value           :=  '2017-01-14 10:00:00';
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_vc_vc_map.put_if_absent(i_key, i_value);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_vc_vc_map.contains_key(i_key), 'Expected to put new key '''||i_key||'''');
      com_assert.is_true(l_vc_vc_map.get_varchar_value(i_key) = i_value, 
        'Expected to put new varchar value '''||i_value||''' for key '''||i_key||'''');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_put_if_absent_absent;

  procedure test_put_if_absent_not_absent
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--   Test for put_if_absent when the key is not absent
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map     vc_vc_map;
    l_old_value     com_types.t_varchar2_max_size_db;
    i_key           com_types.t_varchar2_max_size_db;
    i_value         com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      i_key             :=  'startTime';
      l_old_value       :=  '2017-01-14 09:00:00';
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', i_key, 'maxRows'), 
                              table_of_varchar('123', l_old_value, '100')
                            );
      i_value           :=  '2017-01-14 10:00:00';
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_vc_vc_map.put_if_absent(i_key, i_value);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_vc_vc_map.get_varchar_value(i_key) = l_old_value, 
        'Expected map to contain varchar old value '''||l_old_value||''' for key '''||i_key||''' but found '''||l_vc_vc_map.get_varchar_value(i_key));
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_put_if_absent_not_absent;
  
  procedure test_remove
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for remove
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map vc_vc_map;
    i_key       com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      i_key             :=  'startTime';
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', i_key, 'maxRows'), 
                              table_of_varchar('123', '2017-01-14 09:00:00', '100')
                            );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_vc_vc_map.remove(i_key);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_false(l_vc_vc_map.contains_key(i_key), 
        'Expected map NOT to contain entry for key '''||i_key||''' but found '''||l_vc_vc_map.get_varchar_value(i_key));
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_remove;

  procedure test_to_string
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for to_string
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result com_types.t_varchar2_max_size_db;
    i_delimiter       com_types.t_varchar2_max_size_db;
    l_expected_result com_types.t_varchar2_max_size_db;

    procedure with_given_conditions
    is
    begin
      l_vc_vc_map :=  vc_vc_map(
        table_of_varchar('id', 'startTime', 'maxRows'), 
        table_of_varchar('123', '2017-01-14 09:00:00', '100')
      );
      i_delimiter :=  ',';
      l_expected_result :=  '[id,123],[startTime,2017-01-14 09:00:00],[maxRows,100]';
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.to_string(i_delimiter);
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected Map as a string to be '''||l_expected_result||
        ''' but got '''||l_vc_vc_map.to_string(i_delimiter)||'''');
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_to_string;

  procedure test_value_set
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14 Chris
--    Auto-generated test for value_set
------------------------------------------------------------------------------------------------------------------------
  is
    l_vc_vc_map       vc_vc_map;
    l_function_result table_of_varchar;
    l_expected_result table_of_varchar;

    procedure with_given_conditions
    is
    begin
      l_expected_result :=  table_of_varchar('123', '2017-01-14 09:00:00', '100');
      l_vc_vc_map       :=  vc_vc_map(
                              table_of_varchar('id', 'startTime', 'maxRows'), 
                              l_expected_result
                            );
    end with_given_conditions;
  
    procedure when_executing
    is
    begin
      l_function_result := l_vc_vc_map.value_set();
    end when_executing;
  
    procedure should_result_in
    is
    begin
      com_assert.is_true(l_function_result = l_expected_result, 
        'Expected to get value_set '||com_collection_utils.varchars_to_string(l_expected_result)||
        ' but got '||com_collection_utils.varchars_to_string(l_function_result));
    end should_result_in;
  
    procedure should_raise(i_error_code in number)
    is
    begin
      com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
    end should_raise;
  
    procedure teardown
    is
    begin
      null; -- no state and no transaction here so nothing to teardown.
    end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_value_set;

end vc_vc_map_test;