create or replace type vc_vc_map force  
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2016-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This object type is intended to provide constructors and functions to work like a map. The implementation has the 
--    semantics of a LinkedHashMap.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-19  Chris Roderick
--    Added functions get_timestamp_tz_value and get_timestamp_tz_value_at OCOMM-461
--  2017-01-10  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
is object (
  l_keys    table_of_varchar,
  l_values  table_of_varchar,

  constructor function vc_vc_map (
    self      in out nocopy vc_vc_map 
  )
  return self as result,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This constructor initialises an empty map.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  constructor function vc_vc_map (
    self      in out nocopy vc_vc_map, 
    l_keys    in table_of_varchar, 
    l_values  in table_of_varchar 
  ) 
  return self as result,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Constructs a vc_vc_map with the given set of keys and values.
--    This overloads the would-be default constructor (with attributes named identically to the object definition), to
--    allow implementation of required validations and initialisations
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR when the size of keys and values do not match.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  map member function equals_comparator 
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Used internally by Oracle to compare instances of vc_vc_map for equality.
--    NOTE - current implementation requires order of map entries to be identical in order to be equal. To be discussed.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function to_string(i_delimiter in varchar2 default ',') 
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the map entries concatenated as a single delimited string
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_delimiter - the delimiter to use between the map entries 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function is_empty
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns true if this map contains no key-value mappings.  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-10  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function contains_key(i_key in varchar2)
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns true if this map contains a mapping for the specified key.  
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_key - the key whose presence in this map is to be tested
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-10  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function index_of(i_key in varchar2)
  return integer,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the index in the map of the given key. Returns -1 if the map does not contain the given key.  
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_key - the key whose index in this map is to be returned
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-13  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  
  member function get_varchar_value(i_key in varchar2)
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.  
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_key - the key for which to get the value 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-10  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function get_varchar_value_at(i_index in integer)
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the value corresponding to the key at the given index, or null if this map contains no such index.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_index - the index in the map for which to get the value.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises ASSERTION_ERROR if i_index is not > 0.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function get_number_value(i_key in varchar2)
  return number,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the value to which the specified key is mapped, converted to a number.
--    Returns null if this map contains no mapping for the key.  
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_key - the key for which to get the value.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
---   Raises PROCESSING_ERROR if value is not convertible.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-15  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function get_number_value_at(i_index in integer)
  return number,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the value corresponding to the key at the given index, converted to a number.
--    Returns null if this map contains no such index.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_index - the index in the map for which to get the value.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
---   Raises PROCESSING_ERROR if value is not convertible.
--    Raises ASSERTION_ERROR if i_index is not > 0.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function get_timestamp_value(
    i_key               in varchar2,
    i_timestamp_format  in varchar2 default 'YYYY-MM-DD HH24:MI:SS'
  )
  return timestamp,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the value to which the specified key is mapped, converted to a timestamp using the given date format.
--    Returns null if this map contains no mapping for the key.  
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_key - the key for which to get the value
--    i_timestamp_format - the timestamp format to be used to convert the varchar value to a timestamp
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
---   Raises PROCESSING_ERROR if value is not convertible
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-15  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

 member function get_timestamp_tz_value(
    i_key               in varchar2,
    i_timestamp_format  in varchar2 default 'YYYY-MM-DD"T"HH24:MI:SSTZH:TZM'
  )
  return timestamp with time zone,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the value to which the specified key is mapped, converted to a timestamp with a time zone using the given 
--    date format. Returns null if this map contains no mapping for the key.  
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_key - the key for which to get the value
--    i_timestamp_format - the timestamp with time zone format to be used to convert the varchar value to a timestamp
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
---   Raises PROCESSING_ERROR if value is not convertible
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-19  Chris Roderick
--    Creation OCOMM-461
------------------------------------------------------------------------------------------------------------------------

  member function get_timestamp_value_at(
    i_index             in integer,
    i_timestamp_format  in varchar2 default 'YYYY-MM-DD HH24:MI:SS'
  )
  return timestamp,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the value corresponding to the key at the given index, converted to a timestamp using the given date 
--    format.
--    Returns null if this map contains no such index.  
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_index - the index in the map for which to get the value.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
---   Raises PROCESSING_ERROR if value is not convertible.
--    Raises ASSERTION_ERROR if i_index is not > 0.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-17  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function get_timestamp_tz_value_at(
    i_index             in integer,
    i_timestamp_format  in varchar2 default 'YYYY-MM-DD"T"HH24:MI:SSTZH:TZM'
  )
  return timestamp with time zone,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the value corresponding to the key at the given index, converted to a timestamp with a time zone using the 
--    given date format. Returns null if this map contains no such index.  
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_index - the index in the map for which to get the value.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
---   Raises PROCESSING_ERROR if value is not convertible.
--    Raises ASSERTION_ERROR if i_index is not > 0.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-04-19  Chris Roderick
--    Creation OCOMM-461
------------------------------------------------------------------------------------------------------------------------

  member function key_set
  return table_of_varchar,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the keys contained in this map.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-10  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member function value_set
  return table_of_varchar,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the values contained in this map.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-10  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  
  member function entry_count
  return number,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns a count of the number of entries in this map.  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-10  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
  
  member procedure put (
    i_key   in  varchar2,
    i_value in  varchar2
  ),
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Puts the given key value pair into the map. If the map previously contained a mapping for the key, the old value 
--    is replaced by the specified value. 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member procedure remove (
    i_key   in  varchar2
  ),
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Removes the mapping for a key from this map if it is present. 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-11  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member procedure put_if_absent (
    i_key   in  varchar2,
    i_value in  varchar2
  ),
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    If the specified key is not already contained in the map, puts the given key value pair into the map. Otherwise 
--    does nothing.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------

  member procedure clear_entries
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Removes all entries from the map.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-12  Chris Roderick
--    Creation OCOMM-443
------------------------------------------------------------------------------------------------------------------------
) 
not final;