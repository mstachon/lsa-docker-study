create or replace package com_template_util
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is a counterpart of the com_template type. It should not be used directly.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-14 Jose Rolland
--    Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-425
--  2016-03-04 Lukasz Burdzanowski
--    Added bind(i_template_text in clob, i_collection in table_of_varchar) OCOMM-378
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------
is

  function bind(i_template_text in clob, i_key in varchar2, i_value in varchar2)
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Searches passed template for all occurrences of the binding key (starting from #), replaces them with the passed 
--    value and returns modified template text. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_template_text - template text to parse
--    i_key - binding key to look for
--    i_value - value replacing all occurrences of i_key  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INVALID_ARGUMENT assertion on unknown binding
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------

  function bind(i_template_text in clob, i_collection_key in varchar2, i_collection in table_of_varchar)
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Searches passed template for first occurrence of the collection binding key (starting from $) and replaces it 
--    n-times based on provided table. Returns modified template text. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_template_text - template text to parse
--    i_collection_key - binding key to look for
--    i_collection - collection of values replacing the i_key   
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INVALID_ARGUMENT assertion on unknown binding
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------
  
  function bind(
    i_template_text in clob, 
    i_collection_key in varchar2, 
    i_cursor in sys_refcursor, 
    i_close_cursor in boolean default false)    
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Searches passed template for first occurrence of the collection binding key (starting from $) and replaces it 
--    n-times based on provided cursor. Returns modified template text. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_template_text - template text to parse
--    i_collection_key - binding key to look for
--    i_cursor - collection of values replacing the i_key 
--    i_close_cursor - optionally closes the passed cursor 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INVALID_ARGUMENT assertion on unknown binding
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------  

  function bind(i_template_text in clob, i_collection in table_of_varchar)    
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Binds all occurrences of # with consecutive values from the passed array. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_template_text - template text to parse
--    i_collection - collection of values replacing consecutive # anchors 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INVALID_ARGUMENT when collection size does not match count of # anchors
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-03-04 Lukasz Burdzanowski
--    Creation OCOMM-378
------------------------------------------------------------------------------------------------------------------------
end com_template_util;