create or replace package body com_template_util
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is a counterpart of the com_template type. It should not be used directly.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------
is

  c_single_bind_marker constant varchar(1) := '#';
  c_collection_bind_marker constant varchar(1) := '$';
  c_collection_end_marker constant varchar(1) := ']';
  
  function bind(i_template_text in clob, i_key in varchar2, i_value in varchar2)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.arg_not_null(i_template_text, 'i_template_text');
    com_assert.arg_not_null(i_key, 'i_key');
    com_assert.is_true(instr(i_key, c_single_bind_marker) = 1, 
      'Single value placeholder should start from '||c_single_bind_marker||' but got '||i_key);    
    com_assert.is_true(regexp_instr(i_template_text, '('||i_key||')') > 0, 
      'Could not find '||i_key||' binding');
      
    return regexp_replace(i_template_text, '(\'||i_key||')', i_value, 1, 0);
  end bind;
  
  procedure assert_collection_bind(i_template_text in clob, i_collection_key in varchar2)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------  
  is
  begin
    com_assert.arg_not_null(i_template_text, 'i_template_text');
    com_assert.arg_not_null(i_collection_key, 'i_collection_key');
    com_assert.is_true(instr(i_collection_key, c_collection_bind_marker) = 1, 
      'Collection value placeholder should start from '||c_collection_bind_marker||' but got '||i_collection_key);
    com_assert.is_true(regexp_instr(i_template_text, '\'||i_collection_key) > 0,
      'Could not find '||i_collection_key||' binding'); 
  end assert_collection_bind;
  
  function extract_loop_pattern(i_template_text in clob, i_collection_key in varchar2)
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------  
  is
    l_loop_pattern_start integer;
    l_loop_pattern_end integer;
  begin
    l_loop_pattern_start := instr(i_template_text, i_collection_key)+length(i_collection_key)+1;
    l_loop_pattern_end := instr(i_template_text, ']', l_loop_pattern_start);      
    return substr(i_template_text, l_loop_pattern_start, l_loop_pattern_end-l_loop_pattern_start);
  end extract_loop_pattern;
  
  function replace_loop_pattern(i_template_text in clob, i_collection_key in varchar2, i_collection_value in clob)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------  
  is
    l_loop_pattern_replace_start integer;
    l_loop_pattern_replace_end integer;
  begin
    l_loop_pattern_replace_start := instr(i_template_text, i_collection_key);     
    l_loop_pattern_replace_end := instr(i_template_text, c_collection_end_marker, l_loop_pattern_replace_start)+1;
    return substr(i_template_text, 1, l_loop_pattern_replace_start-1)||i_collection_value||
      substr(i_template_text, l_loop_pattern_replace_end);
  end replace_loop_pattern;
  
  function bind(i_template_text in clob, i_collection_key in varchar2, i_collection in table_of_varchar)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------
  is
    l_loop_pattern com_types.t_varchar2_max_size_db;
    l_output clob;
  begin
    assert_collection_bind(i_template_text, i_collection_key);
        
    l_loop_pattern := extract_loop_pattern(i_template_text, i_collection_key);
    
    for ir in 1..i_collection.count loop      
      l_output := l_output||regexp_replace(l_loop_pattern, '(\'||c_single_bind_marker||'\w*)', i_collection(ir));
    end loop;
    
    return replace_loop_pattern(i_template_text, i_collection_key, l_output);
  end bind;
 
  function bind(
    i_template_text in clob, 
    i_collection_key in varchar2, 
    i_cursor in sys_refcursor, 
    i_close_cursor in boolean default false)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-11 Lukasz Burdzanowski
--    Creation OCOMM-337
------------------------------------------------------------------------------------------------------------------------  
  is
    l_loop_pattern com_types.t_varchar2_max_size_db;
    l_loop_fragment clob;
    l_output clob;
    l_is_first_fragment_row boolean := true;
  begin
    assert_collection_bind(i_template_text, i_collection_key);
        
    l_loop_pattern := extract_loop_pattern(i_template_text, i_collection_key);
    
    for r_row_as_xml in (select * from table (xmlsequence (i_cursor)))  loop
      l_loop_fragment := l_loop_pattern;  
      for r_row_columns in (
        select
          row_entry.column_value.getrootelement() as vc_key,
          extractvalue (row_entry.column_value, 'node()') as vc_value
        from
          table (xmlsequence (extract (r_row_as_xml.column_value, '/ROW/node()'))) row_entry) loop
            if l_is_first_fragment_row then
               com_assert.is_true(
                regexp_instr(l_loop_fragment, '('||c_single_bind_marker||r_row_columns.vc_key||')', 1, 1, 0, 'i') > 0, 
                'Could not find '||c_single_bind_marker||r_row_columns.vc_key||' column binding for collection '||i_collection_key);
              l_is_first_fragment_row := false; 
            end if;            
            
            l_loop_fragment := bind(l_loop_fragment, c_single_bind_marker||lower(r_row_columns.vc_key), r_row_columns.vc_value);            
      end loop;  
      l_output := l_output||l_loop_fragment;
    end loop;

    if i_close_cursor then
      close i_cursor;
    end if;
    
    return replace_loop_pattern(i_template_text, i_collection_key, l_output);    
  end bind;
  
  function bind(i_template_text in clob, i_collection in table_of_varchar)    
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-03-04 Lukasz Burdzanowski
--    Creation OCOMM-378
------------------------------------------------------------------------------------------------------------------------  
  is
    l_output clob := i_template_text;
  begin
    com_assert.is_true(regexp_count(i_template_text, c_single_bind_marker) = i_collection.count,
      'Size of the collection does not match number of '||c_single_bind_marker||' anchors');

    for ir in 1..i_collection.count loop      
      com_assert.is_true(i_collection(ir) != c_single_bind_marker, 
        'Binding of character '||i_collection(ir)||' is not allowed.');
      l_output := regexp_replace(l_output, c_single_bind_marker, i_collection(ir), ir, 1);
    end loop;
    return l_output;
  end bind;  
end com_template_util;