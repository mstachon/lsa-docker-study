create or replace package body com_template_test 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--  	Auto-generated test package for com_template_test
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-26 Lukasz
--    Auto-generated 
------------------------------------------------------------------------------------------------------------------------
is

  procedure test_simple_binding 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-26 Lukasz
--    Auto-generated 
------------------------------------------------------------------------------------------------------------------------
  is
    l_template com_template := com_template('bind #first then #second and #many #many other values');
    l_expected_test_result clob := 'bind bind1 then bind2 and bind3 bind3 other values';
    l_result clob;
    l_varchar1 varchar2(30);    
    l_varchar2 varchar2(30);
    l_varchar3 varchar2(30);

  procedure with_given_conditions
  is
  begin
    l_varchar1 := 'bind1';
    l_varchar2 := 'bind2';
    l_varchar3 := 'bind3';    
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_result := l_template.bind('#first', l_varchar1).bind('#second', l_varchar2).bind('#many', l_varchar3).generate;
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_result = l_expected_test_result, 'Expected '''||l_expected_test_result||''' but got '''||l_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;  
  end test_simple_binding;
  
   procedure test_varchar_coll_binding 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-26 Lukasz
--    Auto-generated 
------------------------------------------------------------------------------------------------------------------------
  is
    l_template com_template := com_template('collection of: $mycollection[item #bind here,]');
    l_expected_test_result clob := 'collection of: item one here,item two here,';
    l_result clob;
    l_mycollection table_of_varchar;

  procedure with_given_conditions
  is
  begin
    l_mycollection := table_of_varchar('one','two'); 
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_result := l_template.bind('$mycollection', l_mycollection).generate;
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_result = l_expected_test_result, 'Expected '''||l_expected_test_result||''' but got '''||l_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;  
  end test_varchar_coll_binding;
  
   procedure test_cursor_coll_binding 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-26 Lukasz
--    Auto-generated 
------------------------------------------------------------------------------------------------------------------------
  is
    l_template com_template := com_template('collection of: $mycollection[item #bind here,]');
    l_expected_test_result clob := 'collection of: item one here,item two here,';
    l_result clob;
    l_cursor sys_refcursor;

  procedure with_given_conditions
  is
  begin
     open l_cursor for select 'one' as bind from dual union all select 'two' as bind from dual;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_result := l_template.bind('$mycollection', l_cursor).generate;
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_result = l_expected_test_result, 'Expected '''||l_expected_test_result||''' but got '''||l_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;  
  end test_cursor_coll_binding;
  
   procedure test_anchor_binding 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-03-04 Lukasz Burdzanowski
--    Creation OCOMM-378
------------------------------------------------------------------------------------------------------------------------
 is
    l_template com_template := com_template('bind # then # and thats all');
    l_expected_test_result clob := 'bind bind1 then bind2 and thats all';
    l_result clob;
    l_bind table_of_varchar;

  procedure with_given_conditions
  is
  begin
    l_bind := table_of_varchar('bind1', 'bind2');
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_result := l_template.bind(l_bind);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_result = l_expected_test_result, 'Expected '''||l_expected_test_result||''' but got '''||l_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;  
  end test_anchor_binding;
  
end com_template_test;