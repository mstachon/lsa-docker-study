--  Define the table to store the metadata about how to manage the time range partitioned tables
create table com_time_range_part_tables (
	table_name           varchar2(30)               constraint comtrpt_pk primary key,
	part_prefix          varchar2(19)               constraint comtrpt_pp_nn not null,
	no_parts_advance     number(38)     default 5   constraint comtrpt_npa_nn not null,
	no_parts_keep        number(38),
	part_range_int_cnt   number(38)                 constraint comtrpt_pric_nn not null,
	part_range_int_type  varchar2(30)               constraint comtrpt_prit_nn not null,
	tablespace_prefix    varchar2(30),
	dynamic_tablespace   char(1)        default 'N' constraint comtrpt_dynamts_nn not null,
  constraint comtrpt_prt_chk check (part_range_int_type in ('HOUR', 'DAY', 'MONTH', 'YEAR')),
	constraint comtrpt_tsprefix_chk check (dynamic_tablespace = 'N' or (dynamic_tablespace = 'Y' and tablespace_prefix is not null and length(tablespace_prefix) <= 19)),
  constraint comtrpt_nopartadv_chk check (no_parts_advance >= 0)
);
comment on table com_time_range_part_tables is 'Defines the time range partitioned tables which should be managed by the com_partition_mgr';
comment on column com_time_range_part_tables.table_name is 'The name of the time range partitioned table to be managed';
comment on column com_time_range_part_tables.part_prefix is 'The prefix to be used when generating partition names (the suffix will be _YYYYMMDDHH24)';
comment on column com_time_range_part_tables.no_parts_advance is 'The no of time range partitions to be created in advance of now';
comment on column com_time_range_part_tables.no_parts_keep is 'The no of time range partitions to be kept, with respect to now (a NULL value indicates all partitions should be kept)';
comment on column com_time_range_part_tables.part_range_int_cnt is 'Used to determine the no of intervals (combined with part_range_int_type) to be contained in a single time range partition';
comment on column com_time_range_part_tables.part_range_int_type is 'Used to determine the type of intervals (combined with part_range_int_cnt) to be contained in a single time range partition (e.g. HOUR, DAY, WEEK, MONTH, YEAR)';
/

--  Define the events used internally by the com_part_mgr
declare
  l_event_id   com_event_definitions.event_id%type;
begin

  com_event_mgr.define_event(
    'FAILED_ADD_NEW_PARTITION'
    ,com_logger.c_error
    ,'Creation of new partition failed'
    ,8
    ,16
    ,l_event_id
  );
    
  com_event_mgr.define_event(
    'FAILED_PARTITIONED_TABLE_REG'
    ,com_logger.c_error
    ,'Registration of time range partitioned table configuration failed'
    ,8
    ,16
    ,l_event_id
  );
    
  com_event_mgr.define_event(
    'FAILED_MANAGE_PARTITION'
    ,com_logger.c_error
    ,'Some problem occour during the menagement of some partitions'
    ,8
    ,16
    ,l_event_id
  );
  
end;
/
commit;

--  2011-04-12  Chris Roderick
--    Add and run the code to submit a scheduler job which can be used to manage the partitions
begin
    dbms_scheduler.create_job(
     job_name          =>  'com_part_management',
     job_type          =>  'PLSQL_BLOCK',
     job_action        =>  'com_partition_mgr.manage_partitions;',
     start_date        =>   sysdate,
     repeat_interval   =>  'freq = hourly; byminute = 0',
     enabled           =>   true
    );
  
  dbms_scheduler.set_attribute(
     name          => 'com_part_management',
     attribute     => 'max_run_duration',
     value         => interval '5' minute
  );
  
  commit;

end;
/