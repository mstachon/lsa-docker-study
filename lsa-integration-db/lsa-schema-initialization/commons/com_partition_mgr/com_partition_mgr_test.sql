set serveroutput on
/
--------------------------------------------------------------------------------
--Test Partition Manager - reg_time_range_part_table procedure
--------------------------------------------------------------------------------
--drop table PARTITIONED_TABLE_TEST1;

create table PARTITIONED_TABLE_TEST1(
   item_id number
  ,item_value varchar2(2000)
  ,input_date timestamp default systimestamp
  ,constraint part_tab_test1_pk primary key (item_id)
)
partition by range (input_date)
(partition PART_2011030417 values less than (timestamp'2011-03-04 17:00:00.0000'))
;
/
begin
  com_partition_mgr.reg_time_range_part_table(
  'PARTITIONED_TABLE_TEST1'
  ,'PART'
  ,2
  ,3
  ,12
  ,'HOUR'
  ,NULL
  ,'N'
  );
  
  commit;
end;
/
--------------------------------------------------------------------------------
-- Run the test
--------------------------------------------------------------------------------
declare
  
  l_result_cnt number;
    
begin
  
  select count(*) into l_result_cnt
  from commons.com_time_range_part_tables
  where upper(table_name) = 'PARTITIONED_TABLE_TEST1';

  if l_result_cnt = 1 then 
    dbms_output.put_line('Registration Test - passed');
  else
    dbms_output.put_line('Registration Test - failed!!');
  end if;
  
end;
/
--------------------------------------------------------------------------------
--Test Partition Manager - manage_partitions procedure
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Run the test
--------------------------------------------------------------------------------
declare
  l_range_part_table_row  commons.com_time_range_part_tables%rowtype; 
  l_result_cnt number;    
  l_parts_advance_cnt number;
  l_parts_keep_cnt number;
begin

  commons.com_partition_mgr.manage_partitions;
  commit;
  
  select * into l_range_part_table_row
  from commons.com_time_range_part_tables
  where upper(table_name) = 'PARTITIONED_TABLE_TEST1';
  
  --check number partition created in advance
  select count(*) into l_parts_advance_cnt
  from user_tab_partitions
  where upper(table_name) = 'PARTITIONED_TABLE_TEST1'
    and to_timestamp(substr(partition_name, instr( partition_name, '_', -1)+1), 'YYYYMMDDHH24') > trunc(systimestamp,'HH24');
  
  if l_parts_advance_cnt = l_range_part_table_row.no_parts_advance then
     dbms_output.put_line(' number of partions in advance created correctly');
  else
     dbms_output.put_line(' WRONG number of partions in advance!!');
  end if;
  
  --check number partition to keep
  select count(*)-1 into l_parts_keep_cnt
  from user_tab_partitions
  where upper(table_name) = 'PARTITIONED_TABLE_TEST1'
    and to_timestamp(substr(partition_name, instr( partition_name, '_', -1)+1), 'YYYYMMDDHH24') < trunc(systimestamp,'HH24');
  
  if l_parts_keep_cnt = l_range_part_table_row.no_parts_keep then
     dbms_output.put_line(' number of partions to keep created correctly');
  else
     dbms_output.put_line(' WRONG number of partions to keep!!');
  end if;
end;
/
--------------------------------------------------------------------------------
--Test Partition Manager - manage_partitions procedure with attributes
--------------------------------------------------------------------------------
create table PARTITIONED_TABLE_TEST2(
   item_id number
  ,item_value varchar2(2000)
  ,input_date timestamp default systimestamp
  ,constraint part_tab_test2_pk primary key (item_id)
)
partition by range (input_date)
(partition PART_20110110 values less than (timestamp'2011-01-10 00:00:00.0000'))
;
/
begin
  com_partition_mgr.reg_time_range_part_table(
  'PARTITIONED_TABLE_TEST2'
  ,'PART'
  ,2
  ,3
  ,12
  ,'DAY'
  ,NULL
  ,'N'
  );
  
  commit;
end;
/
--------------------------------------------------------------------------------
-- Run the test
--------------------------------------------------------------------------------
declare
  l_range_part_table_row  commons.com_time_range_part_tables%rowtype; 
  l_result_cnt number;    
  l_parts_advance_cnt number;
  l_parts_keep_cnt number;
begin

  com_partition_mgr.manage_partitions('PARTITIONED_TABLE_TEST2');
  commit;
  
  select * into l_range_part_table_row
  from commons.com_time_range_part_tables
  where upper(table_name) = 'PARTITIONED_TABLE_TEST2';
  
  --check number partition created in advance
  select count(*) into l_parts_advance_cnt
  from user_tab_partitions
  where upper(table_name) = 'PARTITIONED_TABLE_TEST2'
    and to_timestamp(substr(partition_name, instr( partition_name, '_', -1)+1), 'YYYYMMDD') > trunc(systimestamp,'DD');
  
  if l_parts_advance_cnt = l_range_part_table_row.no_parts_advance then
     dbms_output.put_line(' number of partions in advance created correctly');
  else
     dbms_output.put_line(' WRONG number of partions in advance!!');
  end if;
  
  --check number partition to keep
  select count(*)-1 into l_parts_keep_cnt
  from user_tab_partitions
  where upper(table_name) = 'PARTITIONED_TABLE_TEST2'
    and to_timestamp(substr(partition_name, instr( partition_name, '_', -1)+1), 'YYYYMMDD') < trunc(systimestamp,'DD');
  
  if l_parts_keep_cnt = l_range_part_table_row.no_parts_keep then
     dbms_output.put_line(' number of partions to keep created correctly');
  else
     dbms_output.put_line(' WRONG number of partions to keep!!');
  end if;
end;
/
--------------------------------------------------------------------------------
--Test Partition Manager - establish_part_attributes procedure
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Run the test
--------------------------------------------------------------------------------
declare
  l_i_part_range_cnt       com_time_range_part_tables.part_range_int_cnt%type := 3;
  l_i_part_range_int_type  com_time_range_part_tables.part_range_int_type%type := 'MONTH';
  o_part_suffix varchar2(2000);
  o_high_value  timestamp;
begin
com_partition_mgr.establish_part_attributes(
  o_part_suffix,
  o_high_value,
  l_i_part_range_cnt,
  l_i_part_range_int_type
  );
  dbms_output.put_line(' SUFFIX : '||o_part_suffix||' HV : '||o_high_value);
end;
/
--------------------------------------------------------------------------------
-- clean
--------------------------------------------------------------------------------  

delete from com_time_range_part_tables
 where upper(table_name) in ('PARTITIONED_TABLE_TEST1','PARTITIONED_TABLE_TEST2');
  
drop table partitioned_table_test1;  
drop table partitioned_table_test2;  
  
commit;