create or replace package com_partition_mgr 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
-- Description:
--   This package is intended to add and remove partitions to/from time-range partitioned tables in a data driven manner.
--   Also supports creating and optionally dropping tablespaces to which the partitions are mapped.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-25 - Nikolay Tsvetkov
--    Re-factoring. New tablespace management functionality has been added.
--  2011-04-12  Chris Roderick
--    Modification of some default values plus reformatting
--  2011-01-07  Zereyakob Makonnen, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  procedure reg_time_range_part_table (
    i_table_name                 in com_time_range_part_tables.table_name%type,
    i_part_prefix                in com_time_range_part_tables.part_prefix%type,
    i_no_parts_advance           in com_time_range_part_tables.no_parts_advance%type           default 5,
    i_no_parts_keep              in com_time_range_part_tables.no_parts_keep%type              default 7,
    i_part_range_int_cnt         in com_time_range_part_tables.part_range_int_cnt%type         default 1,
    i_part_range_int_type        in com_time_range_part_tables.part_range_int_type%type        default 'DAY',
    i_part_init_size_kb          in com_time_range_part_tables.part_init_size_kb%type          default null,
    i_part_pct_free              in com_time_range_part_tables.part_pct_free%type              default null,
    i_tablespace_full_name       in com_time_range_part_tables.tablespace_full_name%type       default null,
    i_dynamic_tablespace_prefix  in com_time_range_part_tables.dynamic_tablespace_prefix%type  default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers metadata to be used in order to be manage partitions of a time range partitioned table.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name
--      The name of the time range partitioned table to be managed.
--    i_part_prefix
--      The prefix to be used when generating names for new partitions (the suffix will be _YYYYMMDDHH24).
--    i_no_parts_advance
--      The no of time range partitions to be created in advance of the current time.
--    i_no_parts_keep
--      The no of time range partitions in the past (with respect to now) to be kept (a NULL value indicates all 
--      partitions should be kept).
--    i_part_range_int_cnt
--      The the number of intervals to be contained in a single time range partition (e.g. 24 HOUR, 2 DAY).
--    i_part_range_int_type
--      The type of intervals to be contained in a single time range partition (e.g. HOUR, DAY, WEEK, MONTH, YEAR).
--    i_tablespace_full_name
--      The complete tablespace name to be used.
--    i_dynamic_tablespace_prefix
--      Indicates the prefix of dynamic tablespace to be used. The tablespace should be registered in advance for 
--      dynamic management in table com_time_range_tablespaces. In case i_tablespace_full_name is set this one must be null
--      as only one of both can be used for the same table registration.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INTERNAL_ERROR - Raised when attempting to register the same partitioned table twice.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-07 Nikolay Tsvetkov
--    Re-factoring. Modified tablespace related variables.
--  2011-04-12 Chris Roderick
--    Modified some of the default input values
--  2011-01-20 Zereyakob Makonnen
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure manage_partitions(i_table_name in com_time_range_part_tables.table_name%type default null) ;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Manage the partitions of time range-partitioned tables automatically - adding and dropping partitions 
--    in a data driven manner (using range partition delimiters, and partition name prefixes from a table).
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name
--      The name of the time range partitioned table you would like to manage. If this parameter is 
--      NULL all the registered partition will be managed.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INTERNAL_ERROR - Raised on any exception.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Re-factoring.
--    2011-03-15 - Zereyakob Makonnen
--      Add parameter i_table_name
--    2011-01-20 - Zereyakob Makonnen
--      Creation
------------------------------------------------------------------------------------------------------------------------

  procedure establish_part_attributes(
    o_part_suffix         out varchar2,
    o_part_high_value     out timestamp,
    i_part_range_int_cnt  in  com_time_range_part_tables.part_range_int_cnt%type  default 1,
    i_part_range_int_type in  com_time_range_part_tables.part_range_int_type%type default 'DAY',
    i_part_start_time     in  timestamp default systimestamp
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Establish a suffix and high value for a time range partioned table which has a partion interval
--    size which can be determined from the given inputs.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_part_range_int_cnt
--      The the number of intervals to be contained in a single time range partition (e.g. 24 HOUR, 2 DAY).
--    i_part_range_int_type
--      The type of intervals to be contained in a single time range partition (e.g. HOUR, DAY, WEEK, MONTH, YEAR).
--    i_part_start_time
--      The referenced start time for a given partition
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--    o_part_suffix
--      A time suffix to be used when generating partition names (in the format YYYYMMDDHH24, based 
--      on the actual systimestamp and the interval specified as imput).
--    o_part_high_value
--      A high value for partition values (based on the actual systimestamp and the interval 
--      specified as imput).
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INTERNAL_ERROR - When it was not possible to establish the partition attributes based on the given inputs
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-14 Nikolay Tsvetkov
--    Added new input parameter i_part_start_time with systimestamp default value
--  2011-04-12  Chris Roderick
--    Modified the default input value of i_part_range_int_type to be DAY instead of HOUR
--  2011-03-15 - Zereyakob Makonnen,Eve Fortescue-Beck
--    Creation
------------------------------------------------------------------------------------------------------------------------

   procedure manage_tablespaces(i_ts_name_prefix in com_time_range_tablespaces.ts_name_prefix%type default null);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Manage the registered tablespaces automatically - adding, dropping, set to READONLY status
--    in a data driven manner (using range delimiters, and name prefixes).
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_ts_name_prefix
--      The prefix of the tablespace to be managed.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    raises INTERNAL_ERROR on any exception
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-01 - Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure reg_time_range_tablespaces(
      i_ts_name_prefix          in com_time_range_tablespaces.ts_name_prefix%type,
      i_ts_range_int_cnt        in com_time_range_tablespaces.ts_range_int_cnt%type         default 1,
      i_ts_range_int_type       in com_time_range_tablespaces.ts_range_int_type%type        default 'DAY',
      i_ts_datafile_size_mb     in com_time_range_tablespaces.ts_datafile_size_mb%type      default null,
      i_ts_datafile_extend_mb   in com_time_range_tablespaces.ts_datafile_extend_mb%type    default null,
      i_ts_datafile_maxsize_mb  in com_time_range_tablespaces.ts_datafile_maxsize_mb%type  default null,
      i_no_ts_advance           in com_time_range_tablespaces.no_ts_advance%type            default 5,
      i_no_ts_keep_online       in com_time_range_tablespaces.no_ts_keep_online%type        default null,
      i_no_ts_keep_total        in com_time_range_tablespaces.no_ts_keep_total%type         default null,
      i_ts_is_bigfile           in com_time_range_tablespaces.ts_is_bigfile%type            default 'N'
      );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers metadata to be used in order to manage time range tablespaces automatically.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_ts_name_prefix
--      The prefix of the tablespace to be managed.
--    i_ts_range_int_cnt
--      The number of intervals to be contained in a single tablespace (e.g. 1 DAY, 2 DAY, 1 MONTH).
--    i_ts_range_int_type
--      The type of intervals to be contained in a single tablespace (one of HOUR, DAY, MONTH, YEAR).
--    i_ts_datafile_size_mb
--      The initial size of a single tablespace datafile in megabytes (MB).
--    i_ts_datafile_extend_mb
--      The size of the blocks that will extend the datafile after reaching the initial size in MB.
--    i_ts_datafile_maxsize_mb
--      The maximum size of a single tablespace datafile in MB.
--    i_no_ts_advance
--      The number of tablespaces to be created in advance.
--    i_no_ts_keep_online
--      The number of tablespaces from a given definition to be kept in ONLINE mode (the mode they are created).
--    i_no_ts_keep_total
--      The total number of tablespace from a given definition to be kept in the database .
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    raises INTERNAL_ERROR on any exception
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
end com_partition_mgr;