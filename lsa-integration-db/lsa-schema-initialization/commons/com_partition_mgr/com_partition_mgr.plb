create or replace package body com_partition_mgr
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to add and remove partitions to/from time-range partitioned tables in 
--    a data driven manner.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-28 - Jose Rolland
--    Modified manage_partitions. Notify non-blocking error when processing non-existing table. OCOMM-436
--  2016-02-25 - Nikolay Tsvetkov
--    Refactoring. New tablespace management functionality has been added.
--  2013-05-30 - Chris Roderick
--    Bug fix in procedure manage_partitions
--  2012-12-14  Chris Roderick
--    Bug fix in procedure add_new_partition 
--  2012-10-25  Chris Roderick
--    Bug fix in procedure add_new_partition
--  2012-05-31  Chris Roderick
--    Bug fix in procedure add_new_partition
--  2012-01-26  Chris Roderick
--    Modifications / bug fix in procedure add_new_partition
--  2011-11-08  Pascal Le Roux
--    Fix a problem in get_time_suffix : suffix should always be in the format 'YYYYMMDDHH24'
--  2011-04-12  Chris Roderick
--    Modification of some default values plus reformatting
--  2011-01-20  Zereyakob Makonnen, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  subtype t_event_name    is com_event_definitions.event_name%type;  
  c_internal_error           constant  t_event_name := 'INTERNAL_ERROR';
  
  c_int_type_hour   constant  varchar2(4) := 'HOUR';
  c_int_type_day    constant  varchar2(3) := 'DAY';    
  c_int_type_month  constant  varchar2(5) := 'MONTH';
  c_int_type_year   constant  varchar2(4) := 'YEAR'; 
  
  c_suffix_time_range_format constant varchar2(12) := 'YYYYMMDDHH24';
  c_suffix_format_length     constant pls_integer  := 10;
  
  type t_com_time_range_tablespaces is table of com_time_range_tablespaces%rowtype;

  procedure reg_time_range_part_table (
    i_table_name                in com_time_range_part_tables.table_name%type,
    i_part_prefix               in com_time_range_part_tables.part_prefix%type,
    i_no_parts_advance          in com_time_range_part_tables.no_parts_advance%type           default 5,
    i_no_parts_keep             in com_time_range_part_tables.no_parts_keep%type              default 7,
    i_part_range_int_cnt        in com_time_range_part_tables.part_range_int_cnt%type         default 1,
    i_part_range_int_type       in com_time_range_part_tables.part_range_int_type%type        default 'DAY',
    i_part_init_size_kb         in com_time_range_part_tables.part_init_size_kb%type          default null,
    i_part_pct_free             in com_time_range_part_tables.part_pct_free%type              default null,    
    i_tablespace_full_name      in com_time_range_part_tables.tablespace_full_name%type       default null,
    i_dynamic_tablespace_prefix in com_time_range_part_tables.dynamic_tablespace_prefix%type  default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-07 Nikolay Tsvetkov
--    Refactoring. Modified tablespace related variables.
--  2011-04-12 Chris Roderick
--    Modified some of the default input values
--  2011-01-20 Zereyakob Makonnen
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  
    procedure assert_valid_part_table
    is
      l_partition_name  user_tab_partitions.partition_name%type;
      l_partition_ts    timestamp;
    begin
      select partition_name, to_timestamp(part_start_time,c_suffix_time_range_format) into l_partition_name, l_partition_ts
      from(
        select partition_name, substr(partition_name, instr(partition_name, '_', -1)+1) part_start_time 
        from (
          select partition_name 
          from user_tab_partitions
          where upper(table_name) = upper(i_table_name)
          order by partition_position desc
        )
        where rownum = 1
      );
    
    exception
     when no_data_found then
        com_event_mgr.raise_error_event('ASSERTION_ERROR','The table '||i_table_name||
                                                                ' doesn''t exist or is not a partitioned table');
      when others then
        com_event_mgr.raise_error_event('ASSERTION_ERROR','The table '||i_table_name||
          ' cannot be managed: latest partition '''||l_partition_name||''' does not adhere to the required naming conventions');
    end;
  
  begin
    assert_valid_part_table;

    merge into com_time_range_part_tables
      using (
        select upper(i_table_name) table_name, i_part_prefix part_prefix, i_no_parts_advance no_parts_advance, 
          i_no_parts_keep no_parts_keep, i_part_range_int_cnt part_range_int_cnt, i_part_range_int_type part_range_int_type,
          i_part_init_size_kb part_init_size_kb, i_part_pct_free part_pct_free, i_tablespace_full_name tablespace_full_name, 
          i_dynamic_tablespace_prefix dynamic_tablespace_prefix
        from dual     
      )temp on (temp.table_name = upper(com_time_range_part_tables.table_name))
    when matched then update
      set part_prefix = temp.part_prefix,
          no_parts_advance = temp.no_parts_advance,
          no_parts_keep = temp.no_parts_keep,
          part_range_int_cnt = temp.part_range_int_cnt,
          part_range_int_type = temp.part_range_int_type,
          part_init_size_kb = temp.part_init_size_kb,
          part_pct_free = temp.part_pct_free,
          tablespace_full_name = temp.tablespace_full_name,
          dynamic_tablespace_prefix = temp.dynamic_tablespace_prefix
    when not matched then insert  
      (table_name, part_prefix, no_parts_advance, no_parts_keep, part_range_int_cnt, part_range_int_type, 
       part_init_size_kb, part_pct_free, tablespace_full_name, dynamic_tablespace_prefix)
      values(temp.table_name, temp.part_prefix, temp.no_parts_advance, temp.no_parts_keep, temp.part_range_int_cnt, 
         temp.part_range_int_type, temp.part_init_size_kb, temp.part_pct_free, temp.tablespace_full_name, 
         temp.dynamic_tablespace_prefix);
     
    exception
      when others then
        com_event_mgr.raise_error_event(c_internal_error, 'Partitioned table '||i_table_name||
                                             ' : problem during the registration.' || dbms_utility.format_error_stack );
  end reg_time_range_part_table;
  

  function  long_to_timestamp(i_time in long)
    return timestamp
  is
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Convert a long value that contain a time value into a timestamp.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_time      - Timestamp stored in long variable.
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--    timestamp   - The value converted
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2012-01-26  Chris Roderick
--      Modified code to use execute immediate to parse the long value directly into a string and 
--      then timestamp.
--      Removed the exception handler, so that any exception will just be raised to the calling code
--      rather than being masked.
--    2011-03-07 - Zereyakob Makonnen
--      Creation
------------------------------------------------------------------------------------------------------------------------
    l_time  timestamp := null;  
  begin
    execute immediate 'select '||replace(i_time,'TIMESTAMP'' ', 'TIMESTAMP ''')||' from dual' into l_time;
   
    return l_time;
  end long_to_timestamp;
  
  procedure get_time_suffix( 
    io_time             in out  timestamp,
    i_entity_int_type   in      com_time_range_part_tables.part_range_int_type%type,
    o_suffix            out     varchar2
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Takes the given inputs timestamp and interval type and truncates the timestamp to the interval
--    precision specified by the interval type, returning both the truncated timestamp, and a
--    string representation of the timestamp in the format defined by variable c_suffix_time_range_format, 
--    typically used for time range partition name suffix.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_time
--      The timestamp to be truncated and converted to a string
--    i_interval_type
--      The time interval type to use to truncate the given timestamp (HOUR, DAY, MONTH, YEAR).
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--    o_suffix
--      The truncated timestamp converted to a string in the format c_suffix_time_range_format
--    io_time
--      The truncated timestamp
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    unknown
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2016-01-21  Nikolay Tsvetkov
--      Refactoring
--    2012-01-26  Chris Roderick
--      Added procedure comments.
--      Modified the type of the input parameter i_time to be timestamp rather than date
--      Renamed the input parameter i_time_suffix_type to i_interval_type.
--    2011-11-08  Pascal Le Roux
--      Fix a problem - ensure suffix is always in the format 'YYYYMMDDHH24'
--    2011-03-08 - Zereyakob Makonnen
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    io_time := trunc(io_time,
                  case i_entity_int_type    
                    when c_int_type_hour then 'HH24'        
                    when c_int_type_day then 'DD'
                    when c_int_type_month then 'MM'
                    when c_int_type_year then 'YYYY'
                   end);
    o_suffix :=  to_char(io_time, c_suffix_time_range_format);
  end get_time_suffix;  
 
  function is_ds_interval_type(i_part_int_type in com_time_range_part_tables.part_range_int_type%type)
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Returns TRUE if a given interval type could be converted using interval day to second function 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_part_int_type - Partition or tablespace range interval type in ('HOUR','DAY','MONTH','YEAR')
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    no any known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return (i_part_int_type in (c_int_type_hour, c_int_type_day));
  end is_ds_interval_type; 
  
  function get_num_months_by_int_type(i_part_int_type in com_time_range_part_tables.part_range_int_type%type)
  return number
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Returns number of months covered by a given interval type (12 for YEAR and 1 for MONTH)
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_part_int_type - Partition or tablespace range interval type in ('HOUR','DAY','MONTH','YEAR')
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    no any known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return case i_part_int_type 
            when c_int_type_month then 1 
            when c_int_type_year  then 12
            else 0
          end;
  end get_num_months_by_int_type; 
 
  function get_entity_name_start_times(i_entity_int_cnt         in com_time_range_part_tables.part_range_int_cnt%type, 
                                       i_entity_int_type        in com_time_range_part_tables.part_range_int_type%type,
                                       i_entity_advance         in com_time_range_part_tables.no_parts_advance%type,
                                       i_time_reference         in timestamp)
  return table_of_timestamp
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Returns table of timestamp, used for reference during the suffix generation
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_entity_int_cnt 
--       The number of intervals to be contained in a single entity (partition, tablespace).
--    i_entity_int_type 
--       The type of intervals to be contained in a single entity (e.g. HOUR, DAY, MONTH, YEAR).
--    i_entity_advance 
--       The no of time range entities to be created in advance of the current time.
--    i_time_reference
--       The starting date to use for the first entity to add.
--    i_get_past_period_times
--       The flag to convert the function for creating start times in the past. If this flag is set to TRUE then the
--       variable i_entity_advance will be used for number of entities to be created in the past
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    raises INTERNAL_ERROR in case of any exception
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_part_name_tstamps       table_of_timestamp;
    l_num_months_in_interval  pls_integer;
  begin
  
    if is_ds_interval_type(i_entity_int_type) then
      select i_time_reference + ((rownum-1)* part_interval)
      bulk collect into l_part_name_tstamps
      from (
        select numtodsinterval(i_entity_int_cnt,i_entity_int_type) part_interval
        from dual
      )
      where systimestamp + ((i_entity_advance+1)*part_interval) > i_time_reference + (rownum*part_interval)      
      connect by 1=1 and systimestamp + ((i_entity_advance+1)*part_interval) > i_time_reference + (rownum*part_interval);     
      
    else 
      l_num_months_in_interval := get_num_months_by_int_type(i_entity_int_type);
      select part_name_tstamps
      bulk collect into l_part_name_tstamps
      from (
        select add_months(i_time_reference, ((rownum-1)*i_entity_int_cnt*l_num_months_in_interval)) part_name_tstamps
        from dual
        connect by 1=1 and rownum <= i_entity_advance + 1
      )
      where add_months(systimestamp, (i_entity_advance*l_num_months_in_interval)) > part_name_tstamps;
      
    end if;
    
    return l_part_name_tstamps;
  exception
    when others then
      com_event_mgr.raise_event(c_internal_error,'Error during the entity name time reference calculation');
  end get_entity_name_start_times;
  
  function tablespace_exists(i_tablespace_name in com_time_range_part_tables.tablespace_full_name%type)
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Returns TRUE if a tablespace with name i_tablespace_name exists
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_tablespace_name 
--       Tablespace name to check whether exist in the database
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    no any exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is  
    l_tablespace_cnt pls_integer;
  begin
    select count(*) 
    into l_tablespace_cnt
    from user_tablespaces 
    where tablespace_name = i_tablespace_name;
    
    return l_tablespace_cnt > 0 ;
  end tablespace_exists;

  function get_dynamic_ts_name(i_ts_name_prefix in com_time_range_tablespaces.ts_name_prefix%type,
                               i_part_timestamp       in timestamp)
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Returns full tablespace name to be used for creating partitions set to dynamic tablespace
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_tablespace_prefix 
--       Tablespace prefix used by tablespace_manager to generate tablespaces dynamicaly 
--    i_part_timestamp 
--       Timestamp reference of the given partition to be created in the tablespace
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    no any exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_tablespace_full_name   com_time_range_part_tables.tablespace_full_name%type;
    l_suffix            varchar2(10);
    l_ts_range_int_type com_time_range_tablespaces.ts_range_int_type%type;
    l_part_timestamp    timestamp;
  begin
     l_part_timestamp := i_part_timestamp;
     
    select  ts_range_int_type 
    into l_ts_range_int_type
    from com_time_range_tablespaces
    where ts_name_prefix = i_ts_name_prefix;
  
    get_time_suffix(l_part_timestamp, l_ts_range_int_type, l_suffix);
    l_tablespace_full_name := com_string_utils.get_as_valid_oracle_identifier('_', i_ts_name_prefix, l_suffix);
    
    return l_tablespace_full_name;
  exception
    when no_data_found then
      com_event_mgr.raise_event(c_internal_error,'Tablespace prefix '||i_ts_name_prefix||  
                                                            ' is not set for dynamic tablespace management!');
  end get_dynamic_ts_name;
    
  function get_entity_keep_time_ref(i_no_keep        in com_time_range_part_tables.no_parts_keep%type,
                                    i_range_int_cnt  in com_time_range_part_tables.part_range_int_cnt%type,
                                    i_range_int_type in com_time_range_part_tables.part_range_int_type%type)
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Returns the timestamp of the oldest entity (partition or tablespace) we should not modify
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_no_keep 
--       The number of entity units we must keep in the creation state 
--    i_range_int_cnt 
--       The number of intervals to be contained in a single entity (partition, tablespace).
--    i_range_int_type 
--       The type of intervals to be contained in a single entity (e.g. HOUR, DAY, MONTH, YEAR).
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    no any exceptions defined
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_time_reference timestamp;
  begin
    if is_ds_interval_type(i_range_int_type) then
      l_time_reference := systimestamp - (i_no_keep * numtodsinterval(i_range_int_cnt,i_range_int_type));
      
    else 
      l_time_reference := add_months(systimestamp, -(i_no_keep * i_range_int_cnt * get_num_months_by_int_type(i_range_int_type)));
               
    end if;
      
    return l_time_reference;
  end get_entity_keep_time_ref;
   
  procedure establish_part_attributes(
    o_part_suffix         out varchar2,
    o_part_high_value     out timestamp,
    i_part_range_int_cnt  in  com_time_range_part_tables.part_range_int_cnt%type  default 1,
    i_part_range_int_type in  com_time_range_part_tables.part_range_int_type%type default 'DAY',
    i_part_start_time     in  timestamp default systimestamp
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-14 Nikolay Tsvetkov
--    Added new input parameter i_part_start_time with systimestamp default value
--  2011-04-12  Chris Roderick
--    Modified the default input value of i_part_range_int_type to be DAY instead of HOUR
--  2011-03-15 - Zereyakob Makonnen,Eve Fortescue-Beck
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_part_start_time timestamp;
  begin
    l_part_start_time := i_part_start_time;
    
    get_time_suffix(l_part_start_time, i_part_range_int_type, o_part_suffix); 
    
    if is_ds_interval_type(i_part_range_int_type) then
      o_part_high_value :=  l_part_start_time + numtodsinterval(i_part_range_int_cnt, i_part_range_int_type);         
    else   
      o_part_high_value :=  add_months(l_part_start_time, (i_part_range_int_cnt * get_num_months_by_int_type(i_part_range_int_type)));  
      
    end if;
  
  exception
    when others then
      com_event_mgr.raise_error_event(c_internal_error, 'Failed to extablish attributes' || dbms_utility.format_error_stack);  
  end establish_part_attributes;


  procedure reg_time_range_tablespaces(
      i_ts_name_prefix          in com_time_range_tablespaces.ts_name_prefix%type,
      i_ts_range_int_cnt        in com_time_range_tablespaces.ts_range_int_cnt%type         default 1,
      i_ts_range_int_type       in com_time_range_tablespaces.ts_range_int_type%type        default 'DAY',
      i_ts_datafile_size_mb     in com_time_range_tablespaces.ts_datafile_size_mb%type      default null,
      i_ts_datafile_extend_mb   in com_time_range_tablespaces.ts_datafile_extend_mb%type    default null,
      i_ts_datafile_maxsize_mb  in com_time_range_tablespaces.ts_datafile_maxsize_mb%type  default null,
      i_no_ts_advance           in com_time_range_tablespaces.no_ts_advance%type            default 5,
      i_no_ts_keep_online       in com_time_range_tablespaces.no_ts_keep_online%type        default null,
      i_no_ts_keep_total        in com_time_range_tablespaces.no_ts_keep_total%type         default null,
      i_ts_is_bigfile           in com_time_range_tablespaces.ts_is_bigfile%type            default 'N'
      )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin    
    merge into com_time_range_tablespaces 
      using (
        select i_ts_name_prefix ts_name_prefix, i_ts_datafile_size_mb ts_datafile_size_mb, 
          i_ts_datafile_extend_mb ts_datafile_extend_mb, i_ts_datafile_maxsize_mb ts_datafile_maxsize_mb, 
          i_ts_range_int_cnt ts_range_int_cnt, i_ts_range_int_type ts_range_int_type, i_no_ts_advance no_ts_advance, 
          i_no_ts_keep_online no_ts_keep_online, i_no_ts_keep_total no_ts_keep_total, i_ts_is_bigfile ts_is_bigfile
        from dual
        )temp on (temp.ts_name_prefix = com_time_range_tablespaces.ts_name_prefix)
    when matched then update
      set ts_datafile_size_mb = temp.ts_datafile_size_mb,
          ts_datafile_extend_mb = temp.ts_datafile_extend_mb,
          ts_datafile_maxsize_mb = temp.ts_datafile_maxsize_mb,
          ts_range_int_cnt = temp.ts_range_int_cnt,
          ts_range_int_type = temp.ts_range_int_type,
          no_ts_advance = temp.no_ts_advance,
          no_ts_keep_online = temp.no_ts_keep_online,
          no_ts_keep_total = temp.no_ts_keep_total,
          ts_is_bigfile = temp.ts_is_bigfile
    when not matched then 
      insert(ts_name_prefix, ts_datafile_size_mb, ts_datafile_extend_mb, ts_datafile_maxsize_mb, ts_range_int_cnt, 
              ts_range_int_type, no_ts_advance, no_ts_keep_online, no_ts_keep_total, ts_is_bigfile)
      values(temp.ts_name_prefix, temp.ts_datafile_size_mb, temp.ts_datafile_extend_mb, temp.ts_datafile_maxsize_mb, 
        temp.ts_range_int_cnt, temp.ts_range_int_type, temp.no_ts_advance, temp.no_ts_keep_online, 
        temp.no_ts_keep_total, temp.ts_is_bigfile);
    
  exception
    when others then
      com_event_mgr.raise_error_event(c_internal_error, 'Tablespace '||i_ts_name_prefix||
                                                              ' : problem during the registration.');
  end reg_time_range_tablespaces;
    
  function get_com_time_range_tablespaces(i_ts_name_prefix in com_time_range_tablespaces.ts_name_prefix%type default null)
  return t_com_time_range_tablespaces
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns collection of time range tablespaces to be managed
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_ts_prefix 
--      The prefix of the time range tablespace to be managed.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    no any exceptions defined
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_com_time_range_tablespaces t_com_time_range_tablespaces;
  begin
     select * bulk collect into l_com_time_range_tablespaces
     from com_time_range_tablespaces 
     where (ts_name_prefix = i_ts_name_prefix and i_ts_name_prefix is not null) or (i_ts_name_prefix is null);
     return l_com_time_range_tablespaces;
  end get_com_time_range_tablespaces;
  
  function create_ts_names_collection(i_com_time_range_tablespaces   in com_time_range_tablespaces%rowtype,
                                      i_num_tablespace_names_advance in com_time_range_tablespaces.no_ts_advance%type,
                                      i_start_time_reference         in timestamp)
    return table_of_varchar 
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Creates collection of tablespace names to be used for operations over the tablespaces
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_time_range_tablespaces 
--      Collection with all the configuration parameters for a given time range tablespace
--    i_number_tablespace_names
--      Number of tablespace names to be generated
--    i_start_time_reference
--      The time that will be used for reference as a starting point
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    raises INTERNAL_ERROR on any exception
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_time_reference          timestamp ;
    l_tablespace_start_times  table_of_timestamp;
    l_table_of_ts_names       table_of_varchar;
    l_suffix                  varchar2(10);
  begin
    l_table_of_ts_names := table_of_varchar();
    l_tablespace_start_times := get_entity_name_start_times(i_com_time_range_tablespaces.ts_range_int_cnt, 
                                                            i_com_time_range_tablespaces.ts_range_int_type, 
                                                            i_num_tablespace_names_advance, 
                                                            i_start_time_reference);
                                                    
    for r_time in 1..l_tablespace_start_times.count loop
       get_time_suffix(l_tablespace_start_times(r_time),i_com_time_range_tablespaces.ts_range_int_type,l_suffix);
       l_table_of_ts_names.extend;
       l_table_of_ts_names(l_table_of_ts_names.count) := com_string_utils.get_as_valid_oracle_identifier('_', 
                                                                 i_com_time_range_tablespaces.ts_name_prefix, l_suffix);  
    end loop;

   return l_table_of_ts_names;
  exception
    when others then
      com_event_mgr.raise_error_event(c_internal_error, 'Failed to build TS names collection'||dbms_utility.format_error_stack);  
  end create_ts_names_collection;
  
  procedure add_new_tablespaces(i_ts_prefix  in com_time_range_tablespaces.ts_name_prefix%type default null)  
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Creates new time range tablespaces base d on the configuration defind in table com_time_range_tablespaces.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_ts_prefix 
--      The prefix of the time range tablespace to be managed.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    raises INTERNAL_ERROR on any exception
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_sql_create_ts_stmt    varchar2(1000); 
    l_tablespace_name       user_ts_quotas.tablespace_name%type;
    l_ts_datafile_size_mb   com_time_range_tablespaces.ts_datafile_size_mb%type;
    l_ts_datafile_maxsize_mb   com_time_range_tablespaces.ts_datafile_maxsize_mb%type;
          
    l_com_time_range_tablespaces  t_com_time_range_tablespaces;
    l_tablespace_names_to_create  table_of_varchar;
    
    c_ratio_df_maxsize_to_initsize constant number := 1.5;
    
    function get_ts_max_time_reference(i_ts_prefix in com_time_range_tablespaces.ts_name_prefix%type)
    return timestamp
    is
      l_time_reference timestamp;
      
      function is_first_tablespace
      return boolean
      is
        l_ts_cnt pls_integer; 
      begin
        select count(*)
        into l_ts_cnt
        from user_tablespaces
        where tablespace_name like i_ts_prefix||'\_%' escape'\';
      
        return l_ts_cnt = 0;
      end is_first_tablespace;

    begin
      if is_first_tablespace then
        l_time_reference := systimestamp ;
      else 
        select max(to_timestamp(replace(tablespace_name,i_ts_prefix||'_',''),'YYYYMMDD HH24:MI:SS'))
        into l_time_reference
        from user_tablespaces
        where tablespace_name like i_ts_prefix||'\_%' escape'\';
      end if;
      
      return l_time_reference;
    end get_ts_max_time_reference;
  
    function get_estimated_ts_datafile_size(i_com_time_range_tablespaces in com_time_range_tablespaces%rowtype)
    return com_time_range_tablespaces.ts_datafile_size_mb%type
    is
      c_num_ts_used_for_estimation   constant pls_integer := 30;
      c_pct_to_overestimate          constant pls_integer := 20;
      c_num_ts_advance               constant pls_integer := 0;
      
      l_ts_datafile_size             com_time_range_tablespaces.ts_datafile_size_mb%type;      
      l_tablespace_names_to_inspect  table_of_varchar;
     
    begin
      l_tablespace_names_to_inspect := create_ts_names_collection(i_com_time_range_tablespaces,
                                                c_num_ts_advance,
                                                get_entity_keep_time_ref(c_num_ts_used_for_estimation,
                                                                    i_com_time_range_tablespaces.ts_range_int_cnt,
                                                                    i_com_time_range_tablespaces.ts_range_int_type));
  
      select ceil(max(ts_size)*(1 + c_pct_to_overestimate/100))
      into l_ts_datafile_size
      from (
        select (sum(bytes)/1024/1024) ts_size, tablespace_name 
        from user_segments 
        where tablespace_name in (select column_value from table(l_tablespace_names_to_inspect))
        group by tablespace_name);
  
      return coalesce(l_ts_datafile_size,0);
    end get_estimated_ts_datafile_size;
    
  begin          
    l_com_time_range_tablespaces := get_com_time_range_tablespaces(i_ts_prefix);
    
    for r_ts_config in 1..l_com_time_range_tablespaces.count  loop
      l_ts_datafile_size_mb := greatest(coalesce(l_com_time_range_tablespaces(r_ts_config).ts_datafile_size_mb,0), 
                                        get_estimated_ts_datafile_size(l_com_time_range_tablespaces(r_ts_config)));
      
      l_ts_datafile_maxsize_mb := greatest(ceil(l_ts_datafile_size_mb*c_ratio_df_maxsize_to_initsize), 
                                          coalesce(l_com_time_range_tablespaces(r_ts_config).ts_datafile_maxsize_mb ,0));
                                        
      l_tablespace_names_to_create := create_ts_names_collection(l_com_time_range_tablespaces(r_ts_config),
                                        l_com_time_range_tablespaces(r_ts_config).no_ts_advance,
                                        get_ts_max_time_reference(l_com_time_range_tablespaces(r_ts_config).ts_name_prefix));    
    
    for r_ts_name in 1..l_tablespace_names_to_create.count loop
      l_tablespace_name := l_tablespace_names_to_create(r_ts_name);
      if not tablespace_exists(l_tablespace_name) then
        l_sql_create_ts_stmt:= 'create ' ||
          case when l_com_time_range_tablespaces(r_ts_config).ts_is_bigfile = 'Y' then 'bigfile tablespace ' else 'tablespace ' end || 
          l_tablespace_name ||
          case when l_ts_datafile_size_mb is not null and l_ts_datafile_size_mb > 0 
              then ' datafile size '|| l_ts_datafile_size_mb||'m ' else null end ||
          case when l_com_time_range_tablespaces(r_ts_config).ts_datafile_extend_mb is not null then ' autoextend on next '||
            l_com_time_range_tablespaces(r_ts_config).ts_datafile_extend_mb||'m ' else null end ||
          case when l_ts_datafile_maxsize_mb is not null and l_ts_datafile_maxsize_mb > 0 
              then ' maxsize '|| l_ts_datafile_maxsize_mb ||'m ' else null end ||
          ' extent management local segment space management auto';
        com_logger.debug(l_sql_create_ts_stmt, $$PLSQL_UNIT);
        
        execute immediate l_sql_create_ts_stmt;
      end if;
    end loop;
  end loop;
    
  exception
    when others then
      com_event_mgr.raise_error_event(c_internal_error, 'Tablespace prefix:'||l_tablespace_name||
                                                                           ' '||dbms_utility.format_error_stack);
  end add_new_tablespaces;
    
  
  procedure assign_tablespaces_read_only(i_ts_prefix  in com_time_range_tablespaces.ts_name_prefix%type  default null)
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Changes status of the tablespaces to READ ONLY. It applies only over tablespaces which are outside the defined  
--    interval indicating how long we need to keep them in ONLINE mode.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_ts_prefix 
--      The prefix of the time range tablespace to be managed.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    raises INTERNAL_ERROR on any exception
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_sql_assign_ro_stmt          varchar2(100) := 'alter tablespace #tablespace_name read only'; 
        
    l_com_time_range_tablespaces  t_com_time_range_tablespaces;
    l_tablespace_names_assign_ro  table_of_varchar;
    
    procedure establish_ts_assign_readonly(i_com_time_range_tablespaces in com_time_range_tablespaces%rowtype)   
    is
      l_tablespace_names_keep_online    table_of_varchar;
      l_start_time_reference            timestamp;
      
      c_tablespace_status_online constant varchar2(6) := 'ONLINE';
    begin
      l_tablespace_names_keep_online := 
                create_ts_names_collection(i_com_time_range_tablespaces,
                                           i_com_time_range_tablespaces.no_ts_advance,
                                           get_entity_keep_time_ref(i_com_time_range_tablespaces.no_ts_keep_online,
                                                                    i_com_time_range_tablespaces.ts_range_int_cnt,
                                                                    i_com_time_range_tablespaces.ts_range_int_type));

      select ut.tablespace_name 
      bulk collect into l_tablespace_names_assign_ro
      from user_tablespaces ut
      where ut.tablespace_name like i_com_time_range_tablespaces.ts_name_prefix || '%'
      and ut.status = c_tablespace_status_online
      and ut.tablespace_name not in (select column_value from table(l_tablespace_names_keep_online));
    end establish_ts_assign_readonly;
    
  begin
   l_com_time_range_tablespaces := get_com_time_range_tablespaces(i_ts_prefix);
      
      for r_tablespace_config in 1..l_com_time_range_tablespaces.count  loop
        if l_com_time_range_tablespaces(r_tablespace_config).no_ts_keep_online is not null then
          establish_ts_assign_readonly(l_com_time_range_tablespaces(r_tablespace_config));
          
          for r_ts_name in 1..l_tablespace_names_assign_ro.count loop
            com_logger.debug(replace(l_sql_assign_ro_stmt,'#tablespace_name',l_tablespace_names_assign_ro(r_ts_name)), $$PLSQL_UNIT);
            execute immediate replace(l_sql_assign_ro_stmt,'#tablespace_name',l_tablespace_names_assign_ro(r_ts_name));
          end loop;    
        end if;
    end loop;
  exception
    when others then
      com_event_mgr.raise_error_event(c_internal_error, 'Tablespace prefix:'||i_ts_prefix ||
                                                                    ' '||dbms_utility.format_error_stack);
  end assign_tablespaces_read_only;
  
  procedure drop_obsolete_tablespaces(i_ts_prefix  in com_time_range_tablespaces.ts_name_prefix%type  default null)
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Drops empty tablespaces which are outside the defined interval for a given time range teablespace
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_ts_prefix 
--      The prefix of the time range tablespace to be managed.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    raises INTERNAL_ERROR on any exception
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_sql_drop_stmt               varchar2(100) := 'drop tablespace #tablespace_name';   
    l_com_time_range_tablespaces  t_com_time_range_tablespaces;
    l_tablespace_names_to_drop    table_of_varchar;
     
    procedure establish_ts_to_drop(i_com_time_range_tablespaces in com_time_range_tablespaces%rowtype)   
    is
      l_tablespace_names_to_keep    table_of_varchar;
      
    begin
      l_tablespace_names_to_keep := create_ts_names_collection(i_com_time_range_tablespaces,
                                       i_com_time_range_tablespaces.no_ts_advance,
                                       get_entity_keep_time_ref(i_com_time_range_tablespaces.no_ts_keep_total,
                                                                i_com_time_range_tablespaces.ts_range_int_cnt,
                                                                i_com_time_range_tablespaces.ts_range_int_type));

      select ut.tablespace_name 
      bulk collect into l_tablespace_names_to_drop
      from user_tablespaces ut
      where ut.tablespace_name like i_com_time_range_tablespaces.ts_name_prefix || '%'
      and not exists (select null from user_segments us where ut.tablespace_name = us.tablespace_name and us.segment_type not like 'TEMPORARY')
      and ut.tablespace_name not in (select column_value from table(l_tablespace_names_to_keep));
    end establish_ts_to_drop;
    
  begin
    l_com_time_range_tablespaces := get_com_time_range_tablespaces(i_ts_prefix);
    
    for r_tablespace_config in 1..l_com_time_range_tablespaces.count  loop
      if l_com_time_range_tablespaces(r_tablespace_config).no_ts_keep_total is not null then
        establish_ts_to_drop(l_com_time_range_tablespaces(r_tablespace_config));
        
        for r_ts_name in 1..l_tablespace_names_to_drop.count loop
          com_logger.debug(replace(l_sql_drop_stmt,'#tablespace_name',l_tablespace_names_to_drop(r_ts_name)), $$PLSQL_UNIT);
          execute immediate replace(l_sql_drop_stmt,'#tablespace_name',l_tablespace_names_to_drop(r_ts_name));      
        end loop;
      end if;
    end loop;
  exception
    when others then
    com_event_mgr.raise_error_event(c_internal_error, 'Tablespace prefix:'||i_ts_prefix||
                                                              ' '||dbms_utility.format_error_stack);
  end drop_obsolete_tablespaces;
  
  procedure manage_tablespaces(i_ts_name_prefix in com_time_range_tablespaces.ts_name_prefix%type default null)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-01 - Nikolay Tsvetkov
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
      add_new_tablespaces(i_ts_name_prefix);
      assign_tablespaces_read_only(i_ts_name_prefix);  
      drop_obsolete_tablespaces(i_ts_name_prefix);
  exception
    when others then
      com_event_mgr.raise_event(c_internal_error,'Failed to manage tablespaces !' ||  dbms_utility.format_error_stack, true);
  end manage_tablespaces;

  procedure add_new_partition(i_table_name      in user_tables.table_name%type, 
                              i_time_reference  in timestamp)
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Add new partitions to the partitioned table.
--    NOTICE: The table must exist and be a time range partitioned table.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name
--      The name of the time range partitioned table to which partitions should be added.
--    i_time_reference
--      The starting date to use for the first partition to add.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INTERNAL_ERROR - raised when some problem accour during the creation of a new partition.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01  Nikolay Tsvetkov
--      Refactoring.
--    2012-12-14  Chris Roderick
--      Modified the code which determines the timestamps of future range partitions based on MONTHS 
--      or YEARS to consider creating up to 10000 partitions for months, but only 10 partitions for 
--      YEARS, from the starting partition (otherwise we will overflow on the number of years). 
--    2012-10-25  Chris Roderick
--      Modified the code which determines the timestamps of future range partitions based on MONTHS 
--      or YEARS to consider creating up to 10000 partitions from the starting partition, (determined 
--      by the attribute which specifies the number of partitions in advance of now, which should be
--      created). Before making this change, the behaviour was as follows... Given a starting 
--      partition of 1 year ago, configured to have monthly partitions created 3 months in advance 
--      of now, running the partition manager would only create 3 partitions after the start 
--      partition, instead of the past year (i.e. 12 partitions) and then 3 months in advance of now
--      (i.e. another 3 partitions - so 15 in total).
--    2012-05-31  Chris Roderick
--      Modified the code which determines the timestamps of future range partitions based on MONTHS 
--      or YEARS to use the add_months function, since adding intervals to dates which don't exist  
--      in future months (e.g. 31st May + 1 MONTH = 31st July) will cause exceptions.
--    2012-01-26  Chris Roderick
--      Modified the code which generates the details of the partitions to be created to ...
--      Changed l_tablespace_name type from varchar2(4000) to user_ts_quotas.tablespace_name%type;
--      Changed i_table_name type from varchar2 to user_tables.table_name%type;
--    2011-03-08 - Zereyakob Makonnen
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_suffix           varchar2(10);    
    l_tablespace_name  user_ts_quotas.tablespace_name%type;
    l_tab_time_range_part  com_time_range_part_tables%rowtype;

    l_part_name_tstamps table_of_timestamp;
    l_part_hval_tstamps timestamp;
    
    procedure add_range_partition(i_tab_name      in  user_tables.table_name%type,
                                  i_part_name     in  user_tab_partitions.partition_name%type,
                                  i_high_value    in  timestamp,
                                  i_tablespace    in  user_tablespaces.tablespace_name%type,
                                  i_init_size_kb  in  com_time_range_part_tables.part_init_size_kb%type,
                                  i_part_pct_free in  com_time_range_part_tables.part_pct_free%type)
    is	
      l_sql_stmt  varchar2(2000);                      
    begin
      l_sql_stmt := 'ALTER TABLE '||i_tab_name||' ADD PARTITION '||i_part_name||
                    ' VALUES LESS THAN (TIMESTAMP '''||to_char(i_high_value, 'YYYY-MM-DD HH24:MI:SS')||''')'||
                    case when i_tablespace is not null then ' TABLESPACE '||i_tablespace else '' end ||
                    case when i_init_size_kb is not null then ' STORAGE(INITIAL '||i_init_size_kb||'k)' else '' end ||
                    ' PCTFREE '||case when i_part_pct_free is not null then i_part_pct_free else '10' end;

       execute immediate l_sql_stmt;	     
    exception
      when others then
        com_event_mgr.raise_error_event(c_internal_error, 'Fail to add range partition using: '||l_sql_stmt||
                                          ' Err: '||dbms_utility.format_error_stack);
    end add_range_partition;
  
  begin
    select *
    into l_tab_time_range_part
    from com_time_range_part_tables 
    where table_name = i_table_name;   
    
    l_part_name_tstamps := get_entity_name_start_times(l_tab_time_range_part.part_range_int_cnt, 
                                                       l_tab_time_range_part.part_range_int_type, 
                                                       l_tab_time_range_part.no_parts_advance, 
                                                       i_time_reference);
                                                       
    for i in 1..l_part_name_tstamps.count 
    loop   

      establish_part_attributes(l_suffix, l_part_hval_tstamps, l_tab_time_range_part.part_range_int_cnt, 
                                 l_tab_time_range_part.part_range_int_type,l_part_name_tstamps(i));
                                 
      if l_tab_time_range_part.tablespace_full_name is not null 
        and tablespace_exists(l_tab_time_range_part.tablespace_full_name) then
          l_tablespace_name := l_tab_time_range_part.tablespace_full_name;
        
      elsif l_tab_time_range_part.dynamic_tablespace_prefix is not null then
        l_tablespace_name := get_dynamic_ts_name(l_tab_time_range_part.dynamic_tablespace_prefix, i_time_reference);
        if not tablespace_exists(l_tablespace_name) then
          com_event_mgr.raise_error_event(c_internal_error, 'Fail to create range partition using tablespace: '
                                                                ||l_tablespace_name||' The tablespace is not created.');
        end if;
      end if;
      
      add_range_partition(i_table_name, 
                      com_string_utils.get_as_valid_oracle_identifier('_', l_tab_time_range_part.part_prefix, l_suffix), 
                      l_part_hval_tstamps, l_tablespace_name, l_tab_time_range_part.part_init_size_kb, 
                      l_tab_time_range_part.part_pct_free);
    end loop;
    
  exception
    when no_data_found then
      com_event_mgr.raise_error_event(c_internal_error,'NO DATA FOUND '|| dbms_utility.format_error_stack );
  end add_new_partition;

  procedure drop_range_partition(i_table_name in com_time_range_part_tables.table_name%type)
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Drop time range partitions which are over the interval we must keep in the DB
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name 
--       Time range partitioned table registered for partition management
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    raises INTERNAL_ERROR on any exception
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-12-01 - Nikolay Tsvetkov
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_time_range_part_tables com_time_range_part_tables%rowtype;
    
    procedure drop_time_range_part(i_table_name     in com_time_range_part_tables.table_name%type,
                                   i_time_reference in timestamp)
    is
    begin
      for i in (select 'ALTER TABLE '||table_name||' DROP PARTITION '||partition_name||'  UPDATE GLOBAL INDEXES' stmt, 
                  high_value
                from user_tab_partitions 
                where table_name = i_table_name   
                order by partition_position)
      loop
        if(long_to_timestamp(i.high_value) < i_time_reference) then     
           execute immediate i.stmt;
           com_logger.log_entry(com_logger.c_info,'Partition deleted is :'||i.stmt);
        else 
          exit;
        end if;
      end loop;
    
    end drop_time_range_part;
    
  begin
    select * 
    into l_time_range_part_tables
    from com_time_range_part_tables
    where table_name = i_table_name;

    drop_time_range_part(l_time_range_part_tables.table_name,
                            get_entity_keep_time_ref(l_time_range_part_tables.no_parts_keep,
                                                       l_time_range_part_tables.part_range_int_cnt,
                                                       l_time_range_part_tables.part_range_int_type));
  exception
    when others then
       com_event_mgr.raise_error_event(c_internal_error, 'Fail to drop time range partition for table '
                                        ||i_table_name ||' '||dbms_utility.format_error_stack );
  end drop_range_partition;
  
  procedure manage_partitions(i_table_name in com_time_range_part_tables.table_name%type default null)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2016-07-28 - Jose Rolland
--      Notify FAILED_MANAGE_PARTITION non-blocking error when processing non-existing table. OCOMM-436
--    2015-12-01 - Nikolay Tsvetkov
--      Refactoring.
--    2013-05-30 - Chris Roderick
--      Bug fix when establishing the oldest partition to keep - now using add_months procedure
--    2011-03-15 - Zereyakob Makonnen
--      Add parameter i_table_name
--    2011-01-20 - Zereyakob Makonnen
--      Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_time_reference      timestamp;
    
    procedure assert_tab_reg_for_part_mgmt(i_table_name in varchar2)
    is
      l_table_cnt pls_integer;
    begin
      select count(*)
      into l_table_cnt
      from user_tab_partitions utp
      join com_time_range_part_tables com_trp on (com_trp.table_name = utp.table_name)
      join user_part_tables upt on (utp.table_name = upt.table_name and utp.partition_position = upt.partition_count)
      where utp.table_name = i_table_name;
    
      com_assert.is_true(l_table_cnt = 1, 
           'The table '||i_table_name||' is either not partitioned or not registered for commons partition management');
    end assert_tab_reg_for_part_mgmt;
  
  begin
    add_new_tablespaces;
    
    if i_table_name is not null then
      assert_tab_reg_for_part_mgmt(i_table_name);
    end if;
    
    for part in (select com_trp.table_name, utp.high_value, upt.partition_count, com_trp.no_parts_keep,
                    case when upt.table_name is not null then 'Y' else 'N' end table_exists
                 from com_time_range_part_tables com_trp
                 left join user_part_tables upt on (com_trp.table_name = upt.table_name)
                 left join user_tab_partitions utp on (upt.table_name = utp.table_name 
                                                         and upt.partition_count = utp.partition_position) 
                 where (com_trp.table_name = i_table_name and i_table_name is not null ) or (i_table_name is null)) 
    loop
      if com_util.char_to_boolean(part.table_exists) then
        l_time_reference := long_to_timestamp(part.high_value);  
        com_logger.debug('tref = '||l_time_reference, $$PLSQL_UNIT);
        
        add_new_partition(part.table_name ,l_time_reference);
           
        if(part.no_parts_keep is not null and part.no_parts_keep < part.partition_count) then
          drop_range_partition(part.table_name);
        end if;
      else
        com_event_mgr.raise_event('FAILED_MANAGE_PARTITION','Failed to manage partitions on ' || part.table_name ||
          '. Table registered in com_time_range_part_tables as ' || part.table_name || 
          ' does not exist or might have been renamed. Please update com_time_range_part_tables accordingly.');
      end if;
    end loop;
    
    assign_tablespaces_read_only;
    drop_obsolete_tablespaces;
  exception
   when others then
      com_event_mgr.raise_error_event(c_internal_error,'Failed to manage partitions !'||dbms_utility.format_error_stack);
  end manage_partitions;
  
end com_partition_mgr;