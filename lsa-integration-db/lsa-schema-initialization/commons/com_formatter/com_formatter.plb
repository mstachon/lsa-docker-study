create or replace package body com_formatter 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2010-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Provides utilities to transform data (ref_cursor, varchar2, etc.) into different formats (HTML table, JIRA table)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-20 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-405
--  2015-09-09 Jose Rolland
--    Changed ref_cursor_to_jira_table to handle null values [OCOMM-334]
--  2015-08-20 Chris Roderick
--    Formatting
--  2015-07-23 Pascal Le Roux
--    Added functions ref_cursor_to_json and select_stmt_to_json
--  2014-07-30 Gkergki Strakosa
--    Added (overloaded) to_string to convert nested tables of different data types to string.
--  2014-07-03 Pascal Le Roux
--    Changed the output method from "text" to "html" in ref_cursor_to_jira_table
--  2014-07-01 Pascal Le Roux
--    Changed default value of i_disable_output_escaping to "NO" in ref_cursor_to_html_table 
--    and ref_cursor_to_jira_table
--  2014-05-16 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  c_yes_no_supported_values constant table_of_varchar := table_of_varchar(com_constants.c_yes, com_constants.c_no);

  function ref_cursor_to_html_table (
    i_ref_cu                  in sys_refcursor, 
    i_disable_output_escaping in com_types.t_yes_no default com_constants.c_no
  )
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-20 Jose Rolland
--    Added assertion on i_disable_output_escaping - OCOMM-405
--  2014-07-01 Pascal Le Roux
--    Changed default value of i_disable_output_escaping to "NO"
--  2014-05-21 Pascal Le Roux
--    Added Optional parameter i_disable_output_escaping (value: YES/NO) to enable/disable output escaping:
--    "YES" indicates that special characters (like "<") should be output as is. 
--    "NO" indicates that special characters (like "<") should be output in HTML format. 
--    Default is "YES"
--  2014-05-19 Pascal Le Roux
--    Test if l_xml_data is not null prior to process the transformation to avoid
--    ORA-30625: method dispatch on NULL SELF argument is disallowed 
--    in case the ref_cursor doesn't retrun anything
--  2014-05-16 Pascal Le Roux
--    Creation OCOMM-254
------------------------------------------------------------------------------------------------------------------------
  is
    l_return                  clob;
    l_html_output             xmltype;
    l_xsl                     clob;
    l_xml_data                xmltype;
    l_context                 dbms_xmlgen.ctxhandle;
    l_disable_output_escaping com_types.t_yes_no;
  begin
    com_assert.is_true(lower(trim(i_disable_output_escaping)) member of c_yes_no_supported_values, 
        'Parameter i_disable_output_escaping must be yes/no.');
    l_disable_output_escaping := lower(trim(i_disable_output_escaping));
    
    -- Get a handle on the ref cursor
    l_context := dbms_xmlgen.newcontext (i_ref_cu);
    
    -- SetNullHandling to 1 (or 2) to allow null columns to be displayed --
    dbms_xmlgen.setnullhandling (l_context, 1) ;
    
    -- Create XML from ref cursor
    l_xml_data := dbms_xmlgen.getxmltype (l_context, dbms_xmlgen.none);
    
    if l_xml_data is not null then
      
      -- Generic XSL for Oracle's default XML row and rowset tags
      l_xsl := l_xsl || q'[<?xml version="1.0" encoding="ISO-8859-1"?>]';
      l_xsl := l_xsl || q'[<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">]';
      l_xsl := l_xsl || q'[ <xsl:output method="html"/>]';
      l_xsl := l_xsl || q'[ <xsl:template match="/">]';
      l_xsl := l_xsl || q'[   <table>]';
      l_xsl := l_xsl || q'[     <thead>]';
      l_xsl := l_xsl || q'[     <tr>]';
      l_xsl := l_xsl || q'[      <xsl:for-each select="/ROWSET/ROW[1]/*">]';
      l_xsl := l_xsl || q'[       <th><xsl:value-of select="name()"/></th>]';
      l_xsl := l_xsl || q'[      </xsl:for-each>]';
      l_xsl := l_xsl || q'[     </tr>]';
      l_xsl := l_xsl || q'[     </thead>]';
      l_xsl := l_xsl || q'[     <tbody>]';
      l_xsl := l_xsl || q'[     <xsl:for-each select="/ROWSET/*">]';
      l_xsl := l_xsl || q'[      <tr>]';
      l_xsl := l_xsl || q'[       <xsl:for-each select="./*">]';
      l_xsl := l_xsl || q'[        <td><xsl:attribute name="class"><xsl:value-of select="name()"/></xsl:attribute><xsl:value-of select="text()" disable-output-escaping="]'||l_disable_output_escaping||q'["/> </td>]';
      l_xsl := l_xsl || q'[       </xsl:for-each>]';
      l_xsl := l_xsl || q'[      </tr>]';
      l_xsl := l_xsl || q'[     </xsl:for-each>]';
      l_xsl := l_xsl || q'[     </tbody>]';
      l_xsl := l_xsl || q'[   </table>]';
      l_xsl := l_xsl || q'[ </xsl:template>]';
      l_xsl := l_xsl || q'[</xsl:stylesheet>]';
      
      -- XSL transformation to convert XML to HTML
      l_html_output := l_xml_data.transform (xmltype (l_xsl)) ;
      
      -- Convert XMLType to Clob
      l_return := l_html_output.getclobval () ;
      
      -- Close Context
      dbms_xmlgen.closecontext (l_context) ;
    end if;
    
    return l_return;  
  end ref_cursor_to_html_table;

  function ref_cursor_to_jira_table (
    i_ref_cu                   in sys_refcursor,
    i_disable_output_escaping  in com_types.t_yes_no default com_constants.c_no
  )
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-20 Jose Rolland
--    Added assertion on i_disable_output_escaping - OCOMM-405
--  2015-09-09 Jose Rolland
--    Test if column value is null to add a blank space, to avoid breaking rendering of the table in JIRA [OCOMM-334]
--  2014-07-03 Pascal Le Roux
--    Changed the output method from "text" to "html"
--  2014-07-01 Pascal Le Roux
--    Changed default value of i_disable_output_escaping to "NO"
--  2014-06-16 Pascal Le Roux
--    Added missing double quote in com_formatter.ref_cursor_to_jira_table
--      <xsl:value-of select="text()" disable-output-escaping
--  2014-05-21 Pascal Le Roux
--    Added Optional parameter i_disable_output_escaping (value: YES/NO) to enable/disable output escaping:
--    "YES" indicates that special characters (like "<") should be output as is. 
--    "NO" indicates that special characters (like "<") should be output in HTML format.  
--    Default is "YES"
--  2014-05-19 Pascal Le Roux
--    Test if l_xml_data is not null prior to process the transformation to avoid
--    ORA-30625: method dispatch on NULL SELF argument is disallowed 
--    in case the ref_cursor doesn't retrun anything
--  2014-05-16 Pascal Le Roux
--    Creation See JIRA issue https://issues.cern.ch/browse/OCOMM-254
------------------------------------------------------------------------------------------------------------------------is
  is
    l_return      clob;
    l_html_output xmltype;
    l_xsl         clob;
    l_xml_data    xmltype;
    l_context     dbms_xmlgen.ctxhandle;
    l_disable_output_escaping com_types.t_yes_no;
  begin
    com_assert.is_true(lower(trim(i_disable_output_escaping)) member of c_yes_no_supported_values, 
        'Parameter i_disable_output_escaping must be yes/no.');
    l_disable_output_escaping := lower(trim(i_disable_output_escaping));
    
    -- Get a handle on the ref cursor
    l_context := dbms_xmlgen.newcontext (i_ref_cu) ;
    
    -- SetNullHandling to 1 (or 2) to allow null columns to be displayed --
    dbms_xmlgen.setnullhandling (l_context, 1) ;
    
    -- Create XML from ref cursor
    l_xml_data := dbms_xmlgen.getxmltype (l_context, dbms_xmlgen.none) ;
  
    if l_xml_data is not null then
      
      -- Generic XSL for Oracle's default XML row and rowset tags
      l_xsl := l_xsl || q'[<?xml version="1.0" encoding="ISO-8859-1"?>]';
      l_xsl := l_xsl || q'[<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">]';
      l_xsl := l_xsl || q'[<xsl:output method="html"/>]';
      l_xsl := l_xsl || q'[<xsl:variable name="new_line" select="'&#10;'" />]';
      l_xsl := l_xsl || q'[<xsl:template match="/">]';
      l_xsl := l_xsl || q'[<xsl:for-each select="/ROWSET/ROW[1]/*">]';
      l_xsl := l_xsl || q'[||<xsl:value-of select="name()"/>]';
      l_xsl := l_xsl || q'[</xsl:for-each>||<xsl:value-of select="$new_line" />]';
      l_xsl := l_xsl || q'[<xsl:for-each select="/ROWSET/*">]';
      l_xsl := l_xsl || q'[<xsl:for-each select="./*">]';
      l_xsl := l_xsl || q'[|<xsl:choose><xsl:when test="string-length(text())=0"><xsl:text> </xsl:text></xsl:when></xsl:choose><xsl:value-of select="text()" disable-output-escaping="]'||l_disable_output_escaping||q'["/>]';
      l_xsl := l_xsl || q'[</xsl:for-each>|<xsl:value-of select="$new_line" />]';
      l_xsl := l_xsl || q'[</xsl:for-each>]';
      l_xsl := l_xsl || q'[</xsl:template>]';
      l_xsl := l_xsl || q'[</xsl:stylesheet>]';
      
      
      -- XSL transformation to convert XML
      l_html_output := l_xml_data.transform (xmltype (l_xsl)) ;
      
      -- Convert XMLType to Clob
      l_return := l_html_output.getclobval () ;
      
      -- Close Context
      dbms_xmlgen.closecontext (l_context) ;
    
    end if;
    return l_return; 
  end ref_cursor_to_jira_table;


  function ref_cursor_to_json (
    i_ref_cu                   in sys_refcursor, 
    i_disable_output_escaping  in com_types.t_yes_no default com_constants.c_no
  )
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-20 Jose Rolland
--    Added assertion on i_disable_output_escaping - OCOMM-405
--  2015-07-23 Pascal Le Roux
--    Creation OCOMM-326
--    (adapted from http://www.experts-exchange.com/articles/12232/Creating-JSON-from-Oracle-using-SYS-REFCURSOR.html)
------------------------------------------------------------------------------------------------------------------------
  is
    l_result      clob;
    l_xsl         clob;
    l_xml_data    xmltype;
    l_context     dbms_xmlgen.ctxhandle;  
    l_disable_output_escaping com_types.t_yes_no;  
  begin
    com_assert.is_true(lower(trim(i_disable_output_escaping)) member of c_yes_no_supported_values, 
        'Parameter i_disable_output_escaping must be yes/no.');
    l_disable_output_escaping := lower(trim(i_disable_output_escaping));
    
    -- Get a handle on the ref cursor
    l_context := dbms_xmlgen.newcontext (i_ref_cu) ;
    
    -- SetNullHandling to 1 (or 2) to allow null columns to be displayed --
    dbms_xmlgen.setnullhandling (l_context, 1) ;
    
    -- Create XML from ref cursor
    l_xml_data := dbms_xmlgen.getxmltype (l_context, dbms_xmlgen.none) ;
        
    if l_xml_data is not null then
        -- XSL for JSON
        l_xsl := l_xsl || q'[<?xml version="1.0" encoding="ISO-8859-1"?>]';
        l_xsl := l_xsl || q'[<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">]';
        l_xsl := l_xsl || q'[<xsl:output method="html"/>]';
        l_xsl := l_xsl || q'[<xsl:variable name="new_line" select="'&#10;'" />]';
        l_xsl := l_xsl || q'[<xsl:template match="/">]';
        l_xsl := l_xsl || q'[{"metadata":[<xsl:value-of select="$new_line" />]';
        l_xsl := l_xsl || q'[<xsl:for-each select="/ROWSET/ROW[1]/*">]';
        l_xsl := l_xsl || q'[  {"name":"<xsl:value-of select="name()"/>"}]';
        l_xsl := l_xsl || q'[<xsl:choose><xsl:when test="position()!= last()">,<xsl:value-of select="$new_line" /></xsl:when></xsl:choose>]';
        l_xsl := l_xsl || q'[</xsl:for-each>]';      
        l_xsl := l_xsl || q'[],<xsl:value-of select="$new_line" /> "data":[<xsl:value-of select="$new_line" />]';
        l_xsl := l_xsl || q'[<xsl:for-each select="/ROWSET/*">]';
        l_xsl := l_xsl || q'[  {<xsl:for-each select="./*">]';
        l_xsl := l_xsl || q'[<xsl:choose><xsl:when test="position() != 1"><xsl:text>   </xsl:text></xsl:when></xsl:choose>"<xsl:value-of select="name()"/>":"<xsl:value-of select="text()" disable-output-escaping="]'||l_disable_output_escaping||q'["/>"]';
        l_xsl := l_xsl || q'[<xsl:choose><xsl:when test="position()!= last()">,<xsl:value-of select="$new_line" /></xsl:when></xsl:choose>]';
        l_xsl := l_xsl || q'[   </xsl:for-each><xsl:value-of select="$new_line" />]';
        l_xsl := l_xsl || q'[  }<xsl:choose>]';
        l_xsl := l_xsl || q'[      <xsl:when test="position() != last()">,<xsl:value-of select="$new_line" /></xsl:when>]';
        l_xsl := l_xsl || q'[    </xsl:choose>]';
        l_xsl := l_xsl || q'[   </xsl:for-each>]<xsl:value-of select="$new_line" />}]';
        l_xsl := l_xsl || q'[</xsl:template></xsl:stylesheet>]';
      
        -- Convert XMLType to Clob
        l_result := dbms_xmlgen.convert(l_xml_data.transform(xmltype (l_xsl)).getclobval(), dbms_xmlgen.entity_decode);
    
        -- Close Context
        dbms_xmlgen.closecontext (l_context) ;
    end if;
    return l_result;
  end ref_cursor_to_json;

  function select_stmt_to_json (
    i_sql_stmt                 in varchar2, 
    i_disable_output_escaping  in com_types.t_yes_no default com_constants.c_no
  )
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-20 Jose Rolland
--    Added assertion on i_disable_output_escaping - OCOMM-405
--  2015-07-23 Pascal Le Roux
--    Creation OCOMM-327
------------------------------------------------------------------------------------------------------------------------
  is
    l_result      clob;
    l_data        clob;
    l_xsl         clob;
    l_xml_data    xmltype;
    l_html_output xmltype;
    l_context     dbms_xmlgen.ctxhandle;
  
    l_cursor_id   NUMBER;  
    l_refcursor   sys_refcursor;
  
    l_desc_cur    number;
    l_descr_tab   dbms_sql.desc_tab3;
    l_num_cols    number;
    l_row_data    varchar2(500);
    l_disable_output_escaping com_types.t_yes_no;
  
  begin
    com_assert.is_true(lower(trim(i_disable_output_escaping)) member of c_yes_no_supported_values, 
        'Parameter i_disable_output_escaping must be yes/no.');
    l_disable_output_escaping := lower(trim(i_disable_output_escaping));
    
      l_result := '{"metadata":['||chr(10);
      l_cursor_id := dbms_sql.open_cursor;
  
      dbms_sql.parse(l_cursor_id, i_sql_stmt, dbms_sql.native);
  
      dbms_sql.describe_columns3(l_cursor_id, l_num_cols, l_descr_tab);
  
      for i in 1 .. l_num_cols loop
              l_row_data := '  {"name":"'|| l_descr_tab(i).col_name||'"';
              l_row_data := l_row_data ||',"type":';
              case
                      when l_descr_tab(i).col_type = 1 then
                              l_row_data := l_row_data ||'"varchar2"';
                      when l_descr_tab(i).col_type = 2 then
                              l_row_data := l_row_data ||'"number"';
                      when l_descr_tab(i).col_type = 8 then
                              l_row_data := l_row_data ||'"long"';
                      when l_descr_tab(i).col_type = 11 then
                              l_row_data := l_row_data ||'"rowid"';
                      when l_descr_tab(i).col_type = 12 then
                              l_row_data := l_row_data ||'"date"';
                      when l_descr_tab(i).col_type = 96 then
                              l_row_data := l_row_data ||'"char"';
                      when l_descr_tab(i).col_type = 100 then
                              l_row_data := l_row_data ||'"binary_float"';
                      when l_descr_tab(i).col_type = 101 then
                              l_row_data := l_row_data ||'"binary_double"';
                      when l_descr_tab(i).col_type = 109 then
                              l_row_data := l_row_data ||'"user_defined"';
                      when l_descr_tab(i).col_type = 112 then
                              l_row_data := l_row_data ||'"clob"';
                      when l_descr_tab(i).col_type = 113 then
                              l_row_data := l_row_data ||'"blob"';
                      when l_descr_tab(i).col_type = 114 then
                              l_row_data := l_row_data ||'"bfile"';
                      when l_descr_tab(i).col_type = 180 then
                              l_row_data := l_row_data ||'"timestamp"';
                      when l_descr_tab(i).col_type = 181 then
                              l_row_data := l_row_data ||'"timestamp_tz"';
                      when l_descr_tab(i).col_type = 182 then
                              l_row_data := l_row_data ||'"interval_year_to_month"';
                      when l_descr_tab(i).col_type = 183 then
                              l_row_data := l_row_data ||'"interval_day_to_second"';
                      when l_descr_tab(i).col_type = 231 then
                              l_row_data := l_row_data ||'"timestamp_ltz"';
                      else
                              l_row_data := l_row_data ||'"unknown"';
              end case;
              if l_descr_tab(i).col_type in (1,96,180,181,231) then 
                l_row_data := l_row_data||',"max_len":"'||l_descr_tab(i).col_max_len||'"';
              end if;
              if l_descr_tab(i).col_type in (2,8,100,101) then
                l_row_data := l_row_data||',"precision":"'||l_descr_tab(i).col_precision||'"';
                l_row_data := l_row_data||',"scale":"'||l_descr_tab(i).col_scale||'"';
              end if;
              if l_descr_tab(i).col_type in (109) then --user_defined type
                l_row_data := l_row_data||',"type_name":"'||l_descr_tab(i).col_type_name||'"';
              end if;
              l_row_data := l_row_data||'},'||chr(10);
              dbms_lob.writeappend(l_result,length(l_row_data),l_row_data);
      end loop;
      
      l_result := rtrim(l_result,','||chr(10)) || '],'||chr(10)||' "data":';
  
    open l_refcursor for i_sql_stmt;
    
    -- Get a handle on the ref cursor
    l_context := dbms_xmlgen.newcontext (l_refcursor) ;
    
    -- SetNullHandling to 1 (or 2) to allow null columns to be displayed --
    dbms_xmlgen.setnullhandling (l_context, 1) ;
    
    -- Create XML from ref cursor
    l_xml_data := dbms_xmlgen.getxmltype (l_context, dbms_xmlgen.none) ;
        
    if l_xml_data is not null then
        -- XSL for JSON
        l_xsl := l_xsl || q'[<?xml version="1.0" encoding="ISO-8859-1"?>]';
        l_xsl := l_xsl || q'[<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">]';
        l_xsl := l_xsl || q'[<xsl:output method="html"/>]';
        l_xsl := l_xsl || q'[<xsl:variable name="new_line" select="'&#10;'" />]';
        l_xsl := l_xsl || q'[<xsl:template match="/">[<xsl:value-of select="$new_line" /><xsl:for-each select="/ROWSET/*">]';
        l_xsl := l_xsl || q'[  {<xsl:for-each select="./*">]';
        l_xsl := l_xsl || q'[<xsl:choose><xsl:when test="position() != 1"><xsl:text>   </xsl:text></xsl:when></xsl:choose>"<xsl:value-of select="name()"/>":"<xsl:value-of select="text()" disable-output-escaping="]'||l_disable_output_escaping||q'["/>"]';
        l_xsl := l_xsl || q'[<xsl:choose>]';
        l_xsl := l_xsl || q'[      <xsl:when test="position()!= last()">,<xsl:value-of select="$new_line" /></xsl:when>]';
        l_xsl := l_xsl || q'[    </xsl:choose>]';
        l_xsl := l_xsl || q'[   </xsl:for-each><xsl:value-of select="$new_line" />]';
        l_xsl := l_xsl || q'[  }<xsl:choose>]';
        l_xsl := l_xsl || q'[      <xsl:when test="position() != last()">,<xsl:value-of select="$new_line" /></xsl:when>]';
        l_xsl := l_xsl || q'[    </xsl:choose>]';
        l_xsl := l_xsl || q'[   </xsl:for-each>]<xsl:value-of select="$new_line" />}]';
        l_xsl := l_xsl || q'[</xsl:template></xsl:stylesheet>]';
      
        -- Convert XMLType to Clob
        l_data := dbms_xmlgen.convert(l_xml_data.transform(xmltype (l_xsl)).getclobval(), dbms_xmlgen.entity_decode);
        
        dbms_lob.append(l_result,l_data);
    
        dbms_xmlgen.closecontext (l_context) ;
        close l_refcursor;
    end if;
    return l_result;
  end select_stmt_to_json;

end com_formatter;