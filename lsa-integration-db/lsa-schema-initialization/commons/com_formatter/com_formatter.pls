create or replace package com_formatter
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2010-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Provides utilities to transform data (ref_cursor, varchar2, etc.) into different formats (HTML table, JIRA table)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-20 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10. Moved c_yes and c_no to com_constants - OCOMM-405
--  2015-08-20 Chris Roderick
--    Formatting
--  2015-07-23 Pascal Le Roux
--    Added functions ref_cursor_to_json and select_stmt_to_json
--  2014-07-30 Gkergki Strakosa
--    Added (overloaded) to_string to convert nested tables of different data types to string.
--  2014-05-21 Pascal Le Roux
--    Added Optional parameter i_disable_output_escaping to ref_cursor_to_html_table and ref_cursor_to_jira_table
--  2014-05-16 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  function ref_cursor_to_html_table (
    i_ref_cu                  in sys_refcursor, 
    i_disable_output_escaping in com_types.t_yes_no default com_constants.c_no
  )
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Transform a sys_refcursor into an HTML formatted table
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_ref_cu                    - refcursor to transform into HTML table
--    i_disable_output_escaping   - enable/disable output escaping (by default is "no"):
--              "yes" indicates that special characters (like "<") should be output as is. 
--              "no" indicates that special characters (like "<") should be output in HTML format. 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-21 Pascal Le Roux
--    Added Optional parameter i_disable_output_escaping (value: YES/NO) to enable/disable output escaping
--  2014-07-01 Pascal Le Roux
--    Changed default value of i_disable_output_escaping to "NO"
--  2014-05-16 Pascal Le Roux
--    Creation See JIRA issue https://issues.cern.ch/browse/OCOMM-254
------------------------------------------------------------------------------------------------------------------------

  function ref_cursor_to_jira_table (
    i_ref_cu                   in sys_refcursor,
    i_disable_output_escaping  in com_types.t_yes_no default com_constants.c_no
  )
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Transform a sys_refcursor into an JIRA formatted table
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_ref_cu                    - refcursor to transform into JIRA table
--    i_disable_output_escaping   - enable/disable output escaping (by default is "no"):
--              "yes" indicates that special characters (like "<") should be output as is. 
--              "no" indicates that special characters (like "<") should be output in HTML format. 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-21 Pascal Le Roux
--    Added Optional parameter i_disable_output_escaping (value: YES/NO) to enable/disable output escaping
--  2014-07-01 Pascal Le Roux
--    Changed default value of i_disable_output_escaping to "NO"
--  2014-05-16 Pascal Le Roux
--    Creation See JIRA issue https://issues.cern.ch/browse/OCOMM-254
------------------------------------------------------------------------------------------------------------------------

  function ref_cursor_to_json (
    i_ref_cu                   in sys_refcursor, 
    i_disable_output_escaping  in com_types.t_yes_no default com_constants.c_no
  )
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Transform a refcursor into JSON 
--    The JSON metadata tags generated by this function are: name
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_ref_cu                    - refcursor to transform into JSON
--    i_disable_output_escaping   - enable/disable output escaping (by default is "no"):
--              "yes" indicates that special characters (like "<") should be output as is. 
--              "no" indicates that special characters (like "<") should be output in HTML format. 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Pascal Le Roux
--    Creation OCOMM-326
------------------------------------------------------------------------------------------------------------------------

  function select_stmt_to_json (
    i_sql_stmt                 in varchar2, 
    i_disable_output_escaping  in com_types.t_yes_no default com_constants.c_no
  )
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Transform select statement (input as varchar2) into JSON 
--    The JSON metadata tags generated by this function are: name, type, precision, scale, max_len
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_sql_stmt                  - string with SQL select statement
--    i_disable_output_escaping   - enable/disable output escaping (by default is "no"):
--              "yes" indicates that special characters (like "<") should be output as is. 
--              "no" indicates that special characters (like "<") should be output in HTML format. 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Pascal Le Roux
--    Creation OCOMM-327
------------------------------------------------------------------------------------------------------------------------

end com_formatter;