create or replace package body com_code_analyser 
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Facilitates code analysis via a code analysis runner, which runs code analysis rules,
--    that should be implemented in the package body, and registered via the reg_code_analysis_rule
--    procedure.
--  Scope:
--    Operates on the database level, but depends on individual code analysis rule implementations.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-09 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-395
--  2014-06-2S  Chris Roderick
--    Removal of procedures which exist in com_util
--  2014-06-23  Chris Roderick and Eve Fortescue-Beck
--    Added procedure analyse_code_if_changed and supporting procedures
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure reg_code_analysis_rule (
    i_code_analysis_rule_name       in  com_code_analysis_rules.code_analysis_rule_name%type, 
    i_code_analysis_category        in  t_rule_category, 
    i_code_analysis_rule_exec       in  com_code_analysis_rules.code_analysis_rule_exec%type, 
    i_should_bind_obj_owner_like    in  boolean,  
    i_should_bind_obj_type_like     in  boolean, 
    i_should_bind_obj_name_like     in  boolean, 
    i_code_analysis_rule_severity   in  com_code_analysis_rules.code_analysis_rule_severity%type, 
    i_extra_info                    in  com_code_analysis_rules.extra_info%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers the given inputs as a code analysis rule in the table com_code_analysis_rules
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_code_analysis_rule_name     - The unique name to be given to identify this rule
--    i_code_analysis_category      - The category of the rule 
--    i_code_analysis_rule_exec     - The function (without owner prefix) to be called to obtain analysis results.
--    i_should_bind_obj_owner_like  - flag indicating if object owner filter should be passed to code analysis function
--    i_should_bind_obj_type_like   - flag indicating if object type filter should be passed to code analysis function 
--    i_should_bind_obj_name_like   - flag indicating if object name filter should be passed to code analysis function 
--    i_code_analysis_rule_severity - Integer between 1 (least) and 10 (most) indicating the severity of the rule
--    i_extra_info                  - Extra info to indicate why the rule should not be violated     
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-03  Chris Roderick and Vasilis Giannoutsos
--    Replaced local boolean_to_char function with call to com_util.boolean_to_char 
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is    
    l_should_bind_obj_owner_like    com_types.t_boolean_as_char;  
    l_should_bind_obj_type_like     com_types.t_boolean_as_char; 
    l_should_bind_obj_name_like     com_types.t_boolean_as_char; 
  begin  
    l_should_bind_obj_owner_like    := com_util.boolean_to_char(i_should_bind_obj_owner_like);  
    l_should_bind_obj_type_like     := com_util.boolean_to_char(i_should_bind_obj_type_like); 
    l_should_bind_obj_name_like     := com_util.boolean_to_char(i_should_bind_obj_name_like); 
  
    insert into com_code_analysis_rules (
      code_analysis_rule_id, code_analysis_rule_name, code_analysis_category, 
      code_analysis_rule_exec, should_bind_obj_owner_like,  should_bind_obj_type_like, 
      should_bind_obj_name_like, code_analysis_rule_severity, extra_info)
    values (
      com_code_analysis_rules_seq.nextval, i_code_analysis_rule_name, i_code_analysis_category, 
      i_code_analysis_rule_exec, l_should_bind_obj_owner_like, l_should_bind_obj_type_like, l_should_bind_obj_name_like,
      i_code_analysis_rule_severity, i_extra_info
    );
    
  end reg_code_analysis_rule;

  function analyse_code (
    i_code_analysis_category  in  t_rule_category         default c_all, 
    i_obj_owner_like          in  all_source.owner%type   default user, 
    i_obj_type_like           in  all_source.type%type    default '%', 
    i_obj_name_like           in  all_source.name%type    default '%'
  )
  return code_analysis_results pipelined
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Executes all registered code analysis rules matching the given category and returns a table or results
--
--  Example Usage:
--    select * from table(com_code_analyser.analyse_code());
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_code_analysis_category      - Optional filter to only run rules of the given category
--    i_obj_owner_like              - Optional filter to only run rules against objects with matching owners
--    i_should_bind_obj_type_like   - Optional filter to only run rules against objects with matching types 
--    i_should_bind_obj_name_like   - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;    
  begin
    for code_rules in (
      select code_analysis_rule_id, code_analysis_rule_name, code_analysis_category, 
        code_analysis_rule_exec, should_bind_obj_owner_like, should_bind_obj_type_like, 
        should_bind_obj_name_like, code_analysis_rule_severity, extra_info
      from com_code_analysis_rules
      where code_analysis_category like (case i_code_analysis_category when c_all then '%' else i_code_analysis_category end) 
    ) loop    
    
      execute immediate 'begin :l_result := '||code_rules.code_analysis_rule_exec||'(:obj_owner_like, :obj_type_like, :obj_name_like); end;' 
      using out l_result, i_obj_owner_like, i_obj_type_like, i_obj_name_like;        
        
      for i in 1..l_result.count loop
        pipe row (code_analysis_result(code_rules.code_analysis_rule_name, code_rules.code_analysis_category, code_rules.code_analysis_rule_severity, coalesce(l_result(i).extra_info, code_rules.extra_info), 
          l_result(i).code_owner, l_result(i).code_name, l_result(i).code_type, l_result(i).line_no, 
          l_result(i).line_text, l_result(i).line_column_pos
        ));
      end loop;
      
    end loop;

    return;
  end analyse_code;
  
  function last_code_analysis_run_time
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-19 : Chris Roderick and Vasilis Giannoutsos
--    Creation - OCOMM-255
------------------------------------------------------------------------------------------------------------------------  
  is
    l_last_code_analysis_run_time com_code_analysis_runs.code_analysis_start_time%type;
  begin
    select max(code_analysis_start_time) 
    into l_last_code_analysis_run_time
    from com_code_analysis_runs;
    
    return l_last_code_analysis_run_time;
    
  end last_code_analysis_run_time;
  
  function last_code_change_time (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-19 : Chris Roderick and Vasilis Giannoutsos
--    Creation - OCOMM-255
------------------------------------------------------------------------------------------------------------------------
  is
    l_last_code_change_time all_objects.last_ddl_time%type;
  begin
    select max(last_ddl_time) 
    into l_last_code_change_time
    from all_objects
    where owner like i_obj_owner_like
    and object_type in ('PROCEDURE', 'FUNCTION', 'PACKAGE', 'PACKAGE BODY', 'TRIGGER', 'TYPE', 'TYPE BODY')
    and object_type like i_obj_type_like
    and object_name like i_obj_name_like;
    
    return l_last_code_change_time;
    
  end last_code_change_time;
  
  function code_change_since_last_run (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return boolean
  is
  begin
    return last_code_change_time(i_obj_owner_like, i_obj_type_like, i_obj_name_like) 
      > coalesce(last_code_analysis_run_time, timestamp '2000-01-01 00:00:00');
  
  end code_change_since_last_run;
  
  function code_change_since_last_run_chr (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return char
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-19 : Chris Roderick and Vasilis Giannoutsos
--    Creation - OCOMM-255
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return com_util.boolean_to_char(code_change_since_last_run);
  end code_change_since_last_run_chr;

  procedure persist_code_analysis_results(
    i_code_analysis_run_id  in  com_code_analysis_runs.code_analysis_run_id%type,
    i_results               in  code_analysis_results
  )
  is
  begin
    forall r_result in 1..i_results.count
      insert into com_code_analysis_run_results (code_analysis_run_id, code_analysis_rule_name, code_analysis_category, code_analysis_rule_severity, extra_info, code_owner, code_name, code_type, line_no, line_text, line_column_pos)
      values (
        i_code_analysis_run_id, i_results(r_result).code_analysis_rule_name, i_results(r_result).code_analysis_category, i_results(r_result).code_analysis_rule_severity, 
        i_results(r_result).extra_info, i_results(r_result).code_owner, i_results(r_result).code_name, i_results(r_result).code_type, i_results(r_result).line_no, i_results(r_result).line_text, i_results(r_result).line_column_pos
      );
        
  end persist_code_analysis_results;
  
  procedure start_code_analysis_run (o_code_analysis_run_id out com_code_analysis_runs.code_analysis_run_id%type)
  is
  begin
    insert into com_code_analysis_runs (code_analysis_run_id, code_analysis_start_time)
    values (com_code_analysis_run_id_seq.nextval, sys_extract_utc(systimestamp))
    return code_analysis_run_id into o_code_analysis_run_id;
  end start_code_analysis_run;
  
  procedure end_code_analysis_run (i_code_analysis_run_id in com_code_analysis_runs.code_analysis_run_id%type)
  is
  begin
    update com_code_analysis_runs set code_analysis_end_time = sys_extract_utc(systimestamp) 
    where code_analysis_run_id = i_code_analysis_run_id;
  end end_code_analysis_run;
  
  procedure analyse_code_if_changed (
    i_code_analysis_category  in  t_rule_category         default c_all, 
    i_obj_owner_like          in  all_source.owner%type   default user, 
    i_obj_type_like           in  all_source.type%type    default '%', 
    i_obj_name_like           in  all_source.name%type    default '%'
  )
  is
    l_code_analysis_run_id  com_code_analysis_runs.code_analysis_run_id%type;
    l_results               code_analysis_results;    
  begin
    if code_change_since_last_run(i_obj_owner_like, i_obj_type_like, i_obj_name_like) then    
      start_code_analysis_run(l_code_analysis_run_id);
      
      select code_analysis_result(code_analysis_rule_name, code_analysis_category, code_analysis_rule_severity, extra_info, code_owner, code_name, code_type, line_no, line_text, line_column_pos)
      bulk collect into l_results 
      from table(analyse_code(i_code_analysis_category, i_obj_owner_like, i_obj_type_like, i_obj_name_like));
      
      persist_code_analysis_results(l_code_analysis_run_id, l_results);
      end_code_analysis_run(l_code_analysis_run_id);      
    end if;

  end analyse_code_if_changed;
    
end com_code_analyser;