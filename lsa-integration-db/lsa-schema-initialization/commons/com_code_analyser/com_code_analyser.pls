create or replace package com_code_analyser
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Facilitates code analysis via a code analysis runner, which runs code analysis rules.
--    Such rules should be implemented in the package body, and registered via the reg_code_analysis_rule
--    procedure.
--  Scope:
--    Operates on the database level, but depends on individual code analysis rule implementations.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-09 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-395
--  2014-06-23  Chris Roderick and Eve Fortescue-Beck
--    Added procedure analyse_code_if_changed - OCOMM-255
--  2014-05-19 : Chris Roderick and Vasilis Giannoutsos
--    Adding support for periodic code analysis and storage of results - OCOMM-255
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

--    Categories for log entries.
  subtype t_rule_category is com_code_analysis_rules.code_analysis_category%type;
  c_all         constant t_rule_category :=  'All';
  c_format      constant t_rule_category :=  'Formatting';
  c_syntax      constant t_rule_category :=  'Syntax';
  c_size        constant t_rule_category :=  'Size';
  
  procedure reg_code_analysis_rule (
    i_code_analysis_rule_name       in  com_code_analysis_rules.code_analysis_rule_name%type, 
    i_code_analysis_category        in  t_rule_category, 
    i_code_analysis_rule_exec       in  com_code_analysis_rules.code_analysis_rule_exec%type, 
    i_should_bind_obj_owner_like    in  boolean,  
    i_should_bind_obj_type_like     in  boolean, 
    i_should_bind_obj_name_like     in  boolean, 
    i_code_analysis_rule_severity   in  com_code_analysis_rules.code_analysis_rule_severity%type, 
    i_extra_info                    in  com_code_analysis_rules.extra_info%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers the given inputs as a code analysis rule in the table com_code_analysis_rules
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_code_analysis_rule_name     - The unique name to be given to identify this rule
--    i_code_analysis_category      - The category of the rule 
--    i_code_analysis_rule_exec     - The function (without owner prefix) to be called to obtain analysis results.
--    i_should_bind_obj_owner_like  - flag indicating if object owner filter should be passed to code analysis function
--    i_should_bind_obj_type_like   - flag indicating if object type filter should be passed to code analysis function 
--    i_should_bind_obj_name_like   - flag indicating if object name filter should be passed to code analysis function 
--    i_code_analysis_rule_severity - Integer between 1 (least) and 10 (most) indicating the severity of the rule
--    i_extra_info                  - Extra info to indicate why the rule should not be violated     
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure analyse_code_if_changed (
    i_code_analysis_category  in  t_rule_category         default c_all, 
    i_obj_owner_like          in  all_source.owner%type   default user, 
    i_obj_type_like           in  all_source.type%type    default '%', 
    i_obj_name_like           in  all_source.name%type    default '%'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Executes all registered code analysis rules matching the given category and stores the results
--    in the tables com_code_analysis_runs and com_code_analysis_run_results
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_code_analysis_category  - Optional filter to only run rules of the given category
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-06-23 : Chris Roderick and Eve Fortescue-Beck
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  function analyse_code (
    i_code_analysis_category  in  t_rule_category         default c_all, 
    i_obj_owner_like          in  all_source.owner%type   default user, 
    i_obj_type_like           in  all_source.type%type    default '%', 
    i_obj_name_like           in  all_source.name%type    default '%'
  )
  return code_analysis_results pipelined;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Executes all registered code analysis rules matching the given category and returns a table or results
--
--  Example Usage:
--    select * from table(com_code_analyser.analyse_code());
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_code_analysis_category  - Optional filter to only run rules of the given category
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 : Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  function last_code_analysis_run_time
  return timestamp;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the last time (if any) at which code analysis was run
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-19 : Chris Roderick and Vasilis Giannoutsos
--    Creation - OCOMM-255
------------------------------------------------------------------------------------------------------------------------  

  function last_code_change_time (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return timestamp;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the last time (if any) at which objects containing code were changed
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-19 : Chris Roderick and Vasilis Giannoutsos
--    Creation - OCOMM-255
------------------------------------------------------------------------------------------------------------------------  

  function code_change_since_last_run_chr (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return char;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns 'Y' if the code matching the given owner, category and object names has changed since the last code 
--    analysis run, otherwise it returns 'N'.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-05-19 : Chris Roderick and Vasilis Giannoutsos
--    Creation - OCOMM-255
------------------------------------------------------------------------------------------------------------------------

end com_code_analyser;