create or replace package com_schema_analysis_views
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to views.
--  Scope:
--    Operates on the database level, but depends on the implementation of analysis rules in each individual schema.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-14 Jose Rolland
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 -  OCOMM-422
--  2016-02-05  Jose Rolland
--    Added chk_rownum_predicates function [OCOMM-287]
--  2015-09-08  Jose Rolland
--    Added chk_order_by function [OCOMM-286]
--  2014-07-15  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  function chk_view_nesting (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for views built on top of views, the level of view nesting influences the returned severity factor
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-15  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function chk_order_by (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for views containing order by clauses
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-08  Jose Rolland
--    Creation [OCOMM-286]
------------------------------------------------------------------------------------------------------------------------

  function chk_rownum_predicates (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Check for views containing rownum predicates
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-05  Jose Rolland
--    Creation [OCOMM-287]
------------------------------------------------------------------------------------------------------------------------

end com_schema_analysis_views;