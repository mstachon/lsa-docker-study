create or replace package body com_schema_analysis_views
is
-----------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to views.
--  Scope:
--    Operates on the database level, but depends on individual schema analysis rule implementations.
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-05  Jose Rolland
--    Added chk_rownum_predicates function [OCOMM-287]
--  2015-09-08  Jose Rolland
--    Added chk_order_by function [OCOMM-286]
--  2014-07-15  Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------

  function chk_view_nesting (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for views built on top of views, the level of view nesting influences the returned severity factor
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-05-29  Pascal Le Roux
--    Bug fix: added and prior referenced_owner = owner to avoid CONNECT BY loop in user data when referenced_name and name
--    are the same from two different database
--    see https://issues.cern.ch/browse/OCOMM-315
--  2014-07-15  Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin     
    select schema_analysis_rule_result(owner, name, 'VIEW', level||' levels of nesting: '||substr(sys_connect_by_path(name, '-'), 2), 1+(level/10))
    bulk collect into l_result
    from all_dependencies 
    start with owner like i_obj_owner_like
    and type = 'VIEW' 
    and referenced_type = 'VIEW' 
    connect by (prior referenced_name = name and prior referenced_type = type and referenced_type = 'VIEW' and prior referenced_owner = owner)
    order by name;
    
    return l_result; 
  end chk_view_nesting;
  
  function chk_order_by (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-09-08  Jose Rolland
--    Creation [OCOMM-286]
-----------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin
    with db_views as (
      select object_name as view_name, owner, 
             case when object_type = 'VIEW' then lower(dbms_metadata.get_ddl('VIEW',object_name,owner)) end view_source
      from all_objects
      where object_name like i_obj_name_like
      and owner like i_obj_owner_like
      and object_type = 'VIEW'
    )
    select schema_analysis_rule_result(owner, view_name, 'VIEW', 'This view contains order by clauses', 1)
    bulk collect into l_result
    from db_views 
    where regexp_like(view_source,'([[:space:]]*)order([[:space:]]+)by([[:space:]]+)')
    order by view_name;
    
    return l_result;
  end chk_order_by;

  function chk_rownum_predicates (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-02-05  Jose Rolland
--    Creation [OCOMM-287]
-----------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin
    with db_views as (
      select object_name as view_name, owner, 
             case when object_type = 'VIEW' then lower(dbms_metadata.get_ddl('VIEW',object_name,owner)) end view_source
      from all_objects
      where object_name like i_obj_name_like
      and owner like i_obj_owner_like
      and object_type = 'VIEW'
    )
    select schema_analysis_rule_result(owner, view_name, 'VIEW', 'This view contains rownum predicates', 1)
    bulk collect into l_result
    from db_views 
    where regexp_like(view_source,'([[:space:]]+)rownum\W')
    order by view_name;
    
    return l_result;
  end chk_rownum_predicates;
  
end com_schema_analysis_views;