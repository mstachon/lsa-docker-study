create or replace package com_schema_analysis_naming
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to naming.
--  Scope:
--    Operates on the database level, but depends on the implementation of analysis rules in each individual schema.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-05 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-420
--  2014-10-14 Hadrian Villar
--    OCOMM-290 Creation
------------------------------------------------------------------------------------------------------------------------
is
  function chk_singular_table_names (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Check for Tables with Singular Names
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-05 Jose Rolland
--    Updated rule name chk_singular_tabs to chk_singular_table_names OCOMM-420
--  2014-10-14 Hadrian Villar
--    OCOMM-290 Creation
------------------------------------------------------------------------------------------------------------------------

end com_schema_analysis_naming;