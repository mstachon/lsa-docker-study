create or replace package body com_schema_analysis_naming
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to naming.
--  Scope:
--    Operates on the database level, but depends on the implementation of analysis rules in each individual schema.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-10-14  Hadrian Villar
--    OCOMM-290 Creation
------------------------------------------------------------------------------------------------------------------------
is
  function chk_singular_table_names (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-10-14  Hadrian Villar
--    OCOMM-290 Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin
  
    select schema_analysis_rule_result(alt.owner, alt.table_name, 'TABLE', 'This table has a singular name', 1)
      bulk collect into l_result
      from all_tables alt
      where alt.owner like i_obj_owner_like  
      and alt.table_name like i_obj_name_like
      and  not regexp_like(alt.table_name, '[a-z]*[A-Z]*[sS]+')
      order by alt.owner, alt.table_name;
    return l_result;
  
  end chk_singular_table_names;
  
end com_schema_analysis_naming;