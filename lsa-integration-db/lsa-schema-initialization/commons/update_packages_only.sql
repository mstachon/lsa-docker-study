------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Updates the COMMONS PLSQL packages.
--
--    NOTE: Exceptions may be encountered if the principal objects are not up-to-date.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-04  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

--  Continue if there are any errors
whenever sqlerror continue

--  run package installation scripts
@install/packages.sql