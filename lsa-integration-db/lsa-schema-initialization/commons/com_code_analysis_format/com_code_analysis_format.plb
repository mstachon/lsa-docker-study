create or replace package body com_code_analysis_format 
-----------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains implementations of code rules related to formatting
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-10 Eve Fortescue-Beck
--    Added comment blocks - OCOMM-397
--  2014-07-19  Chris Roderick
--    Modified analyse_linebreaks_use - OCOMM-293
--  2013-08-08 Chris Roderick
--    creation
-----------------------------------------------------------------------------------------------------------------------
is
  function analyse_uppercase_use(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
  is
    l_result  code_analysis_rule_results;
  begin
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
      select owner code_owner, type code_type, name code_name, 
        line line_no, text line_text, regexp_instr(text, '[A-Z]') line_column_pos, null extra_info
      from all_source_v
      where owner like i_obj_owner_like
      and type like i_obj_type_like
      and name like i_obj_name_like
      and lower(text) != text
    );

    return l_result;
  end analyse_uppercase_use; 
  
  function analyse_linebreaks_use(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-19  Chris Roderick
--    Modified the check of line breaks before 'from' keyword to exlcude 'delete from' - OCOMM-293
--    Moved the lower function into all_source_clean 'with' block
--    Moved the null value used for extra_info into the code_analysis_rule_result constructor instead of in every select
--  2013-12-xx  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, null)
    bulk collect into l_result
    from (
      with all_source_clean as (
        select /*+ materialize */ owner code_owner, type code_type, name code_name, line line_no, lower(text) line_text
        from all_source_v
        where owner = i_obj_owner_like
        and type like i_obj_type_like
        and name like i_obj_name_like
      )
--  from    
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'from') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)from')
      and not regexp_like(line_text, '(.*)extract(.+)from')
      and not regexp_like(line_text, '(.*)delete(.+)from')
      and not regexp_like(line_text, '^([[:space:]]+)from')
      union all
--  join
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'join') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)join([[:space:]]+)')
      and not regexp_like(line_text,'^([[:space:]]*)(full([[:space:]]+))?(right([[:space:]]+))?(left([[:space:]]+))?(outer([[:space:]]+))?join([[:space:]]+)')      
      union all
--  where
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'where') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)where')
      and not regexp_like(line_text, '^([[:space:]]+)where')
      union all
--  case
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'case') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)case')
      and not regexp_like(line_text,'^([[:space:]]*)end([[:space:]]+)case')      
      and not regexp_like(line_text, '^([[:space:]]+)case')
      union all
--  when
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'when') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)when')
      and not regexp_like(line_text,'^([[:space:]]*)exit([[:space:]]+)when')      
      and not regexp_like(line_text, '^([[:space:]]+)when')
      union all
--  end
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'end') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)end([[:space:]]+)')
      and not regexp_like(line_text, '^([[:space:]]+)end([[:space:]]+)')
      union all
--  group by
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'group') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)group([[:space:]]+)')
      and not regexp_like(line_text, '^([[:space:]]+)group([[:space:]]+)')
      union all
--  having
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'having') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)having([[:space:]]+)')
      and not regexp_like(line_text, '^([[:space:]]+)having([[:space:]]+)')
      union all
--  order
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'order') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)order([[:space:]]+)')
      and not regexp_like(line_text, '^([[:space:]]+)order([[:space:]]+)')
      union all
--  values
      select code_owner, code_type, code_name, line_no, line_text, regexp_instr(line_text, 'values') line_column_pos
      from all_source_clean
      where regexp_like(line_text, '([[:space:]]+)values([[:space:]]+)')
      and not regexp_like(line_text, '^([[:space:]]+)values([[:space:]]+)')
    );

    return l_result;
  
  end analyse_linebreaks_use;
  
  function analyse_constant_use(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
          select 
            ai.owner code_owner, 
            asv.type code_type, 
            asv.name code_name, 
            ai.line line_no,
            asv.text line_text,
            ai.col line_column_pos,
            null extra_info
          from all_identifiers ai
          join all_source_v asv
          on (ai.owner = asv.owner and 
              ai.object_name = asv.name and
              ai.object_type = asv.type and
              ai.line = asv.line)
          where ai.owner like i_obj_owner_like
          and ai.type like '%CONSTANT%'
          and ai.usage = 'DECLARATION'
          and ai.object_type like i_obj_type_like
          and ai.object_name like i_obj_name_like
          and ai.name not like 'C_%'
    );    
    return l_result;
  
  end analyse_constant_use;
  
  function analyse_global_use(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
          select 
            ai.owner code_owner, 
            asv.type code_type, 
            asv.name code_name, 
            ai.line line_no,
            asv.text line_text,
            ai.col line_column_pos,
            null extra_info
          from all_identifiers ai
          join all_source_v asv
          on (ai.owner = asv.owner and 
              ai.object_name = asv.name and
              ai.object_type = asv.type and
              ai.line = asv.line)
          where ai.owner like i_obj_owner_like
          and ai.type = 'VARIABLE'
          and ai.usage_context_id = 1
          and ai.usage = 'DECLARATION'
          and ai.object_type like i_obj_type_like
          and ai.object_name like i_obj_name_like
          and ai.name not like 'G_%'
          and ai.object_type in ('PACKAGE', 'PACKAGE BODY')
    );    
    return l_result;
  
  end analyse_global_use;
  
  function analyse_local_var_names(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
          select var.owner code_owner, src.type code_type, src.name code_name, var.line line_no, src.text line_text,
            var.col line_column_pos, null extra_info
          from all_identifiers var
          join all_identifiers mdl on (var.usage_context_id = mdl.usage_id
                                       and var.object_type = mdl.object_type
                                       and var.object_name = mdl.object_name
                                       and var.owner = mdl.owner)
          join all_source src on (var.owner = src.owner and 
                                  var.object_name = src.name and
                                  var.object_type = src.type and
                                  var.line = src.line)
          where var.owner like i_obj_owner_like
          and var.object_type like i_obj_type_like
          and var.object_name like i_obj_name_like
          and var.type = 'VARIABLE'
          and mdl.type in ('FUNCTION', 'PROCEDURE')
          and var.name not like 'L_%'
          and var.usage = 'DECLARATION'          
          and var.usage_context_id != 1
    );    
    return l_result;
  
  end analyse_local_var_names;
  
  function analyse_type_def_names(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
          select ai.owner code_owner, src.type code_type, src.name code_name, ai.line line_no, src.text line_text,
            ai.col line_column_pos, null extra_info
          from all_identifiers ai
          join all_source src on (ai.owner = src.owner and 
                                  ai.object_name = src.name and
                                  ai.object_type = src.type and
                                  ai.line = src.line)
          where ai.owner like i_obj_owner_like
          and ai.object_type like i_obj_type_like
          and ai.object_name like i_obj_name_like
          and ai.object_type in ('PACKAGE', 'PACKAGE BODY')
          and ai.usage = 'DECLARATION'
          and ai.type in ('ASSOCIATIVE ARRAY', 'INDEX TABLE', 'NESTED TABLE', 'OBJECT', 'RECORD', 'REFCURSOR', 'SUBTYPE', 'VARRAY')
          and ai.name not like 'T_%'
    );
    
    return l_result;    
  
  end analyse_type_def_names;
  
  function analyse_formal_param_names(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
      select code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info
      from (
        select ai.owner code_owner, ai.name, src.type code_type, src.name code_name, ai.line line_no, src.text line_text,
          ai.col line_column_pos, 
          case ai.type
            when 'FORMAL IN' then
              'Input parameters should be prefixed with i_'
            when 'FORMAL OUT' then
              'Output parameters should be prefixed with o_'
            when 'FORMAL IN OUT' then
              'In/Out parameters should be prefixed with io_'
          end as extra_info,
          case ai.type
            when 'FORMAL IN' then
              'I_'
            when 'FORMAL OUT' then
              'O_'
            when 'FORMAL IN OUT' then
              'IO_'
          end as expected_arg_prefix
        from all_identifiers ai
        join all_source src on (ai.owner = src.owner 
                                and ai.object_name = src.name
                                and ai.object_type = src.type
                                and ai.line = src.line)
        where ai.owner like i_obj_owner_like
          and ai.object_type like i_obj_type_like
          and ai.object_name like i_obj_type_like
          and ai.type in ('FORMAL IN', 'FORMAL OUT', 'FORMAL IN OUT')
          and ai.usage = 'DECLARATION'
      )
      where name not like expected_arg_prefix||'%'    
    );
    
    return l_result;    
  
  end analyse_formal_param_names;
  
  function analyse_comments(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  code_analysis_rule_results;
  begin
  
    select code_analysis_rule_result(code_owner, code_type, code_name, line_no, line_text, line_column_pos, extra_info)
    bulk collect into l_result
    from (
      select owner code_owner, type code_type, name code_name, line line_no, text line_text, 
        instr(lower(text), '/') line_column_pos, null extra_info
      from all_source
      where owner like i_obj_owner_like
        and type like i_obj_type_like
        and name like i_obj_type_like
        and ((text like '%/*%' or text like '%*/%') and text not like '%/*+%')      
    );
    
    return l_result;    
  
  end analyse_comments;  

end com_code_analysis_format;