create or replace package com_code_analysis_format 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Package applies rules related to code formatting
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-10 Eve Fortescue-Beck
--    Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-397
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
is
  function analyse_uppercase_use(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Detects inappropriate use of uppercase characters in source code and returns list of occurrences. 
--    Results can be optionally filtered by owner, object  type and name.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  
  function analyse_linebreaks_use(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Detects inappropriate use of linebreaks in source code and returns list of occurrences. 
--    Results can be optionally filtered by owner, object  type and name.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  
  function analyse_constant_use (
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Detects inappropriate use of constants in source code and returns list of occurrences. 
--    Results can be optionally filtered by owner, object  type and name.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  
  function analyse_global_use (
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Detects inappropriate use of global variables in source code and returns list of occurrences. 
--    Results can be optionally filtered by owner, object  type and name.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  
  function analyse_local_var_names(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Detects named local variables in source code without an l_ prefix and returns list of occurrences. 
--    Results can be optionally filtered by owner, object  type and name.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  
  function analyse_type_def_names(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Detects types in source code without the prefix t_ and returns list of occurrences. 
--    Results can be optionally filtered by owner, object  type and name.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------
  
  function analyse_formal_param_names(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Detects formal parameters in source code without i_, io_ or o_ prefixes and returns a list of occurrences. 
--    Results can be optionally filtered by owner, object  type and name.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------

  function analyse_comments(
    i_obj_owner_like  in  all_source.owner%type  default user, 
    i_obj_type_like   in  all_source.type%type   default '%', 
    i_obj_name_like   in  all_source.name%type   default '%'  
  )
  return code_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Detects comments in source code delimited by /**/ and returns list of occurrences. 
--    Results can be optionally filtered by owner, object  type and name.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-08-08 Chris Roderick
--    creation
------------------------------------------------------------------------------------------------------------------------  
end com_code_analysis_format;