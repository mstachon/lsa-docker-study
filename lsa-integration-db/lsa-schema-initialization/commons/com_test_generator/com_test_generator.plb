create or replace package body com_test_generator
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Commons4Oracle testing framework code generation utility. The package facilitates generation of complete test
--    packages or test procedures. The generation uses data dictionary to generate test stubs.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-29 Jose Rolland
--    Added support for generation of arguments of boolean, and use com_constants.c_horizontal_rule - OCOMM-373
--  2015-10-30 Chris Roderick
--    Modified procedure gen_test_proc_exceptions. OCOMM-361
--  2015-10-10 Chris Roderick
--    Modified function gen_test_proc_attributes. OCOMM-356
--  2015-09-30 Chris Roderick
--    Updates to parse_type_metadata - OCOMM-351
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    Bug fix in procedure gen_test_proc_execution - OCOMM-351
--  2015-08-23 Chris Roderick
--    Improved code in generated procedures (from gen_test_proc_assertion and gen_test_proc_excp_assertion) - OCOMM-338
--  2015-08-22 Chris Roderick
--    Formatting improvements, adding support for PL/SQL tables as parameters, and modifications to deal with procedures 
--    having local nested procedures - OCOMM-338
--  2015-08-19  Chris Roderick
--    Formatting
--  2015-07-31 Lukasz Burdzanowski
--    Creation [OCOMM-322]
------------------------------------------------------------------------------------------------------------------------
is
  c_gen_comment_line_break      constant  varchar2(121) := com_constants.c_horizontal_rule||chr(10);
  c_default_varchar2_arg_length constant  pls_integer   :=  4000;
 
  function gen_header_doc(i_description in varchar2)
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_output varchar2(4000);
  begin      
    l_output := l_output||c_gen_comment_line_break;
    l_output := l_output||'--  (c) CERN 2005-'||to_char(sysdate, 'YYYY')||' copyright'||chr(10);
    l_output := l_output||c_gen_comment_line_break;
    l_output := l_output||'--  Description:'||chr(10);
    l_output := l_output||'--  '||chr(9)||i_description||chr(10);
    l_output := l_output||c_gen_comment_line_break;
    l_output := l_output||'--  Change Log (descending):'||chr(10);
    l_output := l_output||'--  '||to_char(sysdate,'YYYY-MM-DD')||' '||sys_context('USERENV','OS_USER')||chr(10);
    l_output := l_output||'--  '||chr(9)||' Auto-generated'||chr(10);
    l_output := l_output||c_gen_comment_line_break;
    return l_output;
  end gen_header_doc;  
  
  function parse_type_metadata (i_user_argument in user_arguments%rowtype)
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-29 Jose Rolland
--    Added support for generation of arguments of boolean - OCOMM-373
--  2015-09-30 Chris Roderick
--    Added support for generation of arguments of user defined SQL types and sys_refcursor - OCOMM-351
--  2015-08-22 Chris Roderick
--    Modifications to deal with procedure having: custom type attributes - OCOMM-338
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_output varchar2(200);
  begin
    if i_user_argument.argument_name is null then
      l_output := l_output||rpad('function_result ', 33)||
        case 
          when i_user_argument.data_type = 'VARCHAR2' then 'varchar2('||coalesce(i_user_argument.data_length, c_default_varchar2_arg_length)||')'
          when i_user_argument.data_type = 'TABLE' then i_user_argument.type_name
          when i_user_argument.data_type = 'REF CURSOR' then 'sys_refcursor'
          when i_user_argument.data_type = 'PL/SQL BOOLEAN' then 'boolean'
          else i_user_argument.data_type
        end ||';';        
    else
      l_output := l_output||rpad(i_user_argument.argument_name, 33)||
        case 
          when i_user_argument.data_type = 'VARCHAR2' then 'varchar2('||coalesce(i_user_argument.data_length, c_default_varchar2_arg_length)||')'
          when i_user_argument.data_type = 'TABLE' then i_user_argument.type_name
          when i_user_argument.data_type = 'REF CURSOR' then 'sys_refcursor'
          when i_user_argument.data_type = 'PL/SQL TABLE' then i_user_argument.type_name||'.'||i_user_argument.type_subname
          when i_user_argument.data_type = 'PL/SQL BOOLEAN' then 'boolean'
          else i_user_argument.data_type  
        end ||';';   
    end if;      
    return l_output; 
  end parse_type_metadata;
    
  function gen_test_proc_attributes (
    i_package_name        in user_objects.object_name%type,
    i_procedure_name      in user_procedures.procedure_name%type,
    i_procedure_overload  in user_procedures.overload%type
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-10 Chris Roderick
--    Modified to deal with overloaded procedures. OCOMM-356
--  2015-08-22 Chris Roderick
--    Modifications to deal with procedure names being passed in lower case, and procedures having: custom type attributes, local nested procedures - OCOMM-338
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_output varchar2(4000);
  begin 
    for r_user_argument in (
      select * 
      from user_arguments 
      where package_name = upper(i_package_name) 
      and object_name = upper(i_procedure_name)
      and (overload = i_procedure_overload or (overload is null and i_procedure_overload is null))
      and data_level = 0
      order by sequence
    ) loop
      l_output := l_output||'    '||parse_type_metadata(r_user_argument)||chr(10);
    end loop;
    return l_output;
  end gen_test_proc_attributes;
  
  function gen_test_proc_fixture
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-10 Chris Roderick
--    Clean-up to remove unused code. OCOMM-356
--  2015-08-19  Chris Roderick
--    Formatting
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return
      '  procedure with_given_conditions'||chr(10)||
      '  is'||chr(10)||
      '  begin'||chr(10)||
      '--  TODO: Test fixture'||chr(10)||
      '    null;'||chr(10)||
      '  end with_given_conditions;';
  end gen_test_proc_fixture;
  
  function gen_test_proc_execution (
    i_package_name        in user_objects.object_name%type,
    i_procedure_name      in user_procedures.procedure_name%type,
    i_procedure_overload  in user_procedures.overload%type
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-10 Chris Roderick
--    Modified to deal with overloaded procedures. OCOMM-356
--  2015-09-29 Chris Roderick and Piotr Sowinski
--    Small bug fix concerning bracket for function test call - OCOMM-351
--  2015-08-22 Chris Roderick
--    Modifications to deal with procedure names being passed in lower case, and procedures having: custom type attributes, local nested procedures - OCOMM-338
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_output          varchar2(4000);
    l_procedure_call  varchar2(512);
    l_is_function     boolean := false;
  begin
    l_output := 
      '  procedure when_executing'||chr(10)||
      '  is'||chr(10)||
      '  begin'||chr(10);
      
    l_procedure_call := lower(i_package_name)||'.'||lower(i_procedure_name);
  
    for r_arg in (
      select argument_name, position
      from all_arguments 
      where package_name = upper(i_package_name) 
      and object_name = upper(i_procedure_name)
      and (overload = i_procedure_overload or (overload is null and i_procedure_overload is null))
      and data_level = 0
      order by position
    ) loop

--    2015-09-29 Chris Roderick - this next block is nasty... couldn't figure out from the data dictionary if something was a function or not, and settled on this.  Any improvement welcomne!     
      if r_arg.position = 0 then
        l_procedure_call := 'function_result := '||l_procedure_call||'(';
        l_is_function := true;          
      elsif r_arg.position = 1 then
        if l_is_function then
          l_procedure_call := l_procedure_call||r_arg.argument_name;
        else
          l_procedure_call := l_procedure_call||'('||r_arg.argument_name;
        end if;
      else          
          l_procedure_call := l_procedure_call||', '||r_arg.argument_name;
      end if;        
        
    end loop;
    
    l_procedure_call := l_procedure_call||');';  
    l_output := l_output||'    '||l_procedure_call||chr(10)||
      '  end when_executing;';
    return l_output;
  end gen_test_proc_execution;
 
  function gen_test_proc_assertion
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-10 Chris Roderick
--    Clean-up to remove unused code. OCOMM-356
--  2015-08-23 Chris Roderick
--    Adapting the generated code to remove pre-filled call to com_test_runner.fail
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------ 
  is
  begin
    return
      '  procedure should_result_in'||chr(10)||
      '  is'||chr(10)||
      '  begin'||chr(10)||
      '--  TODO: Test assertions'||chr(10)||
      '--    com_assert....'||chr(10)||
      '    null;'||chr(10)||
      '  end should_result_in;';
 end gen_test_proc_assertion;
 
  function gen_test_proc_excp_assertion
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-10 Chris Roderick
--    Clean-up to remove unused code. OCOMM-356
--  2015-08-23 Chris Roderick
--    Adapting the generated code to give pre-filled options to uncomment and customise in case of exceptions during 
--    test execution - OCOMM-338
--  2015-07-24 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------ 
  is
  begin
    return
      '  procedure should_raise(i_error_code in number)'||chr(10)||
      '  is'||chr(10)||
      '  begin'||chr(10)||
      '--  TODO: If the test should raise and exception, then assert that the correct exception has been raised e.g.'||chr(10)||      
      '--    com_assert.is_true(com_event_mgr.event_equals(''MY_ERROR_EVENT'', i_error_code));'||chr(10)||
      '--  TODO: If the test should not raise an exception at all, then it should probably just fail at this point e.g.'||chr(10)||            
      '--    com_test_runner.fail(''Unexpected failure, Err: ''||dbms_utility.format_error_stack);'||chr(10)||
      '    null;'||chr(10)||
      '  end should_raise;';
  end gen_test_proc_excp_assertion;
 
  function gen_test_proc_teardown
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-10 Chris Roderick
--    Clean-up to remove unused code. OCOMM-356
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------ 
  is
  begin
    return 
      '  procedure teardown'||chr(10)||
      '  is'||chr(10)||
      '  begin'||chr(10)||
      '--  TODO: Test teardown'||chr(10)||
      '    rollback;'||chr(10)||
      '  end teardown;';
  end gen_test_proc_teardown;
 
  function gen_test_proc_body
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------ 
  is
  begin  
    return
      '    with_given_conditions;'||chr(10)||
      '    begin'||chr(10)||
      '      when_executing;'||chr(10)||
      '      should_result_in;'||chr(10)||
      '    exception'||chr(10)||
      '      when others then'||chr(10)||
      '        should_raise(sqlcode);'||chr(10)||
      '    end;'||chr(10)||
      '    teardown;';
  end gen_test_proc_body;
 
  function gen_test_proc_exceptions
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-30 Chris Roderick
--    Improve handling of errors within the test itself OCOMM-361
--  2015-10-10 Chris Roderick
--    Clean-up to remove unused code. OCOMM-356
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------ 
  is
  begin  
    return
      '  exception'||chr(10)||
      '    when others then'||chr(10)||
      '      teardown;'||chr(10)||
      '      if not com_event_mgr.event_equals(''COM_TEST_RUNNER_TEST_FAILED'', sqlcode) then'||chr(10)||
      '        com_test_runner.fail(''Test code problem (not actual test failing), err: ''||dbms_utility.format_error_stack);'||chr(10)||
      '      end if;'||chr(10)||
      '      raise;';  
  end gen_test_proc_exceptions;

  function gen_test_procedure_body(
    i_package_name        in com_test_packages.package_name%type,
    i_procedure_name      in user_procedures.procedure_name%type,
    i_procedure_overload  in user_procedures.overload%type,
    i_custom_test_name    in com_test_run_results.test_name%type default null
  )
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-10 Chris Roderick
--    Modified to deal with overloaded procedures. OCOMM-356
--  2015-07-22 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_output          clob;
    l_procedure_name  user_objects.object_name%type;    
  begin      
    if i_custom_test_name is null then
      l_procedure_name := 
        lower(com_string_utils.get_as_valid_oracle_identifier(i_procedure_name, 'test_', i_procedure_overload));
    else         
      com_assert.arg_length_less_equal(i_custom_test_name, 30);
      l_procedure_name := i_custom_test_name;
    end if;
  
    l_output := 
      '  procedure '||l_procedure_name||chr(10)||
      c_gen_comment_line_break||
      '--  Change Log (descending):'||chr(10)||
      '--  '||to_char(sysdate,'YYYY-MM-DD')||' '||sys_context('USERENV','OS_USER')||chr(10)||
      '--    Auto-generated test for '||lower(i_procedure_name)||(case 
        when i_procedure_overload is not null then ' overload number '||i_procedure_overload end)||chr(10)||
      c_gen_comment_line_break||
      '  is'||chr(10)||
      lower(gen_test_proc_attributes(i_package_name, i_procedure_name, i_procedure_overload))||chr(10)||
      lower(gen_test_proc_fixture())||chr(10)||chr(10)||
      lower(gen_test_proc_execution(i_package_name, i_procedure_name, i_procedure_overload))||chr(10)||chr(10)||
      lower(gen_test_proc_assertion())||chr(10)||chr(10)||
      lower(gen_test_proc_excp_assertion())||chr(10)||chr(10)||
      lower(gen_test_proc_teardown())||chr(10)||chr(10)||
      '  begin'||chr(10)||
      lower(gen_test_proc_body)||chr(10)||
      lower(gen_test_proc_exceptions)||chr(10)||
      '  end '||l_procedure_name||';'; 
    return l_output;
  end gen_test_procedure_body;
  
  function gen_test_package_procedures(
    i_package_name  in  user_objects.object_name%type, 
    i_is_header     in  boolean
  )
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-10 Chris Roderick
--    Modified to deal with overloaded procedures. OCOMM-356
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_output clob; 
  begin
   for r_proc in (
      select lower(procedure_name) as procedure_name, overload
      from user_procedures ap 
      join user_objects ao on (ao.object_name = ap.object_name and ao.object_type = 'PACKAGE')
      where ao.object_name = i_package_name 
      and ap.procedure_name is not null 
      order by ap.procedure_name, ap.overload
    ) loop
      l_output := l_output||chr(10);

      if i_is_header then 
        l_output := l_output||
          '  procedure '||com_string_utils.get_as_valid_oracle_identifier(
            r_proc.procedure_name, 'test_', r_proc.overload)||';'||chr(10)||
          c_gen_comment_line_break||
          '--  Change Log (descending):'||chr(10)||
          '--  '||to_char(sysdate,'YYYY-MM-DD')||' '||sys_context('USERENV','OS_USER')||chr(10)||
          '--    Auto-generated test for '||r_proc.procedure_name||chr(10)||
          c_gen_comment_line_break;
      else 
        l_output := l_output||gen_test_procedure_body(i_package_name, r_proc.procedure_name, r_proc.overload)||chr(10);      
      end if;
    end loop;
    
    return l_output;
  end gen_test_package_procedures;
    
  function gen_test_package(i_package_name in com_test_packages.package_name%type, i_is_header in boolean)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-08-03 Lukasz Burdzanowski
--    Creation [OCOMM-322]
------------------------------------------------------------------------------------------------------------------------
  is
    l_test_package_name user_objects.object_name%type;
    l_output clob;
    
    function package_exists
    return boolean
    is
      l_pkg_count number;
    begin
      select count(*) into l_pkg_count 
      from user_objects 
      where object_name = upper(i_package_name) 
      and object_type = 'PACKAGE';
      
      return l_pkg_count > 0;
    end package_exists;
    
  begin
    if package_exists then
      l_test_package_name := lower(com_string_utils.get_as_valid_oracle_identifier(i_package_name, null, '_test'));   
      com_logger.debug('Generating test for existing package: '||i_package_name, $$PLSQL_UNIT);
    else
      l_test_package_name := lower(com_string_utils.get_as_valid_oracle_identifier(i_package_name));
      com_logger.debug('Generating empty test. Not existing ppackage: '||i_package_name, $$PLSQL_UNIT);
    end if;
  
    l_output := 
      'create or replace package'||case when i_is_header then ' ' else ' body ' end ||l_test_package_name||chr(10)||
      gen_header_doc('Auto-generated test package for '||lower(i_package_name))||
      'is'||chr(10)||
      gen_test_package_procedures(upper(i_package_name), i_is_header)||chr(10)||
      'end '||l_test_package_name||';';
    return l_output;
  end gen_test_package;
  
  function gen_test_package_header (i_package_name in com_test_packages.package_name%type)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is    
  begin    
    return gen_test_package(i_package_name, true);
  end gen_test_package_header;  
  
  function gen_test_package_body (i_package_name in com_test_packages.package_name%type)
  return clob
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
  begin    
    return gen_test_package(i_package_name, false);
  end gen_test_package_body;   
end com_test_generator;