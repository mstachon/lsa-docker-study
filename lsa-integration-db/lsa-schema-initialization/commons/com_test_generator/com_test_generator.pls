create or replace package com_test_generator
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Commons4Oracle testing framework code generation utility. The package facilitates generation of complete test
--    packages or test procedures. The generation uses the data dictionary to generate test stubs.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-13 Eve Fortescue-Beck
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-426
--  2015-10-10 Chris Roderick
--    Modified function gen_test_procedure_body. OCOMM-356
--  2015-07-31 Lukasz Burdzanowski
--    Creation [OCOMM-322]
------------------------------------------------------------------------------------------------------------------------
is

 function gen_test_package_header (i_package_name in com_test_packages.package_name%type)
 return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Generates a test package header. If the provided name, in uppercase, corresponds to an existing package, test 
--    procedures for all public procedures and functions in that package will be generated 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name - name of the test package; optionally defines the source package based on parity of the name
------------------------------------------------------------------------------------------------------------------------
--  Returns:
--    returns generated header of the test package
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------

 function gen_test_package_body (i_package_name in com_test_packages.package_name%type)
 return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Generates a test package body.  If the provided name, in uppercase, corresponds to an existing package, test 
--    procedures for all public procedures and functions in that package will be generated 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name - name of the test package; optionally defines the source package based on parity of the name
------------------------------------------------------------------------------------------------------------------------
--  Returns:
--    returns generated body of the test package
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------

  function gen_test_procedure_body(
    i_package_name        in com_test_packages.package_name%type,
    i_procedure_name      in user_procedures.procedure_name%type,
    i_procedure_overload  in user_procedures.overload%type        default null,
    i_custom_test_name    in com_test_run_results.test_name%type  default null
  )
  return clob;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Generates body of the test procedure wrapper. Paste generated code to the body of your test case package.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name - name of the package containing the procedure
--    i_procedure_name - name of the procedure to test
--    i_procedure_overload - the number of the procedure overload in case of generating code for an overloaded procedure
--    i_custom_test_name - custom name of the generated test procedure
------------------------------------------------------------------------------------------------------------------------
--  Returns:
--    returns generated body of the test procedure
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR when the custom name is too long (30 chars max)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-10 Chris Roderick
--    Modified to deal with overloaded procedures. OCOMM-356
--  2015-07-22 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------

end com_test_generator;