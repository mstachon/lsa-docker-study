create or replace package com_event_mgr
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to predefine, and raise different categories of events, such as INFO, 
--    DEBUG, WARN, ERROR, FATAL. It should also be possible to define notifications for each event, 
--    and assign a list of recipients.  Notifications should be sent using the com_notifier package
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-28 Lukasz Burdzanowski
--    Added handle_error procedure  - OCOMM-439
--  2016-05-10 Eve Fortescue-Beck
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 -  OCOMM-403
--  2015-07-09  Jose Rolland
--    Modified the procedures raise_event and raise_error_event. - OCOMM-313
--  2015-06-16  Lukasz Burdzanowski ,Nikolay Tsvetkov
--    Added 'raise_event' with common pattern command as argument - OCOMM-310
--  2015-05-26  Nikolay Tsvetkov
--    Modified the function 'get_recipients_for_event' adding a notification filters functionality - OCOMM-310        
--  2015-04-24  Lukasz Burdzanowski
--    Added 'raise_error_event_no_stack' - OCOMM-311 
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Modified the procedures raise_error_event and raise_event - OCOMM-308
--  2013-07-14  Gkergki Strakosa
--    Added raise_error_event to simplify error handling
--  2014-07-11  Chris Roderick
--    removed overloaded procedure raise_event with event_id as input which should only exist in the package body 
--  2014-07-11  Chris Roderick
--    Modified the raise event procedures to take a clob as input for the event message instead of 
--    com_logger.log_entry%type - OCOMM-278
--    Added function get_recipients_for_event - OCOMM-279
--  2010-11-03  Chris Roderick
--    Added optional input to raise_event procedures
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  function get_recipients_for_event(
    i_event_name           in  com_event_definitions.event_name%type,
    i_filt_key_value_table in  vc_vc_map default null
  )
  return com_notifier.table_of_recipients;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns all recipients who should be notified of the event with the given name 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name           - the name of the event for which to lookup the recipients
--    i_filt_key_value_table - optional table of key values describing the payload of the event which are used to 
--                             establish recipients taking into account potential event notification filters 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-05-26  Nikolay Tsvetkov
--    Added i_filt_key_value_table and functionality to get recipient by event filters definition  - OCOMM-310
--  2014-07-11  Chris Roderick
--    Creation - OCOMM-279
------------------------------------------------------------------------------------------------------------------------

  procedure define_event (
    i_event_name            in  com_event_definitions.event_name%type,
    i_event_category        in  com_logger.t_log_category,
    i_event_description     in  com_event_definitions.event_description%type,
    i_event_sms_start_hour  in  com_event_definitions.event_sms_start_hour%type,
    i_event_sms_end_hour    in  com_event_definitions.event_sms_end_hour%type,
    i_event_notify_admin    in  com_event_definitions.event_notify_admin%type,
    o_event_id              out com_event_definitions.event_id%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Define new events and insert them into the com_event_definitions table. The information about
--    who defined the event and when is stored with the event definition via the default values of 
--    the columns of the underlying table.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name
--      The name of the event (in uppercase).
--    i_event_category
--      The category of the event (see com_logger t_log_category for available categories).
--    i_event_description
--      A description of what the event represents.
--    i_event_sms_start_hour
--      The start of the time window within which an SMS notification can be sent for this event.
--    i_event_sms_end_hour
--      The end of the time window within which an SMS notification can be sent for this event.
--    i_event_notify_admin
--      'Y' means that recipients who are administrators will be notified of the occurrence of this 
--      event regardless of whether or not a notification has been explicitly configured for the 
--      recipient.
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--    o_event_id
--      The numeric ID generated for this event.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    EVENT_EXISTS - when an event already exists with the given name in the COM_EVENT_DEFINITIONS 
--    table.  
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions
--    Added parameter to indicate if the occurrence of the given event should be notified to 
--    administrators regardless of whether or not an explicit event notification has been set up.
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure update_event (
    i_event_name            in  com_event_definitions.event_name%type,
    i_event_category        in  com_logger.t_log_category, 
    i_event_description     in  com_event_definitions.event_description%type,
    i_event_sms_start_hour  in  com_event_definitions.event_sms_start_hour%type,
    i_event_sms_end_hour    in  com_event_definitions.event_sms_end_hour%type,
    i_event_notify_admin    in  com_event_definitions.event_notify_admin%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Updates columns in the com_event_definitions table for a given event, corresponding to the parameter values.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name
--      The name of the event (in uppercase).
--    i_event_category
--      The category of the event (see com_logger t_log_category for available categories).
--    i_event_description
--      A description of what the event represents.
--    i_event_sms_start_hour
--      The start of the time window within which an SMS notification can be sent for this event.
--    i_event_sms_end_hour
--      The end of the time window within which an SMS notification can be sent for this event.
--    i_event_notify_admin
--      'Y' means that recipients who are administrators will be notified of the occurrence of this 
--      event regardless of whether or not a notification has been explicitly configured for the 
--      recipient.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:  
--    NO_EVENT - when an given event name does not exist in the COM_EVENT_DEFINITIONS table 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions
--    Added parameter to indicate if the occurrence of the given event should be notified to 
--    administrators regardless of whether or not an explicit event notification has been set up.
--  2011-01-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure delete_event (
    i_event_name            in  com_event_definitions.event_name%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Deletes existing events from the com_event_definitions table. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name - The name of the event to be deleted
------------------------------------------------------------------------------------------------------------------------
--  Exceptions: 
--    NO_EVENT - when the given event name does not exist in the COM_EVENT_DEFINITIONS table 
--    EVENT_HAS_SRC_DEPS - when the event to be deleted has source dependencies
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-01-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure raise_error_event(
    i_event_name          in  com_event_definitions.event_name%type,
    i_event_msg           in  clob                              default null,
    i_dynamic_recipients  in  com_notifier.table_of_recipients  default cast(null as com_notifier.table_of_recipients), 
    i_event_title         in  varchar2                          default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Logs error events saving the stacktrace/SQLERRM and the given event_msg, by calling raise_event  
--    with the appropriate arguments.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name  -   The name of the event to be raised
--    i_event_msg   -  The message to be sent with the event
--    i_dynamic_recipients - a list of recipients
--    i_event_title - The title of the event, for email subject lines
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Jose Rolland
--    Added the parameter i_event_title. - OCOMM-313
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Added the parameter i_dynamic_recipients - OCOMM-308
--  2013-07-15  Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure raise_event(
    i_event_name          in  com_event_definitions.event_name%type, 
    i_event_msg           in  clob                               default null,
    i_raise_exception     in  boolean                            default false,
    i_is_auton            in  boolean                            default false,
    i_dynamic_recipients  in  com_notifier.table_of_recipients   default cast(null as com_notifier.table_of_recipients),
    i_event_title         in  varchar2                           default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Log events (to a range / list partitioned table) using the com_logger package
--    When raising events, give the possibility to specify if an ERROR type event should cause an exception to be raised 
--    or not. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name
--      The name of the event to be raised
--    i_event_msg
--      The message to be sent with the event
--    i_raise_exception
--      Boolean flag which indicates whether the event should raise an exception
--    i_is_auton
--      Boolean flag to set the autonomous transaction mode: 
--        'True' meaning that the event is raised independently of the calling transaction.  
--        'False' meaning that the event raised is dependent on the calling transaction.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Jose Rolland
--    Added the parameter i_event_title - OCOMM-313
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Added the parameter i_dynamic_recipients - OCOMM-308
--  2011-03-07  Eve Fortescue-Beck, Chris Roderick
--    Added i_is_auton flag to allow the event to be raised autonomously or not.
--  2010-11-03  Chris Roderick
--    Added i_event_msg parameter as optional input
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure clear_event_logs (
    i_older_than_utc  in  timestamp default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Clear event logs for the current user, either completely or based on a given time 
--      window using the com_logger functionality
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_older_than_utc - lowest timestamp threshold
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions   
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure register_event_notification (
    i_event_name      in  com_event_definitions.event_name%type,
    i_recipient       in  com_recipients.recipient_id%type,
    i_sms             in  com_event_notifications.sms%type,
    i_filter_id       in  com_event_notif_filters.filter_id%type default null
  );

------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Manage event notification configuration.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name - The name of the event for which the recipients should receive notifications
--    i_recipient - A recipient to be notified when the event with the given name occurs
--    i_sms - A flag which states if the recipient should receive SMS messages about the particular event
--    i_filter_id - An optional reference to a filter to be applied for an occurrence of the given event, in order to 
--                  determine whether or not to notify the given recipient
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    RECIPIENT_OF_EVENT_EXISTS - Recipient-Event combination already exists in COM_EVENT_NOTIFICATIONS table 
--    EVENT_OR_RECIPIENT_NOT_FOUND - parent key missing in either event or recipient table
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04  Nikolay Tsvetkov
--    Added option for specifying filter per recipient and event
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure delete_event_notification (
    i_event_name      in  com_event_definitions.event_name%type,
    i_recipient       in  com_recipients.recipient_id%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Delete an event notification configuration.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name - The name of the event notification to be deleted
--    i_recipient - The recipient of the event notification to be deleted
------------------------------------------------------------------------------------------------------------------------
--  Exceptions: 
--    EVENT_OR_RECIPIENT_NOT_FOUND - parent key missing in either event or recipient table
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-01-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure update_event_notification (
    i_event_name      in  com_event_definitions.event_name%type,
    i_recipient       in  com_recipients.recipient_id%type,
    i_sms             in com_event_notifications.sms%type default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Update the SMS flag for an event notification configuration.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name - The name of the event notification to be updated
--    i_recipient - The recipient of the event notification to be updated
--    i_sms - the sms flag that requires an update 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions: 
--    NO_NOTIFICATIONS - parent key missing in notifications table (recipient not registered for notifications)
--    NO_EVENT - parent key missing in event table
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-01-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function event_equals (
    i_event_name      in  com_event_definitions.event_name%type,
    i_event_id        in  com_event_definitions.event_id%type
  ) 
  return boolean result_cache;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   Checks whether a given Event_ID corresponds to the given Event_Name.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name - The name of the Event
--    i_event_id   - The ID of the Event
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--    returns true when the given event_name and event_id correspond, else false.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    NO_DATA_FOUND -  The Event Name was not found in the com_event_definitions table.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-11  Chris Roderick
--    Added result_cache
--  2010-12-10  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure raise_error_event_no_stack(
    i_event_name          in  com_event_definitions.event_name%type,
    i_event_msg           in  clob                                default null,
    i_dynamic_recipients  in  com_notifier.table_of_recipients    default cast(null as com_notifier.table_of_recipients)
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Wrapper procedure over 'raise_error_event' which does not add a stack-trace to the i_event_msg
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name  -   The name of the event to be raised
--    i_event_msg   -  The message to be sent with the event
--    i_dynamic_recipients - a list of recipients
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-04-24 Lukasz  Burdzanowski 
--    OCOMM-311 Creation
------------------------------------------------------------------------------------------------------------------------

 procedure raise_event(i_command in com_notifier.t_notification_command);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--     Wrapper procedure over 'raise_event' which uses common pattern command as argument
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_command - Common pattern command holding all the arguments in a record
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-16 Lukasz Burdzanowski ,Nikolay Tsvetkov   
--    Creation - OCOMM-310
------------------------------------------------------------------------------------------------------------------------

  procedure unhandled_exception(
    i_exception_message in varchar2,
    i_handled_exception in com_event_definitions.event_name%type default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks if SQLCODE in current exception context matches passed event name and if true propagates the event. 
--    Otherwise the procedure raises a PROCESSING_ERROR event. Raised event (original or the PROCESSING_ERROR) 
--    includes format_error_stack attached to the message.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_exception_message - Message of the (re)raised event
--    i_handled_exception - Name of the event to check against the SQLCODE
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Re-raises original exception or raises PROCESSING_ERROR
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-28 Lukasz Burdzanowski
--    Creation - OCOMM-439
------------------------------------------------------------------------------------------------------------------------

  procedure unhandled_exceptions(
    i_exception_message in varchar2,
    i_handled_exceptions in table_of_varchar default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    See unhandled_exception for details. Overloaded version supporting collection of handled exceptions. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_exception_message - Message of the (re)raised event
--    i_handled_exceptions - Names of the events to check against the SQLCODE
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Re-raises original exception or PROCESSING_ERROR
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-28 Lukasz Burdzanowski
--    Creation - OCOMM-439
------------------------------------------------------------------------------------------------------------------------

end com_event_mgr;