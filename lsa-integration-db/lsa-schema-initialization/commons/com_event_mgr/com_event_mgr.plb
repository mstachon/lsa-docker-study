create or replace package body com_event_mgr
-----------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2017 copyright
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to predefine, and raise different categories of events, such as INFO, 
--    DEBUG, WARN, ERROR, FATAL. It should also be possible to define notifications for each event, 
--    and assign a list of recipients.  Notifications should be sent using the com_notifier package
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14  Chris Roderick
--    Modified function get_recipients_for_event and removed function get_event_filter_predicate OCOMM-443
--  2015-10-06  Chris Roderick
--    Added debug statement to raise_event_internal. OCOMM-354
--    Formatting
--  2015-07-09  Chris Roderick
--    updated function conv_event_msg_to_err_msg
--  2015-06-16  Lukasz Burdzanowski ,Nikolay Tsvetkov
--    [OCOMM-310] Added 'raise_event' with common pattern command as argument
--  2015-05-26  Nikolay Tsvetkov
--    [OCOMM-310] Modified the function 'get_recipients_for_event' adding a notification filters functionality
--  2015-04-24  Lukasz Burdzanowski
--    OCOMM-311 Added 'raise_error_event_no_stack'
--  2015-03-04  Chris Roderick, Maciej Peryt
--    [OCOMM-308] Modified the procedures raise_error_event, raise_event and raise_event_internal.
--  2014-07-11  Chris Roderick
--    Modified the raise event procedures to take a clob as input for the event message instead of 
--    com_logger.log_entry%type - OCOMM-278
--    Added function get_recipients_for_event - OCOMM-279
--  2013-07-14  Gkergki Strakosa
--    Added raise_error_event to simplify error handling
--  2013-06-25  Gkergki Strakosa
--    Bug fix in procedure raise_event_internal which was always notifying admins of all events
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions
--  2011-03-24  Pascal Le Roux
--    Updated the call to the com_logger.clear_log due to changes in the com_logger package
--  2011-03-18  Chris Roderick
--    Replaced procedure raise_event_internal_unauton and raise_event_internal_auton with 
--    raise_event_internal which uses its parameter i_is_auton to serve as input to log and notify 
--    procedures in order to generate log entries and notifications in an autonomous transaction.
--  2011-03-07  Eve Fortescue-Beck, Chris Roderick
--    Modification of raise event implementations to allow autonomous transactions
--  2010-12-09  Chris Roderick
--    Added new procedure get_event_sms_window
--  2010-12-03  Chris Roderick
--    Added optional input to raise_event procedures
--  2010-11-11  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
is
  function conv_event_msg_to_err_msg(
    i_event_msg       in  clob
  ) 
  return varchar2
-----------------------------------------------------------------------------------------------------------------------
--  2015-07-09  Chris Roderick
--    Extended length of error message to 2000 chars
--  2014-07-11  Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    return substr(i_event_msg, 1, 2000);
  end conv_event_msg_to_err_msg;
  
  function conv_event_msg_to_log_msg(
    i_event_msg       in  clob
  ) 
  return com_logs.log_entry%type
-----------------------------------------------------------------------------------------------------------------------
--  2014-07-11  Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    return substr(i_event_msg, 1, 3900);
  end conv_event_msg_to_log_msg;

  function get_new_event_id(
    i_event_category  in com_event_definitions.event_category%type
  ) 
  return com_event_definitions.event_id%type 
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-11-11  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is 
    l_event_id com_event_definitions.event_id%type;
  begin  
    case 
      when i_event_category = 'ERROR' then     
--      work from -20,000 to 20,999 therefore error codes are descending        
        select coalesce(min(event_id)-1, -20000) into l_event_id
        from com_event_definitions
        where event_category = 'ERROR';

--    If this event id is outside the allowable range of user defined errors, raise an exception
      if l_event_id < -20999 then
        raise_error_event('NO_ERROR_IDS', 'There are no more event id''s in the range -20,000 to 20,999');
      end if;

    else    
      l_event_id  :=  com_event_ids_seq.nextval;      
    end case;
    
  return l_event_id;
  
  end get_new_event_id;
  
 function get_event_category(
    i_event_name  in com_event_definitions.event_name%type
  ) 
  return com_event_definitions.event_category%type
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    private function which returns the event_category for a given event_name, 
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name - Event Name
-----------------------------------------------------------------------------------------------------------------------
--  Returns:
--    l_event_category - Event Category
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-11-17  Eve Fortescue-Beck, Chris Roderick
--    Creation
----------------------------------------------------------------------------------------------------------------------- 
  is 
    l_event_category com_event_definitions.event_category%type;
  begin
    
    select event_category into l_event_category
    from com_event_definitions
    where event_name = i_event_name;
    
    return l_event_category;

  end get_event_category;
  
  function get_event_name(
    i_event_id  in com_event_definitions.event_id%type
  ) 
  return com_event_definitions.event_name%type
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    a local function which returns the event_name for a given event_id
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_id - Event id
-----------------------------------------------------------------------------------------------------------------------
--  Returns:
--    l_event_name - Event Name
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    NO_DATA_FOUND - when there is no event defined with the given id
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-11-26  Eve Fortescue-Beck, Chris Roderick
--    Creation
----------------------------------------------------------------------------------------------------------------------- 
  is 
    l_event_name com_event_definitions.event_name%type;
  begin
    
    select event_name into l_event_name
    from com_event_definitions
    where event_id = i_event_id;
    
    return l_event_name;
  exception
  when no_data_found then   
    return null; 
  end get_event_name;
  
  function get_event_id(
    i_event_name  in com_event_definitions.event_name%type
  ) 
  return com_event_definitions.event_id%type
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    a local function which returns the event_id for a given event_name, 
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name - Event Name
-----------------------------------------------------------------------------------------------------------------------
--  Returns:
--    l_event_id - Event id
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No Known Exceptions (no_data_found handled in calling procedure)
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-11-26  Eve Fortescue-Beck, Chris Roderick
--    Creation
----------------------------------------------------------------------------------------------------------------------- 
  is 
    l_event_id com_event_definitions.event_id%type;
  begin    
    select event_id into l_event_id
    from com_event_definitions
    where event_name = i_event_name;
    
    return l_event_id;

  end get_event_id;


  function get_event_description(
    i_event_name  in com_event_definitions.event_description%type
  ) 
  return com_event_definitions.event_description%type
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    a local function which returns the event_description for a given event_name, 
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name - Event Name
-----------------------------------------------------------------------------------------------------------------------
--  Returns:
--    l_event_description - Event Description
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    None
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-11-30  Eve Fortescue-Beck, Chris Roderick
--    Creation
----------------------------------------------------------------------------------------------------------------------- 
  is 
    l_event_description com_event_definitions.event_description%type;
  begin
    
    select event_description into l_event_description
    from com_event_definitions
    where event_name = i_event_name;
    
    return l_event_description;

  end get_event_description; 
  
  procedure get_event_sms_window(
    i_event_name      in  com_event_definitions.event_name%type
  , o_sms_start_hour  out com_event_definitions.event_sms_start_hour%type
  , o_sms_end_hour    out com_event_definitions.event_sms_start_hour%type
  )
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    retrieves the SMS time window parameters (if defined) for a given event
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name
--      The name of the event for which to retrieve the SMS time window parameters
-----------------------------------------------------------------------------------------------------------------------
--  Returns:
--    o_sms_start_hour
--      The start hour of the day (in 24 hours) from which SMS notifications for the given event can 
--      be sent. NULL if not defined.
--    o_sms_end_hour
--      The end hour of the day (in 24 hours) up until which SMS notifications for the given event 
--      can be sent. NULL if not defined.
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    None
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-12-09  Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    select event_sms_start_hour, event_sms_end_hour 
    into o_sms_start_hour, o_sms_end_hour
    from com_event_definitions
    where event_name = i_event_name;
  
  exception 
    when no_data_found then 
      null;
    when others then
      raise;
  end get_event_sms_window;

  procedure add_admin_recipients_for_event (i_recipients  in out com_notifier.table_of_recipients,
                                            i_event_name  in com_event_definitions.event_name%type)
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Adds all the admins in the recipients list if the event is defined to notify the administrators
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_recipients   - key (column name) to search for in the payload
--    i_event_name  - search rule operators 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-09  Nikolay Tsvetkov
--    Creation - OCOMM-310
------------------------------------------------------------------------------------------------------------------------
  is 
  begin
     select r.recipient_id, r.recipient_name, r.recipient_email, null, r.recipient_is_admin
      bulk collect into i_recipients
     from com_recipients r, com_event_definitions d
     where d.event_name = i_event_name
       and r.recipient_is_admin = 'Y' and d.event_notify_admin = 'Y';
  
  end add_admin_recipients_for_event;
  
  function get_recipients_for_event(i_event_name           in  com_event_definitions.event_name%type,
                                    i_filt_key_value_table in  vc_vc_map default null)
  return com_notifier.table_of_recipients
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-01-14  Chris Roderick
--    adapted to changes in vc_vc_map definition OCOMM-443
--  2015-06-30  Nikolay Tsvetkov
--    Creataed local functions get_filter_result and should_notify_for_filt_values
--  2015-05-26  Nikolay Tsvetkov
--    Added i_cu_filt_key_values and functionality to get recipient by event filters definition
--  2014-07-11  Chris Roderick
--    Creation - OCOMM-279
------------------------------------------------------------------------------------------------------------------------
  is
    l_event_id              com_event_definitions.event_id%type;
    l_recipients            com_notifier.table_of_recipients;
        
    c_combination_rule_or   constant com_event_notif_filters.filter_combination_rule%type := 'OR';
    c_combination_rule_and  constant com_event_notif_filters.filter_combination_rule%type := 'AND';
      
    function filter_key_value_rule_matches(
      i_filter_config_key_value in com_event_notif_filter_values.filter_values%type,
      i_filter_config_rule      in com_event_notif_filter_values.filter_rule%type,
      i_filter_event_value    in com_event_notif_filter_values.filter_values%type)
    return boolean
    is
      l_filter_match_count pls_integer := 0;
    begin
      execute immediate '
        select count(*) 
        from dual 
        where  :config_key_value '||i_filter_config_rule ||' :event_value' 
        into l_filter_match_count
        using i_filter_config_key_value, i_filter_event_value;
          
      return l_filter_match_count > 0;
    end filter_key_value_rule_matches;
    
    function should_notify_for_filt_values(i_filter_id in com_event_notif_filters.filter_id%type)
    return boolean
    is
      l_is_filter_match   boolean;
    begin
      for r_filter_config in (select cenfv.filter_key, cenfv.filter_values, cenfv.filter_rule, cenf.filter_combination_rule
                              from com_event_notif_filters cenf
                              join com_event_notif_filter_values cenfv on cenf.filter_id = cenfv.filter_id
                              where cenf.filter_id = i_filter_id)
      loop   
        l_is_filter_match := 
          i_filt_key_value_table.contains_key(r_filter_config.filter_key)
          and filter_key_value_rule_matches(
            i_filt_key_value_table.get_varchar_value(r_filter_config.filter_key), 
            r_filter_config.filter_rule, 
            r_filter_config.filter_values);
        
        if (r_filter_config.filter_combination_rule = c_combination_rule_or and l_is_filter_match)
            or (r_filter_config.filter_combination_rule = c_combination_rule_and and not l_is_filter_match) then
          exit;
        end if;
      end loop;
      
      return l_is_filter_match ;   
    end should_notify_for_filt_values;
     
  begin
    l_event_id := get_event_id(i_event_name);
    
    add_admin_recipients_for_event(l_recipients, i_event_name);
  
    for cur_recipients in (select cr.recipient_id, cr.recipient_name, cr.recipient_email, 
                              case when cen.sms = 'Y' then cr.recipient_sms_number 
                                    else null 
                                    end as recipient_sms_number, 
                              cr.recipient_is_admin, cen.filter_id
                            from com_event_notifications cen
                            join com_recipients cr on cr.recipient_id = cen.recipient_id
                            where cen.event_id = l_event_id)
    loop
      if cur_recipients.filter_id is null 
          or (i_filt_key_value_table is not null 
              and should_notify_for_filt_values(cur_recipients.filter_id)
             )
             then 
        com_notifier.add_recipient_to_list(
          l_recipients
          ,com_notifier.build_new_recipient_for(cur_recipients.recipient_name, cur_recipients.recipient_email, 
                         cur_recipients.recipient_sms_number, cur_recipients.recipient_is_admin, cur_recipients.recipient_id)
         );
      end if;  
    end loop;
  
    return l_recipients; 
  end get_recipients_for_event;
  
  procedure raise_event_internal(
    i_event_name          in  com_event_definitions.event_name%type,
    i_event_msg           in  clob     default null,
    i_raise_exception     in  boolean  default false,
    i_is_auton            in  boolean  default false,
    i_dynamic_recipients  in  com_notifier.table_of_recipients      default cast(null as com_notifier.table_of_recipients),
    i_attach_stack        in  boolean  default false,
    i_filter_payload      in  vc_vc_map default null,
    i_event_title         in  varchar2 default null
  )
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Raises the given event
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_event_name - Event Name
--    i_event_msg - Event Message
--    i_raise_exception - A flag which indicates whether the event should raise an exception
--    i_is_auton - indicates if this event shuold log and notify inside autonomous transactions.
--    i_attach_stack - indicates if the stack back trace should be attached to the event msg
--    i_filter_payload - table of type event_filter_payload holding key-value pairs used for filtering of the recipients
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    NO_EVENT
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-06  Chris Roderick
--    Added debug statement to support OCOMM-354
--  2015-07-09  Jose Rolland
--    [OCOMM-313] Added the parameter i_event_title.
--  2015-06-10 Nikolay Tsvetkov   
--    [OCOMM-310] Added i_filter_payload
--  2015-04-24 Lukasz   
--    OCOMM-311 Added i_attach_stack
--  2015-03-04  Chris Roderick, Maciej Peryt
--    [OCOMM-308] Added the parameter i_dynamic_recipients.
--  2014-07-11  Chris Roderick
--    Modified to take clob as input for event message instead of com_logger.log_entry%type - OCOMM-278
--  2013-12-20  Chris Roderick
--    Changed com_loggger entry to be autonomous when no event is defined, otherwise the log entry is never made.
--  2013-06-25  Gkergki Strakosa
--    Added missing predicate needed to only notify admins when an event has the appropriate flag
--  2011-08-03  Chris Roderick
--    Modified the query which establishes the recipients to be notified for this event, to take 
--    into account administrators that need to be notified in case the event requires admin 
--    notifications.
--    Moved call to get_event_sms_window inside the check if recipients were found for the event to 
--    avoid unecessary calls.
--  2011-03-18  Chris Roderick
--    Added parameter i_is_auton, which is passed to the com_logger and com_notifier calls
--  2011-03-07  Eve Fortescue-Beck, Chris Roderick
--    Re-naming of previous implementation of raise_event_internal
--  2010-12-09  Chris Roderick
--    Re-factored to use get_event_sms_window
--  2010-11-17  Eve Fortescue-Beck, Chris Roderick
--    Creation
----------------------------------------------------------------------------------------------------------------------- 
  is 
    l_event_category  com_event_definitions.event_category%type;
    l_recipients      com_notifier.table_of_recipients;
    l_sms_start_hour  com_event_definitions.event_sms_start_hour%type;
    l_sms_end_hour    com_event_definitions.event_sms_end_hour%type;
  begin
    l_event_category := get_event_category(i_event_name);
    com_logger.log_entry(l_event_category, (i_event_name||' : '||coalesce(
      conv_event_msg_to_log_msg(i_event_msg), get_event_description(i_event_name))), i_is_auton);

    l_recipients  :=  get_recipients_for_event(i_event_name, i_filter_payload);
    l_recipients  :=  com_notifier.combine_recipients(l_recipients, i_dynamic_recipients);
    if l_recipients is not null and l_recipients.count > 0 then 
      
      com_logger.debug(l_recipients.count||' recipients for event '||i_event_name, $$plunit);
      
      get_event_sms_window(i_event_name, l_sms_start_hour, l_sms_end_hour);
      com_notifier.notify(
        i_recipients      => l_recipients,
        i_subject         => coalesce(i_event_title,i_event_name),
        i_message         => coalesce(i_event_msg, get_event_description(i_event_name)) ,        
        i_sms_start_hour  => l_sms_start_hour,
        i_sms_end_hour    => l_sms_end_hour,
        i_is_auton        => i_is_auton
      );
    else
      com_logger.debug('No recipients found for event '||i_event_name, $$plunit);
    end if;
   
    if i_raise_exception and l_event_category = com_logger.c_error then
      com_error_mgr.raise_error(get_event_id(i_event_name), coalesce(conv_event_msg_to_err_msg(i_event_msg), get_event_description(i_event_name))||(
        case when i_attach_stack then ' Err: '||dbms_utility.format_error_backtrace end));
    end if;
   
  exception
    when no_data_found then    
      com_logger.log_entry(com_logger.c_error, 'Event is not defined! '||i_event_name||' Msg: '||i_event_msg, true);
      com_error_mgr.raise_error(get_event_id('NO_EVENT'), get_event_description('NO_EVENT'));     
    when others then 
      raise;
   
  end raise_event_internal;
    
  procedure define_event (
      i_event_name            in  com_event_definitions.event_name%type
    , i_event_category        in  com_logger.t_log_category
    , i_event_description     in  com_event_definitions.event_description%type
    , i_event_sms_start_hour  in  com_event_definitions.event_sms_start_hour%type
    , i_event_sms_end_hour    in  com_event_definitions.event_sms_end_hour%type
    , i_event_notify_admin    in  com_event_definitions.event_notify_admin%type
    , o_event_id              out com_event_definitions.event_id%type
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions
--    Added parameter to indicate if the occurence of the given event should be notified to 
--    administrators regardless of whether or not an explicit event notification has been set up.
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is 
  begin
    o_event_id  := get_new_event_id(i_event_category);
    
    insert into com_event_definitions (event_id ,event_name ,event_category ,event_description, event_sms_start_hour, event_sms_end_hour, event_notify_admin)
    values (o_event_id ,i_event_name, i_event_category, i_event_description, i_event_sms_start_hour, i_event_sms_end_hour, i_event_notify_admin);
 
  exception
    when dup_val_on_index then
      raise_error_event('EVENT_EXISTS', 'Event already exists with name '||i_event_name);
 
  end define_event;
 
  procedure update_event (
      i_event_name            in  com_event_definitions.event_name%type
    , i_event_category        in  com_logger.t_log_category
    , i_event_description     in  com_event_definitions.event_description%type
    , i_event_sms_start_hour  in  com_event_definitions.event_sms_start_hour%type
    , i_event_sms_end_hour    in  com_event_definitions.event_sms_end_hour%type
    , i_event_notify_admin    in  com_event_definitions.event_notify_admin%type
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions
--    Added parameter to indicate if the occurence of the given event should be notified to 
--    administrators regardless of whether or not an explicit event notification has been set up.
--  2011-01-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_event_id com_event_definitions.event_id%type; 
  begin
    l_event_id := get_event_id(i_event_name);

    if i_event_category is not null then
      update com_event_definitions 
      set event_category = i_event_category 
      where event_name = i_event_name;
    end if;
  
    if i_event_description is not null then 
      update com_event_definitions 
      set event_description = i_event_description 
      where event_name = i_event_name;
    end if; 
   
    if i_event_sms_start_hour is not null then
      update com_event_definitions 
      set event_sms_start_hour = i_event_sms_start_hour 
      where event_name = i_event_name;
    end if;
  
    if i_event_sms_end_hour is not null then
      update com_event_definitions 
      set event_sms_end_hour = i_event_sms_end_hour 
      where event_name = i_event_name;     
    end if; 
    
    if i_event_notify_admin is not null then
      update com_event_definitions 
      set event_notify_admin = i_event_notify_admin 
      where event_name = i_event_name;     
    end if; 

  exception
    when no_data_found then --if event doesn't exist in table    
      raise_error_event('NO_EVENT', 'Event is not defined! '||i_event_name);
    when others then 
      raise;
  end update_event;

  procedure delete_event (
    i_event_name            in  com_event_definitions.event_name%type
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-03  Chris Roderick
--    Replaced "return" call with "exit" call when combining the list of dependencies and reaching  
--    the size limit.
--  2011-03-03  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_event_id com_event_definitions.event_id%type;
--  Collection to hold source lines which have dependencies on the event that is to be deleted
    l_src_deps  dbms_sql.varchar2_table;   
--  variables for constructing the error message    
    l_errm_prefix       varchar2(200);
    l_errm_suffix       varchar2(200);
    l_errm_body         varchar2(1800);
    l_no_remaining_deps pls_integer;
  begin
--  first check that event exists, if not throw no_data_found exception
    l_event_id := get_event_id(i_event_name);
--  should search user_source for dependencies and raise an error with the name of the affected packages / procedures in case something is found.
--  Load any user_source with dependencies into the collection
--  NOTE: Event name is constrained to be in upper case in the table com_event_definitions, therefore no 
--  no specific case-sensitive handling is required here.
    select type||', '||name||' on line '||line src_dep
    bulk collect into l_src_deps
    from user_source 
    where text like '%'''||i_event_name||'''%'; 

--  If no dependecies, delete the event
    if (l_src_deps.count = 0) then
      delete from com_event_definitions
      where event_name = i_event_name;
    else
--    log the details of the dependcies and inform the user      
--    first, establish the details     
      l_errm_prefix :=  'Error:'||i_event_name||' found in '||l_src_deps.count ||' lines of source code:';
      l_errm_suffix :=  'Please remove all instances from code before attempting to delete this event.';
--    body value cannot be null else the sum of the lengths creates a numeric error
      l_errm_body :=  chr(10); 
        
      for i in 1..l_src_deps.count loop
             
        if (length(l_errm_prefix) + length(l_errm_suffix) + length(l_errm_body) +  length(chr(10)||l_src_deps(i)) < 1990) then          
         
          l_errm_body := l_errm_body||l_src_deps(i)||chr(10);  
        else 
          l_errm_body :=  l_src_deps.count-i||' more'||chr(10);
          exit;
        
        end if;
      
      end loop;
   
    raise_error_event('EVENT_HAS_SRC_DEPS', l_errm_prefix||l_errm_body||l_errm_suffix);
   
    end if;
   
  exception
    when no_data_found then --if event doesn't exist in table    
      raise_error_event('NO_EVENT', 'Event is not defined! '||i_event_name);
    when others then 
      raise;
  end delete_event;
 
  procedure raise_error_event(
    i_event_name      in  com_event_definitions.event_name%type
  , i_event_msg       in  clob  default null
  , i_dynamic_recipients  in  com_notifier.table_of_recipients      default cast(null as com_notifier.table_of_recipients) 
  , i_event_title         in  varchar2                              default null
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Jose Rolland
--    Added the parameter i_event_title. - OCOMM-313
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Added the parameter i_dynamic_recipients - OCOMM-308
--  2014-07-11  Chris Roderick
--    Changed the error message to not include sqlerrm which is usually 0 when throwing a user defined exception.
--  2013-07-15  Gkergki Strakosa
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is  
    l_final_err_msg   clob;
  begin
    l_final_err_msg := i_event_name||' : '||i_event_msg||(chr(10))||dbms_utility.format_error_backtrace;    
    raise_event(i_event_name, l_final_err_msg, true, true, i_dynamic_recipients, i_event_title);  
  end raise_error_event;
 
  procedure raise_event(
    i_event_name          in  com_event_definitions.event_name%type
  , i_event_msg           in  clob                                  default null
  , i_raise_exception     in  boolean                               default false
  , i_is_auton            in  boolean                               default false
  , i_dynamic_recipients  in  com_notifier.table_of_recipients      default cast(null as com_notifier.table_of_recipients) 
  , i_event_title         in  varchar2                              default null
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Jose Rolland
--    Added the parameter i_event_title. - OCOMM-313
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Added the parameter i_dynamic_recipients - OCOMM-308
--  2011-03-07  Eve Fortescue-Beck, Chris Roderick
--    Added i_is_auton flag to allow the event to be raised autonomously or not.
--  2010-11-03  Chris Roderick
--    Added i_event_msg parameter as optional input
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    raise_event_internal(i_event_name, i_event_msg , i_raise_exception, i_is_auton, i_dynamic_recipients, true, null, i_event_title);
  end raise_event; 
  
  procedure raise_event(
    i_event_id            in  com_event_definitions.event_id%type
  , i_event_msg           in  clob                                  default null
  , i_raise_exception     in  boolean                               default false
  , i_is_auton            in  boolean                               default false
  , i_dynamic_recipients  in  com_notifier.table_of_recipients      default cast(null as com_notifier.table_of_recipients)
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-03-04  Chris Roderick, Maciej Peryt
--    Added the parameter i_dynamic_recipients - OCOMM-308 
--  2011-03-07  Eve Fortescue-Beck, Chris Roderick
--    Added i_is_auton flag to allow the event to be raised autonomously or not.
--  2010-11-03  Chris Roderick
--    Added i_event_msg parameter as optional input
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    raise_event_internal(get_event_name(i_event_id), i_event_msg , i_raise_exception, i_is_auton, i_dynamic_recipients);
  exception
    when no_data_found then --if call to get_event_name fails, then raise NO_EVENT   
     com_logger.log_entry(com_logger.c_error, 'Event is not defined! '||i_event_id||' Msg: '||i_event_msg);
     com_error_mgr.raise_error(get_event_id('NO_EVENT'), get_event_description('NO_EVENT'));
  end raise_event;
  
  procedure clear_event_logs (
    i_older_than_utc  in  timestamp default null
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-24  Pascal Le Roux
--    Updated the call to the com_logger.clear_log due to changes in the com_logger package
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_logger.clear_log(null, null, i_older_than_utc);
  end clear_event_logs;
  
  procedure register_event_notification (
    i_event_name      in  com_event_definitions.event_name%type,
    i_recipient       in  com_recipients.recipient_id%type,
    i_sms             in  com_event_notifications.sms%type,
    i_filter_id       in  com_event_notif_filters.filter_id%type default null
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-04  Nikolay Tsvetkov
--    Added option for specifying filter per recipient and event
--  2010-10-19  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_event_id  com_event_definitions.event_id%type;
  begin
    l_event_id  := get_event_id(i_event_name);   
    
    insert into com_event_notifications (event_id ,recipient_id, sms, filter_id)
    values (l_event_id ,i_recipient, i_sms, i_filter_id);
 
  exception
    when dup_val_on_index then
      raise_error_event('RECIPIENT_OF_EVENT_EXISTS', 'Recipient already recieves notification of '||i_event_name);
    when others then
--    exception if one of the parent keys does not exist.
      if sqlcode = -2291 then 
        raise_error_event('EVENT_OR_RECIPIENT_NOT_FOUND', 'The Event or the Recipient is not found in parent table');
      else 
        raise;
      end if;
  end register_event_notification;

  procedure delete_event_notification (
    i_event_name      in  com_event_definitions.event_name%type,
    i_recipient       in  com_recipients.recipient_id%type
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-08-01  Chris Roderick
--    Formatting to COMMONS conventions
--    Removed unecessary select to check if notification was configured for the recipient, and used
--    the sql%rowcount instead
--  2011-03-03  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is 
    l_event_id com_event_notifications.event_id%type;
  begin
    begin   
      l_event_id := get_event_id(i_event_name); 
    exception
      when no_data_found then --if event doesn't exist in table    
        raise_error_event('NO_EVENT',  'Event is not defined! '||i_event_name);
    end;   
         
    delete from com_event_notifications
    where event_id = l_event_id
    and recipient_id = i_recipient;
    
--  if event notification was not configured, raise an exception
    if sql%rowcount = 0 then
      raise_error_event('NO_NOTIFICATIONS', 'The Recipient is not registered for notification of the Event'); 
    end if;
    
  end delete_event_notification; 

  procedure update_event_notification (
    i_event_name      in  com_event_definitions.event_name%type,
    i_recipient       in  com_recipients.recipient_id%type,
    i_sms             in com_event_notifications.sms%type
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-03  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_event_id com_event_notifications.event_id%type;
  begin  
    l_event_id := get_event_id(i_event_name); 
  
    update com_event_notifications
    set sms = i_sms 
    where event_id = l_event_id
    and recipient_id = i_recipient;
  
--  verify that notification exists and was updated sucessfully 
    if sql%rowcount != 1 then
      raise_error_event('NO_NOTIFICATIONS', 'The Recipient is not registered for notification');
    end if;

  exception
    when no_data_found then     
      raise_error_event('NO_EVENT', 'Event is not defined! '||i_event_name);
    when others then 
      raise;
  end update_event_notification; 

  function event_equals (
    i_event_name    in  com_event_definitions.event_name%type,
    i_event_id      in  com_event_definitions.event_id%type
  ) 
  return boolean result_cache
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-11  Chris Roderick
--    Added result_cache
--  2010-12-10  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is 
  begin
    return get_event_id(i_event_name) = i_event_id ;
  exception
    when no_data_found then    
      raise_error_event('NO_EVENT', 'Event is not defined! '||i_event_name);
        
  end event_equals;
  
  procedure raise_error_event_no_stack(
    i_event_name          in  com_event_definitions.event_name%type,
    i_event_msg           in  clob                                  default null,
    i_dynamic_recipients  in  com_notifier.table_of_recipients      default cast(null as com_notifier.table_of_recipients)
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-04-24 Lukasz   
--    OCOMM-311 Initall creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin       
    raise_event_internal(i_event_name, i_event_msg , true, true, i_dynamic_recipients, false);
  end raise_error_event_no_stack;
  
  procedure raise_event(i_command in com_notifier.t_notification_command)
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-06-16 Lukasz Burdzanowski ,Nikolay Tsvetkov   
--    [OCOMM-310] Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    raise_event_internal(i_command.event_name, i_command.event_msg, i_command.raise_exception, i_command.is_auton, 
                          i_command.dynamic_recipients, i_command.attach_stack, i_command.filter_payload);
  end raise_event;
  
  procedure unhandled_exception(
    i_exception_message in varchar2,
    i_handled_exception in com_event_definitions.event_name%type default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-28 Lukasz Burdzanowski
--    Creation - OCOMM-439
------------------------------------------------------------------------------------------------------------------------
  is
    l_event_name com_event_definitions.event_name%type := 'PROCESSING_ERROR';
  begin
    if i_handled_exception is not null and com_event_mgr.event_equals(i_handled_exception, sqlcode) then
      l_event_name := i_handled_exception;
    end if;   
    com_event_mgr.raise_error_event(l_event_name, i_exception_message||' '||dbms_utility.format_error_stack);   
  end unhandled_exception;
  
  procedure unhandled_exceptions(
    i_exception_message in varchar2,
    i_handled_exceptions in table_of_varchar default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-28 Lukasz Burdzanowski
--    Creation - OCOMM-439
------------------------------------------------------------------------------------------------------------------------
  is
    l_trigger_event com_event_definitions.event_name%type;     
    l_event_name com_event_definitions.event_name%type := 'PROCESSING_ERROR';
  begin
    if i_handled_exceptions is not empty then
      l_trigger_event := get_event_name(sqlcode);
      if l_trigger_event member of i_handled_exceptions then
        l_event_name := l_trigger_event;
      end if;
    end if;   
    com_event_mgr.raise_error_event(l_event_name, i_exception_message||' '||dbms_utility.format_error_stack);   
  end unhandled_exceptions;
  
end com_event_mgr;