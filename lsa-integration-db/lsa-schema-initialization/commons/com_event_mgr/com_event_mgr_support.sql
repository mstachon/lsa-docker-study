-- Define user events, with a given name, and category (like log4j levels: INFO, WARN, ERROR, FATAL) and store them in a table
create table com_event_definitions (
  event_id              integer       constraint comevntdefs_pk primary key,
  event_name            varchar2(30)  constraint comevntdefs_name_nn not null,
  event_category        varchar2(30)  constraint comevntdefs_cat_nn not null,
  event_description     varchar2(100) constraint comevntdefs_desc_nn not null,
  event_sms_start_hour  integer,
  event_sms_end_hour    integer,
  event_create_time_utc timestamp     default sys_extract_utc(systimestamp) constraint comevntdefs_ctimeutc_nn not null,
  event_creator_db_user varchar2(30)  default sys_context('USERENV', 'SESSION_USER') constraint comevntdefs_dbusr_nn not null,
  event_creator_os_user varchar2(30)  default sys_context('USERENV', 'OS_USER') constraint comevntdefs_osusr_nn not null,
  event_creator_client  varchar2(30)  default sys_context('USERENV', 'CLIENT_IDENTIFIER') constraint comevntdefs_client_nn not null,
  event_creator_module  varchar2(30)  default sys_context('USERENV', 'MODULE') constraint comevntdefs_module_nn not null,
  event_creator_action  varchar2(30)  default sys_context('USERENV', 'ACTION') constraint comevntdefs_action_nn not null,
  event_creator_host    varchar2(30)  default sys_context('USERENV', 'HOST') constraint comevntdefs_host_nn not null,
  event_creator_ip      varchar2(30)  default sys_context('USERENV', 'IP_ADDRESS') constraint comevntdefs_ip_nn not null,
  constraint comevntdefs_name_uk unique (event_name),
  constraint comevntdefs_name_uc_chk check (event_name = upper(event_name)),
  constraint comevntdefs_cat_chk check (event_category in ('INFO', 'WARN', 'ERROR', 'FATAL')),
  constraint comevntdefs_err_id_chk check (event_category not in ('ERROR', 'FATAL') or (event_id between -20999 and -20000)),
  constraint comevntdefs_smss_chk check (event_sms_start_hour between 0 and 24),
  constraint comevntdefs_smse_chk check (event_sms_end_hour between 0 and 24)
);

comment on column com_event_definitions.event_id is 'This uniquely identifes all events, if the event is of the category ERROR or FATAL, then the event_if must be between -20999 and -20000 in order to be able to raise application errors';

-- Create a sequence for generating event_ids.
create sequence com_event_ids_seq;

-- TODO: Need to makes calls to create the com_event_mgr package header and body
--@plsql/com_event_mgr.pls
--@plsql/com_event_mgr.plb

--  insert errors referenced in the event_mgr code into the events table
declare
  l_event_id com_event_definitions.event_id%type;

begin
--	Define the session level information which will be stored with the event definitions as part of the captured instrumentation
	dbms_application_info.set_module('COMMONS_SETUP', 'DEFINE_INTERNAL_EVENTS');
	dbms_session.set_identifier('COMMONS_SETUP');

  com_event_mgr.define_event('NO_ERROR_IDS', 'ERROR', 'There are no more event id''s in the range -20,000 to 20,999', 8, 20 ,l_event_id);
  com_event_mgr.define_event('NO_EVENT', 'ERROR', 'Event is not defined!',null,null ,l_event_id);
  com_event_mgr.define_event('EVENT_HAS_SRC_DEPS', 'ERROR', 'Event has source dependencies!',null,null ,l_event_id);
  com_event_mgr.define_event('NO_NOTIFICATIONS', 'ERROR', 'There are no matching notifications',null,null ,l_event_id);
  com_event_mgr.define_event('EVENT_EXISTS', 'ERROR', 'Event already exists!',null , null, l_event_id);
  com_event_mgr.define_event('RECIPIENT_OF_EVENT_EXISTS', 'ERROR','Recipient already receives notification', null, null , l_event_id);
  com_event_mgr.define_event('EVENT_OR_RECIPIENT_NOT_FOUND','ERROR', 'The Event or the Recipient is not found in parent table', null, null, l_event_id);
  com_event_mgr.define_event('ERROR_IN_SET_DML_TIME', 'ERROR', 'There was an error when trying to set DML time', null, null, l_event_id);
  com_event_mgr.define_event('DDL_FAILED', 'ERROR', 'There was an error when trying to run ddl', null, null, l_event_id);
  com_event_mgr.define_event('TABLE_NOT_FOUND', 'ERROR', 'Table not found', null, null, l_event_id );
  com_event_mgr.define_event('HISTORY_TABLE_EXISTS', 'ERROR', 'History Table already exists!', null, null, l_event_id );
  com_event_mgr.define_event('NO PK', 'ERROR', 'The Table does not have a Primary Key', null, null, l_event_id );
  com_event_mgr.define_event('NO_NON_KEY_COLS', 'ERROR', 'The Table does not have any non-primary key columns', null, null, l_event_id );
  com_event_mgr.define_event('HIST_STAMP_TRIGGER_FAILED', 'ERROR', 'Failed to create history stamp trigger', null, null, l_event_id);
  com_event_mgr.define_event('HIST_WRITE_TRIGGER_FAILED', 'ERROR', 'Failed to create history write trigger', null, null, l_event_id);
  com_event_mgr.define_event('HIST_TRIGGERS_FAILED', 'ERROR', 'Failed to create history triggers', null, null, l_event_id);  
  com_event_mgr.define_event('INIT_FAILED', 'ERROR', 'Cannot initialise history table', null, null, l_event_id);  
  com_event_mgr.define_event('ENABLE_HIST_FAILED', 'ERROR', 'Cannot enable history table', null, null, l_event_id);  
  com_event_mgr.define_event('HISTORY_NOT_ENABLED', 'ERROR', 'History was not enabled', null, null, l_event_id);  
  com_event_mgr.define_event('DISABLE_HIST_FAILED', 'ERROR', 'Cannot disable history table', null, null, l_event_id); 
  com_event_mgr.define_event('NOT_CLEANABLE', 'ERROR', 'Cannot disable history table', null, null, l_event_id); 
  com_event_mgr.define_event('CLEAN_HIST_FAILED', 'ERROR', 'Cannot clean history table', null, null, l_event_id); 
  com_event_mgr.define_event('HIST_TRIGGER_ERROR', 'ERROR', 'History Trigger Error', null, null, l_event_id);
  com_event_mgr.define_event('CONCAT_TOO_LONG', 'ERROR', 'Concatenation of values more than 4000 characters', null, null, l_event_id);
  com_event_mgr.define_event('HIST_TABLE_STRUCT_CHANGED', 'ERROR', 'History Table exists with a different structure. Intervention required for History to be re-enabled', null, null, l_event_id);
  


end;
/
commit;
--Define a lookup table to join Events to Recipients in a many:many relationship.
create table com_event_notifications (
  event_id			integer,
  recipient_id  	integer,
  sms 				char(1) default 'N'	constraint comevntnot_sms_nn not null,
  constraint comevntnot_pk primary key (event_id, recipient_id),
  constraint comevntnot_id_fk foreign key (event_id) references com_event_definitions(event_id),
  constraint comevntnot_recipient_fk foreign key (recipient_id) references com_recipients(recipient_id),
  constraint comevntnot_sms_chk check (sms in ('Y', 'N'))
) organization index;

-- Index on FK to recipients
create index comevntnot_recipient_fk on com_event_notifications (recipient_id);

declare
  l_event_id com_event_definitions.event_id%type;

begin
--	Define the session level information which will be stored with the event definitions as part of the captured instrumentation
	dbms_application_info.set_module('COMMONS_CLIENT_SETUP', 'DEFINE_COMMONS_CLIENT_EVENTS');
	dbms_session.set_identifier('COMMONS_CLIENT_SETUP');

  for rec in (select recipient_id from com_recipients where recipient_id in (1,21)) loop  
    begin
      com_event_mgr.register_event_notification('EVENT_HAS_SRC_DEPS', rec.recipient_id, 'N');
    exception
      when others then
        null;
    end;
  end loop;
  commit;
end;
/


select * from com_recipients;

select * from com_event_definitions;

begin
  com_event_mgr.delete_event('FAILED_NOTIFY');
end;
/
rollback;
truncate table com_logs;
select * from com_logs order by utc_timestamp desc;



