create or replace package body com_test_runner 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Commons4Oracle testing facility providing utility procdures to register and run test packages and test suites.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-19 Chris
--    Modified procedure run_test_suite. OCOMM-360
--  2015-08-19 Jose
--    Added procedure fail [CCS-6381]
--  2015-08-19  Chris Roderick
--    Formatting, and added procedure assert_pkg_procedure_exists
--  2015-07-22 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
is
  c_test_result_success constant com_test_run_results.test_result%type := 'SUCCESS';
  c_test_result_failure constant com_test_run_results.test_result%type := 'FAILURE';
  c_message_header      constant varchar2(1000) := '<html>
      <head><title>Commons4Oracle Test Runner results</title></head>
      <style>
        table {border: 1px solid #2854a1;border-collapse:collapse; background:#ffffff;padding:5px;}
        th {border: 1px solid #2854a1;background-color: #2854a1;color: #ffffff;font-family: Arial, Helvetica, Verdana, Lucida, Sans-Serif;font-size: 0.8em;font-weight: bold;text-align: center;}
        td {border: 1px solid #2854a1;background-color: #ffffff;font-family: Arial, Helvetica, Verdana, Lucida, Sans-Serif;font-size: 0.8em;text-align: center;}
      </style>
      <body style="font-family:Arial, Helvetica, Verdana, Lucida, Sans-Serif;font-size: 0.8em;">';
     
  function get_suite_id(i_suite_name in com_test_suites.suite_name%type)
  return com_test_suites.suite_id%type
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_suite_id com_test_suites.suite_id%type;
  begin 
    select suite_id into l_suite_id from com_test_suites where suite_name = i_suite_name;
    return l_suite_id;

  exception 
    when no_data_found then
      com_event_mgr.raise_error_event('NO_DATA_FOUND_ERROR', 'Test suite '||i_suite_name||' does not exist');        
  end get_suite_id;

  procedure record_test_result(
    i_run_id          in  com_test_runs.run_id%type,
    i_suite_id        in  com_test_run_results.suite_id%type,
    i_package_name    in  com_test_packages.package_name%type,
    i_procedure_name  in  user_procedures.procedure_name%type,    
    i_start_utc       in  com_test_run_results.utc_timestamp_start%type,
    i_is_success      in  boolean default true,
    i_message         in com_test_run_results.test_result_message%type default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    pragma autonomous_transaction;
    l_test_result com_test_run_results.test_result%type;
  begin
    l_test_result := case when i_is_success then c_test_result_success else c_test_result_failure end;
  
    insert into com_test_run_results (run_result_id, run_id, suite_id, package_id, test_name, test_result, 
      test_result_message, utc_timestamp_start, utc_timestamp_end)
    select com_test_runner_seq.nextval, i_run_id, i_suite_id, package_id, i_procedure_name, l_test_result, i_message, 
      i_start_utc, sys_extract_utc(systimestamp) 
    from com_test_packages 
    where package_name = i_package_name;
    commit;    
  end record_test_result;  
  
  procedure record_test_run(
    o_run_id      out nocopy  com_test_runs.run_id%type, 
    i_suite_name  in          com_test_suites.suite_name%type default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------  
  is    
    pragma autonomous_transaction;    
    l_suite_id com_test_suites.suite_id%type;
  begin
    if i_suite_name is not null then
      l_suite_id := get_suite_id(i_suite_name); 
    end if;
        
    insert into com_test_runs (run_id, suite_id) values (com_test_runner_seq.nextval, l_suite_id) 
    returning run_id into o_run_id;
    
    commit;
  end record_test_run;  
  
  procedure run_test_procedure_internal(
    i_package_name    in com_test_packages.package_name%type,
    i_procedure_name  in user_procedures.procedure_name%type,
    i_run_id          in com_test_runs.run_id%type,
    i_suite_id        in com_test_run_results.suite_id%type default null
  ) 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-24 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------    
  is
    l_test_start_utc  com_test_run_results.utc_timestamp_start%type;
    l_package_name    com_test_packages.package_name%type;
    l_procedure_name  user_procedures.procedure_name%type;
  begin  
    l_test_start_utc := sys_extract_utc(systimestamp);
    execute immediate 'begin '||i_package_name||'.'||i_procedure_name||'; end;';
    record_test_result(i_run_id, i_suite_id, i_package_name, i_procedure_name, l_test_start_utc);
  exception
    when others then            
      record_test_result(i_run_id, i_suite_id, i_package_name, i_procedure_name, l_test_start_utc, 
        false, substr(sqlerrm, 0, 4000));
  end run_test_procedure_internal;   
  
  procedure assert_package_exists(i_package_name in com_test_packages.package_name%type) 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-08-19  Chris Roderick
--    Replaced NO_DATA_FOUND_ERROR with ASSERTION_ERROR
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_package_count number;
  begin
    select count(*) into l_package_count from com_test_packages where package_name = upper(i_package_name);    
    com_assert.is_true(l_package_count > 0, 'Package '||i_package_name||' does not exist');
  end assert_package_exists;  

  procedure assert_pkg_procedure_exists (
    i_package_name    in com_test_packages.package_name%type,
    i_procedure_name  in user_objects.object_name%type
  ) 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-08-19  Chris Roderick
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_pkg_procedure_count number;
  begin
    select count(*) into l_pkg_procedure_count 
    from user_procedures 
    where object_name = upper(i_package_name)
    and procedure_name = upper(i_procedure_name);
    
    com_assert.is_true(l_pkg_procedure_count > 0, 
      'Procedure '||i_procedure_name||' does not exist in the header of the registered test package '||i_package_name);
  end assert_pkg_procedure_exists;  
  
  procedure run_test_procedure (
    i_package_name    in com_test_packages.package_name%type,
    i_procedure_name  in user_objects.object_name%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-08-19  Chris Roderick
--    Extracted some code to new procedure assert_pkg_procedure_exists
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is    
    l_run_id com_test_runs.run_id%type;
    l_row_count number;
  begin
    assert_pkg_procedure_exists(i_package_name, i_procedure_name);    
    record_test_run(l_run_id);
    run_test_procedure_internal(i_package_name, i_procedure_name, l_run_id);
  end run_test_procedure;    
  
  procedure run_test_package (i_package_name in com_test_packages.package_name%type)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_run_id com_test_runs.run_id%type;
    l_package_id com_test_packages.package_id%type;    
  begin
    assert_package_exists(i_package_name);
    select package_id into l_package_id from com_test_packages where package_name = upper(i_package_name);    
    
    record_test_run(l_run_id);  
    for r_tproc in (
      select ap.procedure_name as test_name 
      from user_procedures ap
      join user_objects ao on (ao.object_name = ap.object_name and ao.object_type = 'PACKAGE')
      where ao.object_name = upper(i_package_name) 
      and ap.procedure_name is not null 
      order by 1
    ) loop
      run_test_procedure_internal(i_package_name, r_tproc.test_name, l_run_id);         
    end loop;      
  end run_test_package;
  
  procedure notify_with_run_summary (i_test_run_id com_test_runs.run_id%type)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-25 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is    
    l_message_footer  varchar2(16)  := '</body><html>';
    l_text_content    clob;
    l_message         clob          := '<p> Results of com_test_runner: ';
  begin          
    select 'run id <b>'||ctr.run_id||'</b> on <b>'||to_char(ctr.utc_timestamp, 'YYYY-MM-DD HH24:MI:SS')
      ||'</b> for suite <b>'||cts.suite_name||'</b>' into l_text_content 
    from com_test_runs ctr 
    join com_test_suites cts on (ctr.suite_id = cts.suite_id) 
    where run_id = i_test_run_id;        
    
    l_message := l_message||l_text_content||'</p>';
    
    select com_formatter.ref_cursor_to_html_table (cursor (
      select * from (
        select test_result, cts.suite_name, ctp.package_name 
        from com_test_run_results  crr
        join com_test_packages ctp on (crr.package_id = ctp.package_id) 
        join com_test_suites cts on (crr.suite_id = cts.suite_id) 
        where run_id = i_test_run_id
      )
      pivot (count(*) for test_result in ('SUCCESS' as successes, 'FAILURE' as failures)) order by suite_name, package_name
    )) into l_text_content from dual;          
    
    l_message := l_message||'<p>Run summary:</p>'||l_text_content;
    
    select com_formatter.ref_cursor_to_html_table (cursor (
      select test_result, cts.suite_name, ctp.package_name, test_name,  
        com_util.interval_to_seconds(crr.utc_timestamp_end-crr.utc_timestamp_start) as test_duration
      from com_test_run_results  crr
      join com_test_packages ctp on (crr.package_id = ctp.package_id) 
      join com_test_suites cts on (crr.suite_id = cts.suite_id) 
      where run_id = i_test_run_id
      order by utc_timestamp_start
    )) into l_text_content from dual;      
    
    l_message := l_message||'<p>Test summary:</p>'||l_text_content;
  
    com_event_mgr.raise_event ('COM_TEST_RUNNER_RUN_FINISHED', c_message_header||l_message||l_message_footer);
  end notify_with_run_summary;
  
  procedure run_test_suite (
    i_suite_name  in com_test_suites.suite_name%type, 
    i_notify      in boolean default false
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-19 Chris
--    Added order by clause to running of test procedures, as integration tests which have some kind of state 
--    may otherwise interfere with each other if they are run in any random order. OCOMM-360
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
  is
    l_run_id com_test_runs.run_id%type;
  begin
    record_test_run(l_run_id, i_suite_name);    
    for r_tproc in (
      with suite as (
        select distinct level, cts.suite_id, cts.suite_name, ctp.package_name 
        from com_test_suites cts
        left join com_test_suite_packages ctsp on (cts.suite_id = ctsp.suite_id)
        left join com_test_packages ctp on (ctsp.package_id = ctp.package_id)
        start with cts.suite_name = i_suite_name
        connect by prior cts.suite_id = cts.parent_suite_id 
        order by level
      )
      select suite.*, ap.procedure_name 
      from suite
      join user_objects ao on (ao.object_name = suite.package_name and ao.object_type = 'PACKAGE')
      join user_procedures ap on (ap.object_name = ao.object_name)
      where ap.procedure_name is not null
      order by suite.package_name, ap.subprogram_id
    ) loop      
      run_test_procedure_internal(r_tproc.package_name, r_tproc.procedure_name, l_run_id, r_tproc.suite_id);               
    end loop; 
    
    if i_notify then
      notify_with_run_summary(l_run_id);
    end if;
  end run_test_suite;
  
  procedure fail (i_error_message in clob default null)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-08-19 Jose
--    Creation [CCS-6381]
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_event_mgr.raise_error_event('COM_TEST_RUNNER_TEST_FAILED', i_error_message);
  end fail;
end com_test_runner;