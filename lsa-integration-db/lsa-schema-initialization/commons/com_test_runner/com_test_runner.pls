create or replace package com_test_runner 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Commons4Oracle testing framework test runner. The package facilitates execution of single test procedures, 
--    test packages or complete test suites. Results of the run are stored in com_test_runs and com_test_run_results.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-05-13 Eve Fortescue-Beck
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-428
--  2015-08-19 Jose
--    Added procedure fail 
--  2015-07-22 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------
is

  procedure run_test_procedure (
    i_package_name    in com_test_packages.package_name%type,
    i_procedure_name  in user_objects.object_name%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Runs a given test procedure.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name - name of the test package
--    i_procedure_name - name of the test procedure to run
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR when the package or the procedure does not exist
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-24 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------

  procedure run_test_package (i_package_name in com_test_packages.package_name%type);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Runs a given test package.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name - name of the test package to run
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR when the package does not exist
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------

  procedure run_test_suite (
    i_suite_name  in  com_test_suites.suite_name%type,
    i_notify      in  boolean default false
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Runs test in a given suite. If the suite is a parent suite then all dependant suites are run as well.
--    Optionally sends a summary report to recipients of COM_TEST_RUNNER_RUN_FINISHED event.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_suite_name - name of the test suite to run
--    i_notify - flag to notify users about test run results
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    NO_DATA_FOUND_ERROR when the suite does not exist
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-23 Lukasz Burdzanowski
--    Creation [OCOMM-324]
------------------------------------------------------------------------------------------------------------------------

  procedure fail (i_error_message in clob default null);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Raises COM_TEST_RUNNER_TEST_FAILED error event.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_error_message - error message
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-08-19 Jose
--    Creation 
------------------------------------------------------------------------------------------------------------------------

end com_test_runner;