create or replace package body com_periods_test
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--  	Test package for l_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-07-06 Chris
--    Updated procedure pack_shift_start_of_overlaps. OCOMM-465
--  2016-01-07 Chris
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------
is

  procedure add_period
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for add_period
------------------------------------------------------------------------------------------------------------------------
  is
    i_com_period        com_period;
    l_periods           com_periods;
    l_expected_result   com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00')
      ));
    
    i_com_period      :=  com_period(timestamp '2015-01-03 00:00:00', timestamp '2015-01-04 00:00:00');
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-03 00:00:00', timestamp '2015-01-04 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_periods.add_period(i_com_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_periods = l_expected_result, 'Expected '||l_expected_result.to_string()||' but got '||l_periods.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end add_period;

  procedure contains
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for contains
------------------------------------------------------------------------------------------------------------------------
  is
    i_com_period        com_period;
    l_periods           com_periods;
    l_actual_result     com_types.t_boolean_as_char;
    l_expected_result   com_types.t_boolean_as_char;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00')
      ));
    
    i_com_period      :=  com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00');
    
    l_expected_result :=  com_constants.c_true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.contains(i_com_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected com_periods to contain the given period');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end contains;

  procedure count
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for count
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result     number;
    l_expected_result   number;
    l_periods           com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00')
      ));
    l_expected_result :=  2;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.count();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected ...');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end count;

  procedure count_occurrences
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for count_occurrences
------------------------------------------------------------------------------------------------------------------------
  is
    l_expected_result number; 
    l_actual_result   number;
    i_com_period      com_period;
    l_periods         com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    i_com_period      :=  com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00');
    
    l_expected_result :=  2;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.count_occurrences(i_com_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result||' occurences of the given period but got '||l_actual_result);
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end count_occurrences;

  procedure distincts
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for distincts
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result     com_periods;
    l_expected_result   com_periods;
    l_periods           com_periods;
    
  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.distincts();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end distincts;

  procedure duplicates
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for duplicates
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.duplicates();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end duplicates;

  procedure excepts
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for excepts
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;
    i_com_periods     com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    i_com_periods      :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.excepts(i_com_periods);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end excepts;

  procedure excepts_all
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for excepts_all
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;
    i_com_periods     com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    i_com_periods      :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.excepts_all(i_com_periods);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end excepts_all;

  procedure gantt_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for gantt_periods
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   table_of_varchar;
    l_expected_result table_of_varchar;
    i_gantt_width     number;
    l_periods         com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
        com_period(timestamp '2015-11-18 13:36:11', timestamp '2015-11-18 13:59:30', 23657, 'Power Converters'),
        com_period(timestamp '2015-11-18 16:40:47', timestamp '2015-11-18 21:36:11', 23822, 'Power Converters'),
        com_period(timestamp '2015-11-18 17:30:24', timestamp '2015-11-18 20:15:38', 23820, 'Access System'),
        com_period(timestamp '2015-11-18 20:35:59', timestamp '2015-11-18 23:09:34', 23841, 'QPS'),
        com_period(timestamp '2015-11-18 21:24:43', timestamp '2015-11-18 22:09:13', 23781, 'Transverse Damper'),
        com_period(timestamp '2015-11-18 22:09:13', timestamp '2015-11-18 23:04:51', 23844, 'Transverse Damper'),
        com_period(timestamp '2015-11-18 23:09:34', timestamp '2015-11-18 23:43:15', 23847, 'Power Converters')
      ));
      
    i_gantt_width     :=  100;
    l_expected_result :=  table_of_varchar(
        '                        Power Converters|==|', 
        '                        Power Converters|                             |===============================================|', 
        '                           Access System|                                     |==========================|', 
        '                                     QPS|                                                                    |========================|', 
        '                       Transverse Damper|                                                                            |======|', 
        '                       Transverse Damper|                                                                                   |========|', 
        '                        Power Converters|                                                                                             |====|' 
      );
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.gantt_periods(i_gantt_width);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||com_collection_utils.varchars_to_string(l_expected_result)||
      ' but got '||com_collection_utils.varchars_to_string(l_actual_result));
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end gantt_periods;

  procedure intersects
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for intersects
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;
    i_com_periods     com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    i_com_periods      :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-05-02 00:00:00', timestamp '2015-05-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.intersects(i_com_periods);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end intersects;

  procedure intersects_all
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for intersects_all
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;
    i_com_periods     com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    i_com_periods      :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-05-02 00:00:00', timestamp '2015-05-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.intersects_all(i_com_periods);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end intersects_all;

  procedure max_overlapping_period
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for max_overlapping_period
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result     com_period;
    l_expected_result   com_period;
    i_com_period        com_period;
    l_periods           com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-09 00:00:00'),
      com_period(timestamp '2015-01-04 00:00:00', timestamp '2015-01-08 00:00:00'),
      com_period(timestamp '2015-01-03 00:00:00', timestamp '2015-01-08 00:00:00'),
      com_period(timestamp '2015-01-04 00:00:00', timestamp '2015-01-09 00:00:00'),
      com_period(timestamp '2014-12-27 00:00:00', timestamp '2015-01-15 00:00:00'),
      com_period(timestamp '2014-12-31 00:00:00', timestamp '2015-01-08 00:00:00'),
      com_period(timestamp '2015-01-07 00:00:00', timestamp '2015-01-16 00:00:00'),
      com_period(timestamp '2015-01-03 00:00:00', timestamp '2015-01-12 00:00:00')
      ));
    
    i_com_period      :=  com_period(null,null, null, 'Overlap');
    
    l_expected_result :=  com_period(timestamp '2015-01-07 00:00:00', timestamp '2015-01-08 00:00:00', null, 'Overlap');
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.max_overlapping_period(i_com_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end max_overlapping_period;

  procedure order_asc
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for order_asc
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-04 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00'),
      com_period(timestamp '2013-01-02 00:00:00', timestamp '2013-04-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2013-01-02 00:00:00', timestamp '2013-04-03 00:00:00'),
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-04 00:00:00'),
      com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.order_asc();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end order_asc;

  procedure order_desc
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for order_desc
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;
    
  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-04 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00'),
      com_period(timestamp '2013-01-02 00:00:00', timestamp '2013-04-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-04 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00'),
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2013-01-02 00:00:00', timestamp '2013-04-03 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.order_desc();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end order_desc;

  procedure pack_assign_offsets_to_prior
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for pack_assign_offsets_to_prior
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-03 00:00:00', 1, 'A'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-05 00:00:00', 2, 'B'),
      com_period(timestamp '2015-01-04 00:00:00', timestamp '2015-01-06 00:00:00', 3, 'C'),
      com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-02-03 00:00:00', 4, 'D'),
      com_period(timestamp '2015-02-02 00:00:00', timestamp '2015-02-04 00:00:00', 5, 'E')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-06 00:00:00', 1, 'A'),
      com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-02-04 00:00:00', 4, 'D')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.pack_assign_offsets_to_prior();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end pack_assign_offsets_to_prior;

  procedure pack_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for pack_periods
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result             com_periods;
    l_expected_result           com_periods;
    i_overlapping_period_only   com_types.t_boolean_as_char;
    l_periods                   com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods                 :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-03 00:00:00', 1, 'A'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-05 00:00:00', 2, 'B'),
      com_period(timestamp '2015-01-04 00:00:00', timestamp '2015-01-06 00:00:00', 3, 'C'),
      com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-02-03 00:00:00', 4, 'D'),
      com_period(timestamp '2015-02-02 00:00:00', timestamp '2015-02-04 00:00:00', 5, 'E')
      ));
    
    i_overlapping_period_only :=  com_constants.c_true;
    
    l_expected_result         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-06 00:00:00', null, null),
      com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-02-04 00:00:00', null, null)
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.pack_periods(i_overlapping_period_only);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end pack_periods;

  procedure pack_shift_start_of_overlaps
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-07-06 Chris
--    Customised test data. OCOMM-465
--  2016-01-07 Chris
--    Auto-generated test for pack_shift_start_of_overlaps
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2017-05-16 22:25:36', timestamp '2017-05-16 23:05:52', 88669,	'Injector Complex » No Beam » CPS'),
      com_period(timestamp '2017-05-17 15:02:47', timestamp '2017-05-17 19:58:05', 88773,	'Cooling and Ventilation » Cooling'),
      com_period(timestamp '2017-05-17 15:02:47', timestamp '2017-05-17 19:58:05', 88779,	'Electrical Network » Distribution'),
      com_period(timestamp '2017-05-17 16:04:17', timestamp '2017-05-18 17:02:33', 88776,	'Cryogenics » Equipment » Production-1.8K'),
      com_period(timestamp '2017-05-18 17:26:55', timestamp '2017-05-18 19:09:26', 88961,	'Power Converters'),
      com_period(timestamp '2017-05-18 19:50:48', timestamp '2017-05-18 20:05:12', 88973,	'Injection Systems » MKI » Software')
    ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2017-05-16 22:25:36', timestamp '2017-05-16 23:05:52', 88669,	'Injector Complex » No Beam » CPS'), 
      com_period(timestamp '2017-05-17 15:02:47', timestamp '2017-05-17 19:58:05', 88773,	'Cooling and Ventilation » Cooling'),
--      com_period(timestamp '2017-05-17 15:02:47', timestamp '2017-05-17 19:58:05', 88779,	'Electrical Network » Distribution'),  -- skipped as ends before (equal to) prior ends      
      com_period(timestamp '2017-05-17 19:58:05', timestamp '2017-05-18 17:02:33', 88776,	'Cryogenics » Equipment » Production-1.8K'),  -- start time shifted to end of prior
      com_period(timestamp '2017-05-18 17:26:55', timestamp '2017-05-18 19:09:26', 88961,	'Power Converters'),
      com_period(timestamp '2017-05-18 19:50:48', timestamp '2017-05-18 20:05:12', 88973,	'Injection Systems » MKI » Software')
    ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.pack_shift_start_of_overlaps();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end pack_shift_start_of_overlaps;

  procedure remove_period
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for remove_period
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;
    i_com_period      com_period;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-04 00:00:00')
      ));
      
    i_com_period      :=  com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00');  
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-04 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.remove_period(i_com_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end remove_period;

  procedure sum_in_seconds
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for sum_in_seconds
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   number;
    l_expected_result number;
    l_periods         com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-03 00:00:00', 1, 'A'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-05 00:00:00', 2, 'B'),
      com_period(timestamp '2015-01-04 00:00:00', timestamp '2015-01-06 00:00:00', 3, 'C'),
      com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-02-03 00:00:00', 4, 'D'),
      com_period(timestamp '2015-02-02 00:00:00', timestamp '2015-02-04 00:00:00', 5, 'E')
      ));
          
    l_expected_result :=  950400;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.sum_in_seconds();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected sum_in_seconds to be '||l_expected_result||' but got '||l_actual_result);
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end sum_in_seconds;

  procedure unions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for unions
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;
    i_com_periods     com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    i_com_periods      :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00'),
      com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00'),
      com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.unions(i_com_periods);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end unions;

  procedure unions_all
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07 Chris
--    Auto-generated test for unions_all
------------------------------------------------------------------------------------------------------------------------
  is    
    l_actual_result   com_periods;
    l_expected_result com_periods;
    l_periods         com_periods;
    i_com_periods     com_periods;

  procedure with_given_conditions
  is
  begin
    l_periods         :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00')
      ));
    
    i_com_periods      :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00'),
      com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00')
      ));
    
    l_expected_result :=  com_periods(table_of_com_period(
      com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-02 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00'),
      com_period(timestamp '2015-01-02 00:00:00', timestamp '2015-01-03 00:00:00'),
      com_period(timestamp '2015-04-02 00:00:00', timestamp '2015-04-03 00:00:00'),
      com_period(timestamp '2016-01-02 00:00:00', timestamp '2016-01-03 00:00:00')
      ));
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_periods.unions_all(i_com_periods);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 
      'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end unions_all;

end com_periods_test;