create or replace type com_periods force 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This object type is intended to provide the constructor and functions to work with a table of com_period objects.
--    Convention used for period: lower boundary inclusive, upper boundary exclusive
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-11-24 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-414
--  2016-09-21  Chris Roderick
--    Added member function has_containing_period (i_com_period in com_period)
--  2016-01-07  Chris Roderick
--    Added map member function equals_comparator OCOMM-363
--    Added member function to_string OCOMM-363
--  2015-11-23  Chris Roderick OCOMM-363
--    Added member functions / procedures remove_period, add_period, pack_assign_offsets_to_prior and 
--    pack_shift_start_of_overlaps
--    Added constructor function com_period to ensure com_period_table is empty, but not null
--  2015-11-15  Chris Roderick OCOMM-363
--    renamed attribute l_com_period_table to com_period_table as the l_ prefix is not applicable for object attributes 
--    (same as table columns)
--    removed unused parameter i_timestamp_format from function gantt_periods
--  2015-11-11  Chris Roderick OCOMM-363
--    Added member function sum_in_seconds
--    formatting
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
is object (
  com_period_table table_of_com_period,

  constructor function com_periods (
    self              in out nocopy com_periods, 
    com_period_table  in            table_of_com_period
  ) 
  return self as result,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This overloads the would-be default constructor (with attributes named identically to the object definition), to
--    allow required validations and initialisations
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    self             - com_periods object
--    com_period_table - table of com_period
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  map member function equals_comparator 
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This function is implicitly used by Oracle to compare instances of com_periods e.g. when doing distinct, order by, 
--    multiset and = operations
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions  
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  member function to_string(i_delimiter in varchar2 default ',') 
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the periods converted to strings (see com_period.to_string()) as a delimited string
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_delimiter - the delimiter to use between the com_period objects 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  member function pack_periods  (i_overlapping_period_only char default 'Y') 
  return com_periods, 
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Pack overlapping periods (and optionally meeting periods)then return the resulting periods
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_overlapping_period_only - flag to specify if only overlapping periods should be packed together or also periods
--    which meet at the start or end of one of the periods
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function gantt_periods (i_gantt_width in integer default 150) 
  return table_of_varchar,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Return a simplistic gantt chart of the periods, expressed as a table of varchar
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_gantt_width - the width of the gantt chart in chars. 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------

  member function max_overlapping_period  (i_com_period in com_period default com_period(null, null)) 
  return com_period deterministic,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Return the maximum overlapping period in an optional time window given as a com_period
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_period - the optional time period to be checked 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function order_asc 
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns com_periods with the com_period entries sorted in ascending order
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function order_desc 
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns com_periods with the com_period entries sorted in descending order
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function unions (i_com_periods in com_periods) 
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns "self" union distinct i_com_periods (see Oracle multiset documentation for details)
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_periods - the periods to be combined with the periods contained in this com_periods instance to produce a 
--      union distinct set of periods  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function unions_all (i_com_periods in com_periods) 
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns "self" union all i_com_periods (see Oracle multiset documentation for details)
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_periods - the periods to be combined with the periods contained in this com_periods instance to produce a 
--      union all set of periods  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function intersects (i_com_periods in com_periods) 
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns "self" intersect distinct i_com_periods (see Oracle multiset documentation for details)
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_periods - the periods to be combined with the periods contained in this com_periods instance to produce an 
--      intersect distinct set of periods  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function intersects_all (i_com_periods in com_periods) 
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns "self" intersect all i_com_periods (see Oracle multiset documentation for details)
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_periods - the periods to be combined with the periods contained in this com_periods instance to produce an 
--      intersect all set of periods  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
 
  member function excepts (i_com_periods in com_periods) 
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns "self" except distinct i_com_periods (see Oracle multiset documentation for details)
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_periods - the periods to be combined with the periods contained in this com_periods instance to produce a 
--      "except distinct" set of periods  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function excepts_all (i_com_periods in com_periods) 
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns "self" except all i_com_periods (see Oracle multiset documentation for details)
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_periods - the periods to be combined with the periods contained in this com_periods instance to produce a 
--      except all set of periods  
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function count 
  return number,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the number of periods in this com_periods instance
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function count_occurrences(i_com_period in com_period default null) 
  return number,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the number of times the given period occurs in this com_periods instance
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_period - the period to be checked 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function distincts 
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns a distinct set of com_period entries
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function duplicates 
  return com_periods, 
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns distinct set of duplicated com_period entries
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  
  member function contains (i_com_period in com_period) 
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns 'Y' if this com_periods instance contains the given period, otherwise returns 'N'
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_period - the period to be checked 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------

  member function has_containing_period (i_com_period in com_period) 
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns 'Y' if this com_periods instance contains a period which contains (starts before and ends after) the 
--    given period, otherwise returns 'N'
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_period - the period to be checked 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  member function sum_in_seconds 
  return number,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns the total number of seconds across all periods
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-11  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  member function remove_period(i_com_period in com_period)
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns this com_periods instance with the given period removed
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_period - the period to remove
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  member procedure add_period(i_com_period in com_period),
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Adds the given period to this com_periods instance
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_period - the period to insert into com_periods instance 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  member function pack_assign_offsets_to_prior
  return com_periods,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns a new com_periods instance which is derived by looking through this com_periods instance in order of 
--    ascending period start times, and then
--    - removes any periods fully contained in the shadow of the prior period
--    - for periods that start after the start of the prior period, and end after the end of the prior period - 
--      assigns the additional offset in period duration to the prior period (i.e. extending the end time of the prior 
--      period), and remove the period concerned
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  member function pack_shift_start_of_overlaps
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns a new com_periods instance which is derived by looking through this com_periods instance in order of 
--    ascending period start times, and then
--    - removes any periods fully contained in the shadow of the prior period
--    - for periods that start after the start of the prior period, and end after the end of the prior period - 
--      shifts the start time of the period concerned to equal the end time of prior period
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

) 
not final;