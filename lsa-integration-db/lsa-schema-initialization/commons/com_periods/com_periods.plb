create or replace type body com_periods
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-07-06 Chris
--    Updated functions pack_assign_offsets_to_prior and pack_assign_offsets_to_prior. OCOMM-465
--  2016-01-07  Chris Roderick
--    Added map member function equals_comparator OCOMM-363
--    Added member function to_string OCOMM-363
--  2015-11-23  Chris Roderick OCOMM-363
--    Added member functions / procedures remove_period, add_period, pack_assign_offsets_to_prior and 
--    pack_shift_start_of_overlaps
--    Added constructor function com_period to ensure com_period_table is empty, but not null
--  2015-11-11  Chris Roderick OCOMM-363
--    Added member function sum_in_seconds
--    formatting
--  2015-07-09  Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  constructor function com_periods (
    self              in out nocopy com_periods, 
    com_period_table  in            table_of_com_period
  ) 
  return self as result
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    self.com_period_table := coalesce(com_period_table, table_of_com_period());
    return;
  end com_periods;
  
  map member function equals_comparator 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
--  FIXME - Chris Roderick - this is quite fragile, and needs re-thinking how to avoid running out of chars. Oracle does not allow to use a CLOB here.  
    l_periods_as_string com_types.t_varchar2_max_size_plsql;
  begin 
    for r_period in 1..self.com_period_table.count loop
      l_periods_as_string :=  l_periods_as_string||self.com_period_table(r_period).to_string();
    end loop;
    return l_periods_as_string;
  end equals_comparator;  

  member function to_string(i_delimiter in varchar2 default ',') 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
--  FIXME - Chris Roderick - this is quite fragile, and needs re-thinking how to avoid running out of chars. Oracle does not allow to use a CLOB here.  
    l_periods_as_string com_types.t_varchar2_max_size_plsql;
  begin 
    for r_period in 1..self.com_period_table.count loop
      l_periods_as_string :=  l_periods_as_string||self.com_period_table(r_period).to_string();
    end loop;
    return l_periods_as_string;
  end to_string;
  
  member function pack_periods  (i_overlapping_period_only char default 'Y') 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
    l_periods       table_of_com_period;
    c_eternity_date constant timestamp    := com_constants.c_eternity_date;
    c_true          constant char (1)     := com_constants.c_true;
  begin
    if self.com_period_table.count > 0 then
  
      with periods as (
        select row_number() over(order by period_start, period_end) as pid, period_start, period_end
        from (
          select distinct pe.period_start, pe.period_end --remove duplicate periods
           from table(self.com_period_table) pe
        )
      ), removed_internal_periods as (
        select pa.pid, pa.period_start, pa.period_end 
       from periods pa
       where  not exists(select null  
                         from periods pin
                         where pa.pid != pin.pid and pa.period_start >= pin.period_start and pa.period_end <= pin.period_end)  
      ), startofgroup as (
        select pid, period_start, period_end, (case 
          when (--overlappings
            period_start < coalesce(lag(period_end) over(order by period_start), c_eternity_date)
            and period_end > coalesce(lag(period_start) over(order by period_start), c_eternity_date)
            ) 
            or (i_overlapping_period_only != c_true 
              and (--meetings
                (period_end = coalesce(lag(period_start) over(order by period_start), c_eternity_date)) 
                or (coalesce(lag(period_end) over(order by period_start), c_eternity_date) = period_start)
              )
            ) then 0 else 1 end
          ) new_group
        from removed_internal_periods 
      ), grouped_periods as (
        select pid, period_start, period_end, new_group, sum(new_group) over(order by period_start) as group_no
        from startofgroup
      ), packed_by_group as (
        select min(period_start) period_start, max(period_end) period_end
        from grouped_periods
        group by group_no  
      ) 
      select com_period(period_start, period_end) 
      bulk collect into l_periods
      from packed_by_group;
     
    end if;
    return com_periods(l_periods);
  end pack_periods;

  member function gantt_periods (i_gantt_width in integer default 150) 
  return table_of_varchar 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
    l_result      table_of_varchar  := table_of_varchar();
    l_left_margin integer           := 40;  
  begin
    if self.com_period_table.count > 0 then
      with periods as (
        select rownum as rowindex, coalesce(t.period_label, to_char(t.period_id), to_char(rownum)) as label, 
          t.period_start  start_date, t.period_end end_date
        from table(self.com_period_table) t
      ), limits as (
        select min(start_date) period_start, max(end_date) period_end
        from periods
      ), bars as ( 
        select lpad(label, l_left_margin)||'|' activity, 
          (cast(start_date as date) - cast(period_start as date))/(cast(period_end as date) - cast(period_start as date)) * i_gantt_width from_pos, 
          (cast(end_date as date) - cast(period_start as date))/(cast(period_end as date) - cast(period_start as date)) * i_gantt_width to_pos
        from periods
        cross join limits
      )
      select  activity||lpad('|',from_pos)||rpad('=', to_pos - from_pos-1, '=')||'|' gantt 
      bulk collect into l_result
      from bars;
    end if;
    return l_result;
  end gantt_periods;    

  member function max_overlapping_period  (i_com_period in com_period default com_period(null, null)) 
  return com_period deterministic
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
    l_count                           integer     :=0;
    l_from_date                       timestamp;   
    l_to_date                         timestamp;  
    l_com_period                      com_period  := i_com_period;
    c_default_period_start  constant  timestamp   := com_constants.c_start_of_cern; 
    c_default_period_end    constant  timestamp   := com_constants.c_eternity_date;

  begin
    if self.com_period_table.count > 0 then
      begin
--  FIXME - Chris 2015-11-16 - this is not easy to understand what it is actually doing...
--  Pascal says "Could be refactored by implementing a new function checking if a set of periods passed as parameter are all overlapping (We would pass self.com_period_table as parameter), then doing union between self.com_period_table and l_com_period, we would avoid the third select statement"
--    see review comment https://sources.cern.ch/cru/OCOMM-REVIEWS-6#c39734

--      if we do not get a no_data_found error then there are periods which don't overlap...
        select unique 1 into l_count
        from table(self.com_period_table) t1
           , table(self.com_period_table) t2
        where not (t1.period_start,t1.period_end) overlaps (t2.period_start,t2.period_end);
      exception
        when no_data_found then
          l_com_period.period_start := coalesce(l_com_period.period_start, c_default_period_start);
          l_com_period.period_end   := coalesce(l_com_period.period_end, c_default_period_end);
  
          select max(l_start), min(l_end) 
          into l_from_date, l_to_date
          from (
            select greatest (t1.period_start,t2.period_start) l_start, least(t1.period_end,t2.period_end) l_end
            from table(self.com_period_table) t1, table(self.com_period_table) t2
            where (t1.period_start,t1.period_end) overlaps (t2.period_start,t2.period_end)
          );
  
          select greatest (l_from_date, l_com_period.period_start), least(l_to_date, l_com_period.period_end) 
          into l_com_period.period_start, l_com_period.period_end
          from dual
          where (l_from_date, l_to_date) overlaps (l_com_period.period_start, l_com_period.period_end);
          
        when others then
          raise;
      end;

      return l_com_period;      
    else
      return com_period(null,null); 
    end if;
  end max_overlapping_period;

  member function order_asc 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  table_of_com_period;
  begin
    select com_period(period_start, period_end, period_id, period_label) bulk collect into l_result
    from table(self.com_period_table)
    order by period_start, period_end, period_id nulls last, period_label nulls last;  
  
    return com_periods(l_result);
  end order_asc;  

  member function order_desc 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  table_of_com_period;
  begin
    select com_period(period_start, period_end, period_id, period_label) bulk collect into l_result
    from table(self.com_period_table)
    order by period_start desc, period_end desc, period_id desc nulls last, period_label desc nulls last;
    
    return com_periods(l_result);
  end order_desc; 

  member function unions (i_com_periods in com_periods) 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
  begin
--  Requires a "map member function" in the com_period object type
    return com_periods(self.com_period_table multiset union distinct i_com_periods.com_period_table);
  end unions;

  member function unions_all (i_com_periods in com_periods) 
  return com_periods
  is
  begin
--  Requires a "map member function" in the com_period object type  
    return com_periods(self.com_period_table multiset union all i_com_periods.com_period_table);
  end unions_all;  
  
  member function intersects (i_com_periods in com_periods) 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
  begin
--  Requires a "map member function" in the com_period object type  
    return com_periods(self.com_period_table multiset intersect distinct i_com_periods.com_period_table);
  end intersects;
  
  member function intersects_all (i_com_periods in com_periods) 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
  begin
--  Requires a "map member function" in the com_period object type  
    return com_periods(self.com_period_table multiset intersect all i_com_periods.com_period_table);
  end intersects_all;
  
  member function excepts (i_com_periods in com_periods) 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
  begin
--  Requires a "map member function" in the com_period object type  
    return com_periods(self.com_period_table multiset except distinct i_com_periods.com_period_table);  
  end excepts;
  
  member function excepts_all (i_com_periods in com_periods) 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
  begin
--  Requires a "map member function" in the com_period object type  
    return com_periods(self.com_period_table multiset except all i_com_periods.com_period_table);
  end excepts_all;

  member function count return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return self.com_period_table.count;
  end count;

  member function count_occurrences(i_com_period in com_period default null) 
  return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
    l_result                          number;
    c_default_period_start   constant timestamp := com_constants.c_start_of_cern; 
  begin
      select count(*) 
      into l_result
      from table(self.com_period_table) pe
      where coalesce(pe.period_start, c_default_period_start) = coalesce(i_com_period.period_start, c_default_period_start) 
        and coalesce(pe.period_end, c_default_period_start) = coalesce(i_com_period.period_end, c_default_period_start)  
        and coalesce(pe.period_id, -9999)   = coalesce(i_com_period.period_id, -9999)  
        and coalesce(pe.period_label, '-xXx')   = coalesce(i_com_period.period_label, '-xXx');  
    return l_result;
  end count_occurrences;
  
  member function distincts 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  table_of_com_period;
  begin
    select com_period(period_start, period_end, period_id, period_label) 
    bulk collect into l_result
    from (
      select distinct period_start, period_end, period_id, period_label 
      from table(self.com_period_table)
    ) 
    order by period_start, period_end, period_id nulls last, period_label nulls last;
    
    return com_periods(l_result);
  end distincts;

  member function sum_in_seconds 
  return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-11  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
    l_result number :=  0;
  begin
    if self.com_period_table is not null then 
      for r_period in 1..self.com_period_table.count loop
        l_result  :=  l_result  + com_period_table(r_period).duration_in_seconds;
      end loop;
    end if;
    return l_result;
  end sum_in_seconds;

  member function duplicates 
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  table_of_com_period;
  begin
    select com_period(period_start, period_end, period_id, period_label) bulk collect into l_result
    from table(self.com_period_table)
    group by period_start, period_end, period_id, period_label having count(*) > 1;
    
    return com_periods(l_result);
  end duplicates;  

  member function contains (i_com_period in com_period) 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-23  Chris Roderick
--    Changed to PL/SQL only to try and improve performance by removing context switch to SQL. OCOMM-363
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-319
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    for r_period in 1..self.com_period_table.count loop
      if i_com_period = com_period_table(r_period) then
        return com_constants.c_true;
      end if;
    end loop;    
    return com_constants.c_false;
  end contains;
  
  member function remove_period(i_com_period in com_period)
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return self.excepts(com_periods(table_of_com_period(i_com_period)));
  end remove_period;  

  member procedure add_period(i_com_period in com_period)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    self.com_period_table.extend();
    self.com_period_table(self.com_period_table.count)  := i_com_period;
  end add_period;
  
  member function pack_assign_offsets_to_prior
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-07-06 Chris
--    Code simplification following solution to OCOMM-465
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
    l_periods_to_pack                 table_of_com_period;
    l_packed_fault_periods            com_periods;
    l_skipped_fault_periods           com_periods;
    l_this_period                     com_period;
    l_next_period                     com_period;
    l_last_kept_period                com_period;
    
    procedure keep_period(
      i_period_to_keep  in com_period,
      i_reason_to_keep  in  varchar2
    )
    is
    begin
      com_logger.debug('Keeping period '||i_period_to_keep.to_string()||' as '||i_reason_to_keep, $$plsql_unit);
      l_packed_fault_periods.add_period(i_period_to_keep);
      l_last_kept_period  :=  i_period_to_keep;
    end keep_period;
    
    procedure skip_period(
      i_period_to_skip  in  com_period,
      i_reason_to_skip  in  varchar2
    )
    is
    begin
      com_logger.debug('Skipping period '||i_period_to_skip.to_string()||' as '||i_reason_to_skip, $$plsql_unit);
      l_skipped_fault_periods.add_period(i_period_to_skip);
    end skip_period;
    
    function skipped_period(i_period_to_check  in  com_period)
    return boolean
    is
    begin
      return com_util.char_to_boolean(l_skipped_fault_periods.contains(i_period_to_check));
    end skipped_period;
    
    function kept_period(i_period_to_check  in  com_period)
    return boolean
    is
    begin
      return com_util.char_to_boolean(l_packed_fault_periods.contains(i_period_to_check));
    end kept_period;
    
  begin
    l_periods_to_pack       :=  self.com_period_table;
    l_packed_fault_periods  :=  com_periods(null);
    l_skipped_fault_periods :=  com_periods(null);

    if l_periods_to_pack is null or l_periods_to_pack.count <= 1 then
      return self;
    end if;
    
    l_this_period := l_periods_to_pack(1);
      
    for r_period in 2..l_periods_to_pack.count loop
      l_next_period := l_periods_to_pack(r_period);
      
      com_logger.debug('Start processing:'||com_constants.c_line_break
        ||'This period '||l_this_period.to_string()||com_constants.c_line_break
        ||'Next period '||l_next_period.to_string(), $$plsql_unit);

      case
        when l_last_kept_period is not null and l_this_period.ends_before_end_of(l_last_kept_period) then
          skip_period(l_this_period, 'ends before last kept ends');
          l_this_period := l_next_period;
          continue;
        when l_last_kept_period is not null and l_next_period.ends_before_end_of(l_last_kept_period) then
          skip_period(l_next_period, 'ends before last kept ends');
          continue;
        when not l_this_period.overlaps_with(l_next_period) then
          keep_period(l_this_period, 'independent from next');
          l_this_period := l_next_period;
          continue;
        when not l_this_period.ends_before_end_of(l_next_period) then
          skip_period(l_next_period, 'ends before prior ends');
          continue;
        when l_this_period.overlaps_with(l_next_period) then
          com_logger.debug('Adding offset of next period ('||l_next_period.to_string()||') to end of this period ('||l_this_period.to_string()||') due to overlap', $$plsql_unit);
          l_this_period.period_end := l_next_period.period_end;    
          skip_period(l_next_period, 'as assigned offset to prior');
        else
          com_event_mgr.raise_error_event('INTERNAL_ERROR', 'Missing logic whilst treating this period ('||l_this_period.to_string()||' and next period ('||l_next_period.to_string());
      end case;
            
    end loop;
  
-- Handle the current period being treated
    com_logger.debug('Processing remaining period', $$plsql_unit);
      
    if (not skipped_period(l_this_period) and not kept_period(l_this_period)) then
      keep_period(l_this_period, 'last period to be treated and not contained in prior');
    end if;
  
-- Handle the last fault in case it has not been skipped
    if (not skipped_period(l_next_period) and not kept_period(l_next_period)) then
      keep_period(l_next_period, 'last period in set and not contained in prior');
    end if;
    
    return l_packed_fault_periods;
  end pack_assign_offsets_to_prior;

  member function pack_shift_start_of_overlaps
  return com_periods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-07-06 Chris
--    Bug fix and code simplification. OCOMM-465
--  2015-11-23  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
    l_periods_to_pack                 table_of_com_period;
    l_packed_fault_periods            com_periods;
    l_skipped_fault_periods           com_periods;
    l_this_period                     com_period;
    l_next_period                     com_period;
    l_last_kept_period                com_period;
    
    procedure keep_period(
      i_period_to_keep  in com_period,
      i_reason_to_keep  in  varchar2
    )
    is
    begin
      com_logger.debug('Keeping period '||i_period_to_keep.to_string()||' as '||i_reason_to_keep, $$plsql_unit);
      l_packed_fault_periods.add_period(i_period_to_keep);
      l_last_kept_period  :=  i_period_to_keep;
    end keep_period;
    
    procedure skip_period(
      i_period_to_skip  in  com_period,
      i_reason_to_skip  in  varchar2
    )
    is
    begin
      com_logger.debug('Skipping period '||i_period_to_skip.to_string()||' as '||i_reason_to_skip, $$plsql_unit);
      l_skipped_fault_periods.add_period(i_period_to_skip);
    end skip_period;
    
    function skipped_period(i_period_to_check  in  com_period)
    return boolean
    is
    begin
      return com_util.char_to_boolean(l_skipped_fault_periods.contains(i_period_to_check));
    end skipped_period;
    
    function kept_period(i_period_to_check  in  com_period)
    return boolean
    is
    begin
      return com_util.char_to_boolean(l_packed_fault_periods.contains(i_period_to_check));
    end kept_period;
    
  begin
    l_periods_to_pack       :=  self.order_asc().com_period_table;
    l_packed_fault_periods  :=  com_periods(null);
    l_skipped_fault_periods :=  com_periods(null);
    
    if l_periods_to_pack is null or l_periods_to_pack.count <= 1 then
      return self;
    end if;
    
    l_this_period := l_periods_to_pack(1);
      
    for r_period in 2..l_periods_to_pack.count loop
      l_next_period := l_periods_to_pack(r_period);
      
      com_logger.debug('Start processing:'||com_constants.c_line_break
        ||'This period '||l_this_period.to_string()||com_constants.c_line_break
        ||'Next period '||l_next_period.to_string(), $$plsql_unit);

      case
        when l_last_kept_period is not null and l_this_period.ends_before_end_of(l_last_kept_period) then
          skip_period(l_this_period, 'ends before last kept ends');
          l_this_period := l_next_period;
          continue;
        when l_last_kept_period is not null and l_next_period.ends_before_end_of(l_last_kept_period) then
          skip_period(l_next_period, 'ends before last kept ends');
          continue;
        when not l_this_period.overlaps_with(l_next_period) then
          keep_period(l_this_period, 'independent from next');
          l_this_period := l_next_period;
          continue;
        when not l_this_period.ends_before_end_of(l_next_period) then
          skip_period(l_next_period, 'ends before prior ends');
          continue;
        when l_this_period.overlaps_with(l_next_period) then
          l_next_period.period_start :=  l_this_period.period_end;
          com_logger.debug('Shifted start of next period ('||l_next_period.to_string()||') to end of this period ('||l_this_period.to_string()||') due to overlap', $$plsql_unit);          
          keep_period(l_this_period, 'independent from prior');
          l_this_period := l_next_period;
        else
          com_event_mgr.raise_error_event('INTERNAL_ERROR', 'Missing logic whilst treating this period ('||l_this_period.to_string()||' and next period ('||l_next_period.to_string());
      end case;
            
    end loop;
  
-- Handle the current period being treated
    com_logger.debug('Processing remaining period', $$plsql_unit);
      
    if (not skipped_period(l_this_period) and not kept_period(l_this_period)) then
      keep_period(l_this_period, 'last period to be treated and not contained in prior');
    end if;
  
-- Handle the last fault in case it has not been skipped
    if (not skipped_period(l_next_period) and not kept_period(l_next_period)) then
      keep_period(l_next_period, 'last period in set and not contained in prior');
    end if;
    
    return l_packed_fault_periods;
    
  end pack_shift_start_of_overlaps;
  
  member function has_containing_period (i_com_period in com_period) 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-09-21  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    for r_period in 1..self.com_period_table.count loop
      if com_period_table(r_period).contains(i_com_period) then
        return com_constants.c_true;
      end if;
    end loop;    
    return com_constants.c_false;
  end has_containing_period;
  
end;