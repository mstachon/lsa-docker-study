create or replace package body com_string_utils_test
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--  	Auto-generated test package for com_string_utils
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-03 Chris
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------
is
  procedure test_concatenate_varchars
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-03 Chris
--    Auto-generated test for concatenate_varchars
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  com_types.t_varchar2_max_size_plsql;
    i_varchar_1                      com_types.t_varchar2_max_size_plsql;
    i_varchar_2                      com_types.t_varchar2_max_size_plsql;
    i_max_resulting_size             pls_integer;
    l_expected_test_result           com_types.t_varchar2_max_size_plsql;

  procedure with_given_conditions
  is
  begin
    i_varchar_1             :=  'Varchar1';
    i_varchar_2             :=  '_Varchar2';
    i_max_resulting_size    :=  com_constants.c_varchar2_max_size_plsql;
    l_expected_test_result  :=  'Varchar1_Varchar2';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_string_utils.concatenate_varchars(i_varchar_1, i_varchar_2, i_max_resulting_size);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = l_expected_test_result, 'Expected '''||l_expected_test_result||''' but got '''||function_result||'''');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end test_concatenate_varchars;

  procedure test_concatenate_varchars_fail
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-03 Chris
--    Test that concatenate_varchars fails as expected
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  com_types.t_varchar2_max_size_plsql;
    i_varchar_1                      com_types.t_varchar2_max_size_plsql;
    i_varchar_2                      com_types.t_varchar2_max_size_plsql;
    i_max_resulting_size             pls_integer;

  procedure with_given_conditions
  is
  begin
    i_varchar_1             :=  'Varchar1';
    i_varchar_2             :=  '_Varchar2';
    i_max_resulting_size    :=  length(i_varchar_1);
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_string_utils.concatenate_varchars(i_varchar_1, i_varchar_2, i_max_resulting_size);
  end when_executing;

  procedure should_result_in
  is
  begin
    null;
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_assert.is_true(com_event_mgr.event_equals('ASSERTION_ERROR', i_error_code));
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end test_concatenate_varchars_fail;
  
  procedure should_gen_idnt_with_affixes
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-14 lburdzan
--    Auto-generated test for get_as_valid_oracle_identifier
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  varchar2(4000);
    i_name                           varchar2(4000);
    i_prefix                         varchar2(4000);
    i_suffix                         varchar2(4000);

  procedure with_given_conditions
  is
  begin
    i_name := 'JOHN_DOE';
    I_PREFIX := 'SIR_';
    i_suffix := '_THE_MIGHTY';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_string_utils.get_as_valid_oracle_identifier(i_name, i_prefix, i_suffix);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = 'SIR_JOHN_DOE_THE_MIGHTY');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('Test failed, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end should_gen_idnt_with_affixes;  
  
  procedure should_gen_shortened_idnt
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-14 lburdzan
--    Auto-generated test for get_as_valid_oracle_identifier
------------------------------------------------------------------------------------------------------------------------
  is
    function_result                  varchar2(4000);
    i_name                           varchar2(4000);
    i_prefix                         varchar2(4000);
    i_suffix                         varchar2(4000);

  procedure with_given_conditions
  is
  begin
    i_name := 'THIS_STRING_IS_LONGER_THAN_30_CHARACTERS';
    i_prefix := 'PREFIX_';
    i_suffix := '_SUFFIX';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    function_result := com_string_utils.get_as_valid_oracle_identifier(i_name, i_prefix, i_suffix);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(function_result = 'PREFIX_THIS_STRING_IS_L_SUFFIX');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('Test failed, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    rollback;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      raise;
  end should_gen_shortened_idnt;  
  
end com_string_utils_test;