create or replace package com_string_utils
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
-- Description:
--  This package is intended to provide common string related utility methods
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-14  Lukasz Burdzanowski
--    Added get_as_valid_oracle_identifier OCOMM-359
--  2015-10-02 Chris Roderick
--    Creation. OCOMM-343
------------------------------------------------------------------------------------------------------------------------
is
  function concatenate_varchars(
    i_varchar_1           in varchar2,
    i_varchar_2           in varchar2,
    i_max_resulting_size  in pls_integer  default com_constants.c_varchar2_max_size_plsql
  )
  return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Concatenates the 2 given varchars into a single varchar, whilst asserting that the resulting varchar will not 
--    exceed the specified maximum size
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_varchar_1 - the first varchar to be concatenated to
--   i_varchar_2 - the second varchar to concatenate with the first
--   i_max_resulting_size - the maximum size which the resulting varchar should not exceed
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR when the strings to be concatenated exceed the specified max string length
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-02  Chris Roderick
--    Creation. OCOMM-343
------------------------------------------------------------------------------------------------------------------------

  function get_as_valid_oracle_identifier (
    i_name    in varchar2, 
    i_prefix  in varchar2 default null, 
    i_suffix  in varchar2 default null
  )
  return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Concatenates passed name with optional prefix and suffix, returning a varchar which is not longer than 30 
--    characters. The name may be shortened if needed while affixes are preserved.  
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_name - identifier stem
--   i_prefix - optional prefix of the name
--   i_suffix - optional sufix of the name
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    no known exceptions     
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-14  Lukasz Burdzanowski
--    Creation OCOMM-359
------------------------------------------------------------------------------------------------------------------------ 
  
end com_string_utils;