create or replace package body com_string_utils
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-14  Lukasz Burdzanowski
--    Added get_as_valid_oracle_identifier OCOMM-359
--  2015-10-02 Chris Roderick
--    Creation. OCOMM-343
------------------------------------------------------------------------------------------------------------------------
is
  function concatenate_varchars(
    i_varchar_1           in varchar2,
    i_varchar_2           in varchar2,
    i_max_resulting_size  in pls_integer  default com_constants.c_varchar2_max_size_plsql
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-02  Chris Roderick
--    Creation. OCOMM-343
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.is_true(length(i_varchar_1)+length(i_varchar_1)<=i_max_resulting_size, 
      'Concatenating varchars will exceed the maximum specified size ('||i_max_resulting_size||')!');
      
    return i_varchar_1||i_varchar_2;
  end concatenate_varchars;
  
  function get_as_valid_oracle_identifier (
    i_name    in varchar2, 
    i_prefix  in varchar2 default null, 
    i_suffix  in varchar2 default null
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-14  Lukasz Burdzanowski
--    Creation OCOMM-359
------------------------------------------------------------------------------------------------------------------------ 
  is 
    l_name varchar2(30) := substr(i_name, 0, 30);
  begin    
    if i_prefix is not null and length(l_name)+length(i_prefix) > 30 then 
      l_name := i_prefix||substr(l_name, 0, length(l_name)-length(i_prefix));
    elsif i_prefix is not null then
      l_name := i_prefix||l_name;    
    end if;
  
    if i_suffix is not null and length(l_name)+length(i_suffix) > 30 then 
      l_name := substr(l_name, 0, length(l_name)-length(i_suffix))||i_suffix;    
    elsif i_suffix is not null then
      l_name := l_name||i_suffix;    
    end if;  
    return l_name;
  end get_as_valid_oracle_identifier;   
  
end com_string_utils;