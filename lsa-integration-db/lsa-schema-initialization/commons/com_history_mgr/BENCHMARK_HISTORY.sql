create or replace
procedure benchmark_history(i_max_rows number default 10000) as 
  l_test_name varchar2(30);
  l_start     number;
  l_end     number;
  l_no_rows   pls_integer :=  1;
  sql_trigger varchar2(10000);

begin
/******************************************/
/* Test insert and update without history */
/******************************************/

--disable history on test_hist_functionality
  com_history_mgr.disable_history('test_hist_functionality');
  
  while l_no_rows <= i_max_rows loop 
  
--  empty the test table  
    execute immediate 'truncate table test_hist_functionality';
--  instrumentation
    dbms_output.put_line('Inserting '||l_no_rows||' records without history...');
    
    
    l_test_name := 'Insert no hist';
--  Establish the start time  
    l_start :=  dbms_utility.get_cpu_time;

--  Insert all records
    insert into test_hist_functionality (id_1, id_2, a_name, description)
    select rownum, rownum*2, 'name'||rownum, 'some desc for '||rownum 
    from dual connect by 1=1 and rownum <=l_no_rows;
  
--  Establish the end time  
    l_end :=  dbms_utility.get_cpu_time;
      
--  insert timings into statistics file  
    insert into benchmark_history_stats (time_of_operation, start_cpu_time, end_cpu_time ,table_name ,operation ,num_rows ,total_time_seconds)
    values (sysdate, l_start, l_end, 'TEST_HIST_FUNCTIONALITY', l_test_name, l_no_rows, (l_end-l_start)/100);

--  instrumentation
    dbms_output.put_line('Updating '||l_no_rows||' records without history...');
--  update all records
    l_test_name := 'Update no hist';
    l_start :=  dbms_utility.get_cpu_time;
    update test_hist_functionality set a_name = a_name||a_name;
    l_end :=  dbms_utility.get_cpu_time;
      
--  insert timings into statistics file  
    insert into benchmark_history_stats (time_of_operation, start_cpu_time, end_cpu_time ,table_name ,operation ,num_rows ,total_time_seconds)
    values (sysdate, l_start, l_end, 'TEST_HIST_FUNCTIONALITY', l_test_name, l_no_rows, (l_end-l_start)/100);

    l_no_rows := l_no_rows * 10;

  end loop;







/******************************************/
/* Test insert and update with history    */
/******************************************/

--  empty the test table  
  execute immediate 'truncate table test_hist_functionality';
  execute immediate 'truncate table test_hist_functionality_hist';

  
--  enable history on test_hist_functionality
  com_history_mgr.enable_history('TEST_HIST_FUNCTIONALITY', 'TEST_HIST_FUNC_HIST');

-- Ensure the trigger just inserts new values, and does not compare with prior records
  sql_trigger := 'create or replace trigger test_hist_functionality_whist
    after insert or update or delete on test_hist_functionality
    for each row
    begin 
      case 
        when inserting then 
        update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 and RETIRE_TIME_UTC is null order by create_time_utc desc) where rownum = 1); 
        insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''I'', com_history_mgr.get_dml_time());
    
      when updating then 
        update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 order by create_time_utc desc) where rownum = 1); 
        insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:new.ID_1, :new.ID_2, :new.A_NAME, :new.DESCRIPTION,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''U'', com_history_mgr.get_dml_time());
    
     else 
       update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 order by create_time_utc desc) where rownum = 1); 
       insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:old.ID_1, :old.ID_2, :old.A_NAME, :old.DESCRIPTION,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''D'', com_history_mgr.get_dml_time());
    
     end case;
     
    exception 
      when others then
        com_event_mgr.raise_event(''HIST_TRIGGER_ERROR'', ''Error in History Table Trigger'' , false, true);
    
    end test_hist_functionality_whist;';

  dbms_output.put_line('Modifying history trigger...');    
 
  execute immediate sql_trigger;


  l_no_rows := 1;
 
  while l_no_rows <= i_max_rows loop 
  
--  empty the test tables  
    execute immediate 'truncate table test_hist_functionality';
    execute immediate 'truncate table test_hist_functionality_hist';
         
    dbms_output.put_line('Inserting '||l_no_rows||' records with history but no cleaning...');
   
    l_test_name := 'Insert with Hist no clean';
--  Establish the start time      
    l_start :=  dbms_utility.get_cpu_time;

--  Insert some records
    insert into test_hist_functionality (id_1, id_2, a_name, description)
    select rownum, rownum*2, 'name'||rownum, 'some desc for '||rownum 
    from dual connect by 1=1 and rownum <=l_no_rows;
  
--  Establish the end time  
    l_end :=  dbms_utility.get_cpu_time;

--  insert timings into statistics file  
    insert into benchmark_history_stats (time_of_operation, start_cpu_time, end_cpu_time ,table_name ,operation ,num_rows ,total_time_seconds)
    values (sysdate, l_start, l_end, 'TEST_HIST_FUNCTIONALITY', l_test_name, l_no_rows, (l_end-l_start)/100);

--  instrumentation
    dbms_output.put_line('Updating '||l_no_rows||' records with history but no cleaning...');
--  update all records
    l_test_name := 'Update with Hist no clean';
    l_start :=  dbms_utility.get_cpu_time;
    update test_hist_functionality set a_name = a_name||a_name;
    l_end :=  dbms_utility.get_cpu_time;
      
--  insert timings into statistics file  
    insert into benchmark_history_stats (time_of_operation, start_cpu_time, end_cpu_time ,table_name ,operation ,num_rows ,total_time_seconds)
    values (sysdate, l_start, l_end, 'TEST_HIST_FUNCTIONALITY', l_test_name, l_no_rows, (l_end-l_start)/100);

    l_no_rows := l_no_rows * 10;

  end loop;
  
/**********************************************************/
/* Test update with history and cleaning ops and no-ops   */
/**********************************************************/

--  empty the test table  
  execute immediate 'truncate table test_hist_functionality';
  execute immediate 'truncate table test_hist_functionality_hist';

  
--  enable history on test_hist_functionality
  com_history_mgr.enable_history('TEST_HIST_FUNCTIONALITY', 'TEST_HIST_FUNC_HIST');

-- Write the trigger to compare old and new updated values 
  sql_trigger := '-- Now check the values being updated, inside the trigger body to ensure real changes
create or replace trigger test_hist_functionality_whist
after insert or update or delete on test_hist_functionality
for each row
begin 
  case 
    when inserting then 
      update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time()where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 and RETIRE_TIME_UTC is null order by create_time_utc desc) where rownum = 1); 
      insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''I'', com_history_mgr.get_dml_time());
    
    when updating then 
      if 
      (:old.ID_1 != :new.ID_1) or
      (:old.ID_1 is null and :new.ID_1 is not null) or
      (:old.ID_1 is not null and :new.ID_1 is null) or
      (:old.ID_2 != :new.ID_2) or
      (:old.ID_2 is null and :new.ID_2 is not null) or
      (:old.ID_2 is not null and :new.ID_2 is null) or
      (:old.A_NAME != :new.A_NAME) or
      (:old.A_NAME is null and :new.A_NAME is not null) or
      (:old.A_NAME is not null and :new.A_NAME is null) or
      (:old.DESCRIPTION != :new.DESCRIPTION) or
      (:old.DESCRIPTION is null and :new.DESCRIPTION is not null) or
      (:old.DESCRIPTION is not null and :new.DESCRIPTION is null) then

        update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 order by create_time_utc desc) where rownum = 1); 
        insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:new.ID_1, :new.ID_2, :new.A_NAME, :new.DESCRIPTION,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''U'', com_history_mgr.get_dml_time());

      end if;

  else 
    update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 order by create_time_utc desc) where rownum = 1); 
    insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:old.id_1, :old.id_2, :old.a_name, :old.description,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''D'', com_history_mgr.get_dml_time());
    
 end case;
 
exception 
  when others then
    com_event_mgr.raise_event(''HIST_TRIGGER_ERROR'', ''Error in History Table Trigger'' , false, true);

end test_hist_functionality_whist;';

  dbms_output.put_line('Modifying history trigger...');    
 
  execute immediate sql_trigger;


  l_no_rows := 1;
 
  while l_no_rows <= i_max_rows loop 
  
--  empty the test tables  
    execute immediate 'truncate table test_hist_functionality';
    execute immediate 'truncate table test_hist_functionality_hist';
            

--  Insert some records that will be updated
    insert into test_hist_functionality (id_1, id_2, a_name, description)
    select rownum, rownum*2, 'name'||rownum, 'some desc for '||rownum 
    from dual connect by 1=1 and rownum <=l_no_rows;
  
--  instrumentation
    dbms_output.put_line('Updating '||l_no_rows||' records with history and cleaning check (with ops)...');
--  update all records
    l_test_name := 'Update with hist + clean(ops)';
    l_start :=  dbms_utility.get_cpu_time;
    update test_hist_functionality set a_name = a_name||a_name;
    l_end :=  dbms_utility.get_cpu_time;
      
--  insert timings into statistics file  
    insert into benchmark_history_stats (time_of_operation, start_cpu_time, end_cpu_time ,table_name ,operation ,num_rows ,total_time_seconds)
    values (sysdate, l_start, l_end, 'TEST_HIST_FUNCTIONALITY', l_test_name, l_no_rows, (l_end-l_start)/100);

--  instrumentation
    dbms_output.put_line('Updating '||l_no_rows||' records with history and cleaning check (no-ops)...');
--  update all records
    l_test_name := 'Update with hist+clean(no-ops)';
    l_start :=  dbms_utility.get_cpu_time;
    update test_hist_functionality set a_name = a_name;
    l_end :=  dbms_utility.get_cpu_time;
      
--  insert timings into statistics file  
    insert into benchmark_history_stats (time_of_operation, start_cpu_time, end_cpu_time ,table_name ,operation ,num_rows ,total_time_seconds)
    values (sysdate, l_start, l_end, 'TEST_HIST_FUNCTIONALITY', l_test_name, l_no_rows, (l_end-l_start)/100);

    l_no_rows := l_no_rows * 10;

  end loop;  

/*******************************************************/
/* Test insert with history and prior delete cleaning  */
/*******************************************************/

--  empty the test table  
  execute immediate 'truncate table test_hist_functionality';
  execute immediate 'truncate table test_hist_functionality_hist';

  
--  enable history on test_hist_functionality
  com_history_mgr.enable_history('TEST_HIST_FUNCTIONALITY', 'TEST_HIST_FUNC_HIST');

--  Latest trigger code which check inserts do not insert the same values which were previously deleted.
--also checks for updates
  sql_trigger := 'create or replace trigger test_hist_functionality_whist
    after insert or update or delete on test_hist_functionality
    for each row
    declare 
      l_non_key_data varchar2(4000);
      l_op_type_1 test_hist_functionality_hist.op_type%type;
      l_op_type_2 test_hist_functionality_hist.op_type%type;
      l_rid_1 rowid;
      l_rid_2 rowid;
    begin 
      case 
        when inserting then 
    
    --  Example query to get prior values etc. However, it is probably best to:
    --    first just query the op types and rowids (which only requires a PK access)
    --    Check if the prior op type was a delete and if not, stop further checks and insert the new 
    --    history record, otherwise retrieve the prior non-PK values by rowid, and compare them with the 
    --    incoming ones - if they are not the same, continue as normal, otherwise
    --    do not insert the incoming values into the history
    --    delete the prior "delete" record from the history by rowid
    --    set the retire time of the prior record to be null, by rowid
    
        select max(distinct decode(rownum, 1, op_type)) op_type_1
        , max(distinct decode(rownum, 2, op_type)) op_type_2
        , max(distinct decode(rownum, 1, rid)) rid_1
        , max(distinct decode(rownum, 2, rid)) rid_2 
        into l_op_type_1, l_op_type_2, l_rid_1, l_rid_2
        from (
            select rowid rid, op_type, a_name||description hist_value from test_hist_functionality_hist where id_1 = 1 and id_2 = 2 order by create_time_utc desc 
          )
        where rownum <=2;
      
      
        if l_op_type_1 = ''D'' then 
       
          select a_name||description into l_non_key_data
          from test_hist_functionality_hist
          where rowid = l_rid_1;
           
          if l_non_key_data = :new.a_name||:new.description then 
      --    old and new records are the same so delete the delete, do not insert the new history record and set retire time of previous insert/update to null
            delete from test_hist_functionality_hist where rowid = l_rid_1;
            update test_hist_functionality_hist set retire_time_utc = null where rowid = l_rid_2;
                
           else
           
      --    old and new records are different so write the history
            update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 and RETIRE_TIME_UTC is null order by create_time_utc desc) where rownum = 1);
            insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''I'', com_history_mgr.get_dml_time());
           
          end if;
       
        else  
      --  no prior delete so write the history
          update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 and RETIRE_TIME_UTC is null order by create_time_utc desc) where rownum = 1); 
          insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''I'', com_history_mgr.get_dml_time());
      
        end if;
    
      when updating then 
        if 
        (:old.ID_1 != :new.ID_1) or
        (:old.ID_1 is null and :new.ID_1 is not null) or
        (:old.ID_1 is not null and :new.ID_1 is null) or
        (:old.ID_2 != :new.ID_2) or
        (:old.ID_2 is null and :new.ID_2 is not null) or
        (:old.ID_2 is not null and :new.ID_2 is null) or
        (:old.A_NAME != :new.A_NAME) or
        (:old.A_NAME is null and :new.A_NAME is not null) or
        (:old.A_NAME is not null and :new.A_NAME is null) or
        (:old.DESCRIPTION != :new.DESCRIPTION) or
        (:old.DESCRIPTION is null and :new.DESCRIPTION is not null) or
        (:old.DESCRIPTION is not null and :new.DESCRIPTION is null) then
    
          update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 order by create_time_utc desc) where rownum = 1); 
          insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:new.ID_1, :new.ID_2, :new.A_NAME, :new.DESCRIPTION,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''U'', com_history_mgr.get_dml_time());
    
        end if;
    
     else 
       update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where rowid = (select rid from (select rowid rid from test_hist_functionality_hist where ID_1=:old.ID_1 and ID_2=:old.ID_2 order by create_time_utc desc) where rownum = 1); 
       insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:old.ID_1, :old.ID_2, :old.A_NAME, :old.DESCRIPTION,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''D'', com_history_mgr.get_dml_time());
    
     end case;
     
    exception 
      when others then
        com_event_mgr.raise_event(''HIST_TRIGGER_ERROR'', ''Error in History Table Trigger'' , false, true);
    
    end test_hist_functionality_whist;';

  dbms_output.put_line('Modifying history trigger...');    
 
  execute immediate sql_trigger;
  
  l_no_rows := 1;
 
  while l_no_rows <= i_max_rows loop 
  
--  empty the test tables  
    execute immediate 'truncate table test_hist_functionality';
    execute immediate 'truncate table test_hist_functionality_hist';
         
    dbms_output.put_line('Inserting '||l_no_rows||' records with history and prior delete cleaning...');
   
--  Insert some records
    insert into test_hist_functionality (id_1, id_2, a_name, description)
    select rownum, rownum*2, 'name'||rownum, 'some desc for '||rownum 
    from dual connect by 1=1 and rownum <=l_no_rows;


DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => user, TABNAME => 'TEST_HIST_FUNCTIONALITY_HIST', ESTIMATE_PERCENT => null, METHOD_OPT => 'for all columns size 1', NO_INVALIDATE => false);

--taking too long for 100,000 records (more than 1 hour)
--TODO: look into statistics
   delete from test_hist_functionality;  

DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => user, TABNAME => 'TEST_HIST_FUNCTIONALITY_HIST', ESTIMATE_PERCENT => null, METHOD_OPT => 'for all columns size 1', NO_INVALIDATE => false);

--  instrumentation
    dbms_output.put_line('Inserting '||l_no_rows||' records with history and prior cleaning...');
--  update all records
    l_test_name := 'Insert with prior delete check';
    l_start :=  dbms_utility.get_cpu_time;
    
    insert into test_hist_functionality (id_1, id_2, a_name, description)
    select rownum, rownum*2, 'name'||rownum, 'some desc for '||rownum 
    from dual connect by 1=1 and rownum <=l_no_rows;
    
    l_end :=  dbms_utility.get_cpu_time;
      
--  insert timings into statistics file  
    insert into benchmark_history_stats (time_of_operation, start_cpu_time, end_cpu_time ,table_name ,operation ,num_rows ,total_time_seconds)
    values (sysdate, l_start, l_end, 'TEST_HIST_FUNCTIONALITY', l_test_name, l_no_rows, (l_end-l_start)/100);

    l_no_rows := l_no_rows * 10;

  end loop;

commit;

end benchmark_history;
