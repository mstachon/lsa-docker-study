-- Create a table to use to test history functionality
drop table test_hist_functionality;
create table test_hist_functionality (
  id_1        number,
  id_2        number,
  a_name      varchar2(30),
  description varchar2(100),
  constraint thistfunc_pk primary key (id_1, id_2)
);
-- Test the times taken to insert into the base table without history
truncate table test_hist_functionality;
declare
  l_test_name varchar2(30) :=  'Insert no Hist';
  l_start     number;
  l_no_rows   pls_integer :=  100000;
begin

--  Establish the start time  
  l_start :=  dbms_utility.get_cpu_time;

--  Insert some records
  insert into test_hist_functionality (id_1, id_2, a_name, description)
  select rownum, rownum*2, 'name'||rownum, 'some desc for '||rownum 
  from dual connect by 1=1 and rownum <=l_no_rows;

--  Log how long it took
  com_logger.log_entry('DEBUG', l_test_name||' '||l_no_rows||' in '||((dbms_utility.get_cpu_time-l_start)/100)||'s', true);
end;
/

--  Now Enable history on this table, first clearing the existing data
truncate table test_hist_functionality;
drop table test_hist_functionality_hist;
begin
  com_history_mgr.disable_history('TEST_HIST_FUNCTIONALITY');
  com_history_mgr.enable_history('TEST_HIST_FUNCTIONALITY', 'TEST_HIST_FUNC_HIST');
end;
/

select * from test_hist_functionality_hist left join com_app_sessions using (com_app_session_id);


select * from user_objects where status = 'INVALID';
begin compile_invalid_objects; end;
/
insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION, OP_TYPE, CREATE_TIME_UTC, COM_APP_SESSION_ID, MODULE, ACTION, CLIENT_INFO) 
select ID_1, ID_2, A_NAME, DESCRIPTION, 'I', sys_extract_utc(systimestamp), nullcom_app_session_mgr.get_app_session_id(), sys_context('userenv', 'module'), 'HIST_INIT', sys_context('userenv', 'client_info') 
from TEST_HIST_FUNCTIONALITY d where not exists (select null from TEST_HIST_FUNCTIONALITY_HIST where ID_1 = d.ID_1 and ID_2 = d.ID_2);

-- Ensure the trigger just inserts new values, and does not compare with prior records
create or replace trigger test_hist_functionality_whist
after insert or update or delete on test_hist_functionality
for each row
begin 
  case 
    when inserting then 
    update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) FROM TEST_HIST_FUNCTIONALITY_HIST where RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2) and RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2;
    insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'I', com_history_mgr.get_dml_time());

  when updating then 
    update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) from TEST_HIST_FUNCTIONALITY_HIST where ID_1=:old.ID_1 and ID_2=:old.ID_2)  and ID_1=:old.ID_1 and ID_2=:old.ID_2;
    insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:new.ID_1, :new.ID_2, :new.A_NAME, :new.DESCRIPTION,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'U', com_history_mgr.get_dml_time());

 else 
   update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) from TEST_HIST_FUNCTIONALITY_HIST where ID_1=:old.ID_1 and ID_2=:old.ID_2)  and ID_1=:old.ID_1 and ID_2=:old.ID_2;
   insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:old.ID_1, :old.ID_2, :old.A_NAME, :old.DESCRIPTION,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'D', com_history_mgr.get_dml_time());

 end case;
 
exception 
  when others then
    com_event_mgr.raise_event('HIST_TRIGGER_ERROR', 'Error in History Table Trigger' , false, true);

end test_hist_functionality_whist;
/

-- Now test the times taken to insert into the base table with history
declare
  l_test_name varchar2(30) :=  'Insert with Hist';
  l_start     number;
  l_no_rows   pls_integer :=  100000;
begin

--  Establish the start time  
  l_start :=  dbms_utility.get_cpu_time;

--  Insert some records
  insert into test_hist_functionality (id_1, id_2, a_name, description)
  select rownum, rownum*2, 'name'||rownum, 'some desc for '||rownum 
  from dual connect by 1=1 and rownum <=l_no_rows;

--  Log how long it took
  com_logger.log_entry('DEBUG', l_test_name||' '||l_no_rows||' in '||((dbms_utility.get_cpu_time-l_start)/100)||'s', true);
end;
/

--  Now compare the times to update the table without checking old and new values  (i.e. cleaning will be required later on)
--  Clean existing history
truncate table test_hist_functionality_hist;
--  Modify the trigger which writes history to ensure old and new values are not compared
create or replace trigger test_hist_functionality_whist
after insert or update or delete on test_hist_functionality
for each row
begin 
  case 
    when inserting then 
      update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) FROM TEST_HIST_FUNCTIONALITY_HIST where RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2) and RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2;
      insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'I', com_history_mgr.get_dml_time());
    
    when updating then 
      update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) from TEST_HIST_FUNCTIONALITY_HIST where ID_1=:old.ID_1 and ID_2=:old.ID_2)  and ID_1=:old.ID_1 and ID_2=:old.ID_2;
      insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:new.ID_1, :new.ID_2, :new.A_NAME, :new.DESCRIPTION,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'U', com_history_mgr.get_dml_time());

   else 
     update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) from TEST_HIST_FUNCTIONALITY_HIST where ID_1=:old.ID_1 and ID_2=:old.ID_2)  and ID_1=:old.ID_1 and ID_2=:old.ID_2;
     insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:old.ID_1, :old.ID_2, :old.A_NAME, :old.DESCRIPTION,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'D', com_history_mgr.get_dml_time());

 end case;
 
exception 
  when others then
    com_event_mgr.raise_event('HIST_TRIGGER_ERROR', 'Error in History Table Trigger' , false, true);

end test_hist_functionality_whist;
/
-- Run the test
declare
  l_test_name varchar2(30) :=  'Update no-filt no-op';
  l_start     number;
  l_no_rows   pls_integer;
begin

--  Establish the start time  
  l_start :=  dbms_utility.get_cpu_time;

--  Update some records
  update test_hist_functionality set a_name = a_name;
  
  l_no_rows :=  sql%rowcount;

--  Log how long it took
  com_logger.log_entry('DEBUG', l_test_name||' '||l_no_rows||' in '||((dbms_utility.get_cpu_time-l_start)/100)||'s', true);
  
  rollback;
end;
/

-- Re-Run the test making changes this time
declare
  l_test_name varchar2(30) :=  'Update no-filt op';
  l_start     number;
  l_no_rows   pls_integer;
begin

--  Establish the start time  
  l_start :=  dbms_utility.get_cpu_time;

--  Update some records
  update test_hist_functionality set a_name = a_name||a_name;
  
  l_no_rows :=  sql%rowcount;

--  Log how long it took
  com_logger.log_entry('DEBUG', l_test_name||' '||l_no_rows||' in '||((dbms_utility.get_cpu_time-l_start)/100)||'s', true);
  
  rollback;
end;
/

-- Now check the values being updated, inside the trigger body to ensure real changes
create or replace trigger test_hist_functionality_whist
after insert or update or delete on test_hist_functionality
for each row
begin 
  case 
    when inserting then 
      update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) FROM TEST_HIST_FUNCTIONALITY_HIST where RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2) and RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2;
      insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'I', com_history_mgr.get_dml_time());
    
    when updating then 
      if 
      (:old.ID_1 != :new.ID_1) or
      (:old.ID_1 is null and :new.ID_1 is not null) or
      (:old.ID_1 is not null and :new.ID_1 is null) or
      (:old.ID_2 != :new.ID_2) or
      (:old.ID_2 is null and :new.ID_2 is not null) or
      (:old.ID_2 is not null and :new.ID_2 is null) or
      (:old.A_NAME != :new.A_NAME) or
      (:old.A_NAME is null and :new.A_NAME is not null) or
      (:old.A_NAME is not null and :new.A_NAME is null) or
      (:old.DESCRIPTION != :new.DESCRIPTION) or
      (:old.DESCRIPTION is null and :new.DESCRIPTION is not null) or
      (:old.DESCRIPTION is not null and :new.DESCRIPTION is null) then

        update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) from TEST_HIST_FUNCTIONALITY_HIST where ID_1=:old.ID_1 and ID_2=:old.ID_2)  and ID_1=:old.ID_1 and ID_2=:old.ID_2;
        insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:new.ID_1, :new.ID_2, :new.A_NAME, :new.DESCRIPTION,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'U', com_history_mgr.get_dml_time());

      end if;

  else 
    update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) from TEST_HIST_FUNCTIONALITY_HIST where ID_1=:old.ID_1 and ID_2=:old.ID_2)  and ID_1=:old.ID_1 and ID_2=:old.ID_2;
    insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:old.id_1, :old.id_2, :old.a_name, :old.description,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'D', com_history_mgr.get_dml_time());
    
 end case;
 
exception 
  when others then
    com_event_mgr.raise_event('HIST_TRIGGER_ERROR', 'Error in History Table Trigger' , false, true);

end test_hist_functionality_whist;
/
-- Run the test
declare
  l_test_name varchar2(30) :=  'Update body-filt no-op';
  l_start     number;
  l_no_rows   pls_integer;
begin

--  Establish the start time  
  l_start :=  dbms_utility.get_cpu_time;

--  Update some records
  update test_hist_functionality set a_name = a_name;
  
  l_no_rows :=  sql%rowcount;

--  Log how long it took
  com_logger.log_entry('DEBUG', l_test_name||' '||l_no_rows||' in '||((dbms_utility.get_cpu_time-l_start)/100)||'s', true);
  
  rollback;
end;
/

-- Re-Run the test making changes this time
declare
  l_test_name varchar2(30) :=  'Update body-filt op';
  l_start     number;
  l_no_rows   pls_integer;
begin

--  Establish the start time  
  l_start :=  dbms_utility.get_cpu_time;

--  Update some records
  update test_hist_functionality set a_name = a_name||a_name;
  
  l_no_rows :=  sql%rowcount;

--  Log how long it took
  com_logger.log_entry('DEBUG', l_test_name||' '||l_no_rows||' in '||((dbms_utility.get_cpu_time-l_start)/100)||'s', true);
  
  rollback;
end;
/

-- Now check the values being updated, inside the trigger header to ensure real changes
create or replace trigger test_hist_functionality_whist
after update on test_hist_functionality
for each row
when  ((old.ID_1 != new.ID_1) or
 (old.ID_1 is null and new.ID_1 is not null) or
 (old.ID_1 is not null and new.ID_1 is null) or
 (old.ID_2 != new.ID_2) or
 (old.ID_2 is null and new.ID_2 is not null) or
 (old.ID_2 is not null and new.ID_2 is null) or
 (old.A_NAME != new.A_NAME) or
 (old.A_NAME is null and new.A_NAME is not null) or
 (old.A_NAME is not null and new.A_NAME is null) or
 (old.DESCRIPTION != new.DESCRIPTION) or
 (old.description is null and new.description is not null) or
 (old.description is not null and new.description is null))
begin 

  update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) from TEST_HIST_FUNCTIONALITY_HIST where ID_1=:old.ID_1 and ID_2=:old.ID_2)  and ID_1=:old.ID_1 and ID_2=:old.ID_2;
  insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'U', com_history_mgr.get_dml_time());
 
exception 
  when others then
    com_event_mgr.raise_event('HIST_TRIGGER_ERROR', 'Error in History Table Trigger' , false, true);

end test_hist_functionality_whist;
/
-- Run the test
declare
  l_test_name varchar2(30) :=  'Update head-filt no-op';
  l_start     number;
  l_no_rows   pls_integer;
begin

--  Establish the start time  
  l_start :=  dbms_utility.get_cpu_time;

--  Update some records
  update test_hist_functionality set a_name = a_name;
  
  l_no_rows :=  sql%rowcount;

--  Log how long it took
  com_logger.log_entry('DEBUG', l_test_name||' '||l_no_rows||' in '||((dbms_utility.get_cpu_time-l_start)/100)||'s', true);
  
  rollback;
end;
/

-- Re-Run the test making changes this time
declare
  l_test_name varchar2(30) :=  'Update head-filt op';
  l_start     number;
  l_no_rows   pls_integer;
begin

--  Establish the start time  
  l_start :=  dbms_utility.get_cpu_time;

--  Update some records
  update test_hist_functionality set a_name = a_name||a_name;
  
  l_no_rows :=  sql%rowcount;

--  Log how long it took
  com_logger.log_entry('DEBUG', l_test_name||' '||l_no_rows||' in '||((dbms_utility.get_cpu_time-l_start)/100)||'s', true);
  
  rollback;
end;
/ 
        
--  Latest trigger code which check inserts do not insert the same values which were previously deleted.
create or replace trigger test_hist_functionality_whist
after insert or update or delete on test_hist_functionality
for each row
declare 
  l_non_key_data varchar2(4000);
  l_op_type_1 test_hist_functionality_hist.op_type%type;
  l_op_type_2 test_hist_functionality_hist.op_type%type;
  l_rid_1 rowid;
  l_rid_2 rowid;

begin 
  case 
    when inserting then 

--  Example query to get prior values etc. However, it is probably best to:
--    first just query the op types and rowids (which only requires a PK access)
--    Check if the prior op type was a delete and if not, stop further checks and insert the new 
--    history record, otherwise retrieve the prior non-PK values by rowid, and compare them with the 
--    incoming ones - if they are not the same, continue as normal, otherwise
--    don't insert the incoming values into the history
--    delete the prior "delete" record from the history by rowid
--    set the retire time of the prior record to be null, by rowid

    select max(distinct decode(rownum, 1, op_type)) op_type_1
    , max(distinct decode(rownum, 2, op_type)) op_type_2
    , max(distinct decode(rownum, 1, rid)) rid_1
    , max(distinct decode(rownum, 2, rid)) rid_2 
    into l_op_type_1, l_op_type_2, l_rid_1, l_rid_2
    from (
        select rowid rid, op_type, a_name||description hist_value from test_hist_functionality_hist where id_1 = 1 and id_2 = 2 order by create_time_utc desc 
      )
    where rownum <=2;
  
  
    if l_op_type_1 = 'D' then 
   
      select a_name||description into l_non_key_data
      from test_hist_functionality_hist
      where rowid = l_rid_1;
       
      if l_non_key_data = :new.a_name||:new.description then 
  --    old and new records are the same so delete the delete, don't insert the new history record and set retire time of previous insert/update to null
        delete from test_hist_functionality_hist where rowid = l_rid_1;
        update test_hist_functionality_hist set retire_time_utc = null where rowid = l_rid_2;
            
       else
       
  --    old and new records are different so write the history
        update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) FROM TEST_HIST_FUNCTIONALITY_HIST where RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2) and RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2;
        insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'I', com_history_mgr.get_dml_time());
       
      end if;
   
    else  
  --  no prior delete so write the history
      update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) FROM TEST_HIST_FUNCTIONALITY_HIST where RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2) and RETIRE_TIME_UTC is null and ID_1=:new.ID_1 and ID_2=:new.ID_2;
      insert into test_hist_functionality_hist (id_1, id_2, a_name, description,  user_name, host_or_ip_address, op_type, create_time_utc) values (:new.id_1, :new.id_2, :new.a_name, :new.description,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'I', com_history_mgr.get_dml_time());
  
    end if;

  when updating then 
    if 
    (:old.ID_1 != :new.ID_1) or
    (:old.ID_1 is null and :new.ID_1 is not null) or
    (:old.ID_1 is not null and :new.ID_1 is null) or
    (:old.ID_2 != :new.ID_2) or
    (:old.ID_2 is null and :new.ID_2 is not null) or
    (:old.ID_2 is not null and :new.ID_2 is null) or
    (:old.A_NAME != :new.A_NAME) or
    (:old.A_NAME is null and :new.A_NAME is not null) or
    (:old.A_NAME is not null and :new.A_NAME is null) or
    (:old.DESCRIPTION != :new.DESCRIPTION) or
    (:old.DESCRIPTION is null and :new.DESCRIPTION is not null) or
    (:old.DESCRIPTION is not null and :new.DESCRIPTION is null) then

      update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) from TEST_HIST_FUNCTIONALITY_HIST where ID_1=:old.ID_1 and ID_2=:old.ID_2)  and ID_1=:old.ID_1 and ID_2=:old.ID_2;
      insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:new.ID_1, :new.ID_2, :new.A_NAME, :new.DESCRIPTION,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'U', com_history_mgr.get_dml_time());

    end if;

 else 
   update TEST_HIST_FUNCTIONALITY_HIST set RETIRE_TIME_UTC = com_history_mgr.get_dml_time() where CREATE_TIME_UTC = (select max(CREATE_TIME_UTC) from TEST_HIST_FUNCTIONALITY_HIST where ID_1=:old.ID_1 and ID_2=:old.ID_2)  and ID_1=:old.ID_1 and ID_2=:old.ID_2;
   insert into TEST_HIST_FUNCTIONALITY_HIST (ID_1, ID_2, A_NAME, DESCRIPTION,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:old.ID_1, :old.ID_2, :old.A_NAME, :old.DESCRIPTION,  nvl(nvl(sys_context('USERENV', 'CLIENT_IDENTIFIER'), sys_context('USERENV', 'OS_USER')), 'UNKNOWN USER'), nvl(sys_context('USERENV', 'HOST'), sys_context('USERENV', 'IP_ADDRESS')), 'D', com_history_mgr.get_dml_time());

 end case;
 
exception 
  when others then
    com_event_mgr.raise_event('HIST_TRIGGER_ERROR', 'Error in History Table Trigger' , false, true);

end test_hist_functionality_whist;
/


--  Functionality test
--delete record (4th row in history created)
delete from test_hist_functionality where id_1 = 1 and id_2 = 2;
 
select * from test_hist_functionality_hist where id_1 = 1 and id_2 = 2
order by create_time_utc;
 
--re-insert record
insert into test_hist_functionality values(1,         2,            'new_name',     'some desc for 1');
 
--�delete� record removed from history, previous records retire time set to null.
select * from test_hist_functionality_hist where id_1 = 1 and id_2 = 2
order by create_time_utc