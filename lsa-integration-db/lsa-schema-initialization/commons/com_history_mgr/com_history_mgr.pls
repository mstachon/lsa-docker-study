create or replace package com_history_mgr 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package provides the functionality for creating and managing the history of data in a database. 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-08-04 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-406
--  2015-02-13  Lukasz Burdzanowski
--    OCOMM-305 Added i_pre_populate_from_src_table flag to enable_history
--  2015-02-11  Lukasz Burdzanowski, Chris Roderick
--    OCOMM-303 Made i_hist_table_part_prefix optional for enable_history  
--  2014-04-28  Pascal Le Roux, Chris Roderick
--    Added replace_history_trigger and cleaned up some technical debt
--  2014-03-31  James Menzies and Chris Roderick
--    Added procedure log_dml_operation as per OCOMM-251
--  2011-03-15  Eve Fortescue-Beck, Chris Roderick
--    Renamed procedure set_operation_time to set_dml_time
--    Added function get_dml_time
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  procedure set_dml_time; 
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Stores the current time UTC in a Context variable. This is then used to timestamp history table entries.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INTERNAL_ERROR when the DML time cannot be set.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-15  Eve Fortescue-Beck, Chris Roderick
--    Renamed from set_operation_time to set_dml_time, and re-factored to use the commons.com_ctx_mgr
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_dml_time
  return timestamp;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the timestamp associated with a DML Time which is currently set in the Commons context 
--    within the session. Returns null if no value is currently set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):   
--  2011-03-15  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure enable_history (
    i_table_name              in    com_history_tables.table_name%type,
    i_hist_table_part_prefix  in    com_time_range_part_tables.part_prefix%type default null,
    i_ignore_cols             in    com_history_tables.ignored_columns%type default null,
    i_no_parts_advance        in    com_time_range_part_tables.no_parts_advance%type default 3,    
    i_no_parts_keep           in    com_time_range_part_tables.no_parts_keep%type default null,      
    i_part_range_int_cnt      in    com_time_range_part_tables.part_range_int_cnt%type default 1,  
    i_part_range_int_type     in    com_time_range_part_tables.part_range_int_type%type default 'MONTH',
    i_re_raise_exceptions     in    com_history_tables.re_raise_exceptions%type default com_constants.c_false,
    i_pre_populate_from_src_table in boolean default true
  );    
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Creates a table and the triggers necessary to store a history of changes to the data in the table with the 
--    given name.   
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name - Table on which to enable history
--    i_hist_table_part_prefix - Partition Prefix name; if omitted an auto-generated prefix will be used;
--                               custom prefix has to match size of COM_TIME_RANGE_PART_TABLES.PART_PREFIX column;   
--    i_ignore_cols            -  A list of columns in the base table that should not be included in the history 
--                                table. Use com_hist_ignored_cols('COL1', 'COL2') to construct.
--    i_no_parts_advance    - The number of partitions to create in advance
--    i_no_parts_keep       - The number of partitions to keep
--    i_part_range_int_cnt  - The Interval count - combined with type to specify size of partition i.e. 7 days, 3 months
--    i_part_range_int_type - The interval type i.e. minute, month, hour, day, etc.
--    i_re_raise_exceptions - Flag indicating whether or not the generated history trigger code should re-raise any 
--                            exception encountered during history capture. The default is false to avoid problems with
--                            history capture potentially blocking transactions against the corresponding base table.
--    i_pre_populate_from_src_table - Specifies if a history table is pre-populated with data from the source table;
--                                    true by default; 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ENABLE_HIST_FAILED - Enable History failed for i_table_name    
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-02-13  Lukasz Burdzanowski
--    OCOMM-305 Added i_pre_populate_from_src_table
--  2015-02-11  Lukasz Burdzanowski, Chris Roderick
--    OCOMM-303 Made i_hist_table_part_prefix optional 
--  2014-08-07 Chris Roderick
--    Added new parameter i_re_raise_exceptions as per OCOMM-296
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------  
  
  procedure disable_history (
    i_table_name  in  com_history_tables.table_name%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Removes the triggers used to store a history of changes to the data from the table with the given name.	
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name - Table on which to disable the history
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--   HISTORY_NOT_ENABLED - History was not enabled for table i_table_name
--   DISABLE_HIST_FAILED  - Disabling of History for table i_table_name failed  
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure log_dml_operation (
      i_table_name  in com_dml_logs.table_name%type,
      i_op_type     in com_dml_logs.op_type%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Logs the DML operations performed against a table.  Additional information about who made the modifications
--    and when they were made is retrieved from application context values
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name  - the table name against which the DML operation was performed
--    i_op_type     - the type of operation performed (I (insert), U (update), D (delete))
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Not known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-03-31  James Menzies and Chris Roderick
--    Creation as per OCOMM-251
------------------------------------------------------------------------------------------------------------------------

  procedure replace_history_trigger (	
      i_table_name		  in 	com_history_tables.table_name%type
  );
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Procedure which replaces history triggers (used in case of DDL or internal implementation changes)
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name - Name of the table for which to replace the history trigger
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--   PROCESSING_ERROR - History was not enabled for table i_table_name
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-04-28  Pascal Le Roux, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------

end com_history_mgr;