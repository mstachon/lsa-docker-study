create or replace package body com_history_mgr 
-----------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-10-28  Jose Rolland
--    Modified code in order to skip virtual columns when generating the history table and trigger OCOMM-380
--  2016-08-04 Jose Rolland
--    Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-406
--  2016-07-13 Lukasz Burdzanowski
--    Added trigger name and stack to "afer row" exception template. OCOMM-383
--  2015-10-02 Chris Roderick
--    Updated function should_re_raise_exception. OCOMM-347
--  2015-03-10  Lukasz Burdzanowski
--    OCOMM-304 Modified generation of constraints names to prevent colisions
--  2015-02-13  Lukasz Burdzanowski
--    OCOMM-305 Added i_pre_populate_from_src_table flag to enable_history 
--  2015-02-11  Lukasz Burdzanowski, Chris Roderick
--    OCOMM-303 Made i_hist_table_part_prefix optional for enable_history and added check for max length of user
--    provided prefix (COM_TIME_RANGE_PART_TABLES.PART_PREFIX column);   
--  2014-12-18
--    Chris Roderick - updated create_history_tables as per OCOMM-300
--  2014-08-07 Chris Roderick
--    Modified create_history_triggers and enable_history procedures
--  2014-07-19 Chris Roderick
--    Changed c_ddl_get_cinfo used in generated triggers to Add default client info to history data when none - OCOMM-292
--  2014-06-30 Chris Roderick
--    Added bug fix OCOMM-274 in create_history_triggers
--  2014-06-27  Pascal Le Roux
--    Modified  create_history_triggers procedure to generate history triggers which comply 
--    with coding conventions (lower case, no decode)
--  2014-04-28  Pascal Le Roux, Chris Roderick
--    Added replace_history_trigger and cleaned up some technical debt, started storing ignored_cols for history tables
--    2014-04-01  James Menzies
--    Added procedure create_hist_tab_indexes which in turn calls procedures to add indexes to the hist table
--  2014-04-01  James Menzies and Chris Roderick
--    Modified code which generates tables and trigger to the use the transaction_id instead of com_app_session_id
--    as per OCOMM-251
--  2014-03-31  James Menzies and Chris Roderick
--    Added procedure log_dml_operation as per OCOMM-251
--  2013-01-16  Pascal Le Roux
--    Rewrite the create_history_triggers procedure so as to generate one compound trigger 
--    instead of the two separate _WHIST and _HISTSTMP triggers.
--  2013-01-16 Pascal Le Roux
--    Reintroduce the local field l_part_prefix in create_history_triggers
--  2012-11-01 Chris Roderick
--    Bug fix in create_history_triggers
--  2011-11-11 Pascal
--    Modification in create_history_triggers
--  2011-11-08 Pascal
--    Modification in create_history_triggers
--  2011-08-08 Chris Roderick
--    Updates to create_history_triggers and init_history_table procedures
--  2011-08-05 Chris Roderick
--    Updates to create_history_table procedure
--  2011-03-15  Eve Fortescue-Beck, Chris Roderick
--    Renamed procedure set_operation_time to set_dml_time
--    Added function get_dml_time
--    Re-factored to use the commons.com_ctx_mgr
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
is
  type t_varchar_list is table of varchar2(61);
  c_hist_tab_sfx          constant varchar2(5)    :=  '_HIST';
  c_hist_trg_sfx          constant varchar2(6)    :=  '_HIST';
  c_hist_tab_ctime_col    constant varchar2(30)   :=  'CREATE_TIME_UTC';
  c_hist_tab_trans_id_col constant varchar2(30)   :=  'TRANSACTION_ID';
  c_hist_tab_optype_col   constant varchar2(30)   :=  'OP_TYPE';
  c_hist_tab_module_col   constant varchar2(30)   :=  'MODULE';
  c_hist_tab_action_col   constant varchar2(30)   :=  'ACTION';
  c_hist_tab_cinfo_col    constant varchar2(30)   :=  'CLIENT_INFO';
  c_ddl_get_dml_time      constant varchar2(110)  :=  'com_history_mgr.get_dml_time';
  c_ddl_get_trans_id      constant varchar2(110)  :=  'dbms_transaction.local_transaction_id';
  c_ddl_get_cinfo         constant varchar2(192)  :=  'coalesce(sys_context(''userenv'', ''client_info''), sys_context(''USERENV'', ''OS_USER'')||''@''||sys_context(''USERENV'', ''HOST'')||'' via ''||sys_context(''USERENV'', ''SESSION_USER''))';
  c_ddl_get_module        constant varchar2(110)  :=  'sys_context(''userenv'', ''module'')';
  c_ddl_get_action        constant varchar2(110)  :=  'sys_context(''userenv'', ''action'')';
  c_no_of_months_in_advance constant  number    :=   3;
  
  procedure set_dml_time 
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-15  Eve Fortescue-Beck, Chris Roderick
--    Renamed from set_operation_time to set_dml_time, and re-factored to use the commons.com_ctx_mgr
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin   
    commons.com_ctx_mgr.set_dml_time(sys_extract_utc(systimestamp));           
  exception
    when others then 
      com_event_mgr.raise_event('INTERNAL_ERROR','There was an error when trying to set DML time' , true, true);
  end set_dml_time;
  
  function get_dml_time
  return timestamp
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-03-15  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    return commons.com_ctx_mgr.get_dml_time;
  end get_dml_time;
  
  function get_unique_hist_tab_index_name(
    i_hist_table_name in user_indexes.index_name%type, 
    i_idx_suffix in varchar2)   
  return user_indexes.index_name%type
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets unique name of the index based on passed suffix. The name will be auto-generated using sequnce number.
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-03-23 Lukasz Burdzanowski
--    Creation OCOMM-304 
-----------------------------------------------------------------------------------------------------------------------
  is
  l_constraints_count number;   
  l_name_seq_index number := 1;    
  l_idx_name user_indexes.index_name%type := substr(i_hist_table_name, 1, 22)||i_idx_suffix;  
  begin
    loop
      select 
        count(index_name), 
        substr(upper(i_hist_table_name), 1, 22)||case when count(index_name) > 0 then count(index_name) end||upper(i_idx_suffix) 
      into 
        l_constraints_count, 
        l_idx_name 
      from user_indexes 
      where substr(index_name, 1, 22) = substr(upper(i_hist_table_name), 1, 22) and upper(substr(index_name, -6)) = upper(i_idx_suffix);
      
      l_name_seq_index := l_name_seq_index + 1;          
      exit when l_constraints_count = 0  or l_name_seq_index = 10; 
    end loop;       
    return l_idx_name;
  end get_unique_hist_tab_index_name;  

  procedure add_hist_tab_cti_index(
    i_hist_table_name	in 	com_history_tables.history_table_name%type
  )
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Adds create_time_utc index to hist table
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_hist_table_name - The name of the history table
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    DDL_FAILED - The DDL statement could not be executed.  
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-03-10  Lukasz Burdzanowski
--    OCOMM-304 Modified generation of constraints names to prevent colisions
--  2014-07-19 Chris Roderick
--    Changed input parameter type to be bound to table column type
--  2014-04-01  James Menzies
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_idx_sfx	varchar2(6) :=	'_cti_i';
	  l_ddl		  varchar2(32767);
    l_idx_name user_indexes.index_name%type;        
  begin
    l_idx_name := get_unique_hist_tab_index_name(i_hist_table_name, l_idx_sfx);       
    l_ddl	:=	'create index '||l_idx_name||' on '||i_hist_table_name||' ('||c_hist_tab_ctime_col||')';
    execute immediate l_ddl; 
  
  exception
    when others then
      com_event_mgr.raise_event('DDL_FAILED','Problem trying: '||(chr(10))||l_ddl, false, true );	
	end add_hist_tab_cti_index;

  procedure add_hist_tab_trans_id_index(
    i_hist_table_name	in  com_history_tables.history_table_name%type
  )
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Adds transaction_id index to hist table
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_hist_table_name - The name of the history table
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    DDL_FAILED - The DDL statement could not be executed.  
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-03-10  Lukasz Burdzanowski
--    OCOMM-304 Modified generation of constraints names to prevent colisions
--  2014-07-19 Chris Roderick
--    Changed input parameter type to be bound to table column type
--  2014-04-01  James Menzies
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_idx_sfx	varchar2(6) :=	'_trn_i';
	  l_ddl		  varchar2(32767); 	
    l_idx_name user_indexes.index_name%type;
  begin
    l_idx_name := get_unique_hist_tab_index_name(i_hist_table_name, l_idx_sfx);
    l_ddl	:=	'create index '||l_idx_name||' on '||i_hist_table_name||' ('||c_hist_tab_trans_id_col||')';
    execute immediate l_ddl;   
  exception
    when others then
      com_event_mgr.raise_event('DDL_FAILED','Problem trying: '||(chr(10))||l_ddl, false, true );	
	end add_hist_tab_trans_id_index;

  procedure create_hist_tab_indexes(	
    i_hist_table_name	in 	com_history_tables.history_table_name%type
  )
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Adds indexes to the history table (currently only for create_time_utc and transaction_id)
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_hist_table_name - The name of the history table
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-04-01  James Menzies
--    Modified, moved code to create indexes to seperate procedures.
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    add_hist_tab_cti_index(i_hist_table_name);
    add_hist_tab_trans_id_index(i_hist_table_name);
    	
	end create_hist_tab_indexes;
	
  procedure create_history_table(
      i_table_name		          in		        com_history_tables.table_name%type
    , io_hist_table_name	      in out nocopy com_history_tables.history_table_name%type
    , i_hist_table_part_prefix  in            com_time_range_part_tables.part_prefix%type
    , i_ignore_cols             in            com_history_tables.ignored_columns%type
    , i_no_parts_advance        in            com_time_range_part_tables.no_parts_advance%type
    , i_no_parts_keep           in            com_time_range_part_tables.no_parts_keep%type    
    , i_part_range_int_cnt      in            com_time_range_part_tables.part_range_int_cnt%type
    , i_part_range_int_type     in            com_time_range_part_tables.part_range_int_type%type  
   )
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Local procedure which creates a history table corresponding to the given source table  
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name - The name of the table
--    io_hist_table_name	- The name of the History Table       
--    i_hist_table_part_prefix	- The Prefix of the Partition
--    i_ignore_cols  - A list of columns in the base table  which should not be included in the history table
--    i_no_parts_advance    - The number of partitions to create in advance
--    i_no_parts_keep       - The number of partitions to keep
--    i_part_range_int_cnt  - The Interval count - combined with type to specify size of partition i.e. 7 days, 3 months
--    i_part_range_int_type - Ther interval type i.e. minute, month, hour, day etc
-----------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--    io_hist_table_name - The name of the History Table
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    TABLE_NOT_FOUND - The Table referenced by i_table_name was not found
--    NO_PK - The Table i_table_name does not have a primary key defined
--    HISTORY_TABLE_EXISTS - The History Table already exists
--    DDL_FAILED - The DDL statement could not be executed. 
--    HIST_TABLE_STRUCT_CHANGED - history could not be re-enabled becasue the history table exists with a different table structure  
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-10-28  Jose Rolland
--    Modified code in order to skip virtual columns when generating the history table DDL OCOMM-380
--  2015-03-10  Lukasz Burdzanowski
--    OCOMM-304 Modified generation of constraints names to prevent colisions
--  2014-12-18
--    Chris Roderick - replaced data_length with coalesce(char_col_decl_length, data_length) for columns of type like 
--    %CHAR% as per OCOMM-300 
--  2013-01-16 Pascal Le Roux
--    Reintroduce (had disappeared at the lst released) the local field l_part_prefix, and assign 
--    it based on the given value of i_hist_table_part_prefix
--    If no value was provided, assign based on a substr of i_table_name
--  2011-08-05 Chris Roderick
--    in DDL related to partitions, replaced l_part_high_value with 
--    to_char(l_part_high_value, 'YYYY-MM-DD HH24:MI:SS')
--    to ensure that the generated code will work on any database regardless of system or user NLS
--    settings.
--  2011-08-05 Chris Roderick
--    Changed the generated op_type column from varchar2(1) to char(1)
--    Added a number column (com_app_session_id) to reference captured application session 
--    information.
--    Added module, action, client_info columns.
--    Removed the retire_time_utc, user_name, and host_or_ip from the generated columns, since these
--    should be available via the referenced application session.
--  2011-05-31 Eve Fortescue-Beck
--    Change: Code added to verify that when re-enabling an old history table, the new structure is the same as the old structure. If not
--    an exception is raised. 
--  2011-04-13 Eve Fortescue-Beck, Pascal Le Roux
--    Change: if the PK constraint name of the table is long, then the l_ddl_pk_name variable will not be set to the correct constraint name,
--    This means that when the list base table of PK coloumns is generated, nothing will be returned?
--    Therefore code changed to reuse directly the constraint name from the base table.
--    Also Order columns by column id, so that the order of the columns in the history table matches that of the base table. 
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------	
  is
    l_ddl_optype_cons_sfx	varchar(11)	                :=	'_optype_chk';
    l_hist_table_name		  varchar2(30)	              :=	substr(upper(i_table_name), 1, (30-length(c_hist_tab_sfx)))||c_hist_tab_sfx;
    l_ddl_header			    varchar2(200)	              :=	'create table '||l_hist_table_name||' ('||(chr(10));
    l_ddl_history_cols		varchar2(32767)             := 	c_hist_tab_optype_col||' char(1),'||(chr(10))||c_hist_tab_ctime_col||' timestamp,'||(chr(10))||c_hist_tab_trans_id_col||' varchar2(48),'||(chr(10))||c_hist_tab_module_col||' varchar2(48),'||(chr(10))||c_hist_tab_action_col||' varchar2(32),'||(chr(10))||c_hist_tab_cinfo_col||' varchar2(64),'||(chr(10));
    l_ddl_optype_cons		  varchar2(4000);
    l_ddl_optype_cons_name  user_constraints.constraint_name%type;  
    l_ddl_pk_name_pfx		  varchar2(5)		              :=	'HIST_';
    l_ddl_pk_name			    varchar2(30);
    l_ddl_pk_cons			    varchar2(4000);
    l_ddl					        varchar2(32767)	            :=	l_ddl_header;
    l_check					      char(1);
    l_part_suffix         varchar2(11);
    l_part_prefix         com_time_range_part_tables.part_prefix%type;
    l_part_low_value      timestamp;
    l_part_high_value     timestamp;    
    l_hist_cols				    dbms_utility.lname_array;
    l_base_cols				    dbms_utility.lname_array;   
    l_hist_cols_list      varchar2(4000);
    l_base_cols_list      varchar2(4000);
    l_col_cnt             binary_integer;
    l_part_prefix_length  number;
    l_name_seq_index      number                      := 1;    
    l_constraints_count number;     
  begin
    io_hist_table_name	:=	l_hist_table_name;
    begin
--    check the source table exists
      select null 
      into l_check 
      from user_tables 
      where table_name = upper(i_table_name);
			
    exception
      when no_data_found then
        com_event_mgr.raise_error_event('TABLE_NOT_FOUND','The table: '||i_table_name||' was not found'); 			
    end;
    
    begin
--    check the history table does not already exist
	  select null into l_check 
    from dual 
    where not exists (
      select null 
      from user_tables 
      where table_name = upper(l_hist_table_name)
    );
    exception
	  when no_data_found then
        com_event_mgr.raise_error_event('HISTORY_TABLE_EXISTS', 'The History Table: '||upper(l_hist_table_name)||' already exists');		
    end;
			
--  Get all of the columns in the original table and add them to the DDL
    for x in (
      select column_name||' '||data_type|| 
      (
        case 
          when data_type like '%CHAR%' then
		    '('||coalesce(char_col_decl_length, data_length)||')'
		  when data_type = 'NUMBER' 
      and (data_precision is not null and data_scale is not null) then
		    '('||data_precision||','||data_scale||')'							
		  else
		    null
		  end
       )||',' col
      from user_tab_cols 
      where hidden_column = 'NO' 
      and virtual_column = 'NO'
      and table_name = upper(i_table_name)
      and column_name not in (
        select column_value 
        from table(i_ignore_cols)
      )
      order by column_id 
    ) 
    loop
      l_ddl	:=	l_ddl || x.col||(chr(10));    
    end loop;
	
--  Add the history columns to the DDL
    l_ddl	:=	l_ddl ||l_ddl_history_cols;
	
--  Create the primary key DDL
	begin
--    Check For existing PK
      select constraint_name into l_ddl_pk_name
      from user_constraints
      where table_name = upper(i_table_name) and constraint_type = 'P';
               
      l_ddl_pk_cons  := ' constraint '||substr(upper(io_hist_table_name), 1, (30-LENGTH('_pk')))||'_pk primary key (';
               
--    Get the existing PK columns and add the c_hist_tab_ctime_col column
      for x in (
        select column_name 
        from user_cons_columns 
        where constraint_name = l_ddl_pk_name 
        order by position
      ) 
      loop
        l_ddl_pk_cons  := l_ddl_pk_cons||x.column_name||',';            
      end loop;
	
--    Add the c_hist_tab_ctime_col column
      l_ddl_pk_cons	:=	l_ddl_pk_cons||c_hist_tab_ctime_col||')';
	
    exception
      when no_data_found then
        com_event_mgr.raise_error_event('NO_PK', 'The table: '||i_table_name||' does not have a primary key, which is required for history tracking'); 	
    end;
    
-- Generate constraints name  
     l_ddl_optype_cons_name := substr(upper(i_table_name), 1, (30-length(l_ddl_optype_cons_sfx)))||l_ddl_optype_cons_sfx;
     loop
        select count(*) into l_constraints_count from user_constraints where constraint_name = upper(l_ddl_optype_cons_name);
        if  l_constraints_count = 0 then
          exit;
        else                            
          l_ddl_optype_cons_name := substr( l_ddl_optype_cons_name, 0, length(l_ddl_optype_cons_name)-length(l_ddl_optype_cons_sfx)-1 )||l_name_seq_index||l_ddl_optype_cons_sfx;          
          l_name_seq_index := l_name_seq_index + 1;          
        end if;
        exit when l_name_seq_index = 10;
    end loop;    
    l_ddl_optype_cons		:= 	' CONSTRAINT '||l_ddl_optype_cons_name||' CHECK ('||c_hist_tab_optype_col||' IN (''D'', ''I'', ''U''))';
	
--  Add the necessary constraints
    l_ddl	:=	l_ddl ||l_ddl_optype_cons||','||(chr(10))||l_ddl_pk_cons||')';
--  Add the code to range partition the table by month, initially creating 3 partitions
    l_ddl	:=	l_ddl ||(chr(10))||' partition by range ('||c_hist_tab_ctime_col||') ('||chr(10);
--  call partition manager to set up initial partition   
    com_partition_mgr.establish_part_attributes(l_part_suffix, l_part_high_value,  i_part_range_int_cnt ,  i_part_range_int_type);

---  If the given partition prefix was null, set it to be a substr of the table name, with _H to indicate history
    if i_hist_table_part_prefix is null then
      l_part_prefix :=  substr(upper(i_table_name), 1, (30-(length(l_part_suffix)+3)))||'_H';      
    else
      select char_length into l_part_prefix_length from user_tab_columns 
        where table_name = 'COM_TIME_RANGE_PART_TABLES' and column_name = 'PART_PREFIX';            
      com_assert.arg_length_less_equal(i_hist_table_part_prefix, l_part_prefix_length, 
        'Provided partition prefix is longer than allowed limit of '||l_part_prefix_length);
      l_part_prefix := i_hist_table_part_prefix;
    end if;
   
    l_ddl	:=	l_ddl ||'partition '||l_part_prefix||'_'||l_part_suffix||' values less than (timestamp '''||to_char(l_part_high_value, 'YYYY-MM-DD HH24:MI:SS')||'''))';	 

--  Execute the DDL to create the table
    execute immediate l_ddl;
		
--  Create the necessary indexes on the history table
    create_hist_tab_indexes(l_hist_table_name);

--  Register this table for partition management    
    com_partition_mgr.reg_time_range_part_table (
      io_hist_table_name,         
      l_part_prefix,       
      i_no_parts_advance,    
      i_no_parts_keep,       
      i_part_range_int_cnt,  
      i_part_range_int_type 
    );

--  invoke the immediate partition management for this table - requires extension to com_partition_manager
    com_partition_mgr.manage_partitions(io_hist_table_name);

  exception    
    when others then
      if not com_event_mgr.event_equals('HISTORY_TABLE_EXISTS', sqlcode) then    
        com_event_mgr.raise_event('DDL_FAILED', 'Problem trying: '||(chr(10))||l_ddl, true, true);
    
  --  if history_table already exists, compare existing history table columns with those that would normally be created, to ensure compatibility 
      else 
  --    Get the columns names of the history table - but not including the ones added specifically for the history!
      select column_name||data_type||'('||coalesce(char_col_decl_length, data_length)||')' 
        bulk collect into l_hist_cols 
      from user_tab_columns 
      where table_name = upper(io_hist_table_name) 
          and column_name not in (c_hist_tab_optype_col, c_hist_tab_ctime_col, c_hist_tab_trans_id_col, c_hist_tab_module_col, c_hist_tab_action_col, c_hist_tab_cinfo_col) 
        order by column_id;
      
        dbms_utility.table_to_comma(l_hist_cols, l_col_cnt, l_hist_cols_list);
         
  --    Get the columns names of the base table
      select column_name||data_type||'('||coalesce(char_col_decl_length, data_length)||')' 
      bulk collect into l_base_cols 
      from user_tab_cols 
      where hidden_column = 'NO' 
      and virtual_column = 'NO' 
      and table_name = upper(i_table_name) 
      and column_name not in (
        select column_value 
        from table(i_ignore_cols)
      ) 
      order by column_id;
       
      dbms_utility.table_to_comma(l_base_cols, l_col_cnt, l_base_cols_list);
        
      if l_base_cols_list != l_hist_cols_list then   
        com_event_mgr.raise_event('HIST_TABLE_STRUCT_CHANGED', 'History Table already exists but with a different structure. Intervention required before for History can be re-enabled', true, true);
      else
  --    if the columns are equivalent then do nothing except log that the history already exists, but don't raise an error
        null;
      end if;
    end if;
	
  end create_history_table;
  
  function should_re_raise_exception(
    i_table_name		  in 	com_history_tables.table_name%type
  )
  return boolean
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-02 Chris Roderick
--    Updated to use com_types.t_boolean_as_char. OCOMM-347
-----------------------------------------------------------------------------------------------------------------------  
  is
    l_re_raise_exceptions com_types.t_boolean_as_char;
  begin
    select re_raise_exceptions
    into l_re_raise_exceptions
    from com_history_tables
    where table_name = i_table_name;
  
    return com_util.char_to_boolean(l_re_raise_exceptions);
  
  end should_re_raise_exception;
  
  function get_before_stmt_exception_code (	
    i_table_name		  in 	com_history_tables.table_name%type
  )
  return varchar2
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-08-07 Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    c_no_re_raise       constant  varchar2(32767)  :=  'com_event_mgr.raise_event(''HIST_TRIGGER_ERROR'', ''Error in History Trigger before statement on table '||i_table_name||''' , false, true);';
    c_re_raise          constant  varchar2(32767)  :=  'raise;';
  begin
    return case when should_re_raise_exception(i_table_name) then c_re_raise else c_no_re_raise end;
  end get_before_stmt_exception_code;
  
  function get_before_stmt_code(	
    i_table_name		  in 	com_history_tables.table_name%type
  )
  return varchar2
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-08-07 Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    return  (chr(10))||'before statement is '
            ||(chr(10))||'begin'
            ||(chr(10))||(chr(9))||'begin '
            ||(chr(10))||(chr(9))||' com_history_mgr.set_dml_time; '
            ||(chr(10))||(chr(9))||'exception'
            ||(chr(10))||(chr(9))||'when others then'
            ||(chr(10))||(chr(9))||(chr(9))||get_before_stmt_exception_code(i_table_name)
            ||(chr(10))||(chr(9))||'end;'
            ||(chr(10))||(chr(9))||'begin '
            ||(chr(10))||(chr(9))||' case' 
            ||(chr(10))||(chr(9))||(chr(9))||'when inserting  then l_stmt_op_type :=  ''I'';'
            ||(chr(10))||(chr(9))||(chr(9))||'when updating   then l_stmt_op_type :=  ''U'';'
            ||(chr(10))||(chr(9))||(chr(9))||'when deleting   then l_stmt_op_type :=  ''D'';'
            ||(chr(10))||(chr(9))||(chr(9))||'else com_event_mgr.raise_error_event(''EXCEPTION_DUE_TO_MERGE_ERROR'', ''A merge error provoked this trigger exception ex "ORA-30926: unable to get a stable set of rows in the source tables", disable history trigger and repeat DML to verify. Fix the root cause'');'
            ||(chr(10))||(chr(9))||'end case;'
            ||(chr(10))||(chr(9))||'com_history_mgr.log_dml_operation('''||upper(i_table_name)||''', l_stmt_op_type);'
            ||(chr(10))||(chr(9))||'end;'
            ||(chr(10))||'end before statement;';
            
  end get_before_stmt_code;
  
  procedure establish_all_hist_tab_cols (
    o_cols					  out t_varchar_list,
    i_table_name		  in 	com_history_tables.table_name%type,
    i_ignore_cols     in  com_history_tables.ignored_columns%type default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-10-28  Jose Rolland
--    Modified code in order to skip virtual columns OCOMM-380
--  2014-08-07 Chris Roderick
--    Creation - to support OCOMM-296
------------------------------------------------------------------------------------------------------------------------
  is
  begin
	  select lower(column_name) 
	  bulk collect into o_cols 
	  from user_tab_cols 
    where hidden_column = 'NO' and virtual_column = 'NO'
	  and table_name = upper(i_table_name) 
    and column_name not in (
      select column_value 
      from table(i_ignore_cols)
    ) 
    order by column_id;
    
  end establish_all_hist_tab_cols;
  
  procedure establish_pk_hist_tab_cols (
    o_pk_cols					out t_varchar_list,
    i_table_name		  in 	com_history_tables.table_name%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-08-07 Chris Roderick
--    Creation - to support OCOMM-296
------------------------------------------------------------------------------------------------------------------------
  is
  begin  
    select lower(ucc.column_name)
    bulk collect into o_pk_cols 
    from user_cons_columns ucc 
    join user_constraints uc on (ucc.constraint_name = uc.constraint_name) 
    where ucc.table_name = upper(i_table_name) 
    and uc.constraint_type = 'P' 
    order by ucc.position;
    
  end establish_pk_hist_tab_cols;
  
  procedure establish_non_pk_hist_tab_cols (
    o_non_pk_cols		out t_varchar_list,
    i_all_cols		  in 	t_varchar_list,
    i_pk_cols		    in 	t_varchar_list
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-08-07 Chris Roderick
--    Creation - to support OCOMM-296
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    o_non_pk_cols := i_all_cols multiset except i_pk_cols;
    
  end establish_non_pk_hist_tab_cols;
  
  function only_pk_columns (
    i_non_pk_cols		    in 	t_varchar_list
  )
  return boolean
  is
  begin    
    return (i_non_pk_cols is null or i_non_pk_cols.count <1);
    
  end only_pk_columns;
  
  function get_trig_ddl_for_inserting (
    l_all_cols		    in 	t_varchar_list,
    l_pk_cols		      in 	t_varchar_list,
    l_non_pk_cols     in 	t_varchar_list,
    i_hist_table_name	in 	com_history_tables.history_table_name%type
  )
  return varchar2  
  is
    l_ddl_hist_trg_ins		  varchar2(32767)	:=	(chr(10))||(chr(9))||' when inserting then '||(chr(10));
  begin

    if only_pk_columns(l_non_pk_cols) then  
      l_ddl_hist_trg_ins := l_ddl_hist_trg_ins||(chr(9))||'insert into '||lower(i_hist_table_name)||' (';
       
      for i in l_all_cols.first .. l_all_cols.last loop	
        l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||l_all_cols(i)||', ';
      end loop;		
        
      l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||' '||lower(c_hist_tab_optype_col||', '||c_hist_tab_ctime_col||', '||c_hist_tab_trans_id_col||', '||c_hist_tab_module_col||', '||c_hist_tab_action_col||', '||c_hist_tab_cinfo_col)||') values (';
      for i in l_all_cols.first .. l_all_cols.last loop	
        l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||':new.'||l_all_cols(i)||', ';
      end loop;				
      
      l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||' ''I'', '||c_ddl_get_dml_time||', '||c_ddl_get_trans_id||', '||c_ddl_get_module||', '||c_ddl_get_action||', '||c_ddl_get_cinfo||');'||(chr(10));
    
    else
  
  --  PART 1: Should generate code to establish the previous 2 types of operations made, and the corresponding row ids from the history which should look sometihng like this:     
  --    select max(distinct (case when rownum = 1 then op_type else null end)) op_type_11
  --    , max(distinct (case when rownum = 2 then op_type else null end)) op_type_2
  --    , max(distinct (case when rownum = 1 then rid else null end)) rid_1
  --    , max(distinct (case when rownum = 2 then rid else null end)) rid_2 
  --    into l_op_type_1, l_op_type_2, l_rid_1, l_rid_2
  --    from (
  --      select rowid rid, op_type 
  --      from slots_hist 
  --      where slot_id = :new.slot_id 
  --      and version = :new.version
  --      order by create_time_utc desc 
  --     )
  --     where rownum <=2;
  
      l_ddl_hist_trg_ins	:= l_ddl_hist_trg_ins||(chr(9))||(chr(9))||'select max(distinct (case when rownum = 1 then op_type else null end)) op_type_1'
                                    ||(chr(10))||(chr(9))||(chr(9))||'     , max(distinct (case when rownum = 2 then op_type else null end)) op_type_2'
                                    ||(chr(10))||(chr(9))||(chr(9))||'     , max(distinct (case when rownum = 1 then rid else null end)) rid_1'
                                    ||(chr(10))||(chr(9))||(chr(9))||'     , max(distinct (case when rownum = 2 then rid else null end)) rid_2'
                                    ||(chr(10))||(chr(9))||(chr(9))||'into l_op_type_1, l_op_type_2, l_rid_1, l_rid_2'
                                    ||(chr(10))||(chr(9))||(chr(9))||'from ('
                                    ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'select rowid rid, op_type'
                                    ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'from '||lower(i_hist_table_name)
                                    ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'where ';
          
      for i in l_pk_cols.first .. l_pk_cols.last loop	
        l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||l_pk_cols(i)||'=:new.'||l_pk_cols(i);
        
        if i != l_pk_cols.last then
          l_ddl_hist_trg_ins := l_ddl_hist_trg_ins||' and ';
        end if;
        
      end loop;		
            
      l_ddl_hist_trg_ins	:=  l_ddl_hist_trg_ins
                                     ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'order by '||lower(c_hist_tab_ctime_col)||' desc'
                                     ||(chr(10))||(chr(9))||(chr(9))||')'
                                     ||(chr(10))||(chr(9))||(chr(9))||'where rownum <=2;'
                                     ||(chr(10))||(chr(10));
   
  --  PART 2: Should generate code which looks like this:          
  --    if l_op_type_1 = ''D'' then 
  --      select IFT_EXTERNAL_ID||SLOT_TYPE_ID||NAME||ALTERNATE_NAME||FUNCTIONAL_NAME||OCCURENCE||STEL_NAME||VAC_NAME||POS_HC_MIDDLE||QRL_NAME||REGION_ID||FAMILY||BEAM||ACCEPTANCE||S_START_ABS||S_END_ABS||U_START_ABS||U_END_ABS||V_START_ABS||V_END_ABS||S_START_RELATIVE||S_END_RELATIVE||U_START_RELATIVE||U_END_RELATIVE||V_START_RELATIVE||V_END_RELATIVE||A||B||C||STATUS||NOTE||DATE_OF_CREATION||LAST_UPDATE||MTF||MACHINE_CODE||FLAG||RESPONSIBLE_ID||CIVIL_WORK_ID||START_OR_MID||CONNECTION_SIDE||PHASE_START||PHASE_END||NAME_LOC||S_START_LOC||S_END_LOC||CIVIL_WORK_ID_LOC||FAMILY_LOC||OCCURENCE_LOC||ANCHOR_ID||MACHINE_CODE_LOC||RADIATION_ZONE_ID||RADIATION_TAG||VAC_SEC_A_ID||VAC_SEC_B_ID||QRL_SEC_A_ID||QRL_SEC_B_ID||DESCRIPTION||DMU_REF_SLOT_ID||NORMA||EL_GRID_ALPHABETIC||EL_GRID_NUMERIC into l_non_key_data
  --      from slots_hist
  --      where rowid = l_rid_1;  
      
      l_ddl_hist_trg_ins	:=  l_ddl_hist_trg_ins
                 ||(chr(9))||(chr(9))||'if l_op_type_1 = ''D'' then'
      ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'select ';
         
      for i in 1 .. l_non_pk_cols.last loop	
        l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||l_non_pk_cols(i);
       
        if i != l_non_pk_cols.last then
          l_ddl_hist_trg_ins := l_ddl_hist_trg_ins||'||';
        end if;
      end loop;
         
      l_ddl_hist_trg_ins := l_ddl_hist_trg_ins
                 ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'into l_non_key_data'
                 ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'from '||lower(i_hist_table_name)
                 ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'where rowid = l_rid_1;'
                 ||(chr(10))||(chr(10)) ;
      
  -- PART 3:Should generate code which looks like this:               
  --   if l_non_key_data = :new.IFT_EXTERNAL_ID||:new.SLOT_TYPE_ID||:new.NAME||:new.ALTERNATE_NAME||:new.FUNCTIONAL_NAME||:new.OCCURENCE||:new.STEL_NAME||:new.VAC_NAME||:new.POS_HC_MIDDLE||:new.QRL_NAME||:new.REGION_ID||:new.FAMILY||:new.BEAM||:new.ACCEPTANCE||:new.S_START_ABS||:new.S_END_ABS||:new.U_START_ABS||:new.U_END_ABS||:new.V_START_ABS||:new.V_END_ABS||:new.S_START_RELATIVE||:new.S_END_RELATIVE||:new.U_START_RELATIVE||:new.U_END_RELATIVE||:new.V_START_RELATIVE||:new.V_END_RELATIVE||:new.A||:new.B||:new.C||:new.STATUS||:new.NOTE||:new.DATE_OF_CREATION||:new.LAST_UPDATE||:new.MTF||:new.MACHINE_CODE||:new.FLAG||:new.RESPONSIBLE_ID||:new.CIVIL_WORK_ID||:new.START_OR_MID||:new.CONNECTION_SIDE||:new.PHASE_START||:new.PHASE_END||:new.NAME_LOC||:new.S_START_LOC||:new.S_END_LOC||:new.CIVIL_WORK_ID_LOC||:new.FAMILY_LOC||:new.OCCURENCE_LOC||:new.ANCHOR_ID||:new.MACHINE_CODE_LOC||:new.RADIATION_ZONE_ID||:new.RADIATION_TAG||:new.VAC_SEC_A_ID||:new.VAC_SEC_B_ID||:new.QRL_SEC_A_ID||:new.QRL_SEC_B_ID||:new.DESCRIPTION||:new.DMU_REF_SLOT_ID||:new.NORMA||:new.EL_GRID_ALPHABETIC||:new.EL_GRID_NUMERIC then   			   
  --     old and new records are the same so delete the delete, do not insert the new history record and set retire time of previous insert/update to null
  --     delete from slots_hist where rowid = l_rid_1;
  --     update slots_hist set retire_time_utc = null where rowid = l_rid_2;                
  --   else
  --     old and new records are different so write the history      
  --     insert into SLOTS_HIST (SLOT_ID, IFT_EXTERNAL_ID, SLOT_TYPE_ID, NAME, ALTERNATE_NAME, FUNCTIONAL_NAME, OCCURENCE, STEL_NAME, VAC_NAME, POS_HC_MIDDLE, QRL_NAME, REGION_ID, FAMILY, BEAM, ACCEPTANCE, S_START_ABS, S_END_ABS, U_START_ABS, U_END_ABS, V_START_ABS, V_END_ABS, S_START_RELATIVE, S_END_RELATIVE, U_START_RELATIVE, U_END_RELATIVE, V_START_RELATIVE, V_END_RELATIVE, A, B, C, STATUS, NOTE, VERSION, DATE_OF_CREATION, LAST_UPDATE, MTF, MACHINE_CODE, FLAG, RESPONSIBLE_ID, CIVIL_WORK_ID, START_OR_MID, CONNECTION_SIDE, PHASE_START, PHASE_END, NAME_LOC, S_START_LOC, S_END_LOC, CIVIL_WORK_ID_LOC, FAMILY_LOC, OCCURENCE_LOC, ANCHOR_ID, MACHINE_CODE_LOC, RADIATION_ZONE_ID, RADIATION_TAG, VAC_SEC_A_ID, VAC_SEC_B_ID, QRL_SEC_A_ID, QRL_SEC_B_ID, DESCRIPTION, DMU_REF_SLOT_ID, NORMA, EL_GRID_ALPHABETIC, EL_GRID_NUMERIC,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:new.SLOT_ID, :new.IFT_EXTERNAL_ID, :new.SLOT_TYPE_ID, :new.NAME, :new.ALTERNATE_NAME, :new.FUNCTIONAL_NAME, :new.OCCURENCE, :new.STEL_NAME, :new.VAC_NAME, :new.POS_HC_MIDDLE, :new.QRL_NAME, :new.REGION_ID, :new.FAMILY, :new.BEAM, :new.ACCEPTANCE, :new.S_START_ABS, :new.S_END_ABS, :new.U_START_ABS, :new.U_END_ABS, :new.V_START_ABS, :new.V_END_ABS, :new.S_START_RELATIVE, :new.S_END_RELATIVE, :new.U_START_RELATIVE, :new.U_END_RELATIVE, :new.V_START_RELATIVE, :new.V_END_RELATIVE, :new.A, :new.B, :new.C, :new.STATUS, :new.NOTE, :new.VERSION, :new.DATE_OF_CREATION, :new.LAST_UPDATE, :new.MTF, :new.MACHINE_CODE, :new.FLAG, :new.RESPONSIBLE_ID, :new.CIVIL_WORK_ID, :new.START_OR_MID, :new.CONNECTION_SIDE, :new.PHASE_START, :new.PHASE_END, :new.NAME_LOC, :new.S_START_LOC, :new.S_END_LOC, :new.CIVIL_WORK_ID_LOC, :new.FAMILY_LOC, :new.OCCURENCE_LOC, :new.ANCHOR_ID, :new.MACHINE_CODE_LOC, :new.RADIATION_ZONE_ID, :new.RADIATION_TAG, :new.VAC_SEC_A_ID, :new.VAC_SEC_B_ID, :new.QRL_SEC_A_ID, :new.QRL_SEC_B_ID, :new.DESCRIPTION, :new.DMU_REF_SLOT_ID, :new.NORMA, :new.EL_GRID_ALPHABETIC, :new.EL_GRID_NUMERIC,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''I'', com_history_mgr.get_dml_time());
  --   end if;
   
      l_ddl_hist_trg_ins	:=  l_ddl_hist_trg_ins||(chr(9))||(chr(9))||(chr(9))||'if l_non_key_data = to_char(';
         
      for i in l_non_pk_cols.first .. l_non_pk_cols.last loop	
        l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||':new.'||l_non_pk_cols(i);
        
        if i!= l_non_pk_cols.last then
          l_ddl_hist_trg_ins :=	l_ddl_hist_trg_ins||'||';
        end if;
        
      end loop;   
         
      l_ddl_hist_trg_ins := l_ddl_hist_trg_ins||') then'
      ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||(chr(9))||'delete from '||lower(i_hist_table_name)||' where rowid = l_rid_1;'
      ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'else'
      ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||(chr(9))||'insert into '||lower(i_hist_table_name)||' (';
       
      for i in l_all_cols.first .. l_all_cols.last loop	
        l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||l_all_cols(i)||', ';
      end loop;		
        
      l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||' '||lower(c_hist_tab_optype_col||', '||c_hist_tab_ctime_col||', '||c_hist_tab_trans_id_col||', '||c_hist_tab_module_col||', '||c_hist_tab_action_col||', '||c_hist_tab_cinfo_col)||')'
                                ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||(chr(9))||'values (';
            
      for i in l_all_cols.first .. l_all_cols.last loop	
        l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||':new.'||l_all_cols(i)||', ';
      end loop;				
      
      l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||' ''I'', '||c_ddl_get_dml_time||', '||c_ddl_get_trans_id||', '||c_ddl_get_module||', '||c_ddl_get_action||', '||c_ddl_get_cinfo||');'||(chr(10));
      l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||(chr(9))||(chr(9))||(chr(9))||'end if;'||(chr(10))||(chr(10));
              
  --  PART 4:Should generate code which looks like this:     
  --    else 
  --      if the last record wasn't a delete operation - write the history  
  --      insert into SLOTS_HIST (SLOT_ID, IFT_EXTERNAL_ID, SLOT_TYPE_ID, NAME, ALTERNATE_NAME, FUNCTIONAL_NAME, OCCURENCE, STEL_NAME, VAC_NAME, POS_HC_MIDDLE, QRL_NAME, REGION_ID, FAMILY, BEAM, ACCEPTANCE, S_START_ABS, S_END_ABS, U_START_ABS, U_END_ABS, V_START_ABS, V_END_ABS, S_START_RELATIVE, S_END_RELATIVE, U_START_RELATIVE, U_END_RELATIVE, V_START_RELATIVE, V_END_RELATIVE, A, B, C, STATUS, NOTE, VERSION, DATE_OF_CREATION, LAST_UPDATE, MTF, MACHINE_CODE, FLAG, RESPONSIBLE_ID, CIVIL_WORK_ID, START_OR_MID, CONNECTION_SIDE, PHASE_START, PHASE_END, NAME_LOC, S_START_LOC, S_END_LOC, CIVIL_WORK_ID_LOC, FAMILY_LOC, OCCURENCE_LOC, ANCHOR_ID, MACHINE_CODE_LOC, RADIATION_ZONE_ID, RADIATION_TAG, VAC_SEC_A_ID, VAC_SEC_B_ID, QRL_SEC_A_ID, QRL_SEC_B_ID, DESCRIPTION, DMU_REF_SLOT_ID, NORMA, EL_GRID_ALPHABETIC, EL_GRID_NUMERIC,  USER_NAME, HOST_OR_IP_ADDRESS, OP_TYPE, CREATE_TIME_UTC) values (:new.SLOT_ID, :new.IFT_EXTERNAL_ID, :new.SLOT_TYPE_ID, :new.NAME, :new.ALTERNATE_NAME, :new.FUNCTIONAL_NAME, :new.OCCURENCE, :new.STEL_NAME, :new.VAC_NAME, :new.POS_HC_MIDDLE, :new.QRL_NAME, :new.REGION_ID, :new.FAMILY, :new.BEAM, :new.ACCEPTANCE, :new.S_START_ABS, :new.S_END_ABS, :new.U_START_ABS, :new.U_END_ABS, :new.V_START_ABS, :new.V_END_ABS, :new.S_START_RELATIVE, :new.S_END_RELATIVE, :new.U_START_RELATIVE, :new.U_END_RELATIVE, :new.V_START_RELATIVE, :new.V_END_RELATIVE, :new.A, :new.B, :new.C, :new.STATUS, :new.NOTE, :new.VERSION, :new.DATE_OF_CREATION, :new.LAST_UPDATE, :new.MTF, :new.MACHINE_CODE, :new.FLAG, :new.RESPONSIBLE_ID, :new.CIVIL_WORK_ID, :new.START_OR_MID, :new.CONNECTION_SIDE, :new.PHASE_START, :new.PHASE_END, :new.NAME_LOC, :new.S_START_LOC, :new.S_END_LOC, :new.CIVIL_WORK_ID_LOC, :new.FAMILY_LOC, :new.OCCURENCE_LOC, :new.ANCHOR_ID, :new.MACHINE_CODE_LOC, :new.RADIATION_ZONE_ID, :new.RADIATION_TAG, :new.VAC_SEC_A_ID, :new.VAC_SEC_B_ID, :new.QRL_SEC_A_ID, :new.QRL_SEC_B_ID, :new.DESCRIPTION, :new.DMU_REF_SLOT_ID, :new.NORMA, :new.EL_GRID_ALPHABETIC, :new.EL_GRID_NUMERIC,  nvl(nvl(sys_context(''USERENV'', ''CLIENT_IDENTIFIER''), sys_context(''USERENV'', ''OS_USER'')), ''UNKNOWN USER''), nvl(sys_context(''USERENV'', ''HOST''), sys_context(''USERENV'', ''IP_ADDRESS'')), ''I'', com_history_mgr.get_dml_time());   
  --    end if;
      l_ddl_hist_trg_ins := l_ddl_hist_trg_ins||(chr(9))||(chr(9))||'else'
                                   ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'insert into '||lower(i_hist_table_name)||' (';
       
      for i in l_all_cols.first .. l_all_cols.last loop	
        l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||l_all_cols(i)||', ';
      end loop;
          
      l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||' '||lower(c_hist_tab_optype_col||', '||c_hist_tab_ctime_col||', '||c_hist_tab_trans_id_col||', '||c_hist_tab_module_col||', '||c_hist_tab_action_col||', '||c_hist_tab_cinfo_col)||')'
                                ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||'values (';
      
      for i in l_all_cols.first .. l_all_cols.last loop	
        l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||':new.'||l_all_cols(i)||', ';
      end loop;				
      
      l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||' ''I'', '||c_ddl_get_dml_time||', '||c_ddl_get_trans_id||', '||c_ddl_get_module||', '||c_ddl_get_action||', '||c_ddl_get_cinfo||');'||(chr(10));
      l_ddl_hist_trg_ins	:=	l_ddl_hist_trg_ins||(chr(9))||(chr(9))||'end if;'||(chr(10))||(chr(10));

    end if;
    
    return l_ddl_hist_trg_ins;
  
  end get_trig_ddl_for_inserting;
  
  function get_trig_ddl_for_updating (
    l_all_cols		    in 	t_varchar_list,
    i_hist_table_name	in 	com_history_tables.history_table_name%type
  )
  return varchar2  
  is
    l_ddl_hist_trg_upd		  varchar2(32767)	:=	(chr(10))||(chr(9))||' when updating then '||(chr(10));
  begin     
--  generate trigger code which will perform the comparison of :old and :new values at the time of trigger execution
    l_ddl_hist_trg_upd	:= l_ddl_hist_trg_upd||(chr(9))||(chr(9))||(chr(9))||'if '||chr(10);
    
    for i in l_all_cols.first .. l_all_cols.last loop	
      l_ddl_hist_trg_upd	:=	l_ddl_hist_trg_upd
               ||(chr(9))||(chr(9))||(chr(9))||(chr(9))||'(:old.'||l_all_cols(i)||' != :new.'||l_all_cols(i)||') or'
      ||chr(10)||(chr(9))||(chr(9))||(chr(9))||(chr(9))||'(:old.'||l_all_cols(i)||' is null and :new.'||l_all_cols(i)||' is not null) or'
      ||chr(10)||(chr(9))||(chr(9))||(chr(9))||(chr(9))||'(:old.'||l_all_cols(i)||' is not null and :new.'||l_all_cols(i)||' is null)';
      
      if i != l_all_cols.last then
        l_ddl_hist_trg_upd := l_ddl_hist_trg_upd||' or'||chr(10);
      end if;
    end loop;		
     
    l_ddl_hist_trg_upd	:=	l_ddl_hist_trg_upd||' then'||chr(10)||chr(10);

    l_ddl_hist_trg_upd	:= l_ddl_hist_trg_upd||(chr(9))||(chr(9))||(chr(9))||(chr(9))||'insert into '||lower(i_hist_table_name)||' (';   		

    for i in l_all_cols.first .. l_all_cols.last loop	
      l_ddl_hist_trg_upd	:=	l_ddl_hist_trg_upd||l_all_cols(i)||', ';
    end loop;		
	  
    l_ddl_hist_trg_upd	:=	l_ddl_hist_trg_upd||' '||lower(c_hist_tab_optype_col||', '||c_hist_tab_ctime_col||', '||c_hist_tab_trans_id_col||', '||c_hist_tab_module_col||', '||c_hist_tab_action_col||', '||c_hist_tab_cinfo_col)||')'
                                ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||(chr(9))||'values (';
	       
    for i in l_all_cols.first .. l_all_cols.last loop	
      l_ddl_hist_trg_upd	:=	l_ddl_hist_trg_upd||':new.'||l_all_cols(i)||', ';
    end loop;				
    
    l_ddl_hist_trg_upd	:=	l_ddl_hist_trg_upd||' ''U'', '||c_ddl_get_dml_time||', '||c_ddl_get_trans_id||', '||c_ddl_get_module||', '||c_ddl_get_action||', '||c_ddl_get_cinfo||');'||(chr(10));
    l_ddl_hist_trg_upd	:=	l_ddl_hist_trg_upd||chr(10)||(chr(9))||(chr(9))||(chr(9))||'end if;'||chr(10);	
    
    return l_ddl_hist_trg_upd;
  
  end get_trig_ddl_for_updating;
  
  function get_trig_ddl_for_deleting (
    l_all_cols		    in 	t_varchar_list,
    i_hist_table_name	in 	com_history_tables.history_table_name%type
  )
  return varchar2  
  is
    l_ddl_hist_trg_del		  varchar2(32767)	:=	(chr(10))||(chr(9))||' when deleting then '||(chr(10));
  begin
  
    l_ddl_hist_trg_del	:= l_ddl_hist_trg_del||(chr(9))||(chr(9))||'insert into '||lower(i_hist_table_name)||' (';   		
    
    for i in l_all_cols.first .. l_all_cols.last loop	
      l_ddl_hist_trg_del	:=	l_ddl_hist_trg_del||l_all_cols(i)||', ';
    end loop;		
    
    l_ddl_hist_trg_del	:=	l_ddl_hist_trg_del||' '||lower(c_hist_tab_optype_col||', '||c_hist_tab_ctime_col||', '||c_hist_tab_trans_id_col||', '||c_hist_tab_module_col||', '||c_hist_tab_action_col||', '||c_hist_tab_cinfo_col)||')'
                                ||(chr(10))||(chr(9))||(chr(9))||'values (';
   
    for i in l_all_cols.first .. l_all_cols.last loop	
      l_ddl_hist_trg_del	:=	l_ddl_hist_trg_del||':old.'||l_all_cols(i)||', ';
    end loop;				
    
    l_ddl_hist_trg_del	:=	l_ddl_hist_trg_del||' ''D'', '||c_ddl_get_dml_time||', '||c_ddl_get_trans_id||', '||c_ddl_get_module||', '||c_ddl_get_action||', '||c_ddl_get_cinfo||');'||(chr(10));

    return l_ddl_hist_trg_del;
  
  end get_trig_ddl_for_deleting;
  
  function get_trig_ddl_for_case_stmt (
    l_all_cols		    in 	t_varchar_list,
    l_pk_cols		      in 	t_varchar_list,
    l_non_pk_cols     in 	t_varchar_list,
    i_hist_table_name	in 	com_history_tables.history_table_name%type
  )
  return varchar2  
  is
    l_ddl_hist_trg_ins		    varchar2(32767);
    l_ddl_hist_trg_upd		    varchar2(32767);
    l_ddl_hist_trg_del		    varchar2(32767);  
  begin
    l_ddl_hist_trg_ins  :=  get_trig_ddl_for_inserting(l_all_cols, l_pk_cols, l_non_pk_cols, i_hist_table_name);
    l_ddl_hist_trg_upd  :=  get_trig_ddl_for_updating(l_all_cols, i_hist_table_name);
    l_ddl_hist_trg_del  :=  get_trig_ddl_for_deleting(l_all_cols, i_hist_table_name);
    
    return l_ddl_hist_trg_ins||l_ddl_hist_trg_upd||l_ddl_hist_trg_del||(chr(10))||(chr(9))||'end case;';
  
  end get_trig_ddl_for_case_stmt;
  
  function get_trig_ddl_for_header (
    i_trigger_name	  in 	user_triggers.trigger_name%type,
    i_table_name      in  com_history_tables.table_name%type,
    i_hist_table_name in  com_history_tables.table_name%type
  )
  return varchar2  
  is
  begin
    return 	'create or replace trigger '
              ||i_trigger_name
              ||(chr(10))||'for insert or update or delete on '||lower(i_table_name)
              ||(chr(10))||'compound trigger'
              ||(chr(10))||(chr(10))||'l_non_key_data clob;'
              ||(chr(10))||'l_stmt_op_type com_dml_logs.op_type%type;'
              ||(chr(10))||'l_op_type_1 '||lower(i_hist_table_name)||'.op_type%type;'
              ||(chr(10))||'l_op_type_2 '||lower(i_hist_table_name)||'.op_type%type;'
              ||(chr(10))||'l_rid_1 rowid;'||(chr(10))||'l_rid_2 rowid;'
              ||(chr(10));
  end get_trig_ddl_for_header;
  
  function get_trig_ddl_for_footer (
    i_trigger_name		              in 	user_triggers.trigger_name%type,
    i_should_re_raise_trig_excepts  in  boolean
  )
  return varchar2  
  is
  begin
    return (chr(10))||(chr(10))||(chr(9))||'exception '
      ||(chr(10))||(chr(9))||(chr(9))||'when others then'                                               
      ||(chr(10))||(chr(9))||(chr(9))||(chr(9))||
      (case 
        when i_should_re_raise_trig_excepts then 'raise;'
        else 'com_event_mgr.raise_event(''HIST_TRIGGER_ERROR'', ''Error in '||upper(i_trigger_name)||' trigger. Err: ''||dbms_utility.format_error_stack, false, true);'
      end)
      ||(chr(10))||(chr(9))||'end;'
      ||(chr(10))||'end after each row;'                                              
      ||(chr(10))||(chr(10))||'end '||i_trigger_name||';';
  end get_trig_ddl_for_footer;
  
  function get_history_trig_name (
    i_table_name  in  com_history_tables.table_name%type
  )
  return user_triggers.trigger_name%type
  is
  begin
    return lower(substr(i_table_name, 1, (30-length(c_hist_trg_sfx)))||c_hist_trg_sfx);
  end get_history_trig_name;
	
  procedure create_history_triggers(	
      i_table_name		      in 	com_history_tables.table_name%type
    , i_hist_table_name	    in 	com_history_tables.history_table_name%type
    , i_ignore_cols         in  com_history_tables.ignored_columns%type default null
    , i_re_raise_exceptions in  com_history_tables.re_raise_exceptions%type default 'N'
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-11-20  Chris Roderick
--    Added use of new re_raise_exceptions exceptions attribute as part of OCOMM-296
--    Significant re-factoring
--  2014-08-07 Chris Roderick
--    Modified to use CLOB for row comparisons as per OCOMM-296
--    Re-formatting
--  2014-06-30 Chris Roderick
--    Added bug fix OCOMM-274 to handle DML logging with MERGE statements
--  2014-06-27  Pascal Le Roux
--    Modified  the procedure to generate history triggers which comply 
--    with coding conventions (lower case, no decode)
--  2014-03-31  James Menzies and Chris Roderick
--    Modified trigger generation code to use transaction ids instead of com_app_Session ids, and 
--    to use new procedure log_dml_operation as per OCOMM-251.
--  2013-01-16  Pascal Le Roux
--    Rewrite the procedure so as to generate one compound trigger 
--    instead of the two separate _WHIST and _HISTSTMP triggers.
--  2012-11-01 Chris Roderick
--    Bug fix, missing 'else' condition spotted by Pascal before generated insert statement. 
--  2011-11-11 Pascal
--    Modification to handle table with no non key  columns in _WHIST trigger
--    (No more NO_NON_KEY_COLS raised)
--    Modify the trigger generation to have better formatted code generated
--  2011-11-08 Pascal
--    Fix a bug: replace  test_hist_functionality_hist.op_type%type
--    by '||i_hist_table_name||'.op_type%type in the l_ddl_whist_trg_header
--  2011-08-05 Chris Roderick
--    Added a number column (com_app_session_id) to reference captured application session 
--    information.
--    Added module, action, client_info columns.
--    Removed the retire_time_utc, user_name, and host_or_ip from the generated columns, since these
--    should be available via the referenced application session.
--  2011-05-30 Eve Fortescue-Beck
--   Trigger generation code re-written to include cleaning
--  2011-03-18  Eve Fortescue-Beck, Chris Roderick
--    Replaced trigger code to call com_history_mgr rather than history
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_trigger_name                  user_triggers.trigger_name%type;
	  l_all_cols					            t_varchar_list;
	  l_pk_cols				                t_varchar_list;	 
    l_non_pk_cols				            t_varchar_list;	
    l_should_re_raise_trig_excepts  boolean;
    l_ddl_hist_trg_header	  varchar2(32767);
    c_after_each_row_header constant varchar2(32767) :=	           
                (chr(10))
                ||(chr(10))||'after each row is'
                ||(chr(10))||'begin'
                ||(chr(10))||(chr(9))||'begin'
                ||(chr(10))||(chr(9))||'case';
    l_ddl_hist_trg_case_stmt  varchar2(32767);
    l_ddl_hist_trg_footer	    varchar2(32767);    
    l_ddl_hist_trg_executable	clob;    
  begin  
    l_trigger_name        :=  get_history_trig_name(i_table_name);
    l_ddl_hist_trg_header :=  get_trig_ddl_for_header(l_trigger_name, i_table_name, i_hist_table_name);
    
    establish_all_hist_tab_cols(l_all_cols, i_table_name, i_ignore_cols);
    establish_pk_hist_tab_cols(l_pk_cols, i_table_name);
    establish_non_pk_hist_tab_cols(l_non_pk_cols, l_all_cols, l_pk_cols);
    
    com_assert.is_true ((l_pk_cols is not null and l_pk_cols.count > 0), 'The table: '||i_table_name||' does not have a primary key, which is required for history tracking');
    		
    l_ddl_hist_trg_case_stmt :=  get_trig_ddl_for_case_stmt(l_all_cols, l_pk_cols, l_non_pk_cols, i_hist_table_name);
    
    l_should_re_raise_trig_excepts  :=  com_util.char_to_boolean(i_re_raise_exceptions);
    l_ddl_hist_trg_footer :=  get_trig_ddl_for_footer(l_trigger_name, l_should_re_raise_trig_excepts);
    
--    com_logger.debug('Hist trigger header, length: '||length(l_ddl_hist_trg)||' txt: '||l_ddl_hist_trg, $$plsql_unit);
--    com_logger.debug('Hist trigger insert, length: '||length(l_ddl_hist_trg_ins)||' txt: '||l_ddl_hist_trg_ins, $$plsql_unit);
--    com_logger.debug('Hist trigger update, length: '||length(l_ddl_hist_trg_upd)||' txt: '||l_ddl_hist_trg_upd, $$plsql_unit);
--    com_logger.debug('Hist trigger delete, length: '||length(l_ddl_hist_trg_del)||' txt: '||l_ddl_hist_trg_del, $$plsql_unit);
--    com_logger.debug('Hist trigger header, length: '||length(l_ddl_hist_trg_header)||' txt: '||l_ddl_hist_trg_header, $$plsql_unit);
    
    l_ddl_hist_trg_executable	:= l_ddl_hist_trg_header||get_before_stmt_code(i_table_name)||c_after_each_row_header||l_ddl_hist_trg_case_stmt||l_ddl_hist_trg_footer;
	
    begin
      execute immediate (l_ddl_hist_trg_executable);
    exception
      when others then
        com_event_mgr.raise_error_event('HIST_WRITE_TRIGGER_FAILED','Failed to create history write trigger using: '||l_ddl_hist_trg_executable);         
    end;
			
  exception
    when no_data_found then
      com_event_mgr.raise_error_event('TABLE_NOT_FOUND', 'The table: '||i_table_name||' was not found');    
    when others then
      com_event_mgr.raise_error_event('HIST_TRIGGERS_FAILED', 'Problem trying to create history triggers for table: '||i_table_name||' Err '||dbms_utility.format_error_stack);
	
  end create_history_triggers;
	
  procedure init_history_table (	
      i_table_name	    in	com_history_tables.table_name%type
    , i_hist_table_name in 	com_history_tables.history_table_name%type
    , i_ignore_cols     in  com_history_tables.ignored_columns%type default null
  )
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Local procedure which inserts initial data into the 'i_table_name' history table.  
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_table_name - Name of the base table
--    i_hist_table_name	- The name of the History Table       
--    i_ignore_cols  - A list of columns in the base table  which should not be included in the history table
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    INIT_FAILED - Initialisation of history table failed  
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-28  Jose Rolland
--    Modified code in order to skip virtual columns OCOMM-380
--  2011-08-05 Chris Roderick
--    Added a number column (com_app_session_id) to reference captured application session 
--    information.
--    Added module, action, client_info columns.
--    Removed the retire_time_utc, user_name, and host_or_ip from the generated columns, since these
--    should be available via the referenced application session.
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_pk_cols   t_varchar_list;
    l_all_cols	t_varchar_list;
    l_dml	 	    varchar2(32767)	:=	'insert into '||i_hist_table_name;
	begin		
--  get the all base table columns names
	  select column_name 
	  bulk collect into l_all_cols 
    from user_tab_cols 
    where hidden_column = 'NO' 
    and virtual_column = 'NO'
    and table_name = upper(i_table_name) 
    and column_name not in (
      select column_value
      from table(i_ignore_cols)
    )
    order by column_id;	    		
				
--  get the primary key columns names
    select column_name 
    bulk collect into l_pk_cols 
    from user_cons_columns acc 
    join user_constraints using (constraint_name) 
    where acc.table_name = upper(i_table_name) 
    and constraint_type = 'P' 
    order by position;	    
		
--  write the dml		
--  column qualifiers
    l_dml	:=	l_dml||' ('||l_all_cols(l_all_cols.first);
		
    for i in 2 .. l_all_cols.count loop	
      l_dml	:=	l_dml||', '||l_all_cols(i);
    end loop;
		
--  Selected columns for insert
    l_dml	:=	l_dml||', '||c_hist_tab_optype_col||', '||c_hist_tab_ctime_col||', '||c_hist_tab_trans_id_col||', '||c_hist_tab_module_col||', '||c_hist_tab_action_col||', '||c_hist_tab_cinfo_col||') select '||l_all_cols(l_all_cols.first);
		
    if l_all_cols.count > 1 then 
      for i in 2 .. l_all_cols.count loop	
        l_dml	:=	l_dml||', '||l_all_cols(i);
      end loop;
    end if;
    
    l_dml	:=	l_dml||', ''I'', sys_extract_utc(systimestamp), '||c_ddl_get_trans_id||', '||c_ddl_get_module||', ''HIST_INIT'', '||c_ddl_get_cinfo||' from '||i_table_name||' d where not exists (select null from '||i_hist_table_name|| ' where ';

--  Filter by existing rows in history table which match PKs
    l_dml	:=	l_dml||l_pk_cols(l_pk_cols.first)||' = d.'||l_pk_cols(l_pk_cols.first);
				
    if l_pk_cols.count > 1 then 
      for i in 2 .. l_pk_cols.count loop	
        l_dml	:=	l_dml||' and '||l_pk_cols(i)||' = d.'||l_pk_cols(i);
      end loop;
    end if;
		
    l_dml	:=	l_dml||')';

    execute immediate(l_dml);
			
  exception
	  when others then
      com_event_mgr.raise_error_event('INIT_FAILED', 'Unable to init history table: '||i_hist_table_name||' using: '||l_dml);
	
  end init_history_table;
  
  procedure register_history_table_config (
    i_table_name              in  com_history_tables.table_name%type,
    i_hist_table_name	        in  com_history_tables.history_table_name%type,
    i_ignore_cols             in  com_history_tables.ignored_columns%type default null,
    i_re_raise_exceptions     in  com_history_tables.re_raise_exceptions%type default 'N'
  )
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-11-21 Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
  begin
    merge into com_history_tables using (
      select upper(i_table_name) tn, upper(i_hist_table_name) htn, systimestamp ts
      from dual
    )
    on (table_name = tn)
    when matched then 
      update set last_enabled = ts, ignored_columns = i_ignore_cols, re_raise_exceptions = i_re_raise_exceptions
    when not matched then 
      insert (table_name, history_table_name, last_enabled, ignored_columns, re_raise_exceptions)
        values (tn, htn, ts, i_ignore_cols, i_re_raise_exceptions);
  
  end register_history_table_config;

  procedure enable_history (
    i_table_name              in    com_history_tables.table_name%type,
    i_hist_table_part_prefix  in    com_time_range_part_tables.part_prefix%type default null,
    i_ignore_cols             in    com_history_tables.ignored_columns%type default null,
    i_no_parts_advance        in    com_time_range_part_tables.no_parts_advance%type default 3,    
    i_no_parts_keep           in    com_time_range_part_tables.no_parts_keep%type default null,      
    i_part_range_int_cnt      in    com_time_range_part_tables.part_range_int_cnt%type default 1,  
    i_part_range_int_type     in    com_time_range_part_tables.part_range_int_type%type default 'MONTH',
    i_re_raise_exceptions     in    com_history_tables.re_raise_exceptions%type default com_constants.c_false,
    i_pre_populate_from_src_table in boolean default true
  )    
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-02-13  Lukasz Burdzanowski
--    OCOMM-305 Added i_pre_populate_from_src_table
--  2015-02-11  Lukasz Burdzanowski, Chris Roderick
--    OCOMM-303 Made i_hist_table_part_prefix optional
--  2014-08-07 Chris Roderick
--    Added new parameter i_re_raise_exceptions as per OCOMM-296
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
----------------------------------------------------------------------------------------------------------------------- 
  is 
    l_hist_table_name	com_history_tables.history_table_name%type;
	begin  
    create_history_table(upper(i_table_name), l_hist_table_name, i_hist_table_part_prefix, i_ignore_cols,i_no_parts_advance, i_no_parts_keep, i_part_range_int_cnt, i_part_range_int_type );
    if i_pre_populate_from_src_table then
      init_history_table(upper(i_table_name), upper(l_hist_table_name), i_ignore_cols);
    end if;
    register_history_table_config(i_table_name, l_hist_table_name, i_ignore_cols, i_re_raise_exceptions);
    create_history_triggers(i_table_name, l_hist_table_name, i_ignore_cols, i_re_raise_exceptions);
    
  exception
    when others then
      com_event_mgr.raise_event('ENABLE_HIST_FAILED', 'Enable History Failed for :'||i_table_name||', Err: '||dbms_utility.format_error_stack, true, true);
      
  end enable_history;

  procedure disable_history (
    i_table_name  in  com_history_tables.table_name%type
  ) 
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-11-21  Chris Roderick
--    Re-factoring check for trigger name, since we only have a single history trigger since a while now.
--  2013-01-16  Pascal Le Roux
--    Adapted the code following the change to have 1 compound trigger instead of 2 separates ones for history capture 
--  2011-03-08  Eve Fortescue-Beck, Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_hist_tab_name	user_tables.table_name%type;
    l_hist_trigger	user_triggers.trigger_name%type;
	begin
--  Check the table exists and find the name of the appropriate history table
    select table_name 
    into l_hist_tab_name 
    from user_tables 
    where table_name = upper(substr(i_table_name, 1, (30-length(c_hist_tab_sfx)))||c_hist_tab_sfx);
            
--  Check the relevant History trigger exists and if so drop them.
    select trigger_name 
    into l_hist_trigger 
    from user_triggers 
    where table_name = upper(i_table_name) 
    and trigger_name = (upper((substr(i_table_name, 1, (30-length(c_hist_trg_sfx)))||c_hist_trg_sfx)));

    execute immediate ('drop trigger '||l_hist_trigger);
		
    delete from com_history_tables 
    where table_name = i_table_name;
			
  exception
	  when no_data_found then
      com_event_mgr.raise_event('HISTORY_NOT_ENABLED', 'History was not enabled for the table: '||i_table_name, false, true);
    when others then
      com_event_mgr.raise_event('DISABLE_HIST_FAILED', 'Disable History Failed for :'||i_table_name, true, true); 
  end disable_history;

  procedure log_dml_operation (
      i_table_name  in com_dml_logs.table_name%type,
      i_op_type     in com_dml_logs.op_type%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-03-31  James Menzies and Chris Roderick
--    Creation as per OCOMM-251
------------------------------------------------------------------------------------------------------------------------
  is    
  begin
    insert into com_dml_logs (com_app_session_id, create_time_utc, op_type, table_name, transaction_id)
    values (com_app_session_mgr.get_app_session_id(), com_history_mgr.get_dml_time(), i_op_type, i_table_name, dbms_transaction.local_transaction_id);
    
  end log_dml_operation;

  procedure replace_history_trigger(	
      i_table_name		  in 	com_history_tables.table_name%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-11-20  Chris Roderick
--    Added use of new re_raise_exceptions exceptions attribute as part of OCOMM-296 
--  2014-04-28  Pascal Le Roux, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_hist_table_name	    com_history_tables.history_table_name%type;
    l_ignore_cols         com_history_tables.ignored_columns%type;
    l_re_raise_exceptions com_history_tables.re_raise_exceptions%type;
  begin
    select history_table_name, ignored_columns, re_raise_exceptions
    into l_hist_table_name, l_ignore_cols, l_re_raise_exceptions
    from com_history_tables
    where table_name = i_table_name;
    
    create_history_triggers(i_table_name, l_hist_table_name, l_ignore_cols, l_re_raise_exceptions);
  
  exception
    when no_data_found then
      com_event_mgr.raise_error_event('PROCESSING_ERROR', 'Failed to replace history trigger - history was not enabled for table: '||i_table_name);
      
  end replace_history_trigger;
  
end com_history_mgr;