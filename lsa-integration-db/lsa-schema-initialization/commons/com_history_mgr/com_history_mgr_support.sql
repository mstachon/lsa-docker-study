drop table history_cleaning_log;
drop table history_tables;
create table com_history_tables (
    table_name            varchar2(30)  constraint com_hist_tabs_pk primary key, 
    history_table_name    varchar2(30)  constraint com_hist_tabs_hist_tab_nn not null, 
    last_enabled          timestamp(6),
    last_change           timestamp(6), 
--    partition_name_prefix varchar2(20), --will be stored in partition table
    constraint com_hist_tabs_part_prefix_uk unique (partition_name_prefix)
);
comment on table com_history_tables is 'List of tables with History enabled';
comment on column com_history_tables.table_name is 'The name of the table which has history enabled';
comment on column com_history_tables.history_table_name is 'The name of the table which holds the corresponding history data';
comment on column com_history_tables.last_enabled is 'The time that history was last enabled on this table';
comment on column com_history_tables.last_change is '';
--comment on column com_history_tables.partition_name_prefix is 'Will be used to prefix the names of the history table range partitions (prior to the date)';

create table com_history_cleaning_log (
  table_name      varchar2 (30), 
  cleaned         timestamp (6), 
  no_recs_deleted number, 
  constraint history_cleaning_log_pk primary key (table_name, cleaned)
) organization index;
comment on table com_history_cleaning_log is 'Log of the results of History table cleaning';

create or replace context com_history_context using commons.com_history_ctx_pkg;

-- as user acccon_sys, then grant to other users e.g. LSA

create or replace type table_of_varchar as table of varchar2(1000);
/
select * from user_objects where status = 'INVALID';
begin compile_invalid_objects; end;
/

select * from com_history_tables order by table_name;

create table test_hist_functionality (
  id_1        number,
  id_2        number,
  a_name      varchar2(30),
  description varchar2(100)
);

begin
  com_history_mgr.enable_history('TEST_HIST_FUNCTIONALITY', 'TEST_HIST_FUNC_HIST');
end;
/

select * from com_logs order by utc_timestamp desc;

drop table test_hist_functionality;
create table test_hist_functionality (
  id_1        number,
  id_2        number,
  a_name      varchar2(30),
  description varchar2(100),
  constraint thistfunc_pk primary key (id_1, id_2)
);
DELETE FROM COM_TIME_RANGE_PART_TABLES WHERE TABLE_NAME = 'TEST_HIST_FUNCTIONALITY_HIST';
commit;
drop table test_hist_functionality_hist;
begin
  com_history_mgr.enable_history('TEST_HIST_FUNCTIONALITY', 'TEST_HIST_FUNC_HIST');
end;
/

begin
  com_history_mgr.disable_history('TEST_HIST_FUNCTIONALITY');
--  history.enable_history('TEST_HIST_FUNCTIONALITY', 'SUB_SEQUENCES_HIST', table_of_varchar('CREATION_TIME_UTC'));
end;
/


insert into test_hist_functionality (id_1, id_2, a_name, description)
values (1,2, 'name', 'desc');



update test_hist_functionality set description = 'new descr' where a_name = 'name';
update test_hist_functionality set a_name = 'new name' where a_name = 'new name';
commit;
delete from test_hist_functionality;
insert into test_hist_functionality (id_1, id_2, a_name, description)
values (1,2, 'new name', 'new descr 2');

rollback;

select * from test_hist_functionality_hist order by create_time_utc desc;
commit;

begin
  com_history_mgr.clean_history('TEST_HIST_FUNCTIONALITY_HIST');
end;
/

select * from com_history_cleaning_log order by cleaned desc;



--  Updates to support issue OCOMM-22

--  Sequence to generate unique identifiers for actions
create sequence com_hist_actions_seq;

--  Hash partitioned table to store the details of actions which cause history entries. 
--  This table will be referenced by generated history tables, and populated  via the generated
--  history statment triggers which fire before DML on the source tables
create table com_hist_actions (
  hist_action_id      number          constraint comhistacts_pk primary key,
  table_name          varchar2(30)    constraint comhistacts_tabname_nn not null,
  op_type             char(1)         constraint comhistacts_optype_nn not null,
  client_user_name    varchar2(30),
  client_app_name     varchar2(30), 
  client_host_name    varchar2(30),
  client_ip           varchar2(30),
  os_user             varchar2(30)    default sys_context('userenv', 'OS_USER'), 
  os_host_name        varchar2(30)    default sys_context('userenv', 'HOST'), 
  client_info         varchar2(64)    default sys_context('userenv', 'CLIENT_INFO'), 
  module              varchar2(48)    default sys_context('userenv', 'MODULE'), 
  action              varchar2(32)    default sys_context('userenv', 'ACTION'), 
  comm_session_id     number, 
  constraint comhistacts_optype_chk check (op_type in ('D', 'I', 'U')),
  constraint comhistacts_tabname_uc_chk check (table_name = upper(table_name))
) partition by hash (hist_action_id) (
  partition com_hist_actions_p1,
  partition com_hist_actions_p2,
  partition com_hist_actions_p3,
  partition com_hist_actions_p4,
  partition com_hist_actions_p5,
  partition com_hist_actions_p6,
  partition com_hist_actions_p7,
  partition com_hist_actions_p8,
  partition com_hist_actions_p9,
  partition com_hist_actions_p10,
  partition com_hist_actions_p11,
  partition com_hist_actions_p12,
  partition com_hist_actions_p13,
  partition com_hist_actions_p14,
  partition com_hist_actions_p15,
  partition com_hist_actions_p16,
  partition com_hist_actions_p17,
  partition com_hist_actions_p18,
  partition com_hist_actions_p19,
  partition com_hist_actions_p20,
  partition com_hist_actions_p21,
  partition com_hist_actions_p22,
  partition com_hist_actions_p23,
  partition com_hist_actions_p24,
  partition com_hist_actions_p25,
  partition com_hist_actions_p26,
  partition com_hist_actions_p27,
  partition com_hist_actions_p28,
  partition com_hist_actions_p29,
  partition com_hist_actions_p30,
  partition com_hist_actions_p31,
  partition com_hist_actions_p32
);

comment on table com_hist_actions is 'Stores the details (who, what, where, how) of all actions which caused history table entries to be generated';
comment on column com_hist_actions.table_name is 'The name of the history table which was modified by this action';