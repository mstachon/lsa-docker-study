--------------------------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
--------------------------------------------------------------------------------------------------------------------------------------------
--  Description:
--    With version 1.3.0, com_dml_logs table has been created, com_app_session_mgr has been modified 
--    to manage transactions. In the new _HIST tables com_app_session_id is now replaced by transaction_id.  
--    Once upgraded to version 1.3.0, this script can be used to transform the history from the previous format 
--    to the new format with transaction_id
--------------------------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  Pascal Le Roux  2014-05-16
--    Creation
--------------------------------------------------------------------------------------------------------------------------------------------

begin
  for r_table in (select hst.table_name, hst.history_table_name 
                  from com_history_tables hst 
                  join user_tab_columns cols on (hst.history_table_name= cols.table_name and cols.column_name = 'COM_APP_SESSION_ID')
                  ) loop

   execute immediate 'update '||r_table.history_table_name ||' set com_app_session_id = null where com_app_session_id is not null'; 
       
   execute immediate 'alter table '||r_table.history_table_name ||' rename column com_app_session_id to transaction_id';

   execute immediate 'alter table '||r_table.history_table_name ||' modify transaction_id varchar2(48)';
   
   execute immediate 'create index '||substr(r_table.history_table_name, 1, 22)||'_trn_i'||' on '||r_table.history_table_name||' (transaction_id)';
   
   com_history_mgr.replace_history_trigger (r_table.table_name);                
               
  end loop;
end;