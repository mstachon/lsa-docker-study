create or replace package com_types
-----------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
-----------------------------------------------------------------------------------------------------------------------
-- Description:
--  This package is intended to manage definitions of common PL/SQL types and sub-types
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-20 Jose Rolland
--    Added t_yes_no - OCOMM-405
--  2015-10-01 Chris Roderick
--    Creation. OCOMM-348
-----------------------------------------------------------------------------------------------------------------------
is
  subtype t_varchar2_max_size_plsql is varchar2(32767);
  subtype t_varchar2_max_size_db    is varchar2(4000);
  subtype t_boolean_as_char         is  char(1);
  subtype t_yes_no                  is varchar2(3);

end com_types;