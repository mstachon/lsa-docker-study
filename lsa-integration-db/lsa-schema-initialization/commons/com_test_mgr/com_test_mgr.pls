create or replace package com_test_mgr 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Commons4Oracle testing framework manager. The manager provides functionality to register and manage test suites 
--    and test packages. See com_test_runner for more details.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-31 Lukasz Burdzanowski
--    Creation [OCOMM-322]
------------------------------------------------------------------------------------------------------------------------
is

  procedure add_test_suite (
    i_suite_name        in com_test_suites.suite_name%type,
    i_parent_suite_name in com_test_suites.suite_name%type default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Adds a new test suite with an optional parent suite name. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_test_suite_name - name of the test suite to add
--    i_parent_suite_name - optional name of the parent test suite; the parent test suite has to exist
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    DUP_VAL_ON_INDEX when the suite name already exists
--    NO_DATA_FOUND_ERROR when parent suite does not exist
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-31 Lukasz Burdzanowski
--    Creation [OCOMM-322]
------------------------------------------------------------------------------------------------------------------------

  procedure add_test_package (
    i_package_name  in com_test_packages.package_name%type,
    i_suite_name    in com_test_suites.suite_name%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Adds a new test package to the provided test suite
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_test_package_name - name of the test package
--    i_test_suite_name - name of the test suite
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    DUP_VAL_ON_INDEX when the package name already exists
--    NO_DATA_FOUND_ERROR when suite does not exist
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-22 Lukasz Burdzanowski
--    Creation [OCOMM-322]
------------------------------------------------------------------------------------------------------------------------

end com_test_mgr;