create or replace package body com_test_mgr 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Commons4Oracle testing framework manager. The manager provides functionality to register and manage test suites 
--    and test packages. See com_test_runner for more details.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-08-26 Jose Rolland
--    Bug fix in add_test_suite [OCOMM-340]
--  2015-08-19  Chris Roderick
--    Formatting
--  2015-07-31 Lukasz Burdzanowski
--    Creation [OCOMM-322]
------------------------------------------------------------------------------------------------------------------------
is

  procedure add_test_suite (
    i_suite_name        in com_test_suites.suite_name%type,
    i_parent_suite_name in com_test_suites.suite_name%type default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-08-26 Jose Rolland
--    Fix bug getting parent suite id (it was getting the parent of the parent) [OCOMM-340]
--  2015-08-19  Chris Roderick
--    Added exception case when others then raise
--  2015-07-31 Lukasz Burdzanowski
--    Creation [OCOMM-322]
------------------------------------------------------------------------------------------------------------------------
  is
    l_parent_suite_id com_test_suites.parent_suite_id%type;
  begin 
    if i_parent_suite_name is not null then
      select suite_id into l_parent_suite_id from com_test_suites where suite_name = i_parent_suite_name;
    end if;
    
    insert into com_test_suites (suite_id, suite_name, parent_suite_id) 
    values (com_test_suites_seq.nextval, i_suite_name, l_parent_suite_id);
  
  exception
    when no_data_found then
      com_event_mgr.raise_error_event('NO_DATA_FOUND_ERROR', 'Parent suite '||i_parent_suite_name||' does not exist');        
    when dup_val_on_index then
      com_event_mgr.raise_error_event('ASSERTION_ERROR', 'Suite '||i_suite_name||' already exists');
    when others then raise;
  end add_test_suite;
 
  procedure add_test_package (
    i_package_name  in com_test_packages.package_name%type,
    i_suite_name    in com_test_suites.suite_name%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-08-19  Chris Roderick
--    Added exception case when others then raise
--  2015-07-31 Lukasz Burdzanowski
--    Creation [OCOMM-322]
------------------------------------------------------------------------------------------------------------------------
  is
    l_package_id  com_test_packages.package_id%type;
    l_suite_id    com_test_suites.suite_id%type;
  begin  
    select suite_id into l_suite_id from com_test_suites where suite_name = i_suite_name;
  
    l_package_id := com_test_packages_seq.nextval;
    insert into com_test_packages (package_id, package_name) values (l_package_id, upper(i_package_name));
    insert into com_test_suite_packages (suite_id, package_id) values (l_suite_id, l_package_id);
  
  exception
    when no_data_found then
      com_event_mgr.raise_error_event('NO_DATA_FOUND_ERROR', 'Suite '||i_suite_name||' does not exist');        
    when dup_val_on_index then
      com_event_mgr.raise_error_event('ASSERTION_ERROR', 'Test package '||i_package_name||' already exists');
    when others then raise;  
  end add_test_package;  

end com_test_mgr;