create or replace package body com_assert
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2016-05-01 Chris Roderick
--    Added procedure user_table_exists. OCOMM-389
--    Added procedure is_valid_oracle_object_name. OCOMM-388
--  2016-02-18 Jose Rolland
--    Added is_false. OCOMM-358
--  2015-07-16 Chris Roderick
--    Added additional assertions. OCOMM-316
--    Formatting
--  2015-02-11 Lukasz Burdzanowski
--    OCOMM-303 Added arg_length_less_equal
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  procedure not_null (
    i_argument in table_of_varchar,
    i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if i_argument is null then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message); 
    end if;
  end not_null;
  
  procedure not_null (
    i_argument in table_of_number,
    i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if i_argument is null then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message); 
    end if;
  end not_null;
  
  procedure not_null (
    i_argument in table_of_timestamp,
    i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if i_argument is null then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message); 
    end if;
  end not_null;

  procedure is_empty (
      i_collection in table_of_varchar,
      i_message    in varchar2 default '[Assertion failed] - the collection must be empty'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------
  is    
  begin
    not_null(i_collection, 'Cannot check collection is empty, as it is null!');
    if i_collection is not empty then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);
    end if;
  end is_empty;

  procedure not_empty (
      i_collection in table_of_varchar,
      i_message    in varchar2 default '[Assertion failed] - the collection must not be empty'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------
  is    
  begin
    not_null(i_collection, 'Cannot check collection is not empty, as it is null!');
    if i_collection is empty then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);
    end if;
  end not_empty;

  procedure is_empty (
      i_collection in table_of_number,
      i_message    in varchar2 default '[Assertion failed] - the collection must be empty'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------
  is    
  begin
    not_null(i_collection, 'Cannot check collection is empty, as it is null!');
    if i_collection is not empty then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);
    end if;
  end is_empty;

  procedure not_empty (
      i_collection in table_of_number,
      i_message    in varchar2 default '[Assertion failed] - the collection must not be empty'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------
  is    
  begin
    not_null(i_collection, 'Cannot check collection is not empty, as it is null!');
    if i_collection is empty then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);
    end if;
  end not_empty;

  procedure is_empty (
      i_collection in table_of_timestamp,
      i_message    in varchar2 default '[Assertion failed] - the collection must be empty'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------
  is    
  begin
    not_null(i_collection, 'Cannot check collection is empty, as it is null!');
    if i_collection is not empty then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);
    end if;
  end is_empty;

  procedure not_empty (
      i_collection in table_of_timestamp,
      i_message    in varchar2 default '[Assertion failed] - the collection must not be empty'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------
  is    
  begin
    not_null(i_collection, 'Cannot check collection is not empty, as it is null!');
    if i_collection is empty then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);
    end if;
  end not_empty;

  procedure is_true (
    i_expression in boolean,
    i_message    in varchar2 default '[Assertion failed] - this expression must be true'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
begin  
  if not i_expression then
    com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);
  end if;
end is_true;

  procedure is_false (
    i_expression in boolean,
    i_message    in varchar2 default '[Assertion failed] - this expression must be false'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2016-02-18 Jose Rolland
--   Creation OCOMM-358
------------------------------------------------------------------------------------------------------------------------
is
begin  
  if i_expression then
    com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);
  end if;
end is_false;

procedure not_null (
  i_argument in number,
  i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
)
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
begin  
  if i_argument is null then
    com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message); 
  end if;
end not_null;

procedure not_null (
  i_argument in varchar2,
  i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
)
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
begin  
  if i_argument is null then
    com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);  
  end if;
end not_null;

procedure not_null (
  i_argument in timestamp,
  i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
)
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
begin  
  if i_argument is null then
    com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);  
  end if;
end not_null;

procedure arg_not_null (
  i_argument      in number,
  i_argument_name in varchar2
)
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
begin
  not_null(i_argument, 'The ' || i_argument_name || ' is required; it must not be null');
end arg_not_null;

procedure arg_not_null (
  i_argument      in varchar2,
  i_argument_name in varchar2
)
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
begin
  not_null(i_argument, 'The ' || i_argument_name || ' is required; it must not be null');
end arg_not_null;

procedure arg_not_null (
  i_argument      in timestamp,
  i_argument_name in varchar2
)
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
begin
  not_null(i_argument, 'The ' || i_argument_name || ' is required; it must not be null');
end arg_not_null;

 procedure arg_length_less_equal (
    i_argument in varchar2,
    i_length in number,
    i_message in varchar2 default '[Assertion failed] - Length of i_argument exceeds i_length'
  )  
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2015-02-1 Lukasz Burdzanowski
--    OCOMM-303 Creation
------------------------------------------------------------------------------------------------------------------------
  is
    begin
      if (length(i_argument) > i_length) then
        com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);  
    end if;
  end arg_length_less_equal;

  procedure is_valid_oracle_object_name (
    i_argument      in  varchar2,
    i_message       in  varchar2 default '[Assertion failed] - i_argument is not a valid name for an Oracle object',
    i_ignore_nulls  in  boolean default true
  ) 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2016-05-01 Chris Roderick
--    Creation. OCOMM-388
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if not i_ignore_nulls then
      arg_not_null(i_argument, 'i_argument');
    end if;
    
    arg_length_less_equal(i_argument, com_constants.c_max_length_oracle_obj_name, 'length of value of i_argument ('''||
        i_argument||''') exceeds '||com_constants.c_max_length_oracle_obj_name||' chars!');
  exception
    when others then 
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message||com_constants.c_line_break||sqlerrm);  
  end is_valid_oracle_object_name;

  procedure user_table_exists (
    i_argument      in  varchar2,
    i_message       in  varchar2 default '[Assertion failed] - no user table found with name matching i_argument'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2016-05-01 Chris Roderick
--    Creation. OCOMM-389
------------------------------------------------------------------------------------------------------------------------
  is
    l_table_count pls_integer;
  begin
    select count(*) 
    into l_table_count
    from user_tables
    where table_name = upper(i_argument);
    
    if l_table_count = 0 then
      com_event_mgr.raise_error_event ('ASSERTION_ERROR', i_message);
    end if; 
  end user_table_exists;
  
end com_assert;