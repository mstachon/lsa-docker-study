declare
--  l_empty_collection      table_of_number     :=  table_of_number();
--  l_non_empty_collection  table_of_number     :=  table_of_number(1,2,3);
--  l_null_collection       table_of_number;
  
--  l_empty_collection      table_of_timestamp     :=  table_of_timestamp();
--  l_non_empty_collection  table_of_timestamp     :=  table_of_timestamp(systimestamp);
--  l_null_collection       table_of_timestamp;
  
  l_empty_collection      table_of_varchar     :=  table_of_varchar();
  l_non_empty_collection  table_of_varchar     :=  table_of_varchar('A', 'B', 'C');
  l_null_collection       table_of_varchar;
  
begin

--  test not_empty
  begin
    com_assert.not_empty(l_empty_collection);
    dbms_output.put_line('Test failed for not_empty, using an empty collection: '||sqlerrm);
  exception
    when others then 
      if com_event_mgr.event_equals('ASSERTION_ERROR', sqlcode) then
        dbms_output.put_line('Test passed for not_empty, using an empty collection: '||sqlerrm);
      else
        dbms_output.put_line('Test failed for not_empty, using an empty collection: '||sqlerrm);
      end if;
  end;
  
  begin
    com_assert.not_empty(l_non_empty_collection);
    dbms_output.put_line('Test passed for not_empty, using a non empty collection: '||sqlerrm);
  exception
    when others then
      dbms_output.put_line('Test failed for not_empty, using a non empty collection: '||sqlerrm);
  end;
  
  begin
    com_assert.not_empty(l_null_collection);
  exception
    when others then 
      if com_event_mgr.event_equals('ASSERTION_ERROR', sqlcode) then
        dbms_output.put_line('Test passed for not_empty, using a null collection: '||sqlerrm);
      else
        dbms_output.put_line('Test failed for not_empty, using a null collection');
      end if;
  end;

--  test is_empty
  begin
    com_assert.is_empty(l_non_empty_collection);
  exception
    when others then 
      if com_event_mgr.event_equals('ASSERTION_ERROR', sqlcode) then
        dbms_output.put_line('Test passed for is_empty, using a non empty collection: '||sqlerrm);
      else
        dbms_output.put_line('Test failed for is_empty, using a non empty collection: '||sqlerrm);
      end if;
  end;
  
  begin
    com_assert.is_empty(l_empty_collection);
    dbms_output.put_line('Test passed for is_empty, using an empty collection: '||sqlerrm);
  exception
    when others then
      dbms_output.put_line('Test failed for is_empty, using an empty collection: '||sqlerrm);
  end;
  
  begin
    com_assert.is_empty(l_null_collection);
  exception
    when others then 
      if com_event_mgr.event_equals('ASSERTION_ERROR', sqlcode) then
        dbms_output.put_line('Test passed for is_empty, using a null collection: '||sqlerrm);
      else
        dbms_output.put_line('Test failed for is_empty, using a null collection');
      end if;
  end;
  
end;
/