create or replace package com_assert authid current_user
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  description:
--    Assertion utility to help in validating arguments (similar to cern.accsoft.commons.util.Assert)
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2016-05-01 Chris Roderick
--    Added procedure user_table_exists. OCOMM-389
--    Added procedure is_valid_oracle_object_name. OCOMM-388
--  2016-02-18 Jose Rolland
--    Added is_false. OCOMM-358
--  2015-07-16 Chris Roderick
--    Added additional assertions. OCOMM-316
--    Formatting
--  2015-02-11 Lukasz Burdzanowski
--    OCOMM-303 Added arg_length_less_equal
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  procedure is_empty (
      i_collection in table_of_varchar,
      i_message    in varchar2 default '[Assertion failed] - the collection must be empty'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert the given collection is empty, otherwise throws an 'ASSERTION_ERROR' exception
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_collection - the collection to check
--    i_message - The exception message to use if the assertion fails
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the collection is not empty
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------

  procedure not_empty (
      i_collection in table_of_varchar,
      i_message    in varchar2 default '[Assertion failed] - the collection must not be empty'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert the given collection is not empty, otherwise throws an 'ASSERTION_ERROR' exception
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_collection - the collection to check
--    i_message - The exception message to use if the assertion fails
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the collection is empty
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------

  procedure is_empty (
      i_collection in table_of_number,
      i_message    in varchar2 default '[Assertion failed] - the collection must be empty'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert the given collection is empty, otherwise throws an 'ASSERTION_ERROR' exception
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_collection - the collection to check
--    i_message - The exception message to use if the assertion fails
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the collection is not empty
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------

  procedure not_empty (
      i_collection in table_of_number,
      i_message    in varchar2 default '[Assertion failed] - the collection must not be empty'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert the given collection is not empty, otherwise throws an 'ASSERTION_ERROR' exception
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_collection - the collection to check
--    i_message - The exception message to use if the assertion fails
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the collection is empty
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------

  procedure is_empty (
      i_collection in table_of_timestamp,
      i_message    in varchar2 default '[Assertion failed] - the collection must be empty'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert the given collection is empty, otherwise throws an 'ASSERTION_ERROR' exception
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_collection - the collection to check
--    i_message - The exception message to use if the assertion fails
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the collection is not empty
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------

  procedure not_empty (
      i_collection in table_of_timestamp,
      i_message    in varchar2 default '[Assertion failed] - the collection must not be empty'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert the given collection is not empty, otherwise throws an 'ASSERTION_ERROR' exception
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_collection - the collection to check
--    i_message - The exception message to use if the assertion fails
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the collection is empty
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------

  procedure is_true (
    i_expression in boolean,
    i_message    in varchar2 default '[Assertion failed] - this expression must be true'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert a boolean expression is true, otherwise throws an 'ASSERTION_ERROR' exception
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_expression
--      A boolean expression.
--    i_message
--      The exception message to use if the assertion fails.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the result is false  
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure is_false (
    i_expression in boolean,
    i_message    in varchar2 default '[Assertion failed] - this expression must be false'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert a boolean expression is false, otherwise throws an 'ASSERTION_ERROR' exception
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_expression
--      A boolean expression.
--    i_message
--      The exception message to use if the assertion fails.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the result is false  
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2016-02-18 Jose Rolland
--   Creation OCOMM-358
------------------------------------------------------------------------------------------------------------------------

  procedure not_null (
    i_argument in table_of_varchar,
    i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert that the argument is not null
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument - the argument to check
--    i_message - The exception message to use if the assertion fails
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the collection is null
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------

  procedure not_null (
    i_argument in table_of_number,
    i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert that the argument is not null
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument - the argument to check
--    i_message - The exception message to use if the assertion fails
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the collection is null
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------

  procedure not_null (
    i_argument in table_of_timestamp,
    i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert that the argument is not null
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument - the argument to check
--    i_message - The exception message to use if the assertion fails
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the collection is null
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Creation - OCOMM-316
------------------------------------------------------------------------------------------------------------------------

  procedure not_null (
    i_argument in number,
    i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert that the argument is not null.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument
--      The argument to check
--    i_message
--      The exception message to use if the assertion fails.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the argument is null 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure not_null (
    i_argument in varchar2,
    i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert that the argument is not null.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument
--      The argument to check
--    i_message
--      The exception message to use if the assertion fails.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the argument is null 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure not_null (
    i_argument in timestamp,
    i_message  in varchar2 default '[Assertion failed] - this argument is required; it must not null'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Assert that the argument is not null.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument
--      The argument to check
--    i_message
--      The exception message to use if the assertion fails.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the argument is null 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure arg_not_null (
    i_argument      in number,
    i_argument_name in varchar2
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Calls not_null passing the following error message:
--    'The ' || i_argument_name || ' is required; it must not be null'
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument
--      The argument to check.
--    i_argument_name
--      Name of the argument to be used in the message string.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the argument is null 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure arg_not_null (
    i_argument      in varchar2,
    i_argument_name in varchar2
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Calls not_null passing the following error message:
--    'The ' || i_argument_name || ' is required; it must not be null'
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument
--      The argument to check.
--    i_argument_name
--      Name of the argument to be used in the message string.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the argument is null 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure arg_not_null (
    i_argument      in timestamp,
    i_argument_name in varchar2
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Calls not_null passing the following error message:
--    'The ' || i_argument_name || ' is required; it must not be null'
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument
--      The argument to check.
--    i_argument_name
--      Name of the argument to be used in the message string.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the argument is null 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2013-07-18 Gkergki Strakosa
--    Added comments
--  2013-06-24 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure arg_length_less_equal (
    i_argument in varchar2,
    i_length in number,
    i_message in varchar2 default '[Assertion failed] - Length of i_argument exceeds i_length'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks if passed argument's lenght is less or equal to provided limit    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument
--      The argument to check.
--    i_lenght
--      Lenght limit for the passed argument
--    i_message
--      The exception message to use if the assertion fails.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the argument's length exceeds provided limit 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2015-02-11 Lukasz Burdzanowski
--    OCOMM-303 Creation
------------------------------------------------------------------------------------------------------------------------

  procedure is_valid_oracle_object_name (
    i_argument      in  varchar2,
    i_message       in  varchar2 default '[Assertion failed] - i_argument is not a valid name for an Oracle object',
    i_ignore_nulls  in  boolean default true
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks if passed argument is a valid name for an Oracle object    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument - the argument to check.
--    i_message - the exception message to use if the assertion fails.
--    i_ignore_nulls - a flag to indicate whether to ignore the case when i_argument is null or not. 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if the value of i_argument is not a valid name for an Oracle object 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2016-05-01 Chris Roderick
--    Creation. OCOMM-388
------------------------------------------------------------------------------------------------------------------------

  procedure user_table_exists (
    i_argument      in  varchar2,
    i_message       in  varchar2 default '[Assertion failed] - no user table found with name matching i_argument'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks if passed argument corresponds to the name of user table. Case insensitive.    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_argument - the argument to check.
--    i_message - the exception message to use if the assertion fails. 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if no user table found with a name matching the value of i_argument. 
------------------------------------------------------------------------------------------------------------------------
--  Change log (descending):
--  2016-05-01 Chris Roderick
--    Creation. OCOMM-389
------------------------------------------------------------------------------------------------------------------------

end com_assert;