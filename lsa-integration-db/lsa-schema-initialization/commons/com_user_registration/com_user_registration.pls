create or replace package com_user_registration
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers users of the commons 4 oracle framework and the currently installed version
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-14 Jose Rolland
--    Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-431
--  2014-06-24 Chris Roderick and James Menzies
--    Creation, as per OCOMM-266
------------------------------------------------------------------------------------------------------------------------
is

  procedure register_user (i_username in com_users.username%type);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers the given user's commons 4 oracle installation version details centrally in the commons account
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_username - The database username on the server where commons has been installed
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-06-24 Chris Roderick and James Menzies
--    Creation
------------------------------------------------------------------------------------------------------------------------

end com_user_registration;