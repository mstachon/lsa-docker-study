create or replace package body com_user_registration
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers users of the commons 4 oracle framework and the currently installed version
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-06-24 Chris Roderick and James Menzies
--    Creation, as per OCOMM-266
------------------------------------------------------------------------------------------------------------------------

  procedure register_user (i_username in com_users.username%type)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-06-24 Chris Roderick and James Menzies
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is    
  begin
    execute immediate '
      merge into com_users dest using (
        select :i_username username, to_timestamp(install_time_utc_value, ''YYYY-MM-DD HH24:MI:SS'') first_install_time_utc, to_timestamp(update_time_utc_value, ''YYYY-MM-DD HH24:MI:SS'') last_update_time_utc, version_value installed_com_version
        from (
          select parameter_name, parameter_value
          from '||i_username||'.com_parameters
        )
        pivot (
          max(parameter_value) as value 
          for (parameter_name) in (''INSTALL_TIME_UTC'' as install_time_utc, ''UPDATE_TIME_UTC'' as update_time_utc, ''VERSION'' as version)
        )
      ) src 
      on (dest.username = src.username)
      when matched then
        update set dest.last_update_time_utc = src.last_update_time_utc, dest.installed_com_version = src.installed_com_version  
      when not matched then
        insert (username, first_install_time_utc, last_update_time_utc, installed_com_version)
        values (src.username, src.first_install_time_utc, src.last_update_time_utc, src.installed_com_version)' using i_username;  
  end register_user;

end com_user_registration;