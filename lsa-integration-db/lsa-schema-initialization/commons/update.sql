------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Applies necessary updates to commons installation.
--    Change comments should only refer to changes to this script.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-02-12  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
prompt updating com_updater...

@com_updater/com_updater.pls
@com_updater/com_updater.plb

col update_script_name new_val script
select com_updater.get_update_script update_script_name from dual;
prompt need to execute &script
@@ &script
