create or replace package body com_logger
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to log anything (not just errors), which implies the need to 
--    categorize log entries (like log4j levels).
--    It supports dynamic setting of logging level at runtime. The default log level is ERROR,
--    which can be changed like this:
--    1. Set the log level for all sessions without a client identifier set
--       For example: com_logger.set_log_level(com_logger.c_debug);
--    2. Set the log level for all sessions with a specific client identifier
--       For example: com_logger.set_log_level(com_logger.c_debug, 'my_app_user');
--    3. Set the log level for all sessions without a client identifier set for a specific package
--       For example: com_logger.set_package_log_level('MY_PACKAGE', com_logger.c_debug);
--    4. Set the log level for all sessions with a specific client identifier for a specific package
--       For example: com_logger.set_package_log_level('MY_PACKAGE', com_logger.c_debug, 'my_app_user');
--    
--    The logging level can be set back to the default, with one of the following procedures:
--      com_logger.clear_log_level
--      com_logger.clear_package_log_level
--
--    In order to log a message from your application you can call one of the following procedures:
--      com_logger.debug
--      com_logger.info
--      com_logger.warn
--      com_logger.error
--      com_logger.fatal
--      com_logger.notify
--    passing it the package name for example: com_logger.debug('my message for debug', $$PLSQL_UNIT);
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-11-24 Jose Rolland
--   Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-408
--  2015-07-16 Chris Roderick
--    Bug fix for OCOMM-320
--  2014-01-17 Gkergki Strakosa
--    Moved context handling to com_ctx_mgr
--  2013-10-11 Gkergki Strakosa
--    Added modules to support dynamic setting of logging level at runtime
--  2011-03-24 Pascal Le Roux
--    Removed the clear_log overloading
--  2010-11-11 Chris Roderick
--    Modified signature of clear_log procedure
--  2010-10-06  Pascal Le Roux, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  c_log_level_param constant varchar2(30) := 'LOG_LEVEL';  
  c_log_ctx         constant varchar2(30) := 'LOG_CTX';
  
    function get_package_log_level(
    i_package_name  in  user_objects.object_name%type
  )
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return sys_context(c_log_ctx, i_package_name); 
  end get_package_log_level;
  
  function convert_level_char_to_num(
    i_level in t_log_category
  )
  return number
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Converts the provided log level to its corresponding number so that it can be used for
--    comparison.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_level
--      The log level
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_level         pls_integer;
  begin
    case i_level
      when c_debug  then l_level := c_debug_num;
      when c_info   then l_level := c_info_num;
      when c_warn   then l_level := c_warn_num;
      when c_error  then l_level := c_error_num;
      when c_fatal  then l_level := c_fatal_num;
      when c_notify then l_level := c_notify_num;      
    else l_level := null;
    end case;

    return l_level;
  end convert_level_char_to_num;
  
  function check_and_set_log_level(
    i_package_name in user_objects.object_name%type default null
  )
  return pls_integer
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the corresponding number for the current log level. 
--    If it's null then the default log level is set and returned.
--    If a package name is provided, returns its corresponding log level. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name
--      The package name for which the log level will be returned
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_log_level_num           pls_integer;
  begin
    if i_package_name is null then
      l_log_level_num := convert_level_char_to_num(get_log_level);
      if(l_log_level_num is null) then
        l_log_level_num := c_default_level_num;
        set_log_level(c_default_level,
                      sys_context('userenv', 'client_identifier')
                     );
      end if;
    else
      l_log_level_num := convert_level_char_to_num(get_package_log_level(i_package_name));
      if(l_log_level_num is null) then
        l_log_level_num := c_default_level_num;
        set_package_log_level(i_package_name, 
                              c_default_level,
                              sys_context('userenv', 'client_identifier')
                             );
      end if;
    end if;
    
    return l_log_level_num;
  end check_and_set_log_level;
  
  function should_log(
    i_level        in pls_integer,
    i_package_name in user_objects.object_name%type 
  )
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns true if the provided log level is less than or equal than the current log level,
--    false otherwise.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name
--      The name of the package which wants to log this message
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_log_level_num           pls_integer;    
    l_pkg_level_num           pls_integer;
  begin
--  check for general/client_id specific log level         
    l_log_level_num := check_and_set_log_level;
    
--  check for package log level
    l_pkg_level_num := check_and_set_log_level(i_package_name);    
    
    if((i_level <= l_log_level_num) or (i_level <= l_pkg_level_num)) then
      return true;
    end if; 
    
    return false;
  end should_log;  
  
  procedure set_context_info(
    i_module_name in  com_logs.log_module%type
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Description:
--  Set the module name of the caller to the LOG_MODULE/LOG_ACTION column of the com_logs table
--  if they are not set.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_module_name
--      The name of the module that should be logged.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin

    dbms_application_info.set_module(coalesce(sys_context('userenv', 'module'), i_module_name),
                                     coalesce(sys_context('userenv', 'action'), i_module_name)
                                     );
  end set_context_info;
  
  procedure log_entry_unautonomous (
    i_log_category  in  t_log_category,
    i_log_entry     in  com_logs.log_entry%type
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Log any categorized string in com_logs table in a non autonomous transaction
------------------------------------------------------------------------------------------------------------------------
--    i_log_category
--      The category qualifying the log entries ('DEBUG', 'INFO','ERROR'...). 
--    i_log_entry
--      The value to be logged.
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--   none
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    <error_code> - <exception description>
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-10-06 Chris Roderick, Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  begin
			insert into com_logs (log_id ,db_user ,utc_timestamp ,log_category ,log_entry)
				values (com_logs_log_id_seq.nextval ,user, sys_extract_utc(systimestamp) ,i_log_category ,i_log_entry); 
  end log_entry_unautonomous;
  
  procedure log_entry_autonomous (
    i_log_category  in  t_log_category,
    i_log_entry     in  com_logs.log_entry%type
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Set the autonomous transaction mode and call the log_entry_unautonomous procedure
--    which is then autonomous
------------------------------------------------------------------------------------------------------------------------
--    i_log_category
--      The category qualifying the log entries ('DEBUG', 'INFO','ERROR'...). 
--    i_log_entry
--      The value to be logged.
------------------------------------------------------------------------------------------------------------------------
--  Output Arguments:
--   none
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    <error_code> - <exception description>
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2011-04-12  Chris Roderick
--    Replaced rollback on error with raise
--  2010-10-06 Chris Roderick, Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------

    -- The pragma instructs the PL/SQL compiler to establish a PL/SQL block as autonomous 
    -- or independent.   
    pragma autonomous_transaction;

  begin
   -- call to log_entry_unautonomous procedure which does the insert into com_logs table
   log_entry_unautonomous (i_log_category,i_log_entry);
  commit;
  exception
		when others then
      raise;  
  end log_entry_autonomous;

  procedure log_entry (
    i_log_category  in  t_log_category,
    i_log_entry     in  com_logs.log_entry%type,
    i_is_auton      in  boolean                    default false
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-16 Chris Roderick
--    Added call to should_log in order to fix OCOMM-320
--  2010-10-06 Chris Roderick, Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
   com_assert.arg_not_null(i_log_category, 'i_log_category');
   com_assert.arg_not_null(i_log_entry, 'i_log_entry');   
   
   if should_log(convert_level_char_to_num(i_log_category), null) then 
   
     if i_is_auton then 
      -- If the parameter i_is_auton is true then call the log_entry_autonomous procedure to set the 
      -- transaction independent
      log_entry_autonomous (i_log_category,i_log_entry);
     else
      -- If the parameter i_is_auton is false then call the log_entry_unautonomous procedure to insert
      -- into com_logs table    
      log_entry_unautonomous (i_log_category,i_log_entry);
     end if;
     
    end if;
    
  end log_entry;  
  
  procedure set_log_level(
    i_log_level     in t_log_category,
    i_client_id     in varchar2 default null
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin
    com_assert.arg_not_null(i_log_level, 'i_log_level');
    com_assert.is_true(i_log_level in (c_debug, c_info, c_warn, c_error, c_fatal, c_notify),
                       'Log level should be one of : com_logger.c_debug, com_logger.c_info, 
                        com_logger.c_warn, com_logger.c_error, com_logger.c_fatal, com_logger.c_notify');
    
--  attach to the context     
    dbms_session.set_identifier(i_client_id);
    commons.com_ctx_mgr.set_context(c_log_ctx, c_log_level_param, i_log_level, null, i_client_id);
--  de-attach from the context    
    dbms_session.clear_identifier;
  end set_log_level;
  
  function get_log_level
  return t_log_category
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_log_level t_log_category;
  begin
    return sys_context(c_log_ctx, c_log_level_param);
  end get_log_level;
  
  procedure clear_log_level(
    i_client_id     in varchar2 default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin    
    commons.com_ctx_mgr.clear_context(c_log_ctx, i_client_id, c_log_level_param);
  end clear_log_level;
  
  procedure set_package_log_level(
    i_package_name in user_objects.object_name%type,
    i_log_level    in t_log_category,
    i_client_id    in varchar2 default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.arg_not_null(i_package_name, 'i_package_name');
    com_assert.arg_not_null(i_log_level, 'i_log_level');
    com_assert.is_true(i_log_level in (c_debug, c_info, c_warn, c_error, c_fatal, c_notify),
                       'Log level should be one of : com_logger.c_debug, com_logger.c_info, 
                        com_logger.c_warn, com_logger.c_error, com_logger.c_fatal, com_logger.c_notify');
    
--  attach to the context     
    dbms_session.set_identifier(i_client_id);
    commons.com_ctx_mgr.set_context(c_log_ctx, i_package_name, i_log_level, null, i_client_id);
--  de-attach from the context    
    dbms_session.clear_identifier;
    
  end set_package_log_level;
  
  procedure clear_package_log_level(
    i_package_name in  user_objects.object_name%type,
    i_client_id    in  varchar2 default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.arg_not_null(i_package_name, 'i_package_name');
    
    commons.com_ctx_mgr.clear_context(c_log_ctx, i_client_id, i_package_name);
  end clear_package_log_level;
  
  procedure log_internal(
    i_level         in t_log_category,
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Description:
--  Logs the provided log entry if the provided log level is less than or equal to the current log level
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_level
--      The log level.
--    i_log_entry
--      The message that should be logged.
--    i_package_name
--      The name of the package which wants to log this message.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin
    if(should_log(convert_level_char_to_num(i_level), i_package_name)) then    
      set_context_info(i_package_name);
      log_entry_autonomous(i_level, i_log_entry);    
    end if;  
  end log_internal;
  
  procedure debug(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin    
    log_internal(c_debug, i_log_entry, i_package_name);
  end debug;
  
  procedure info(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin    
    log_internal(c_info, i_log_entry, i_package_name);
  end info;
  
  procedure warn(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin    
    log_internal(c_warn, i_log_entry, i_package_name);
  end warn;
  
  procedure error(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin    
    log_internal(c_error, i_log_entry, i_package_name);
  end error;
  
  procedure fatal(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin    
    log_internal(c_fatal, i_log_entry, i_package_name);
  end fatal;
  
  procedure notify(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin    
    log_internal(c_notify, i_log_entry, i_package_name);
  end notify;
  
  procedure clear_log (
    i_log_category    in  t_log_category              default null,
    i_from_utc_time   in  com_logs.utc_timestamp%type default null,
    i_to_utc_time     in  com_logs.utc_timestamp%type default null
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-03-09 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
      delete from com_logs 
      where utc_timestamp >= coalesce(i_from_utc_time, timestamp '0000-01-01 00:00:00.000000') 
        and utc_timestamp <= coalesce(i_to_utc_time, sys_extract_utc(systimestamp)) 
        and (log_category = i_log_category or i_log_category is null);	
  end clear_log;

end com_logger;