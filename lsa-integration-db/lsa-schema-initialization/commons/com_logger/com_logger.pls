create or replace package com_logger authid definer
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This package is intended to log anything (not just errors), which implies the need to 
--    categorize log entries (like log4j levels).
--    It supports dynamic setting of logging level at runtime. The default log level is ERROR,
--    which can be changed like this:
--    1. Set the log level for all sessions without a client identifier set
--       For example: com_logger.set_log_level(com_logger.c_debug);
--    2. Set the log level for all sessions with a specific client identifier
--       For example: com_logger.set_log_level(com_logger.c_debug, 'my_app_user');
--    3. Set the log level for a specific package for all sessions without a client identifier set 
--       For example: com_logger.set_package_log_level('MY_PACKAGE', com_logger.c_debug);
--    4. Set the log level for a specific package for all sessions with a specific client identifier 
--       For example: com_logger.set_package_log_level('MY_PACKAGE', com_logger.c_debug, 'my_app_user');
--    
--    The logging level can be set back to the default, with one of the following procedures:
--      com_logger.clear_log_level
--      com_logger.clear_package_log_level
--
--    In order to log a message from your application, you can call one of the following procedures:
--      com_logger.debug
--      com_logger.info
--      com_logger.warn
--      com_logger.error
--      com_logger.fatal
--      com_logger.notify
--    passing it the package name, for example: com_logger.debug('my message for debug', $$PLSQL_UNIT);
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-11-24 Jose Rolland
--   Applied changes identified in code review OCOMM-REVIEWS-10 - OCOMM-408
--  2013-10-11 Gkergki Strakosa
--    Added modules to support dynamic setting of logging level at runtime
--  2011-03-24 Pascal Le Roux
--    Removed the clear_log overloading
--  2010-11-11 Chris Roderick
--    Modified signature of clear_log procedure
--  2010-10-06  Pascal Le Roux, Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
--    Categories for log entries.
  subtype t_log_category is com_logs.log_category%type;
  
  c_debug         constant t_log_category :=  'DEBUG';
  c_info          constant t_log_category :=  'INFO';
  c_warn          constant t_log_category :=  'WARN';
  c_error         constant t_log_category :=  'ERROR';
  c_fatal         constant t_log_category :=  'FATAL';
  
  c_debug_num     constant pls_integer :=  6;
  c_info_num      constant pls_integer :=  5;
  c_warn_num      constant pls_integer :=  4;
  c_error_num     constant pls_integer :=  3;
  c_fatal_num     constant pls_integer :=  2;

-- @DEPRECATED
  c_notify        constant t_log_category :=  'NOTIFY';
  c_notify_num    constant pls_integer :=  1;
  
  c_default_level     constant t_log_category := c_error;
  c_default_level_num constant pls_integer    := c_error_num;

  procedure log_entry (
    i_log_category  in  t_log_category,
    i_log_entry     in  com_logs.log_entry%type,
    i_is_auton      in  boolean                    default false
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Log any categorized string in com_logs table. 
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log_category - The category of the log entry ('DEBUG', 'INFO','ERROR'...).
--    i_log_entry    - The value to be logged.
--    i_is_auton     - Boolean flag to set the autonomous transaction mode: 
--        'True' meaning that the i_log_entry entry is inserted in the com_logs table and committed 
--        independently of the calling transaction.  
--        'False' meaning that the log entry is dependent on the calling transaction.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - if i_log_category or i_log_entry parameters are null
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-10-06 Chris Roderick, Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure clear_log (
    i_log_category    in  t_log_category              default null,
    i_from_utc_time   in  com_logs.utc_timestamp%type default null,
    i_to_utc_time     in  com_logs.utc_timestamp%type default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Clear logs for the current user, either completely or prior to the given reference time, with 
--    optional filter on category to be cleared.
--    It uses the  application context com_logger_ctx for storing session information
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log_category  - The category of the log entries to be cleared ('DEBUG', 'INFO','ERROR'...). If not 
--                      provided, logs of all categories will be cleared.
--    i_from_utc_time - The UTC time after which notification logs should be cleared. 
--    i_to_utc_time   - The upper UTC time prior to which notification logs should be cleared. 
--                      If not provided, all logs up to now will be cleared.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2010-03-09 Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure set_package_log_level(
    i_package_name  in user_objects.object_name%type,
    i_log_level     in t_log_category,
    i_client_id     in varchar2 default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets the log level for the provided package name and client identifier.
--    If the client identifier is not set, only messages from sessions without a 
--    client identifier will be recorded.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name - The name of the package for which the log level will be set.
--    i_log_level    - The log level to be set.
--    i_client_id    - The client identifier that is associated with a specific session.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - In case an invalid log level is passed
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  function get_package_log_level(
    i_package_name  in user_objects.object_name%type
  )
  return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the log level for the provided package name and for the client identifier 
--    for which it was set.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name - The name of the package for which the log level will be set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure clear_package_log_level(
    i_package_name  in  user_objects.object_name%type,
    i_client_id     in  varchar2 default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets the default log level for the provided package name and client identifier.
--    If the client identifier is not provided, the log level will be set for sessions without a 
--    client identifier.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_package_name - The name of the package for which the log level will be set to default.
--    i_client_id    - The client identifier that is associated with a specific session.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure set_log_level(
    i_log_level     in t_log_category,
    i_client_id     in varchar2 default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets the log level for the provided client identifier. If the client identifier is not set, only messages from 
--    sessions without a client identifier will be recorded.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log_level - The log level to be set.
--    i_client_id - The client identifier that is associated with a specific session.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    ASSERTION_ERROR - In case an invalid log level is passed
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  function get_log_level
  return t_log_category;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the log level for the client identifier for which it was set.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure clear_log_level(
    i_client_id     in varchar2 default null
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Sets the default log level for the provided client identifier. If the client identifier is not provided, the log 
--    level will be set for sessions without a client identifier.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_client_id - The client identifier that is associated with a specific session.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure debug(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Logs entries when the DEBUG log level is set.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log_entry    - The entry to be logged.
--    i_package_name - The name of the package. Use $$PLSQL_UNIT for this.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure info(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Logs entries when the INFO log level is set.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log_entry    - The entry to be logged.
--    i_package_name - The name of the package. Use $$PLSQL_UNIT for this.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure warn(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Logs entries when the WARN log level is set.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log_entry    - The entry to be logged.
--    i_package_name - The name of the package. Use $$PLSQL_UNIT for this.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure error(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Logs entries when the ERROR log level is set.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log_entry    - The entry to be logged.
--    i_package_name - The name of the package. Use $$PLSQL_UNIT for this.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure fatal(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Logs entries when the FATAL log level is set.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log_entry    - The entry to be logged.
--    i_package_name - The name of the package. Use $$PLSQL_UNIT for this.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  procedure notify(
    i_log_entry     in com_logs.log_entry%type,
    i_package_name  in user_objects.object_name%type 
  );
------------------------------------------------------------------------------------------------------------------------
-- @DEPRECATED
--  Description:
--    Logs entries when the NOTIFY log level is set.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_log_entry    - The entry to be logged.
--    i_package_name - The name of the package. Use $$PLSQL_UNIT for this.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-10-11 Gkergki Strakosa
--    Creation
------------------------------------------------------------------------------------------------------------------------

end com_logger;