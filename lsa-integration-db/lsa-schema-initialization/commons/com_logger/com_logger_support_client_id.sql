alter table com_logs add (log_client_id varchar2(30) default sys_context('userenv', 'client_identifier'));

create or replace context log_ctx using com_logger accessed globally;