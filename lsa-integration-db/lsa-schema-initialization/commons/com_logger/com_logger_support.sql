--  sequence required to generate log_ids
create sequence com_logs_log_id_seq;

--  Create a range partitioned table to store log entries, to be manged by the partition manager in 
--  order to add new partitions in advance of when they will be required, and drop older empty 
--  partitions.  An initial partition must be created, and in this case it assumes the range 
--  partition size is 1 month, and that initial partition will span the current month.
declare
--  Establish the timestamp of the start of this month
  l_start_of_this_month timestamp :=  trunc(systimestamp, 'MM');
--  Establish the timestamp of the start of the next month
  l_start_of_next_month timestamp :=  l_start_of_this_month + interval '1' month;
begin

--  Dynamically create the table with the correct partitioning structure.
  execute immediate q'#create table com_logs (
    log_id                    number(10,0)    constraint com_logs_pk primary key,
    db_user                   varchar2(30),
    utc_timestamp             timestamp,
    log_category              varchar2(10),
    log_entry                 varchar2(4000),
    log_os_user               varchar2(30) default sys_context('userenv', 'os_user'),
    log_client_info           varchar2(64) default sys_context('userenv', 'client_info'),
    log_module                varchar2(48) default sys_context('userenv', 'module'),
    log_action                varchar2(32) default sys_context('userenv', 'action'),
    log_host                  varchar2(30) default sys_context('userenv', 'host'),
    log_ip                    varchar2(15) default sys_context('userenv', 'ip_address'),
    constraint com_logs_cat_chk check (log_category in ('DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL', 'NOTIFY'))
  ) partition by range (utc_timestamp) (
    partition part_comlogs_#'||to_char(l_start_of_this_month, 'YYYYMM')||q'# values less than (timestamp '#'||to_char(l_start_of_next_month, 'YYYY-MM-DD HH24:MI:SS')||q'#')
  )#';

end;
/

--  index the log time stamps to allow rapid range searching
create index com_logs_utc_timestamp_i on com_logs(utc_timestamp);

--  register for usage with the partition manager, and perform the initial partition management
begin
  com_partition_mgr.reg_time_range_part_table(
      i_table_name          => 'COM_LOGS'
    , i_part_prefix         => 'COM_LOGS'
    , i_no_parts_advance    => 5
    , i_no_parts_keep       => 7
    , i_part_range_int_cnt  => 1
    , i_part_range_int_type => 'DAY'
  );
  com_partition_mgr.manage_partitions('COM_LOGS');
end;
/