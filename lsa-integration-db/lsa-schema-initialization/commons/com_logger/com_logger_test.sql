select *
from com_logs
order by log_id desc ;
/

-- TESTING log_entry in an unautonomous transaction
begin
commons.com_logger.log_entry(i_log_category => 'INFO'
                           , i_log_entry    => 'test log_entry '||sysdate||' from '||sys_context('userenv', 'session_user')
                           , i_is_auton     => false
                           );
end;
/

-- TESTING log_entry in an autonomous transaction
begin
commons.com_logger.log_entry(i_log_category => 'INFO'
                           , i_log_entry    => 'test log_entry '||sysdate||' from '||sys_context('userenv', 'session_user')
                           , i_is_auton     => true
                           );
end;
/

--TESTING clear_log with time windows.
begin
commons.com_logger.clear_log(i_log_category   => 'INFO'
                           , i_from_utc_time  => '2011-03-11 11:08:26.832565000'
                           , i_to_utc_time    => '2011-03-11 11:10:00.873512000');
end;
/

--TESTING clear_log with time windows with an upper boundary prior to the lower boundary
-- Shouldn't clear anything
begin
commons.com_logger.clear_log(i_log_category   => 'INFO'
                           , i_from_utc_time  => '2011-03-11 11:14:07.305263000'
                           , i_to_utc_time    => '2011-03-11 11:14:04.846207000');
end;
/
--TESTING clear_log with time windows, without defining the upper time boundary
-- should clear any INFO entries from i_from_utc_time to now.
begin
commons.com_logger.clear_log(i_log_category   => 'INFO'
                           , i_from_utc_time  => '2011-03-11 11:08:24.486723000'
                           , i_to_utc_time    => null);
end;
/

--TESTING clear_log with time windows, without defining the lower time boundary
-- should clear any INFO entries up to i_to_utc_time
begin
commons.com_logger.clear_log(i_log_category   => 'INFO'
                           , i_from_utc_time  => null
                           , i_to_utc_time    => '2011-03-11 11:14:06.335207000');
end;
/

--TESTING clear_log with no time windows, clearing all 'INFO' category entries
begin
commons.com_logger.clear_log(i_log_category   => 'INFO'
                           , i_from_utc_time  => null
                           , i_to_utc_time    => null);

end;
/

--TESTING clear_log with no time windows and no category entries
--Should clear all entries
begin
commons.com_logger.clear_log(i_log_category   => null
                           , i_from_utc_time  => null
                           , i_to_utc_time    => null);

end;
/

select *
from com_logs
order by log_id desc ;
/