create or replace package com_updater
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Updating commons typically involves running some scripts. The script to be run differs 
--    according to the version of commons currently installed. This package provides the means to 
--    establish the script to be run, and register that the changes have been applied.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-14 Jose Rolland
--    Applied formatting changes identified in code review OCOMM-REVIEWS-10 - OCOMM-430
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  function get_update_script return varchar2;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Gets the name of script containing necessary updates to commons installation, 
--    based on the current version installed.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions    
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure first_install (i_com_version in com_parameters.parameter_value%type);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers that the commons installation has been made to the given version, which 
--    populates the com_parameters attributes such as version, installer, install and update times.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_version - the commons version which has been installed
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure register_applied_changes (i_com_version in com_parameters.parameter_value%type);
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers that the commons installation has been updated to the given version, which 
--    increments com_parameters attributes such as version and update time.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_version - the commons version which has been installed
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_installed_version
  return com_parameters.parameter_value%type;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the currently installed version of commons.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function get_latest_version
  return com_parameters.parameter_value%type;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the latest version of commons.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

end com_updater;