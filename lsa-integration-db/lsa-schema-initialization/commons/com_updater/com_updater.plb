create or replace package body com_updater
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2017 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Updating commons typically involves running some scripts, the script to be run differs 
--    according to the version of commons currently installed. This package provides the means to 
--    establish the script to be run, and register that the changes have been applied.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2017-07-06 Chris
--    Version 1.17.0 - OCOMM-466
--  2017-05-18 Lukasz and Chris
--    Version 1.16.0 - OCOMM-462
--  2016-05-01 Chris
--    Version 1.15.0 - OCOMM-379
--  2016-01-10 Chris
--    Version 1.14.0 - OCOMM-369
--  2015-10-23 Chris
--    Version 1.13.1 - OCOMM-346
--  2015-10-02 Chris Roderick
--    Version 1.13.0 - OCOMM-353
--  2015-08-31 Chris Roderick
--    Version 1.12.2 - OCOMM-340
--  2015-08-22 Chris Roderick
--    Version 1.12.1 - OCOMM-338
--  2015-08-20 Chris Roderick
--    Version 1.12.0 - OCOMM-333
--  2015-07-16 Chris Roderick
--    Version 1.11.0 - OCOMM-321
--  2015-05-04 Luaksz Burdzanowski
--    Added version 1.10.0 - OCOMM-312
--  2014-12-18
--    Chris Roderick 
--    Added version 1.9.0 - OCOMM-309
--  2014-12-18
--    Chris Roderick 
--    Added version 1.8.0 - OCOMM-302
--  2014-07-23  Chris Roderick
--    Added version 1.7.0
--  2014-06-30 Chris Roderick
--    Added version 1.6.1
--  2014-06-24 Chris Roderick and James Menzies
--    Update to register_applied_changes, version 1.6.0
--  2014-06-23  Chris Roderick and Eve Fortescue-Beck
--    Added version 1.5.0 to support periodic code analysis - OCOMM-255
--  2014-05-16 Pascal Le Roux
--    Added version 1.4.0
--  2014-04-25  Chris Roderick and Pascal Le Roux
--    Added version 1.3.0
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  c_com_param_version   constant  com_parameters.parameter_name%type  :=  'VERSION';
  c_latest_com_version  constant  com_parameters.parameter_value%type :=  '1.17.0';

  function get_latest_version
  return com_parameters.parameter_value%type
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the latest version of commons.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return c_latest_com_version;
  end get_latest_version;

  procedure update_com_parameter(
    i_com_param_name  in  com_parameters.parameter_name%type,
    i_com_param_value in  com_parameters.parameter_value%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    merge the com_parameters table with some new contents. A merge is used in case parameters are 
--    actually being added for the first time.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_com_param_name - the name of the parameter to be merged
--    i_com_param_value - the corresponding value for the parameter
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    merge into com_parameters od using (
      select i_com_param_name parameter_name, i_com_param_value parameter_value
      from dual
    ) nd
    on (od.parameter_name = nd.parameter_name)
    when matched then update set od.parameter_value = nd.parameter_value
    when not matched then insert (parameter_name, parameter_value)
    values (nd.parameter_name, nd.parameter_value);
      
  end update_com_parameter;

  function get_installed_version
  return com_parameters.parameter_value%type
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_curr_com_version    com_parameters.parameter_value%type;    
  begin
    select parameter_value
    into l_curr_com_version
    from com_parameters
    where parameter_name = c_com_param_version;
    
    return l_curr_com_version;
    
  end get_installed_version;
  
  function get_update_script 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_curr_com_version    com_parameters.parameter_value%type;    
  begin    
    case get_installed_version 
      when '1.0.0' then return 'update/update_1_0_0_to_1_1_0.sql';
      when '1.1.0' then return 'update/update_1_1_0_to_1_2_0.sql';
      when '1.2.0' then return 'update/update_1_2_0_to_1_3_0.sql';
      when '1.3.0' then return 'update/update_1_3_0_to_1_4_0.sql';
      when '1.4.0' then return 'update/update_1_4_0_to_1_5_0.sql';
      when '1.5.0' then return 'update/update_1_5_0_to_1_6_0.sql';
      when '1.6.0' then return 'update/update_1_6_0_to_1_7_0.sql';
      when '1.6.1' then return 'update/update_1_6_0_to_1_7_0.sql';
      when '1.7.0' then return 'update/update_1_7_0_to_1_8_0.sql';
      when '1.8.0' then return 'update/update_1_8_0_to_1_9_0.sql';
      when '1.9.0' then return 'update/update_1_9_0_to_1_10_0.sql';
      when '1.10.0' then return 'update/update_1_10_0_to_1_11_0.sql';
      when '1.11.0' then return 'update/update_1_11_0_to_1_12_0.sql';
      when '1.12.0' then return 'update/update_1_12_0_to_1_12_1.sql';
      when '1.12.1' then return 'update/update_1_12_1_to_1_12_2.sql';
      when '1.12.2' then return 'update/update_1_12_2_to_1_13_0.sql';
      when '1.13.0' then return 'update/update_1_13_0_to_1_13_1.sql';
      when '1.13.1' then return 'update/update_1_13_to_1_14_0.sql';
      when '1.14.0' then return 'update/update_1_14_to_1_15_0.sql';
      when '1.15.0' then return 'update/update_1_15_0_to_1_16_0.sql';
      when '1.16.0' then return 'update/update_1_16_0_to_1_17_0.sql';
      when c_latest_com_version then return 'update/update_already_latest_version.sql';
    end case;
  end get_update_script;
  
  procedure register_applied_changes (i_com_version in com_parameters.parameter_value%type)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-06-24 Chris Roderick and James Menzies
--    Added call to make central registration of commons user, as per OCOMM-266
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    update_com_parameter('VERSION', i_com_version);
    update_com_parameter('UPDATE_TIME_UTC', to_char(com_util.get_current_utc_timestamp, 'YYYY-MM-DD HH24:MI:SS'));
    update_com_parameter('UPDATE_OS_USER', sys_context('userenv', 'os_user'));          
    commons.com_user_registration.register_user(user);
 	 	commit;          
  end register_applied_changes;
  
  procedure first_install (i_com_version in com_parameters.parameter_value%type)
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2013-11-23  Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    update_com_parameter('INSTALL_TIME_UTC', to_char(com_util.get_current_utc_timestamp, 'YYYY-MM-DD HH24:MI:SS'));
    update_com_parameter('INSTALL_OS_USER', sys_context('userenv', 'os_user'));
--  Remaining parameters are the same as if an installation update was being applied
    register_applied_changes(i_com_version);
  end first_install;
  
end com_updater;