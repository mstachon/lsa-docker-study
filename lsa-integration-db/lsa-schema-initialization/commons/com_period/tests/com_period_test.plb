create or replace package body com_period_test
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--  	Auto-generated test package for com_period_test
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--  	 Auto-generated
------------------------------------------------------------------------------------------------------------------------
is

  procedure test_contains
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for contains
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2016-01-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-03-01 00:00:00');
    l_expected_result :=  true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.contains(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected initial period to contain given period!');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_contains;
  
  procedure test_not_contains
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-19 Chris
--    Creation of negative test for contains
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2016-01-01 00:00:00');
    i_period          :=  com_period(timestamp '2014-02-01 00:00:00', timestamp '2015-03-01 00:00:00');
    l_expected_result :=  false;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.contains(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected initial period NOT to contain given period!');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_not_contains;

  procedure test_duration_in_days
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for duration_in_days
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   number;
    l_expected_result number;
    l_initial_period  com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-20 12:00:00');
    l_expected_result :=  19.5;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.duration_in_days();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected duration in days to be '||l_expected_result||' but got '||l_actual_result);
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_duration_in_days;

  procedure test_duration_in_seconds
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for duration_in_seconds
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   number;
    l_expected_result number;
    l_initial_period  com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-01-01 01:01:01');
    l_expected_result :=  3661;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.duration_in_seconds();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected duration in seconds to be '||l_expected_result||' but got '||l_actual_result);
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_duration_in_seconds;

  procedure test_ends_before_end_of
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for ends_before_end_of
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-02-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-04-01 00:00:00');
    l_expected_result :=  true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.ends_before_end_of(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected initial period to end before end of given period');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_ends_before_end_of;

  procedure test_ends_before_start_of
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for ends_before_start_of
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-02-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-03-01 00:00:00', timestamp '2015-04-01 00:00:00');
    l_expected_result :=  true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.ends_before_start_of(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected initial period to end before start of given period');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_ends_before_start_of;

  procedure test_end_meets_start_of
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for end_meets_start_of
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-02-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-03-01 00:00:00');
    l_expected_result :=  true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.end_meets_start_of(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected end of initial period to meet start of given period');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_end_meets_start_of;

  procedure test_equals
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for equals
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2016-01-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2016-01-01 00:00:00');
    l_expected_result :=  true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.equals(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected initial period to equal given period');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_equals;

  procedure test_intersection_with
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for intersection_with
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   com_period;
    l_expected_result com_period;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-02-15 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-03-01 00:00:00');
    l_expected_result :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-02-15 00:00:00');
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.intersection_with(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_intersection_with;

  procedure test_offset_after
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for offset_after
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   com_period;
    l_expected_result com_period;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-03-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-02-15 00:00:00');
    l_expected_result :=  com_period(timestamp '2015-02-15 00:00:00', timestamp '2015-03-01 00:00:00');
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.offset_after(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_offset_after;

  procedure test_offset_before
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for offset_before
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   com_period;
    l_expected_result com_period;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-03-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-02-15 00:00:00');
    l_expected_result :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-02-01 00:00:00');
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.offset_before(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_offset_before;

  procedure test_overlaps_with
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for overlaps_with
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2016-01-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-03-01 00:00:00');
    l_expected_result :=  true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.overlaps_with(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected initial period to overlap with given period');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_overlaps_with;

  procedure test_pack_with
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for pack_with
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result           com_period;
    l_expected_result         com_period;
    l_initial_period          com_period;
    i_period                  com_period;
    i_overlapping_period_only varchar2(4000);

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-02-15 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-03-01 00:00:00');
    l_expected_result :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2015-03-01 00:00:00');
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.pack_with(i_period, i_overlapping_period_only);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected '||l_expected_result.to_string()||' but got '||l_actual_result.to_string());
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_pack_with;

  procedure test_starts_after_end_of
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for starts_after_end_of
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-04-01 00:00:00', timestamp '2016-01-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-03-01 00:00:00');
    l_expected_result :=  true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.starts_after_end_of(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected initial period to start after end of given period');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_starts_after_end_of;

  procedure test_starts_before_start_of
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for starts_before_start_of
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2016-01-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-03-01 00:00:00');
    l_expected_result :=  true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.starts_before_start_of(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected initial period to start before start of given period');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_starts_before_start_of;

  procedure test_start_meets_end_of
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for start_meets_end_of
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   boolean;
    l_expected_result boolean;
    l_initial_period  com_period;
    i_period          com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-03-01 00:00:00', timestamp '2016-01-01 00:00:00');
    i_period          :=  com_period(timestamp '2015-02-01 00:00:00', timestamp '2015-03-01 00:00:00');
    l_expected_result :=  true;
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.start_meets_end_of(i_period);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected start of initial period to meet the end of the given period');
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_start_meets_end_of;

  procedure test_to_constructor_call_li
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for to_constructor_call_literal
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   com_types.t_varchar2_max_size_db;
    l_expected_result com_types.t_varchar2_max_size_db;
    l_initial_period  com_period;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2016-01-01 00:00:00');
    l_expected_result :=  'com_period(timestamp ''2015-01-01 00:00:00'', timestamp ''2016-01-01 00:00:00'', , '''')';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.to_constructor_call_literal();
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected '||l_expected_result||' but got '||l_actual_result);
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_to_constructor_call_li;

  procedure test_to_string
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-12-18 Chris
--    Auto-generated test for to_string
------------------------------------------------------------------------------------------------------------------------
  is
    l_actual_result   com_types.t_varchar2_max_size_db;
    l_expected_result com_types.t_varchar2_max_size_db;
    l_initial_period  com_period;
    i_delimiter       com_types.t_varchar2_max_size_db;

  procedure with_given_conditions
  is
  begin
    l_initial_period  :=  com_period(timestamp '2015-01-01 00:00:00', timestamp '2016-01-01 00:00:00');
    i_delimiter       :=  '|';
    l_expected_result :=  '2015-01-01 00:00:00|2016-01-01 00:00:00';
  end with_given_conditions;

  procedure when_executing
  is
  begin
    l_actual_result := l_initial_period.to_string(i_delimiter);
  end when_executing;

  procedure should_result_in
  is
  begin
    com_assert.is_true(l_actual_result = l_expected_result, 'Expected '||l_expected_result||' but got '||l_actual_result);
  end should_result_in;

  procedure should_raise(i_error_code in number)
  is
  begin
    com_test_runner.fail('unexpected failure, err: '||dbms_utility.format_error_stack);
  end should_raise;

  procedure teardown
  is
  begin
    null;
  end teardown;

  begin
    with_given_conditions;
    begin
      when_executing;
      should_result_in;
    exception
      when others then
        should_raise(sqlcode);
    end;
    teardown;
  exception
    when others then
      teardown;
      if not com_event_mgr.event_equals('COM_TEST_RUNNER_TEST_FAILED', sqlcode) then
        com_test_runner.fail('test code problem (not actual test failing), err: '||dbms_utility.format_error_stack);
      end if;
      raise;
  end test_to_string;

end com_period_test;