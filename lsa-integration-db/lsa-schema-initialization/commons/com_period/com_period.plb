create or replace type body com_period 
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07  Chris Roderick
--    Modified equals_comparator post testing. OCOMM-318
--    Modified to_string post testing. OCOMM-318
--  2015-12-20  Chris Roderick
--    removed functions (is_expired, is_valid and is_not_yet_valid) which are all LAYOUT specific. OCOMM-363
--    converted functions returning true/false to be boolean. If it is necessary to call them from SQL, then we should 
--    extend this type with SQL only version, which can wrap these calls with com_util.boolean_to_char. OCOMM-363
--  2015-11-21  Chris Roderick
--    added function to_constructor_call_literal OCOMM-363
--  2015-11-14  Chris Roderick OCOMM-363
--    renamed function less_than to ends_before_start_of
--    renamed function greater_than to starts_after_end_of
--    renamed function sort_key to equals_comparator
--    renamed function contain to contains
--    renamed function meet to end_meets_start_of
--    added function start_meets_end_of
--    renamed function equal to equals
--    renamed function overlap to overlaps_with
--    renamed function intersection to intersection_with
--    renamed function pack to pack_with
--    renamed function get_number_of_days to duration_in_days
--    renamed function ldiff to offset_before
--    renamed function rdiff to offset_after
--    added function starts_before_start_of
--    added function ends_before_end_of
--    formatting
--  2015-11-11  Chris Roderick OCOMM-363
--    Added member function duration_in_seconds
--  2015-07-09  Pascal Le Roux
--    Creation
------------------------------------------------------------------------------------------------------------------------
is
  constructor function com_period (
    self            in out nocopy com_period, 
    period_start  in            timestamp, 
    period_end    in            timestamp, 
    period_id     in            number, 
    period_label  in            varchar2
  ) 
  return self as result
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.is_true(period_start<= period_end , 'period start '||period_start||' should be before the period end '||period_end||' for period_id '||period_id);
    self.period_start := period_start;
    self.period_end   := period_end;
    self.period_id    := period_id;
    self.period_label := period_label;
    return;
  end com_period;

  constructor function com_period (
    self          in out nocopy com_period, 
    period_start  in            timestamp, 
    period_end    in            timestamp
  ) 
  return self as result
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.is_true(period_start <= period_end , 'period start '||period_start||' should be before the period end '||period_end);
    self.period_start := period_start;
    self.period_end   := period_end;
    return;
  end com_period;
  
  map member function equals_comparator 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07  Chris Roderick
--    Changed period_id coalesce call to use a number instead of a string. OCOMM-363
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is
  begin 
    return rpad (to_char(self.period_start,'YYYY-MM-DD HH24:MI:SSXFF'), 30) ||rpad (to_char(self.period_end,'YYYY-MM-DD HH24:MI:SSXFF'), 30)||coalesce (self.period_id, -99999999999)||coalesce (self.period_label, 'X');
  end equals_comparator;

  member function to_constructor_call_literal 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-21  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return 'com_period(timestamp '''||to_char(self.period_start, com_constants.c_iso_timestamp_format_ss)||
      ''', timestamp '''||to_char(self.period_end, com_constants.c_iso_timestamp_format_ss)||
      ''', '||self.period_id||', '''||self.period_label||''')';
  end to_constructor_call_literal;
     
  member function to_string(i_delimiter in varchar2 default ',') 
  return varchar2
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-01-07  Chris Roderick
--    Changed calls to timestamp printing to use to_char with format com_constants.c_iso_timestamp_format_ss. OCOMM-363
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return ltrim(rtrim((self.period_id||i_delimiter||self.period_label||i_delimiter||
      to_char(self.period_start, com_constants.c_iso_timestamp_format_ss)||i_delimiter||
      to_char(self.period_end, com_constants.c_iso_timestamp_format_ss)), i_delimiter), i_delimiter);
  end to_string;
      
  member function duration_in_seconds 
  return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------  
  is
  begin
    return com_util.interval_to_seconds(self.period_end - self.period_start);
  end duration_in_seconds;
  
  member function duration_in_days 
  return number
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------  
  is
  begin 
    return duration_in_seconds/(60*60*24);
  end duration_in_days;
    
  member function equals (i_period in com_period) 
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-21  Chris Roderick
--    removed functions (is_expired, is_valid and is_not_yet_valid) which are all LAYOUT specific. OCOMM-363
--    converted functions returning true/false to be boolean. If it is necessary to call them from SQL, then we should 
--    extend this type with SQL only version, which can wrap these calls with com_util.boolean_to_char. OCOMM-363
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is
  begin 
    return (self.period_start = i_period.period_start and self.period_end = i_period.period_end);
  end equals;
    
  member function contains (i_period in com_period) 
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------  
  is
  begin 
    return (self.period_start <= i_period.period_start and i_period.period_end <= self.period_end);
  end contains;
    
  member function start_meets_end_of (i_period in com_period) 
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin 
    return self.period_start = i_period.period_end;
  end start_meets_end_of;
  
  member function end_meets_start_of (i_period in com_period) 
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin 
    return self.period_end = i_period.period_start;
  end end_meets_start_of;
    
  member function overlaps_with (i_period in com_period) 
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is 
  begin 
    return (self.period_start < i_period.period_end and self.period_end > i_period.period_start);
  end overlaps_with;    

  member function ends_before_start_of  (i_period in com_period) 
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is 
  begin 
    return self.period_end < i_period.period_start;
  end ends_before_start_of;    
    
  member function starts_after_end_of  (i_period in com_period) 
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is 
  begin 
    return self.period_start > i_period.period_end;
  end starts_after_end_of;    

  member function starts_before_start_of (i_period in com_period) 
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return self.period_start < i_period.period_start;
  end starts_before_start_of;
  
  member function ends_before_end_of (i_period in com_period)
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return self.period_end < i_period.period_end;
  end ends_before_end_of;
  
  member function intersection_with (i_period in com_period) 
  return com_period 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is 
  begin 
    if overlaps_with(i_period) then
      return com_period (greatest(self.period_start, i_period.period_start), least(self.period_end,i_period.period_end));
    end if;
    return com_period(period_start => null, period_end => null);
  end intersection_with;

  member function offset_before (i_period in com_period) 
  return com_period 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is 
  begin 
    if overlaps_with(i_period) then
      return com_period (least(self.period_start, i_period.period_start), greatest(self.period_start, i_period.period_start));
    end if;
    return com_period(period_start => null, period_end => null);    
  end offset_before; 

  member function offset_after (i_period in com_period) 
  return com_period 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is
  begin 
    if overlaps_with(i_period) then
      return com_period (least(self.period_end, i_period.period_end), greatest(self.period_end, i_period.period_end));
    end if;
    return com_period(period_start => null, period_end => null);
  end offset_after; 

  member function pack_with (
    i_period                  in  com_period, 
    i_overlapping_period_only in  varchar2    default 'Y'
  ) 
  return com_period
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    if overlaps_with(i_period) 
    or (
      not com_util.char_to_boolean(i_overlapping_period_only) 
      and (
        end_meets_start_of(i_period)
        or 
        start_meets_end_of(i_period)
      )
    ) then 
      return com_period (least(self.period_start, i_period.period_start), greatest(self.period_end, i_period.period_end));
    end if;      
    return com_period(period_start => null, period_end => null);
  end pack_with; 
      
end;