create or replace type com_period force
------------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2014-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This object type is intended to provide constructors and functions to work with time periods.
--    A period is a pair of timestamps: period_start, period_end. period_start <= period_end
--    The relationship checking operators return the integer value 1 if the relationship between the two periods exists
--    otherwise 0 if the relationship does not exist.
--    Integer values are returned to allow calling from SQL (as boolean is not supported)
--    Convention used for period: lower boundary inclusive, upper boundary exclusive
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-11-24 Jose Rolland
--   Applied format changes identified in code review OCOMM-REVIEWS-10 - OCOMM-413
--  2015-12-20  Chris Roderick
--    removed functions (is_expired, is_valid and is_not_yet_valid) which are all LAYOUT specific. OCOMM-363
--    converted functions returning true/false to be boolean. If it is necessary to call them from SQL, then we should 
--    extend this type with SQL only version, which can wrap these calls with com_util.boolean_to_char. OCOMM-363
--  2015-11-21  Chris Roderick
--    added function to_constructor_call_literal OCOMM-363
--  2015-11-14  Chris Roderick OCOMM-363
--    renamed attributes - removing the l_ prefix which is not applicable for object attribute (same as table columns)
--    renamed function less_than to ends_before_start_of
--    renamed function greater_than to starts_after_end_of
--    renamed function sort_key to equals_comparator
--    renamed function contain to contains
--    renamed function meet to end_meets_start_of
--    added function start_meets_end_of
--    renamed function equal to equals
--    renamed function overlap to overlaps_with
--    renamed function intersection to intersection_with
--    renamed function pack to pack_with
--    renamed function get_number_of_days to duration_in_days
--    renamed function ldiff to offset_before
--    renamed function rdiff to offset_after
--    added function starts_before_start_of
--    added function ends_before_end_of
--    formatting
--  2015-11-11  Chris Roderick OCOMM-363
--    Added member function duration_in_seconds
--  2015-07-09  Pascal Le Roux
--    Creation, see OCOMM-318
------------------------------------------------------------------------------------------------------------------------
is object ( 
  period_start timestamp(9),
  period_end   timestamp(9),
  period_id    number,
  period_label varchar2(4000),
  
  constructor function com_period (
    self            in out nocopy com_period, 
    period_start  in            timestamp, 
    period_end    in            timestamp, 
    period_id     in            number, 
    period_label  in            varchar2
  ) 
  return self as result,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This overloads the would-be default constructor (with attributes named identically to the object definition), to
--    allow required validations
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    self         - com_period object
--    period_start - start time
--    period_end   - end time
--    period_id    - period identifier
--    period_label - period label
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  
  constructor function com_period (
    self          in out nocopy com_period, 
    period_start  in            timestamp, 
    period_end    in            timestamp
  ) 
  return self as result,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Overloaded constructor that only requires start and end times (to enable basic validations).
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    self         - com_period object
--    period_start - start time
--    period_end   - end time
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------
  
  map member function equals_comparator 
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    This function is implicitly used by Oracle to compare instances of com_period e.g. when doing distinct, order by, 
--    multiset and = operations
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function to_constructor_call_literal 
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns a string literal which represents a call to the com_period constructor, passing the attribute values 
--    of this com_period as inputs. This is useful for capturing real data to be used as fixed literal values for testing.
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions  
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-21  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  member function to_string(i_delimiter in varchar2 default ',') 
  return varchar2,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns period_id, period_label, period_start, period_end as a delimited string
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_delimiter - the delimiter to use between the com_period attributes 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function equals (i_period in com_period) 
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns true if "this" period is equal to the given period (meaning their start and end times (only) are the same)
--    otherwise returns false
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function contains (i_period in com_period) 
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns true if "this" period contains the given period (meaning the start and end times of the given period are 
--    within those of "this" period), otherwise returns false
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function end_meets_start_of (i_period in com_period) 
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns true if the end of "this" period equals the start of the given period, otherwise returns false
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function start_meets_end_of (i_period in com_period) 
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns true if the start of "this" period equals the end of the given period, otherwise returns false
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  member function overlaps_with (i_period in com_period) 
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns true if "this" period overlaps with the given period, otherwise returns false    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against 
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function ends_before_start_of (i_period in com_period) 
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns true if the end of "this" period is before the start of the given period, otherwise returns false
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function starts_after_end_of (i_period in com_period) 
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    returns true if the start of "this" period is after the end of the given period, otherwise returns false
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function intersection_with (i_period in com_period) 
  return com_period,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the intersection of "this" period and the given period, that is, the time range common to both periods
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function offset_before (i_period in com_period) 
  return com_period,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns a period representing the offset from the start of "this" period up to the start of the given period
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function offset_after (i_period in com_period) 
  return com_period, 
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns a period representing the offset from the end of the given period up to the end of "this" period    
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function pack_with (
    i_period                  in  com_period, 
    i_overlapping_period_only in  varchar2    default 'Y'
  ) 
  return com_period,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns a period which merges "this" period with the given period if they overlap (or optionally meet)
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be packed with "this" period
--    i_overlapping_period_only - 'Y' indicates the periods should be packed only if they overlap, 'N' indicates the
--      periods should also be packed if they meet
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function duration_in_seconds 
  return number,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the duration in seconds between the start_time and end_time of "this" period
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function duration_in_days 
  return number,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the duration in days between the start_time and end_time of "this" period
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-07-09  Pascal Le Roux
--    Creation OCOMM-318
------------------------------------------------------------------------------------------------------------------------

  member function starts_before_start_of (i_period in com_period) 
  return boolean,
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns true if the start of "this" period is before the start of the given period, otherwise returns false
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

  member function ends_before_end_of (i_period in com_period) 
  return boolean
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns true if the end of "this" period is before the end of the given period, otherwise returns false
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_period - the period to be checked against
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-11-14  Chris Roderick
--    Creation OCOMM-363
------------------------------------------------------------------------------------------------------------------------

) 
not final;