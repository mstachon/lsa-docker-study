create or replace package body com_schema_analysis_constraint
is
-----------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to Constraints.
--  Scope:
--    Operates on the database level, but depends on individual schema analysis rule implementations.
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------

  function chk_missing_pk_constraints (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for Missing Primary Key constraints on Tables
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin    
    select schema_analysis_rule_result(alt.owner, alt.table_name, 'TABLE', 'This table does not have a Primary Key Constraint', 1)
    bulk collect into l_result
    from all_tables alt
    where alt.owner like i_obj_owner_like  
    and alt.table_name like i_obj_name_like
    and not exists (
      select null 
      from all_constraints alc
      where alc.owner = alt.owner 
      and alc.table_name = alt.table_name        
      and alc.constraint_type = 'P'
    )
    order by alt.owner, alt.table_name;
    
    return l_result;
    
  end chk_missing_pk_constraints; 
  
  function chk_disabled_constraints (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
-----------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for Disabled constraints on Tables
-----------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
-----------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
-----------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin    
    select schema_analysis_rule_result(ac.owner, ac.table_name, 'TABLE', 'The constraint '||ac.constraint_name||' is disabled, consider enabling or removing the constraint', 1)
    bulk collect into l_result
    from all_constraints ac
    where ac.owner like i_obj_owner_like  
    and ac.table_name like i_obj_name_like
    and ac.status = 'DISABLED'
    order by ac.owner, ac.table_name, ac.constraint_name;
    
    return l_result;
    
  end chk_disabled_constraints; 
  
    function chk_deferred_constraints (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-08-08 Lukasz
--    Creation - OCOMM-297
-----------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin    
    select schema_analysis_rule_result(owner, table_name, 'TABLE', msg, severity_factor) 
    bulk collect into l_result
    from(
      select owner, table_name, constraint_name, msg, severity_factor from (  
        select ac.owner, ac.table_name, ac.constraint_name, 'The constraint '||ac.constraint_name||' is deffferable, consider revising underlying data and make it NOT DEFFERABLE' as msg, 1 as severity_factor 
        from all_constraints ac
        where ac.owner like i_obj_owner_like and ac.table_name like i_obj_name_like
          and ac.deferrable = 'DEFERRABLE'
        union all
        select ac.owner, ac.table_name, ac.constraint_name, 'The constraint '||ac.constraint_name||' is deffered, consider revising underlying data and make it IMMEDIATE' as msg, 1 as severity_factor    
        from all_constraints ac
        where ac.owner like i_obj_owner_like and ac.table_name like i_obj_name_like        
          and ac.deferred = 'DEFERRED'
      ) order by owner, table_name, constraint_name
    );
    return l_result;
    
  end chk_deferred_constraints; 
  
  function chk_not_validated_constraints (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-08-08 Lukasz
--    Creation - OCOMM-298
-----------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;
  begin    
    select schema_analysis_rule_result(ac.owner, ac.table_name, 'TABLE', 'The constraint '||ac.constraint_name||' not validated, consider reviewing stored data and enabling the constraint', 1)
    bulk collect into l_result
    from all_constraints ac
    where ac.owner like i_obj_owner_like  
    and ac.table_name like i_obj_name_like
    and ac.validated = 'NOT VALIDATED'
    order by ac.owner, ac.table_name, ac.constraint_name;
    
    return l_result;
    
  end chk_not_validated_constraints; 

end com_schema_analysis_constraint;