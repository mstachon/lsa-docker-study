create or replace package com_schema_analysis_constraint
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Contains schema analysis rules related to Constraints.
--  Scope:
--    Operates on the database level, but depends on individual schema analysis rule implementations.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-14 Jose Rolland
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 -  OCOMM-417
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------
is

  function chk_missing_pk_constraints (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for Missing Primary Key constraints on Tables
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function chk_disabled_constraints (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for Disabled constraints on Tables
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-14 Pascal Le Roux and Chris Roderick
--    Creation
------------------------------------------------------------------------------------------------------------------------

  function chk_deferred_constraints (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for Deferred and Deferrable constraints on Tables
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-08-08 Lukasz
--    Creation - OCOMM-297
------------------------------------------------------------------------------------------------------------------------

  function chk_not_validated_constraints (
    i_obj_owner_like  in  all_source.owner%type, 
    i_obj_type_like   in  all_source.type%type, 
    i_obj_name_like   in  all_source.name%type
  )
  return schema_analysis_rule_results;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Checks for Not Validated constraints on Tables
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like          - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like           - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like           - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-08-08 Lukasz
--    Creation - OCOMM-298
------------------------------------------------------------------------------------------------------------------------

end com_schema_analysis_constraint;