create or replace package com_constants
-----------------------------------------------------------------------------------------------------------------------
-- (c) CERN 2005-2016 copyright
-----------------------------------------------------------------------------------------------------------------------
-- Description:
--  This package is intended to manage definitions of common constants
-----------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-20 Jose Rolland
--    Added c_yes and c_no - OCOMM-405
--  2016-05-01 Chris Roderick
--    Added constant c_max_length_oracle_obj_name. OCOMM-388
--  2015-12-16  Pascal Le Roux
--    added constants for DML event identifiers OCOMM-368
--  2015-11-21  Chris Roderick
--    added constants for ISO timestamp formats, and start of CERN OCOMM-363
--  2015-11-15 Chris Roderick
--    Added c_true_as_integer and c_false_as_integer as part of OCOMM-363
--  2015-10-01 Chris Roderick
--    Creation. OCOMM-347
-----------------------------------------------------------------------------------------------------------------------
is
  c_true                        constant  com_types.t_boolean_as_char :=  'Y';
  c_false                       constant  com_types.t_boolean_as_char :=  'N';
  c_true_as_integer             constant  integer(1)                  :=  1;
  c_false_as_integer            constant  integer(1)                  :=  0;
  
  c_line_break                  constant  varchar2(1)                 :=  chr(10);
  c_tab                         constant  varchar2(1)                 :=  chr(9);
  c_horizontal_rule             constant  varchar2(120)               :=  '------------------------------------------------------------------------------------------------------------------------';
  
  c_eternity_date               constant  timestamp                   :=  timestamp '9999-12-31 00:00:00';
  c_start_of_cern               constant  timestamp                   :=  timestamp '1954-09-29 00:00:00';
  
  c_varchar2_max_size_plsql     constant  pls_integer                 :=  32767;
  c_varchar2_max_size_db        constant  pls_integer                 :=  4000;
  
  c_iso_timestamp_format_ss     constant  varchar2(21)                :=  'YYYY-MM-DD HH24:MI:SS';
  c_iso_timestamp_format_ff     constant  varchar2(25)                :=  'YYYY-MM-DD HH24:MI:SS.ff9';
  
  c_dml_event_insert            constant varchar2(1)                  := 'I';
  c_dml_event_update            constant varchar2(1)                  := 'U';
  c_dml_event_delete            constant varchar2(1)                  := 'D';
  
  c_max_length_oracle_obj_name  constant pls_integer                  :=  30;
  
  c_yes                         constant com_types.t_yes_no           :=  'yes';
  c_no                          constant com_types.t_yes_no           :=  'no';
  
end com_constants;