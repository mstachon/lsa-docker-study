create or replace package body com_schema_analyser 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-02 Chris Roderick
--    Formatting
--    Updated procedure reg_schema_analysis_rule. OCOMM-347
--  2014-07-03  Chris Roderick and Vasilis Giannoutsos
--    Creation, based on OCOMM-270
------------------------------------------------------------------------------------------------------------------------
is  
  procedure reg_schema_analysis_rule (
    i_rule_name                   in  com_schema_analysis_rules.schema_analysis_rule_name%type, 
    i_rule_category               in  t_rule_category, 
    i_rule_exec                   in  com_schema_analysis_rules.schema_analysis_rule_exec%type, 
    i_should_bind_obj_owner_like  in  boolean,  
    i_should_bind_obj_type_like   in  boolean, 
    i_should_bind_obj_name_like   in  boolean, 
    i_rule_severity               in  com_schema_analysis_rules.schema_analysis_rule_severity%type, 
    i_extra_info                  in  com_schema_analysis_rules.extra_info%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2015-10-02 Chris Roderick
--    Updated to use com_types.t_boolean_as_char. OCOMM-347
--  2014-07-03  Chris Roderick and Vasilis Giannoutsos
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is    
    l_should_bind_obj_owner_like    com_types.t_boolean_as_char;  
    l_should_bind_obj_type_like     com_types.t_boolean_as_char; 
    l_should_bind_obj_name_like     com_types.t_boolean_as_char;     
  begin  
    l_should_bind_obj_owner_like    := com_util.boolean_to_char(i_should_bind_obj_owner_like);  
    l_should_bind_obj_type_like     := com_util.boolean_to_char(i_should_bind_obj_type_like); 
    l_should_bind_obj_name_like     := com_util.boolean_to_char(i_should_bind_obj_name_like); 
  
    insert into com_schema_analysis_rules (
      schema_analysis_rule_id, schema_analysis_rule_name, schema_analysis_category, 
      schema_analysis_rule_exec, should_bind_obj_owner_like,  should_bind_obj_type_like, 
      should_bind_obj_name_like, schema_analysis_rule_severity, extra_info)
    values (
      com_schema_analysis_rules_seq.nextval, i_rule_name, i_rule_category, i_rule_exec, l_should_bind_obj_owner_like, 
      l_should_bind_obj_type_like, l_should_bind_obj_name_like, i_rule_severity, i_extra_info
    );
    
  end reg_schema_analysis_rule;
  
  function schema_change_since_last_run (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return boolean
  is
  begin
  
    return last_schema_change_time(i_obj_owner_like, i_obj_type_like, i_obj_name_like) 
      > coalesce(last_schema_analysis_run_time, timestamp '2000-01-01 00:00:00');
  
  end schema_change_since_last_run; 
  
  procedure start_schema_analysis_run (o_schema_analysis_run_id out com_schema_analysis_runs.schema_analysis_run_id%type)
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-10  Chris Roderick and Daniel Hartley
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin
    insert into com_schema_analysis_runs (schema_analysis_run_id, schema_analysis_start_time)
    values (com_schema_analysis_run_id_seq.nextval, sys_extract_utc(systimestamp))
    return schema_analysis_run_id into o_schema_analysis_run_id;
  end start_schema_analysis_run;
  
  procedure end_schema_analysis_run (i_schema_analysis_run_id in com_schema_analysis_runs.schema_analysis_run_id%type)
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-10  Chris Roderick and Daniel Hartley
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin
    update com_schema_analysis_runs set schema_analysis_end_time = sys_extract_utc(systimestamp) 
    where schema_analysis_run_id = i_schema_analysis_run_id;
  end end_schema_analysis_run;
  
  procedure persist_analysis_results(
    i_schema_analysis_run_id  in  com_schema_analysis_runs.schema_analysis_run_id%type,
    i_results                 in  schema_analysis_results
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-10  Chris Roderick and Daniel Hartley
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    forall r_result in 1..i_results.count
      insert into com_schema_analysis_results(schema_analysis_run_id, schema_analysis_rule_name, schema_analysis_category, schema_analysis_rule_severity, extra_info, schema_name, object_name, object_type)
      values (
        i_schema_analysis_run_id, i_results(r_result).schema_analysis_rule_name, i_results(r_result).schema_analysis_category, i_results(r_result).schema_analysis_rule_severity, 
        i_results(r_result).extra_info, i_results(r_result).schema_name, i_results(r_result).object_name, i_results(r_result).object_type
      );
        
  end persist_analysis_results;

  procedure analyse_schema_if_changed (
    i_rule_category      in  t_rule_category         default c_all, 
    i_obj_owner_like     in  all_source.owner%type   default user, 
    i_obj_type_like      in  all_source.type%type    default '%', 
    i_obj_name_like      in  all_source.name%type    default '%'
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-10  Chris Roderick and Daniel Hartley
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_schema_analysis_run_id  com_schema_analysis_runs.schema_analysis_run_id%type;
    l_results                 schema_analysis_results;    
  begin
    if schema_change_since_last_run(i_obj_owner_like, i_obj_type_like, i_obj_name_like) then    
      start_schema_analysis_run(l_schema_analysis_run_id);
      
      select schema_analysis_result(schema_analysis_rule_name, schema_analysis_category, schema_analysis_rule_severity, extra_info, schema_name, object_name, object_type)
      bulk collect into l_results 
      from table(analyse_schema(i_rule_category, i_obj_owner_like, i_obj_type_like, i_obj_name_like));
      
      persist_analysis_results(l_schema_analysis_run_id, l_results);
      end_schema_analysis_run(l_schema_analysis_run_id);      
    end if;

  end analyse_schema_if_changed;
  
  function analyse_schema (
    i_rule_category      in  t_rule_category         default c_all, 
    i_obj_owner_like     in  all_source.owner%type   default user, 
    i_obj_type_like      in  all_source.type%type    default '%', 
    i_obj_name_like      in  all_source.name%type    default '%'
  )
  return schema_analysis_results pipelined
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-03  Chris Roderick and Vasilis Giannoutsos
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_result  schema_analysis_rule_results;    
  begin
  
    for schema_rules in (
      select schema_analysis_rule_id, schema_analysis_rule_name, schema_analysis_category, 
        schema_analysis_rule_exec, should_bind_obj_owner_like, should_bind_obj_type_like, 
        should_bind_obj_name_like, schema_analysis_rule_severity, extra_info
      from com_schema_analysis_rules
      where schema_analysis_category like (case i_rule_category when c_all then '%' else i_rule_category end) 
    ) loop    
    
      execute immediate 'begin :l_result := '||schema_rules.schema_analysis_rule_exec||'(:obj_owner_like, :obj_type_like, :obj_name_like); end;' 
      using out l_result, i_obj_owner_like, i_obj_type_like, i_obj_name_like;        
        
      for i in 1..l_result.count loop
        pipe row (schema_analysis_result(schema_rules.schema_analysis_rule_name, schema_rules.schema_analysis_category, schema_rules.schema_analysis_rule_severity, 
          coalesce(l_result(i).extra_info, schema_rules.extra_info), l_result(i).schema_name, l_result(i).object_name, l_result(i).object_type
        ));
      end loop;
      
    end loop;

    return;
  end analyse_schema;
  
  function last_schema_analysis_run_time
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-10  Chris Roderick and Daniel Hartley
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_last_analysis_run_time com_schema_analysis_runs.schema_analysis_start_time%type;
  begin
    select max(schema_analysis_start_time) 
    into l_last_analysis_run_time
    from com_schema_analysis_runs;
    
    return l_last_analysis_run_time;
  
  end last_schema_analysis_run_time;  

  function last_schema_change_time (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return timestamp
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-03  Chris Roderick and Vasilis Giannoutsos
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_last_schema_change_time all_objects.last_ddl_time%type;
  begin
    
    select max(last_ddl_time) 
    into l_last_schema_change_time
    from all_objects
    where owner like i_obj_owner_like
    and object_type in ('SEQUENCE', 'TABLE', 'VIEW', 'INDEX', 'MATERIALIZED VIEW', 'TYPE', 'TYPE BODY', 'SYNONYM')
    and object_type like i_obj_type_like
    and object_name like i_obj_name_like;
    
    return l_last_schema_change_time;
    
  end last_schema_change_time;  

  function schema_chng_since_last_run_chr (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return char
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-10  Chris Roderick and Daniel Hartley
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    return com_util.boolean_to_char(schema_change_since_last_run);
  end schema_chng_since_last_run_chr;  

end com_schema_analyser;