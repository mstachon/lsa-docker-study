create or replace package com_schema_analyser 
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2016 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Facilitates schema analysis via a schema analysis runner, which runs schema analysis rules.
--    Such rules should be implemented in dedicated packages per category of schema analysis rules, 
--    and registered via the reg_schema_analysis_rule procedure.
--  Scope:
--    Operates on the database level, but depends on the implementation of analysis rules in each individual schema.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2016-07-14 Jose Rolland
--   Applied formatting changes identified in code review OCOMM-REVIEWS-10 -  OCOMM-416
--  2014-07-01  Chris Roderick and Jakub Janczyk
--    Creation, based on OCOMM-270
------------------------------------------------------------------------------------------------------------------------
is

--    Categories of analysis rules.
  subtype t_rule_category is com_schema_analysis_rules.schema_analysis_category%type;
  c_all         constant t_rule_category :=  'All';
  c_naming      constant t_rule_category :=  'Naming';
  c_constraints constant t_rule_category :=  'Constraints';
  c_indexes     constant t_rule_category :=  'Indexes';
  c_tables      constant t_rule_category :=  'Tables';
  c_views       constant t_rule_category :=  'Views';
  c_invalid     constant t_rule_category :=  'Invalid';
  
  procedure reg_schema_analysis_rule (
    i_rule_name                   in  com_schema_analysis_rules.schema_analysis_rule_name%type, 
    i_rule_category               in  t_rule_category, 
    i_rule_exec                   in  com_schema_analysis_rules.schema_analysis_rule_exec%type, 
    i_should_bind_obj_owner_like  in  boolean,  
    i_should_bind_obj_type_like   in  boolean, 
    i_should_bind_obj_name_like   in  boolean, 
    i_rule_severity               in  com_schema_analysis_rules.schema_analysis_rule_severity%type, 
    i_extra_info                  in  com_schema_analysis_rules.extra_info%type
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Registers the given inputs as a schema analysis rule in the table com_schema_analysis_rules
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_rule_name                  - The unique name to be given to identify this rule
--    i_category                   - The category of the rule 
--    i_rule_exec                  - The function (without owner prefix) to be called to obtain analysis results.
--    i_should_bind_obj_owner_like - flag indicating if object owner filter should be passed to schema analysis function
--    i_should_bind_obj_type_like  - flag indicating if object type filter should be passed to schema analysis function 
--    i_should_bind_obj_name_like  - flag indicating if object name filter should be passed to schema analysis function 
--    i_rule_severity              - Integer between 1 (least) and 10 (most) indicating the severity of the rule
--    i_extra_info                 - Extra info to indicate why the rule should not be violated     
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-01  Chris Roderick and Jakub Janczyk
--    Creation
------------------------------------------------------------------------------------------------------------------------

  procedure analyse_schema_if_changed (
    i_rule_category      in  t_rule_category         default c_all, 
    i_obj_owner_like     in  all_source.owner%type   default user, 
    i_obj_type_like      in  all_source.type%type    default '%', 
    i_obj_name_like      in  all_source.name%type    default '%'
  );
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Executes all registered schema analysis rules matching the given category and stores the results
--    in the tables com_schema_analysis_runs and com_schema_analysis_run_results
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_analysis_category  - Optional filter to only run rules of the given category
--    i_obj_owner_like     - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like      - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like      - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-01  Chris Roderick and Jakub Janczyk
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  function analyse_schema (
    i_rule_category      in  t_rule_category         default c_all, 
    i_obj_owner_like     in  all_source.owner%type   default user, 
    i_obj_type_like      in  all_source.type%type    default '%', 
    i_obj_name_like      in  all_source.name%type    default '%'
  )
  return schema_analysis_results pipelined;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Executes all registered schema analysis rules matching the given category and returns a table or results
--
--  Example Usage:
--    select * from table(com_schema_analyser.analyse_schema());
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_analysis_category  - Optional filter to only run rules of the given category
--    i_obj_owner_like     - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like      - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like      - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-01  Chris Roderick and Jakub Janczyk
--    Creation
------------------------------------------------------------------------------------------------------------------------
  
  function last_schema_analysis_run_time
  return timestamp;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the last time (if any) at which schema analysis was run
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-01  Chris Roderick and Jakub Janczyk
--    Creation
------------------------------------------------------------------------------------------------------------------------  

  function last_schema_change_time (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return timestamp;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns the last time (if any) at which objects containing schema were changed
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like     - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like      - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like      - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-01  Chris Roderick and Jakub Janczyk
--    Creation
------------------------------------------------------------------------------------------------------------------------  

  function schema_chng_since_last_run_chr (
    i_obj_owner_like          in  all_objects.owner%type   default user, 
    i_obj_type_like           in  all_objects.object_type%type    default '%', 
    i_obj_name_like           in  all_objects.object_name%type    default '%'
  )
  return char;
------------------------------------------------------------------------------------------------------------------------
--  Description:
--    Returns 'Y' if the schema has changed since the last analysis run, otherwise it returns 'N'
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--    i_obj_owner_like     - Optional filter to only run rules against objects with matching owners
--    i_obj_type_like      - Optional filter to only run rules against objects with matching types 
--    i_obj_name_like      - Optional filter to only run rules against objects with matching names
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-07-10  Chris Roderick and Daniel Hartley
--    Creation
------------------------------------------------------------------------------------------------------------------------ 

end com_schema_analyser;