create or replace PROCEDURE dump_str (buf IN VARCHAR2) IS
  arr APEX_APPLICATION_GLOBAL.VC_ARR2;
  fHandle  UTL_FILE.FILE_TYPE;
BEGIN
 dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PRETTY',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'STORAGE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PARTITIONING',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'TABLESPACE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SEGMENT_ATTRIBUTES',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SQLTERMINATOR',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'CONSTRAINTS',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'REF_CONSTRAINTS',false);
  arr := APEX_UTIL.string_to_table(buf, CHR(10));
  FOR i IN 1..arr.COUNT LOOP
    IF i < arr.COUNT THEN
      DBMS_OUTPUT.put_line(arr(i));
    ELSE
      DBMS_OUTPUT.put(arr(i));
    END IF;
  END LOOP;
    UTL_FILE.FCLOSE(fHandle);
END dump_str;
 
create or replace PROCEDURE dump_clob (clob IN OUT NOCOPY CLOB) IS
  offset NUMBER := 1;
  amount NUMBER := 32767;
  len    NUMBER := DBMS_LOB.getLength(clob);
  buf    VARCHAR2(32767);
BEGIN
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PRETTY',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'STORAGE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PARTITIONING',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'TABLESPACE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SEGMENT_ATTRIBUTES',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SQLTERMINATOR',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'CONSTRAINTS',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'REF_CONSTRAINTS',false);
  WHILE offset < len LOOP
    DBMS_LOB.read(clob, amount, offset, buf);
    offset := offset + amount;
    dump_str(buf);
  END LOOP;
  DBMS_OUTPUT.new_line;
END dump_clob;