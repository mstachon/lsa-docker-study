set long 1000000
set heading off  
set head off
set echo off
set pagesize 0
set verify off
set feedback off
set pages 999
set longchunksize 32000
set linesize 32000
set newpage none  
set tab off  
set trimspool on
set sqlblanklines on
SET SERVEROUTPUT ON SIZE 100000
column ddl format a50000
declare
clob_out CLOB;

begin
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PRETTY',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'STORAGE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PARTITIONING',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'TABLESPACE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SEGMENT_ATTRIBUTES',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SQLTERMINATOR',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'CONSTRAINTS',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'REF_CONSTRAINTS',false);

export_lsa_dev(clob_out);
dump_clob(clob_out);

end;


