CREATE OR REPLACE PROCEDURE set_dbms_metadata_params
as
begin
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PRETTY',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'STORAGE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'PARTITIONING',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'TABLESPACE',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SEGMENT_ATTRIBUTES',false);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'SQLTERMINATOR',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'CONSTRAINTS',true);
  dbms_metadata.set_transform_param(dbms_metadata.session_transform,'REF_CONSTRAINTS',false);
end set_dbms_metadata_params;
/
  

CREATE OR REPLACE PROCEDURE export_tables
(
    clob_result OUT CLOB
)
as
begin
declare 
export_result CLOB;

BEGIN
set_dbms_metadata_params;
for rec in (
select
    u.object_name, u.object_type
from
   objects_to_export u
where
   object_type = 'TABLE')
   loop
        clob_result := clob_result || dbms_metadata.GET_DDL(rec.object_type,rec.object_name);   
   end loop;   
   end;
END export_tables;
/

CREATE OR REPLACE PROCEDURE export_ref_constraints
(
    clob_result OUT CLOB
)
as
begin
declare 
export_result CLOB;

BEGIN
set_dbms_metadata_params;
for rec in (
select
    u.constraint_name
from
   user_constraints u
   where
    owner = 'LSA' and u.constraint_type in ('R') and table_name in (select object_name from objects_to_export))
   loop
        clob_result := clob_result || dbms_metadata.GET_DDL('REF_CONSTRAINT',rec.constraint_name);      
    end loop;   
    end;
END export_ref_constraints;
/

CREATE OR REPLACE PROCEDURE export_types
(
    clob_result OUT CLOB
)
as
begin
declare 
export_result CLOB;

BEGIN
set_dbms_metadata_params;
for rec in (select
  u.object_name, u.object_type
from
   objects_to_export u
where
   object_type = 'TYPE')
    loop
           clob_result := clob_result || dbms_metadata.GET_DDL(rec.object_type,rec.object_name);   
    end loop;
    end;
end export_types;
/

CREATE OR REPLACE PROCEDURE export_sequences
(
    clob_result OUT CLOB
)
as
begin
declare 
export_result CLOB;

BEGIN
set_dbms_metadata_params;
for rec in (select
 u.object_name, u.object_type
from
   objects_to_export u
where
   object_type = 'SEQUENCE')
   loop
           clob_result := clob_result || dbms_metadata.GET_DDL(rec.object_type,rec.object_name);   
   end loop;
   end;
end export_sequences;
/

CREATE OR REPLACE PROCEDURE export_indexes
(
    clob_result OUT CLOB
)
as
begin
declare 
export_result CLOB;

BEGIN
set_dbms_metadata_params;
for rec in (select
   u.index_name
from
   user_indexes u
where table_name in (select uu.table_name from user_tables uu where IOT_TYPE is null and uu.table_name in(select object_name from objects_to_export)))
    loop
        clob_result := clob_result || dbms_metadata.GET_DDL('INDEX',rec.index_name);   
    end loop;
    end;
end export_indexes;
/

CREATE OR REPLACE PROCEDURE export_synonyms
(
    clob_result OUT CLOB
)
as
begin
declare 
export_result CLOB;

BEGIN
set_dbms_metadata_params;
for rec in (select
   object_name
from
   objects_to_export
where object_type = 'SYNONYM')
loop
        clob_result := clob_result || dbms_metadata.GET_DDL('SYNONYM',rec.object_name);   
end loop;
end;
end export_synonyms;
/ 

CREATE OR REPLACE PROCEDURE export_views
(
    clob_result OUT CLOB
)
as
begin
declare 
export_result CLOB;

BEGIN
set_dbms_metadata_params;
for rec in (select
       u.object_name, u.object_type
from
   objects_to_export u
where
   object_type = 'VIEW')
   loop
           clob_result := clob_result || dbms_metadata.GET_DDL(rec.object_type,rec.object_name);   
   end loop;
   end;
   end export_views;
/

create or replace procedure user_lsa_connect
(
    clob_result OUT CLOB
)
as
begin
    clob_result := 'ALTER USER LSA IDENTIFIED BY LSADEV;';
    clob_result := chr(10) || clob_result || chr(10) || chr(13) || 'CONNECT LSA/LSADEV;';
end user_lsa_connect;
/

create or replace procedure export_data 
(
    clob_result OUT CLOB
)
as
begin
DECLARE
  cur SYS_REFCURSOR;
  curid NUMBER;
  desctab DBMS_SQL.desc_tab;
  colcnt NUMBER;
  namevar VARCHAR2(4000);
  numvar NUMBER;
  datevar DATE;
  user_name VARCHAR(100);

  out_columns varchar2(10000);
  out_values varchar2(10000);
  out_table varchar2(10000);
  out_define varchar2(10000);
  
  
  
BEGIN
set_dbms_metadata_params;

  SELECT user INTO user_name FROM dual;
  FOR rec IN (SELECT table_name 
                FROM user_tables 
               WHERE table_name in (select object_name from objects_to_export where object_type = 'DATA'))
  LOOP
    clob_result := clob_result || CHR(10)||('REM INSERTING into ' || rec.table_name );
      clob_result := clob_result || CHR(10)||('SET DEFINE OFF;');
  
    OPEN cur FOR 'SELECT * FROM '||rec.table_name||' ORDER BY 1';

    curid := DBMS_SQL.to_cursor_number(cur);
    DBMS_SQL.describe_columns(curid, colcnt, desctab);
   
    
    out_columns := 'INSERT INTO '||rec.table_name||'(';

    FOR indx IN 1 .. colcnt LOOP
      out_columns := out_columns||desctab(indx).col_name||',';
      IF desctab (indx).col_type = 2 
      THEN
        DBMS_SQL.define_column (curid, indx, numvar); 
      ELSIF desctab (indx).col_type = 12
      THEN
        DBMS_SQL.define_column (curid, indx, datevar); 
      ELSE
        DBMS_SQL.define_column (curid, indx, namevar, 4000); 
      END IF;
    END LOOP;

    out_columns := rtrim(out_columns,',')||') VALUES (';

    WHILE DBMS_SQL.fetch_rows (curid) > 0 
    LOOP
      out_values := '';
      FOR indx IN 1 .. colcnt 
      LOOP
        IF (desctab (indx).col_type = 1) 
        THEN
          DBMS_SQL.COLUMN_VALUE (curid, indx, namevar);
          out_values := out_values||''''||namevar||''',';
        ELSIF (desctab (indx).col_type = 2)
        THEN
          DBMS_SQL.COLUMN_VALUE (curid, indx, numvar);
          out_values := out_values||numvar||',';
        ELSIF (desctab (indx).col_type = 12)
        THEN
          DBMS_SQL.COLUMN_VALUE (curid, indx, datevar);
          out_values := out_values||
            'to_date('''||to_char(datevar,'DD.MM.YYYY HH24:MI:SS')||
             ''',''DD.MM.YYYY HH24:MI:SS''),';
        END IF;
      END LOOP; 
     clob_result := clob_result || CHR(10)||(out_table||out_define||out_columns||rtrim(out_values,',')||');');

    END LOOP;

    DBMS_SQL.close_cursor (curid);

  END LOOP;
  clob_result := clob_result || CHR(10)||('COMMIT;');
  
END;
end export_data;
/

create or replace procedure export_packages
(
    clob_result OUT CLOB
)
AS
BEGIN
declare
export_result CLOB;

BEGIN
set_dbms_metadata_params;
for rec in (select
    u.object_name, u.object_type
from
   objects_to_export u
where
   object_type = 'PACKAGE')
   loop
        clob_result := clob_result || dbms_metadata.GET_DDL(rec.object_type,rec.object_name);   
   end loop;
   end;
END export_packages;
/

create or replace procedure export_triggers
(
    clob_result OUT CLOB
)
as
begin
declare 
export_result CLOB;

BEGIN
set_dbms_metadata_params;

for rec in (
select
    u.trigger_name
from
   user_triggers u
    where table_name in ('BEAMPROCESSES', 'BEAMPROCESS_TYPES', 'CYCLES'))
     loop
        clob_result := clob_result || dbms_metadata.GET_DDL('TRIGGER', rec.trigger_name); 
   end loop;   
   end;
end export_triggers;
/

create or replace procedure export_lsa_dev
(
    clob_out OUT CLOB
)
as 
begin
declare 
export_result CLOB;

begin
set_dbms_metadata_params;
 dbms_output.enable(null);

  export_tables(export_result);
  clob_out := clob_out || export_result;
  export_ref_constraints(export_result);
  clob_out := clob_out || export_result;
  export_types(export_result);
  clob_out := clob_out || export_result;
  export_sequences(export_result);
  clob_out := clob_out || export_result;
  export_synonyms(export_result);
  clob_out := clob_out || export_result;
  export_views(export_result);
  clob_out := clob_out || export_result;
  user_lsa_connect(export_result);
  clob_out := clob_out || export_result;
  export_data(export_result);
  clob_out := clob_out || export_result;
  export_packages(export_result);
  clob_out := clob_out || export_result;
  export_triggers(export_result);
  clob_out := clob_out || export_result;
  dump_clob(clob_out);
  dump_clob(export_result);
end;
end export_lsa_dev;
/

