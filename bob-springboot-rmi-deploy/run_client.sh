#!/bin/bash

# Generated shell start script for DEV-LSA-RMI-DEMO-PROJECT.jvm


[[ -z "$JAVA_HOME" ]] && export JAVA_HOME=/usr/java/jdk
export INSTALL_DIR=/opt/lsa-rmi-demo-project/dev-lsa-rmi-demo-project
CLASSPATH=`ls $INSTALL_DIR/lib/*.jar | tr -s '\n' ':'`
cd $INSTALL_DIR

JVM_MEM="-Xms256m -Xmx256m"
JVM_OTHER_OPTS=(-server -ea -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=./log/)

exec -a `basename $0` $JAVA_HOME/bin/java -cp "$CLASSPATH" -Dapp.name="dev-lsa-rmi-demo-project"  -Dapp.version="1.0.0-20170720-103841"  $JVM_MEM "${JVM_OTHER_OPTS[@]}" cern.lsa.querydaemon.Client
