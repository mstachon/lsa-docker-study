#!/usr/bin/env bash

export INSTALL_DIR=/usr/local/lsa-project
CLASSPATH=`ls $INSTALL_DIR/lib/*.jar | tr -s '\n' ':'`
cd $INSTALL_DIR

/usr/bin/java -cp "$CLASSPATH" -Dmgt.jmx.port="8080"  cern.lsa.querydaemon.Client