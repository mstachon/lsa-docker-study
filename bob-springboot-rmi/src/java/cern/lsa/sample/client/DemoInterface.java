package cern.lsa.sample.client;

import java.rmi.Remote;

/**
 * Created by mstachon on 7/14/17.
 */
public interface DemoInterface {
    String hello();
}
