package cern.lsa.sample.client;

/**
 * Created by mstachon on 7/14/17.
 */
public class DemoInterfaceImpl implements DemoInterface {
    @Override
    public String hello() {
        System.out.println("called server");
        return "Hello World!";
    }
}
