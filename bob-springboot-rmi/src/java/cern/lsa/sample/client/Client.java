package cern.lsa.sample.client;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//import org.apache.log4j.Logger;

/**
 * Created by mstachon on 7/14/17.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
@SpringBootApplication
public class Client {

    public static void main(String [] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("client-beans.xml");
        DemoInterface demoInterface = (DemoInterface) context.getBean("demoBean");
        System.out.println(demoInterface.hello());
    }
}
