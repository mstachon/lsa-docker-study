#!/usr/bin/env bash

export INSTALL_DIR=/usr/local/lsa-project
mkdir lib
PROJECT_NAME=$1
APPLICATION=$2
EXT=jar
wget  -r -np -nH --cut-dirs=100 -A ".jar" -P $INSTALL_DIR/lib http://abwww.cern.ch/ap/deployments-dev/applications/cern/lsa/${PROJECT_NAME}/PRO/lib/

wget -P $INSTALL_DIR/lib http://abwww.cern.ch/ap/deployments-dev/applications/cern/lsa/${PROJECT_NAME}/PRO/${PROJECT_NAME}.${EXT}


CLASSPATH=`ls $INSTALL_DIR/lib/*.jar | tr -s '\n' ':'`
cd $INSTALL_DIR
/usr/bin/java -cp "$CLASSPATH" -Dmgt.jmx.port="8080"  $APPLICATION
