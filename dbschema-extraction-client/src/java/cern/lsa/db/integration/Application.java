package cern.lsa.db.integration;

import oracle.jdbc.driver.DBConversion;
import oracle.sql.CLOB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mstachon on 9/19/17.
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    Connection dbConnection = null;
    String file = "/opt/oracle/oracle-data/5-lsa-schema.sql";
    String query = "{call export_lsa_dev(?)}";

    @Override
    public void run(String... args) throws Exception {
        log.info("Calling lsa dv extraction stored procedure");

        callLsaDevExportProcedure(query);
    }

    private void callLsaDevExportProcedure(String query) throws SQLException {

        dbConnection = getDbConnection();
        CallableStatement callableStatement =dbConnection.prepareCall(query);
        callableStatement.registerOutParameter(1, Types.CLOB);

        callableStatement.executeUpdate();

        CLOB out_clob = (CLOB) callableStatement.getClob(1);

        printOutClob(out_clob);
        saveOutClobToFile(out_clob);
    }


    private void printOutClob(CLOB out_clob) throws SQLException {
        System.out.println(out_clob.getSubString(1, (int)out_clob.length()));
    }

    private void saveOutClobToFile(CLOB out_clob) {
        try(BufferedWriter bufferedWriter = Files.newBufferedWriter(Paths.get(file))){
            bufferedWriter.write(out_clob.getSubString(1, (int)out_clob.length()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection getDbConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public static void main(String args []) {
        SpringApplication.run(Application.class, args);
    }
}
