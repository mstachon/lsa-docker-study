package cern.lsa.test;

import cern.accsoft.commons.domain.Accelerator;
import cern.accsoft.commons.domain.zones.AcceleratorZone;
import cern.accsoft.commons.util.Nameds;
import cern.accsoft.commons.value.Type;
import cern.lsa.client.DeviceService;
import cern.lsa.client.ServiceLocator;
import cern.lsa.core.devices.*;
import cern.lsa.core.devices.spi.helper.*;
import cern.lsa.domain.cern.devices.DeviceGroupTypeEnum;
import cern.lsa.domain.devices.*;
import cern.lsa.domain.devices.factory.DevicesRequestBuilder;
import cern.lsa.domain.devices.factory.PropertiesRequestBuilder;
import cern.lsa.domain.devices.spi.DeviceGroupImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;
import static cern.accsoft.commons.domain.CernAccelerator.LHC;
import static java.util.Arrays.asList;


/**
 * Created by mstachon on 8/25/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = false)
@ContextConfiguration("/testApplicationContext.xml")
public class CheckLsaServerConnectivityTest {

    private String DEVICE_NAME="test_device";
    private String DEVICE_TYPE_NAME="test_device_type";
    private String ACCELERATOR_NAME="LHC";
    private String DEVICE_GROUP_NAME="test_group_1";
    private String DEVICE_GROUP_NAME_2="test_group_2";
    private String PROPETRY_NAME="test_property_name";
    private String PROPERTY_FIELD_NAME="test_property";
    public static boolean dbInit = false;
    private DeviceGroup deviceGroup;
    private DeviceGroup deviceGroup2;

    @Autowired
    protected AcceleratorFinder acceleratorFinder;

    @Autowired
    protected DeviceFinder deviceFinder;

    @Autowired
    protected DeviceTypeFinder deviceTypeFinder;

    @Autowired
    protected DevicePersister devicePersister;

    @Autowired
    protected AcceleratorTestHelper acceleratorTestHelper;

    @Autowired
    protected DeviceTestHelper deviceTestHelper;

    @Autowired
    protected DeviceTypeTestHelper deviceTypeTestHelper;

    @Autowired
    protected DeviceTypeVersionTestHelper deviceTypeVersionTestHelper;

    @Autowired
    protected PropertyTestHelper propertyTestHelper;

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @Autowired
    protected PropertyFinder propertyFinder;

    @Autowired
    protected DeviceGroupFinder deviceGroupFinder;

    DeviceService deviceService = ServiceLocator.getService(DeviceService.class);

    static {
        System.setProperty("lsa.server","integration");
    }


    @Before
    public void setUp() {

        if(!dbInit) {

            // delete groups
            deviceGroup = deviceGroupFinder.findDeviceGroup(LHC, DEVICE_GROUP_NAME);
            if(deviceGroup != null) {
                jdbcTemplate.execute("delete from device_groups where device_grp_name = 'test_group_1' ");
            }

            deviceGroup2 = deviceGroupFinder.findDeviceGroup(LHC, DEVICE_GROUP_NAME_2);
            if(deviceGroup2 != null) {
                jdbcTemplate.execute("delete from device_groups where device_grp_name = 'test_group_2' ");
            }

            jdbcTemplate.execute("delete from device_group_types where device_grp_type_id = 4");
            jdbcTemplate.execute("insert into device_group_types(device_grp_type_id, device_grp_type_name, device_grp_type_desc) values(4, 'DEVICES', 'Standard device groups which just combine devices or other device groups')");

            // delete property
            SortedMap<DeviceTypeVersion, Set<Property>> property = propertyFinder.findProperties(new PropertiesRequestBuilder().setDeviceTypeName(DEVICE_TYPE_NAME).setPropertyName(PROPETRY_NAME).build());
            if(!property.isEmpty()) {
                jdbcTemplate.execute("delete from lsa_properties where device_type_id > 0");
            }

            // delete device
            Device device = deviceFinder.findDevice(DEVICE_NAME);
            if (device != null)
                this.devicePersister.deleteDevices(asList(device));

            DeviceType deviceType = deviceTypeFinder.findDeviceType(DEVICE_TYPE_NAME);
            if (deviceType != null) {
                jdbcTemplate.execute("delete from device_types where device_type_id = " + deviceType.getId());
                jdbcTemplate.execute("delete from device_type_versions where device_type_id = " + deviceType.getId());

            }
            Assert.assertNull(deviceFinder.findDevice(DEVICE_NAME));


            // create new device
            Accelerator accelerator = acceleratorTestHelper.persistAcceleratorAndAssociatedData(LHC);
            AcceleratorZone acceleratorZone = accelerator.getAcceleratorZones().iterator().next();

            deviceType = deviceTypeTestHelper.new Builder().setName(DEVICE_TYPE_NAME).setPersist(true).build();


            DeviceTypeVersion deviceTypeVersion = deviceTypeVersionTestHelper.new Builder(deviceType).setPersist(true).build();

            device = deviceTestHelper.new Builder(acceleratorZone)
                    .setName(DEVICE_NAME)
                    .setDeviceType(deviceType)
                    .setMultiplexed(true)
                    .setPersist(true)
                    .setDeviceTypeVersion(deviceTypeVersion)
                    .build();

            Assert.assertNotNull(device);

            Property p = propertyTestHelper.new Builder(deviceTypeVersion)
                    .setPropertyName(PROPETRY_NAME)
                    .setFieldName(PROPERTY_FIELD_NAME)
                    .setPersist(true)
                    .setValueType(Type.DOUBLE)
                    .setVirtual(true)
                    .build();
            Assert.assertNotNull(p);

            dbInit = true;
        }
    }

    @Test
    public void findDevicesTest() {
        Set<Device> deviceSet = deviceService.findDevices(new DevicesRequestBuilder().setAccelerator(LHC).setDeviceName(DEVICE_NAME).build());
        Assert.assertEquals(1, deviceSet.size());
        Assert.assertEquals(DEVICE_NAME, deviceSet.iterator().next().getName());
        Assert.assertEquals(ACCELERATOR_NAME, deviceSet.iterator().next().getAccelerator().toString());

    }

    @Test
    public void findDeviceTest() {
        Device device = deviceService.findDevice(DEVICE_NAME);
        Assert.assertNotNull(device);
        Assert.assertEquals(DEVICE_NAME, device.getName());
        Assert.assertEquals(ACCELERATOR_NAME, device.getAccelerator().getName());
    }

    @Test
    public void findDeviceTypeTest() {
        DeviceType deviceType = deviceService.findDeviceType(DEVICE_TYPE_NAME);
        Assert.assertNotNull(deviceType);
        Assert.assertEquals(DEVICE_TYPE_NAME, deviceType.getName());
    }

    @Test
    public void findPropertyTest () {
        SortedMap<DeviceTypeVersion, Set<Property>> properties = propertyFinder.findProperties(new PropertiesRequestBuilder().setDeviceTypeName(DEVICE_TYPE_NAME).setPropertyName(PROPETRY_NAME).build());
        Assert.assertNotNull(properties);

        final Property property = Nameds.findByName(properties.entrySet().iterator().next().getValue(), PROPETRY_NAME);
        final PropertyField field = Nameds.findByName(property.getFields(), PROPERTY_FIELD_NAME);

        Assert.assertNotNull(property);
        Assert.assertNotNull(field);

        Assert.assertEquals(PROPETRY_NAME, property.getName());
        Assert.assertEquals(PROPERTY_FIELD_NAME, field.getName());
    }

    @Test
    public void findDeviceGroupTest() {

        deviceGroup = createDeviceGroup(1, DEVICE_GROUP_NAME, LHC, "disp_name_testgroup_1", DeviceGroupTypeEnum.DEVICES);
        deviceGroup2 = createDeviceGroup(2, DEVICE_GROUP_NAME_2, LHC, "disp_name_testgroup_2", DeviceGroupTypeEnum.DEVICES);

        Assert.assertNotNull(deviceGroup);
        Assert.assertNotNull(deviceGroup2);

        deviceService.saveDeviceGroup(deviceGroup);
        deviceService.saveDeviceGroup(deviceGroup2);

        List<DeviceGroup> deviceGroupList = new ArrayList<>();
        deviceGroupList.add(deviceGroup);
        deviceGroupList.add(deviceGroup2);

        Map<DeviceGroup, List<Device>> dg1 = deviceService.findDevicesByGroups(deviceGroupList);

        Assert.assertNotNull(dg1);
        Assert.assertEquals(2, dg1.size());
        Assert.assertEquals(DEVICE_GROUP_NAME, dg1.entrySet().iterator().next().getKey().getName());
        Assert.assertEquals(LHC, dg1.entrySet().iterator().next().getKey().getAccelerator());
    }

    private DeviceGroup createDeviceGroup(long id, String name, Accelerator accelerator, String displayName,
                                                 DeviceGroupType type) {
        DeviceGroupImpl deviceGroup = new DeviceGroupImpl(name, type, accelerator);
        deviceGroup.setId(id);
        deviceGroup.setDisplayName(displayName);
        return deviceGroup;
    }

}
