/**
 * Copyright (c) 2014 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.lsa.core.devices.spi.helper;

import cern.accsoft.commons.domain.zones.AcceleratorZone;
import cern.accsoft.commons.util.Assert;
import cern.lsa.core.devices.DeviceFinder;
import cern.lsa.core.devices.DevicePersister;
import cern.lsa.domain.devices.Device;
import cern.lsa.domain.devices.DeviceType;
import cern.lsa.domain.devices.DeviceTypeVersion;
import cern.lsa.domain.devices.spi.DeviceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Arrays;

import static java.util.Collections.singletonMap;

/**
 * This class is used to conveniently create an instance of {@link Device} for testing purposes.
 *
 * @author Hanno Huether
 */
public class DeviceTestHelper {

    private static String SQL_TO_INSERT_PPM_FOR_OP_DEVICE = "insert into LSA_DEVICES (DEVICE_ID, IS_MULTIPLEXED) values (:deviceId, 'Y')";

    @Autowired
    private DeviceFinder deviceFinder;

    @Autowired
    private DevicePersister devicePersister;

    @Autowired
    private DeviceTypeTestHelper deviceTypeTestHelper;

    @Autowired
    private DeviceTypeVersionTestHelper deviceTypeVersionTestHelper;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public class Builder {
        private final AcceleratorZone acceleratorZone;
        private DeviceType deviceType = null;
        private DeviceTypeVersion deviceTypeVersion = null;
        private String name = null;
        private boolean persist = true;
        private boolean ppmForOp = false;
        private boolean multiplexed = true;


        public Builder(final AcceleratorZone acceleratorZone) {
            Assert.argNotNull(acceleratorZone, "acceleratorZone");
            this.acceleratorZone = acceleratorZone;
        }

        public Builder setDeviceType(DeviceType deviceType) {
            Assert.argNotNull(deviceType, "deviceType");
            this.deviceType = deviceType;
            return this;
        }
        
        public Builder setDeviceTypeVersion(DeviceTypeVersion deviceTypeVersion) {
            Assert.argNotNull(deviceTypeVersion, "deviceTypeVersion");
            this.deviceTypeVersion = deviceTypeVersion;
            return this;
        }
        
        public Builder setName(final String name) {
            Assert.argNotNull(name, "name");
            this.name = name;
            return this;
        }

        public Builder setPpmForOp(boolean ppmForOp){
            this.ppmForOp = ppmForOp;
            return this;
        }

        public Builder setMultiplexed(boolean multiplexed){
            this.multiplexed = multiplexed;
            return this;
        }

        public Builder setPersist(final boolean persist) {
            this.persist = persist;
            return this;
        }

        public Device build() {
            if (deviceType == null) {
                deviceType = deviceTypeTestHelper.new Builder().setPersist(persist).build();
            }
            
            if (deviceTypeVersion == null) {
                deviceTypeVersion = deviceTypeVersionTestHelper.new Builder(deviceType).setPersist(persist).build();
            }
            
            if (name == null) {
                name = CommonTestHelper.generateRandomName();
            }

            Device device = createDevice();
            if (persist) {
                device = persistDevice(device);

                if (ppmForOp){
                    persistPpmForOp(device);
                }
            }
            return device;
        }

        private Device createDevice() {
            final DeviceImpl device = new DeviceImpl(-1, name);
            device.setDeviceTypeVersion(deviceTypeVersion);
            device.setAcceleratorZone(acceleratorZone);
            device.setMultiplexed(multiplexed);
            return device;
        }

        private Device persistDevice(final Device device) {
            devicePersister.saveDevices(Arrays.asList(device));
            // Reload device from database so ID is set
            final Device deviceFromDb = deviceFinder.findDevice(device.getName());
            return deviceFromDb;
        }

        private void persistPpmForOp(Device device) {
            jdbcTemplate.update(SQL_TO_INSERT_PPM_FOR_OP_DEVICE, singletonMap("deviceId", device.getId()));
        }
    }


}
