package cern.lsa.core.devices.spi.helper;

import cern.accsoft.commons.dbaccess.DaoHelper;
import cern.accsoft.commons.domain.Accelerator;
import cern.accsoft.commons.domain.beams.Beam;
import cern.accsoft.commons.domain.particletransfers.ParticleTransfer;
import cern.accsoft.commons.domain.zones.AcceleratorZone;
import oracle.jdbc.OraclePreparedStatement;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.sql.SQLException;
import java.util.Collection;

public class AcceleratorTestHelper extends NamedParameterJdbcDaoSupport {

    private static final String ACCELERATOR_CODE_P = "acceleratorCode";
    private static final String ACCELERATOR_NAME_P = "acceleratorName";
    private static final String ACCELERATOR_ZONE_NAME_P = "acceleratorZoneName";
    private static final String BEAM_NAME_P = "beamName";
    private static final String BEAM_NUMBER_P = "beamNumber";
    private static final String PARTICLE_TRANSFER_NAME_P = "particleTransferName";
    private static final String PARTICLE_TRANSFER_TYPE_P = "particleTransferType";

    private static final String MERGE_ACCELERATOR = "" //
            + "merge into ACCELERATORS " //
            + "using dual on (ACCELERATOR_NAME = :" + ACCELERATOR_NAME_P + ") " //

            + "when not matched then " //
            + "insert (ACCELERATOR_NAME, ACCELERATOR_CODE) " //
            + "values (:" + ACCELERATOR_NAME_P + ", :" + ACCELERATOR_CODE_P + ")";

    private static final String MERGE_BEAMS = "" //
            + "merge into ACCELERATOR_BEAMS " //
            + "using dual on (ACCELERATOR_NAME = :" + ACCELERATOR_NAME_P + " "//
            + "and BEAM_NAME = :" + BEAM_NAME_P + ") " //

            + "when not matched then " //
            + "insert (ACCELERATOR_NAME, BEAM_NAME, BEAM_NUMBER) " //
            + "values (:" + ACCELERATOR_NAME_P + ", :" + BEAM_NAME_P + ", :" + BEAM_NUMBER_P + ")";

    private static final String MERGE_ACCELERATOR_ZONES = "" //
            + "merge into ACCELERATOR_ZONES " //
            + "using dual on (ACCELERATOR = :" + ACCELERATOR_NAME_P + " " //
            + "and ACCELERATOR_ZONE_NAME = :" + ACCELERATOR_ZONE_NAME_P + ") " //

            + "when not matched then " //
            + "insert (ACCELERATOR_ZONE_ID, ACCELERATOR_ZONE_NAME, ACCELERATOR) " //
            + "values (ACCEL_ZONES_SEQ.NEXTVAL, :" + ACCELERATOR_ZONE_NAME_P + ", :" + ACCELERATOR_NAME_P + ")";

    private static final String MERGE_PARTICLE_TRANSFERS = "" //
            + "merge into PARTICLE_TRANSFERS " //
            + "using dual on (PARTICLE_TRANSFER_NAME = :" + PARTICLE_TRANSFER_NAME_P + ") " //

            + "when not matched then " //
            + "insert (PARTICLE_TRANSFER_ID, PARTICLE_TRANSFER_NAME, PARTICLE_TRANSFER_TYPE) " //
            + "values (PART_TRANSFERS_SEQ.NEXTVAL, :" + PARTICLE_TRANSFER_NAME_P + ", " //
            + ":" + PARTICLE_TRANSFER_TYPE_P + ")";

    // @formatter:off
    private static final String MERGE_PARTICLE_TRANSFER_ACCELERATOR_ZONE_RELATIONS = "" 
            + "merge into PART_TRANS_ACC_ZONES " 
            + "using dual on (PARTICLE_TRANSFER_ID = "
            + "(select PARTICLE_TRANSFER_ID from PARTICLE_TRANSFERS "
            + "where PARTICLE_TRANSFER_NAME = :" + PARTICLE_TRANSFER_NAME_P + ") "
            + "and ACCELERATOR_ZONE_ID = "
            + "(select ACCELERATOR_ZONE_ID from ACCELERATOR_ZONES "
            + "where ACCELERATOR_ZONE_NAME = :" + ACCELERATOR_ZONE_NAME_P + ")) "

            + "when not matched then " 
            + "insert (PARTICLE_TRANSFER_ID, ACCELERATOR_ZONE_ID) " 
            + "values ((select PARTICLE_TRANSFER_ID from PARTICLE_TRANSFERS "
            + "where PARTICLE_TRANSFER_NAME = :" + PARTICLE_TRANSFER_NAME_P + "), "
            + "(select ACCELERATOR_ZONE_ID from ACCELERATOR_ZONES "
            + "where ACCELERATOR_ZONE_NAME = :" + ACCELERATOR_ZONE_NAME_P + ")) ";
    // @formatter:on

    public Accelerator persistTestAcceleratorAndAssociatedData() {
        return persistAcceleratorAndAssociatedData(TestAccelerator.TEST);
    }

    public Accelerator persistAcceleratorAndAssociatedData(Accelerator accelerator) {
        persistAccelerator(accelerator);
        persistBeams(accelerator.getBeams());
        persistAcceleratorZones(accelerator.getAcceleratorZones());
        persistParticleTransfers(accelerator.getParticleTransfers());
        persistParticleTransferAcceleratorZoneRelations(accelerator.getParticleTransfers());
        return accelerator;
    }

    private void persistAccelerator(Accelerator accelerator) {
        final MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue(ACCELERATOR_NAME_P, accelerator.getName());
        parameters.addValue(ACCELERATOR_CODE_P, accelerator.getCode());
        this.getNamedParameterJdbcTemplate().update(MERGE_ACCELERATOR, parameters);
    }

    private void persistBeams(Collection<Beam> beams) {
        getJdbcTemplate().execute(MERGE_BEAMS, new MergeAcceleratorBeamsCallback(beams));
    }

    private void persistAcceleratorZones(Collection<AcceleratorZone> acceleratorZones) {
        getJdbcTemplate().execute(MERGE_ACCELERATOR_ZONES, new MergeAcceleratorZonesCallback(acceleratorZones));
    }

    private void persistParticleTransfers(Collection<ParticleTransfer> particleTransfers) {
        getJdbcTemplate().execute(MERGE_PARTICLE_TRANSFERS, new MergeParticleTransfersCallback(particleTransfers));
    }

    private void persistParticleTransferAcceleratorZoneRelations(Collection<ParticleTransfer> particleTransfers) {
        getJdbcTemplate().execute(MERGE_PARTICLE_TRANSFER_ACCELERATOR_ZONE_RELATIONS,
                new MergeParticleTransferAcceleratorZoneRelationsCallback(particleTransfers));
    }

    private static class MergeAcceleratorBeamsCallback extends DaoHelper.BatchUpdateCallback {

        private final Collection<Beam> beams;

        public MergeAcceleratorBeamsCallback(final Collection<Beam> beams) {
            super();
            this.beams = beams;
        }

        @Override
        protected int computeDataEntriesCount() {
            return this.beams.size();
        }

        @Override
        protected int doInPreparedStatementImpl(OraclePreparedStatement ops) throws SQLException, DataAccessException {
            int updatedRowCount = 0;
            for (final Beam beam : beams) {
                ops.setStringAtName(ACCELERATOR_NAME_P, beam.getAccelerator().getName());
                ops.setStringAtName(BEAM_NAME_P, beam.getName());
                ops.setIntAtName(BEAM_NUMBER_P, beam.getBeamNumber());
                updatedRowCount += ops.executeUpdate();
            }
            return updatedRowCount;
        }
    }

    private static class MergeAcceleratorZonesCallback extends DaoHelper.BatchUpdateCallback {

        private final Collection<AcceleratorZone> acceleratorZones;

        public MergeAcceleratorZonesCallback(final Collection<AcceleratorZone> acceleratorZones) {
            super();
            this.acceleratorZones = acceleratorZones;
        }

        @Override
        protected int computeDataEntriesCount() {
            return this.acceleratorZones.size();
        }

        @Override
        protected int doInPreparedStatementImpl(OraclePreparedStatement ops) throws SQLException, DataAccessException {
            int updatedRowCount = 0;
            for (final AcceleratorZone acceleratorZone : acceleratorZones) {
                ops.setStringAtName(ACCELERATOR_ZONE_NAME_P, acceleratorZone.getName());
                ops.setStringAtName(ACCELERATOR_NAME_P, acceleratorZone.getAccelerator().getName());
                updatedRowCount += ops.executeUpdate();
            }
            return updatedRowCount;
        }
    }

    private static class MergeParticleTransfersCallback extends DaoHelper.BatchUpdateCallback {

        private final Collection<ParticleTransfer> particleTransfers;

        public MergeParticleTransfersCallback(final Collection<ParticleTransfer> particleTransfers) {
            super();
            this.particleTransfers = particleTransfers;
        }

        @Override
        protected int computeDataEntriesCount() {
            return this.particleTransfers.size();
        }

        @Override
        protected int doInPreparedStatementImpl(OraclePreparedStatement ops) throws SQLException, DataAccessException {
            int updatedRowCount = 0;
            for (final ParticleTransfer particleTransfer : particleTransfers) {
                ops.setStringAtName(PARTICLE_TRANSFER_NAME_P, particleTransfer.getName());
                String particleTransferType = hackParticleTransferTypeToMatchConstraintAtCern(particleTransfer
                        .getParticleTransferType().name());
                ops.setStringAtName(PARTICLE_TRANSFER_TYPE_P, particleTransferType);
                updatedRowCount += ops.executeUpdate();
            }
            return updatedRowCount;
        }

        /*
         * See https://www-acc.gsi.de/bugzilla/show_bug.cgi?id=1171
         */
        private String hackParticleTransferTypeToMatchConstraintAtCern(String particleTransferType) {
            if ("TRANSFER".equals(particleTransferType)) {
                return "TRANSFER_LINE";
            } else {
                return particleTransferType;
            }
        }
    }

    private static class MergeParticleTransferAcceleratorZoneRelationsCallback extends DaoHelper.BatchUpdateCallback {

        private final Collection<ParticleTransfer> particleTransfers;

        public MergeParticleTransferAcceleratorZoneRelationsCallback(
                final Collection<ParticleTransfer> particleTransfers) {
            super();
            this.particleTransfers = particleTransfers;
        }

        @Override
        protected int computeDataEntriesCount() {
            int count = 0;
            for (ParticleTransfer particleTransfer : particleTransfers) {
                count += particleTransfer.getAcceleratorZones().size();
            }
            return count;
        }

        @Override
        protected int doInPreparedStatementImpl(OraclePreparedStatement ops) throws SQLException, DataAccessException {
            int updatedRowCount = 0;
            for (final ParticleTransfer particleTransfer : particleTransfers) {
                for (AcceleratorZone acceleratorZone : particleTransfer.getAcceleratorZones()) {
                    ops.setStringAtName(PARTICLE_TRANSFER_NAME_P, particleTransfer.getName());
                    ops.setStringAtName(ACCELERATOR_ZONE_NAME_P, acceleratorZone.getName());
                    updatedRowCount += ops.executeUpdate();
                }
            }
            return updatedRowCount;
        }
    }
}
