package cern.lsa.core.devices.spi.helper;

import cern.accsoft.commons.util.Assert;
import cern.accsoft.commons.util.Nameds;
import cern.accsoft.commons.value.Type;
import cern.accsoft.commons.value.spi.ValueDescriptorImpl;
import cern.lsa.core.devices.PropertyFinder;
import cern.lsa.domain.devices.DeviceTypeVersion;
import cern.lsa.domain.devices.Property;
import cern.lsa.domain.devices.Property.PropertyVisibility;
import cern.lsa.domain.devices.PropertyField;
import cern.lsa.domain.devices.factory.PropertiesRequestBuilder;
import cern.lsa.domain.devices.spi.PropertyBuilder;
import cern.lsa.domain.devices.spi.PropertyBuilder.PropertyFieldInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.support.CompositeCacheManager;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.util.Set;
import java.util.SortedMap;

/**
 * This class is used to conveniently create an instance of {@link Property} for testing purposes.
 *
 * @author Hanno Huether
 */
public class PropertyTestHelper extends NamedParameterJdbcDaoSupport {

    private static final String NAMED_PARAMETER_DEVICE_TYPE_ID = "deviceTypeId";
    private static final String NAMED_PARAMETER_PROPERTY_NAME = "propertyName";
    private static final String NAMED_PARAMETER_FIELD_NAME = "fieldName";
    private static final String NAMED_PARAMETER_VALUE_TYPE = "valueType";
    private static final String NAMED_PARAMETER_IS_VIRTUAL = "isVirtual";

    // @formatter:off
    private static final String INSERT_PROPERTY = ""
            + "insert into LSA_PROPERTIES "
            + "(DEVICE_TYPE_ID, PROPERTY_NAME, IS_READABLE, IS_WRITABLE, IS_MONITORABLE, IS_MULTIPLEXED, "
            + "IS_PARTIAL_SET_SUPPORTED, IS_VIRTUAL, IS_ADDED, REASON) "
            + "values (:" + NAMED_PARAMETER_DEVICE_TYPE_ID + ", "
            + ":" + NAMED_PARAMETER_PROPERTY_NAME + ", "
            + "'Y', 'Y', 'Y', 'Y', 'Y', "
            + ":" + NAMED_PARAMETER_IS_VIRTUAL + ", "
            + "'Y', 'TESTING')";
    // @formatter:on

    // @formatter:off
    private static final String INSERT_PROPERTY_FIELD = "" 
            + "insert into LSA_PROPERTY_FIELDS "
            + "(DEVICE_TYPE_ID, PROPERTY_NAME, FIELD_NAME, VALUE_TYPE, IS_ADDED, REASON) " 
            + "values (:" + NAMED_PARAMETER_DEVICE_TYPE_ID + ", " 
            + ":" + NAMED_PARAMETER_PROPERTY_NAME + ", " 
            + ":" + NAMED_PARAMETER_FIELD_NAME + ", " 
            + ":" + NAMED_PARAMETER_VALUE_TYPE + ", "
            + "'Y', 'TESTING')";
    // @formatter:on

    @Autowired
    private CompositeCacheManager compositeCacheManager;
    
    @Autowired
    private PropertyFinder propertyFinder;

    /**
     * There is no persister for {@link Property properties} because they are not to be inserted through LSA applications,
     * but only on the database level. For testing though, it is necessary to create them at runtime. However, this also
     * means that the cache has to be refreshed manually as it is done here, as otherwise, wrong data may be returned by
     * {@link PropertyFinder}.
     */
    private void clearPropertyCache() {
        compositeCacheManager.getCache("propertyCache").clear();
    }
    
    public class Builder {
        private final DeviceTypeVersion deviceTypeVersion;
        private String fieldName = "value";
        private boolean persist = true;
        private String propertyName = null;
        private Type valueType = Type.FUNCTION;
        private boolean virtual = true;
        
        public Builder(final DeviceTypeVersion deviceTypeVersion) {
            Assert.argNotNull(deviceTypeVersion, "deviceTypeVersion");
            this.deviceTypeVersion = deviceTypeVersion;
        }

        public Builder setPropertyName(final String propertyName) {
            Assert.argNotNull(propertyName, "propertyName");
            this.propertyName = propertyName;
            return this;
        }
        
        public Builder setFieldName(final String fieldName) {
            Assert.argNotNull(fieldName, "fieldName");
            this.fieldName = fieldName;
            return this;
        }

        public Builder setPersist(final boolean persist) {
            this.persist = persist;
            return this;
        }

        public Builder setValueType(final Type valueType) {
            Assert.argNotNull(valueType, "valueType");
            this.valueType = valueType;
            return this;
        }
        
        public Builder setVirtual(final boolean virtual) {
            this.virtual = virtual;
            return this;
        }
        
        public Property build() {
            if (propertyName == null) {
                propertyName = CommonTestHelper.generateRandomName();
            }

            Property property = createProperty();

            if (persist) {
                persistProperty(property);
                final SortedMap<DeviceTypeVersion, Set<Property>> propertiesByDeviceTypeVersions = propertyFinder
                        .findProperties(new PropertiesRequestBuilder()
                                .setDeviceTypeName(deviceTypeVersion.getDeviceType().getName()) //
                                .setPropertyName(propertyName) //
                                .build());
                property = Nameds.findByName(propertiesByDeviceTypeVersions.get(deviceTypeVersion), propertyName);
            }

            return property;
        }

        private Property createProperty() {
            final PropertyFieldInfo propertyFieldInfo = new PropertyFieldInfo(fieldName, valueType, "INOUT",
                    new ValueDescriptorImpl(), null);

            final PropertyBuilder builder = new PropertyBuilder();
            final Property property = builder.setDeviceTypeVersion(deviceTypeVersion).setMonitorable(true)
                    .setMultiplexed(true).setName(propertyName).setPropertyVisibility(PropertyVisibility.OPERATIONAL)
                    .setReadable(true).setSupportingPartialSet(true).setVirtual(virtual).setWritable(true)
                    .addField(propertyFieldInfo).build();

            return property;
        }

        private void persistProperty(final Property property) {
            final long deviceTypeId = property.getDeviceTypeVersion().getDeviceType().getId();

            final MapSqlParameterSource sourceForProperty = new MapSqlParameterSource();
            sourceForProperty.addValue(NAMED_PARAMETER_DEVICE_TYPE_ID, deviceTypeId);
            sourceForProperty.addValue(NAMED_PARAMETER_PROPERTY_NAME, property.getName());
            sourceForProperty.addValue(NAMED_PARAMETER_IS_VIRTUAL, property.isVirtual() ? "Y" : "N");
            getNamedParameterJdbcTemplate().update(INSERT_PROPERTY, sourceForProperty);

            PropertyField field = property.getFields().iterator().next();
            final MapSqlParameterSource sourceForField = new MapSqlParameterSource();
            sourceForField.addValue(NAMED_PARAMETER_DEVICE_TYPE_ID, deviceTypeId);
            sourceForField.addValue(NAMED_PARAMETER_PROPERTY_NAME, property.getName());
            sourceForField.addValue(NAMED_PARAMETER_FIELD_NAME, field.getName());
            sourceForField.addValue(NAMED_PARAMETER_VALUE_TYPE, field.getValueType().getName());
            getNamedParameterJdbcTemplate().update(INSERT_PROPERTY_FIELD, sourceForField);
            
            clearPropertyCache();
        }
    }
}
