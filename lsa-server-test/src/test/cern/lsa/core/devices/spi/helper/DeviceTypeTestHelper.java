/**
 * Copyright (c) 2014 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.lsa.core.devices.spi.helper;

import cern.accsoft.commons.util.Assert;
import cern.lsa.domain.devices.DeviceMetaTypeEnum;
import cern.lsa.domain.devices.DeviceType;
import cern.lsa.domain.devices.spi.DeviceTypeImpl;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

/**
 * This class is used to conveniently create an instance of {@link DeviceType} for testing purposes.
 *
 * @author Hanno Huether
 */
public class DeviceTypeTestHelper extends NamedParameterJdbcDaoSupport {

    private static final String DEVICE_TYPE_INSERT = "" //
            + "insert into DEVICE_TYPES " //

            + "(DEVICE_TYPE_ID, DEVICE_TYPE_NAME, DESCRIPTION, LOGICAL_ACTUAL_BEAM) "

            + "values (DEVICES_TYPES_SEQ.NEXTVAL, " // DEVICE_TYPE_ID
            + ":deviceTypeName, " //
            + ":description, " //
            + ":logicalActualBeam)";

    private static final String DEVICE_TYPE_GET_ID = "" //
            + "select DEVICE_TYPE_ID " //
            + "from DEVICE_TYPES " //
            + "where DEVICE_TYPE_NAME = :deviceTypeName";

    private static final String DEVICE_TYPE_DESCRIPTION = "TEST_DEVICE_TYPE_DESCRIPTION";
    private static final DeviceMetaTypeEnum DEVICE_TYPE_META_TYPE = DeviceMetaTypeEnum.ACTUAL;

    public class Builder {

        private String name = null;
        private boolean persist = true;

        public Builder setName(final String name) {
            this.name = name;
            return this;
        }

        public Builder setPersist(final boolean persist) {
            this.persist = persist;
            return this;
        }

        public DeviceType build() {
            if (name == null) {
                name = CommonTestHelper.generateRandomName();
            }

            DeviceType deviceType = createDeviceType(name);
            if (persist) {
                deviceType = persistDeviceType(deviceType);
            }

            return deviceType;
        }

        private DeviceTypeImpl createDeviceType(final String deviceTypeName) {
            Assert.notNull(deviceTypeName);
            final DeviceTypeImpl deviceType = new DeviceTypeImpl(-1, deviceTypeName);
            deviceType.setDescription(DEVICE_TYPE_DESCRIPTION);
            deviceType.setMetaType(DEVICE_TYPE_META_TYPE);
            return deviceType;
        }

        private DeviceType persistDeviceType(final DeviceType deviceType) {
            final MapSqlParameterSource parametersForDeviceType = new MapSqlParameterSource();
            parametersForDeviceType.addValue("deviceTypeName", deviceType.getName());
            parametersForDeviceType.addValue("description", deviceType.getDescription());
            parametersForDeviceType.addValue("logicalActualBeam",
                    String.valueOf(deviceType.getMetaType().getMetaType()));
            getNamedParameterJdbcTemplate().update(DEVICE_TYPE_INSERT, parametersForDeviceType);

            // It is necessary to use a SQL statement to query for the ID here, because to be able to use
            // DeviceTypeFinder.findDeviceType, there already has to be a DeviceTypeVersion persisted.
            // TODO HH: This should probably be moved to a single class to avoid creating a device type and not being
            // able to find it using DeviceTypeFinder.
            final Long newId = getNamedParameterJdbcTemplate().queryForObject(DEVICE_TYPE_GET_ID,
                    parametersForDeviceType, Long.class);
            final DeviceTypeImpl deviceTypeWithIdSet = new DeviceTypeImpl(newId, deviceType.getName());
            deviceTypeWithIdSet.setDescription(deviceType.getDescription());
            deviceTypeWithIdSet.setMetaType(deviceType.getMetaType());
            return deviceTypeWithIdSet;
        }

    }
}
