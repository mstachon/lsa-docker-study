package cern.lsa.core.devices.spi.helper;

import cern.accsoft.commons.domain.Accelerator;
import cern.accsoft.commons.domain.CernTimingDomain;
import cern.accsoft.commons.domain.TimingDomain;
import cern.accsoft.commons.domain.beamdestinations.BeamDestination;
import cern.accsoft.commons.domain.beamdestinations.SpsBeamDestination;
import cern.accsoft.commons.domain.beams.*;
import cern.accsoft.commons.domain.helper.AccsoftDomainHelper;
import cern.accsoft.commons.domain.modes.*;
import cern.accsoft.commons.domain.particletransfers.*;
import cern.accsoft.commons.domain.zones.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static cern.accsoft.commons.domain.helper.AccsoftDomainHelper.immutableSetOf;

public enum TestAccelerator implements Accelerator {

    /** Stripped down for tests that do not rely on specific accelerators. */
    TEST("TST", TestTimingDomain.TEST_TIMING_DOMAIN, TestBeam.class, null, TestAcceleratorZone.class,
            TestParticleTransfer.class, null),

    /* CERN accelerators */
    /** AD */
    AD("ADE", CernTimingDomain.ADE, AdBeam.class, null, AdAcceleratorZone.class, AdParticleTransfer.class, null),
    /** CTF3 */
    CTF3("CTF", CernTimingDomain.SCT, null, null, Ctf3AcceleratorZone.class, Ctf3ParticleTransfer.class, null),
    /** ISOLDE */
    ISOLDE("ISO", CernTimingDomain.ISO, null, null, IsoldeAcceleratorZone.class, IsoldeParticleTransfer.class, null),
    /** LEIR */
    LEIR("LEI", CernTimingDomain.LEI, LeirBeam.class, LeirAcceleratorMode.class, LeirAcceleratorZone.class,
            LeirParticleTransfer.class, null),
    /** LHC */
    LHC("LHC", CernTimingDomain.LHC, LhcBeam.class, LhcAcceleratorMode.class, LhcAcceleratorZone.class,
            LhcParticleTransfer.class, null),
    /** LINAC4 */
    LINAC4("LN4", CernTimingDomain.LN4, null, null, Linac4AcceleratorZone.class, Linac4ParticleTransfer.class, null),
    /** PS */
    PS("CPS", CernTimingDomain.CPS, PsBeam.class, PsAcceleratorMode.class, PsAcceleratorZone.class,
            PsParticleTransfer.class, null),
    /** PS Booster */
    PSB("PSB", CernTimingDomain.PSB, PsbBeam.class, PsbAcceleratorMode.class, PsbAcceleratorZone.class,
            PsbParticleTransfer.class, null),
    /** SPS */
    SPS("SPS", CernTimingDomain.SPS, SpsBeam.class, SpsAcceleratorMode.class, SpsAcceleratorZone.class,
            SpsParticleTransfer.class, SpsBeamDestination.class),
    /** North Experimental Area */
    // TODO: code for the North Area?
    NORTH("???", CernTimingDomain.SPS, null, null, NorthAcceleratorZone.class, NorthParticleTransfer.class, null),
    /** ELENA */
    ELENA("ELENA", CernTimingDomain.LNA, null, null, ElenaAcceleratorZone.class, ElenaParticleTransfer.class, null);

    private final String code;
    private final TimingDomain timingDomain;
    private final Set<Beam> beams;
    private final Set<AcceleratorMode> acceleratorModes;
    private final Set<AcceleratorZone> acceleratorZones;
    private final Set<ParticleTransfer> particleTransfers;
    private final Set<BeamDestination> beamDestinations;

    private TestAccelerator(final String code, final TimingDomain timingDomain, final Class<? extends Beam> beamClass,
                            final Class<? extends AcceleratorMode> acceleratorModeClass,
                            final Class<? extends AcceleratorZone> acceleratorZoneClass,
                            final Class<? extends ParticleTransfer> particleTransferClass,
                            final Class<? extends BeamDestination> beamDestinationClass) {
        this.code = code;
        this.timingDomain = timingDomain;
        beams = immutableSetOf(beamClass);
        acceleratorModes = immutableSetOf(acceleratorModeClass);
        acceleratorZones = immutableSetOf(acceleratorZoneClass);
        particleTransfers = immutableSetOf(particleTransferClass);
        beamDestinations = immutableSetOf(beamDestinationClass);
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public TimingDomain getTimingDomain() {
        return timingDomain;
    }

    @Override
    public Set<Beam> getBeams() {
        return beams;
    }

    @Override
    public Set<AcceleratorMode> getAcceleratorModes() {
        return acceleratorModes;
    }

    @Override
    public Set<AcceleratorZone> getAcceleratorZones() {
        return acceleratorZones;
    }

    @Override
    public Set<ParticleTransfer> getParticleTransfers() {
        return particleTransfers;
    }

    @Override
    public Set<BeamDestination> getBeamDestinations() {
        return beamDestinations;
    }

    private enum TestTimingDomain implements TimingDomain {
        TEST_TIMING_DOMAIN;

        @Override
        public String getName() {
            return name();
        }
    }

    private enum TestBeam implements Beam {
        BEAM1(1),
        BEAM2(2);

        private final int beamNo;

        private TestBeam(final int beamNumber) {
            beamNo = beamNumber;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Accelerator getAccelerator() {
            return TestAccelerator.TEST;
        }

        @Override
        public int getBeamNumber() {
            return beamNo;
        }
    }

    public enum TestAcceleratorZone implements AcceleratorZone {
        TEST_1,
        TEST_2,
        TEST_3,
        TEST_4;

        private static final Set<ParticleTransfer> TEST_1_PARTICLE_TRANSFERS = AccsoftDomainHelper
                .<ParticleTransfer> immutableSetOf(TestParticleTransfer.TEST_1);
        private static final Set<ParticleTransfer> TEST_2_PARTICLE_TRANSFERS = AccsoftDomainHelper
                .<ParticleTransfer> immutableSetOf(TestParticleTransfer.TEST_2);
        private static final Set<ParticleTransfer> TEST_3_PARTICLE_TRANSFERS = AccsoftDomainHelper
                .<ParticleTransfer> immutableSetOf(TestParticleTransfer.TEST_2, TestParticleTransfer.TEST_3);
        private static final Set<ParticleTransfer> TEST_4_PARTICLE_TRANSFERS = AccsoftDomainHelper
                .<ParticleTransfer> immutableSetOf(TestParticleTransfer.TEST_4);

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Accelerator getAccelerator() {
            return TestAccelerator.TEST;
        }

        @Override
        public Set<ParticleTransfer> getParticleTransfers() {
            switch (this) {
            case TEST_1:
                return TEST_1_PARTICLE_TRANSFERS;
            case TEST_2:
                return TEST_2_PARTICLE_TRANSFERS;
            case TEST_3:
                return TEST_3_PARTICLE_TRANSFERS;
            case TEST_4:
                return TEST_4_PARTICLE_TRANSFERS;
            default:
                throw new IllegalArgumentException("Unknown " + getAccelerator() + " accelerator zone: " + this);
            }
        }
    }

    public enum TestParticleTransfer implements ParticleTransfer {
        TEST_1(ParticleTransferType.TRANSFER),
        TEST_2(ParticleTransferType.TRANSFER),
        TEST_3(ParticleTransferType.TRANSFER),
        TEST_4(ParticleTransferType.TRANSFER);

        private final ParticleTransferType particleTransferType;

        private static final List<AcceleratorZone> TEST_1_ACC_ZONES = Arrays
                .<AcceleratorZone> asList(TestAcceleratorZone.TEST_1);
        private static final List<AcceleratorZone> TEST_2_ACC_ZONES = Arrays.<AcceleratorZone> asList(
                TestAcceleratorZone.TEST_2, TestAcceleratorZone.TEST_3);
        private static final List<AcceleratorZone> TEST_3_ACC_ZONES = Arrays
                .<AcceleratorZone> asList(TestAcceleratorZone.TEST_3);
        private static final List<AcceleratorZone> TEST_4_ACC_ZONES = Arrays
                .<AcceleratorZone> asList(TestAcceleratorZone.TEST_4);

        private TestParticleTransfer(final ParticleTransferType particleTransferType) {
            this.particleTransferType = particleTransferType;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Accelerator getAccelerator() {
            return TestAccelerator.TEST;
        }

        @Override
        public List<AcceleratorZone> getAcceleratorZones() {
            switch (this) {
            case TEST_1:
                return TEST_1_ACC_ZONES;
            case TEST_2:
                return TEST_2_ACC_ZONES;
            case TEST_3:
                return TEST_3_ACC_ZONES;
            case TEST_4:
                return TEST_4_ACC_ZONES;
            default:
                throw new IllegalArgumentException("Unknown " + getAccelerator() + " particle transfer: " + this);
            }
        }

        @Override
        public ParticleTransferType getParticleTransferType() {
            return particleTransferType;
        }
    }
}
