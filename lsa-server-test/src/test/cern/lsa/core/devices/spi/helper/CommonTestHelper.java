package cern.lsa.core.devices.spi.helper;

import cern.lsa.core.settings.TypeFinder;
import cern.lsa.domain.settings.ContextCategory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;
import java.util.UUID;

/**
 * This class provides a set of methods that can be directly or indirectly used to conveniently acquire instances of
 * different basic domain objects that are required in test cases. Use for testing only.
 *
 * @author Hanno Huether
 */
public class CommonTestHelper {

    @Autowired
    private TypeFinder typeFinder;

    /**
     * @return An arbitrary (doesn't matter which) {@link ContextCategory}
     */
    public ContextCategory findArbitraryContextCategory() {
        final Set<ContextCategory> contextCategories = typeFinder.findContextCategories();
        if (contextCategories.isEmpty() == false) {
            return contextCategories.iterator().next();
        }

        return null;
    }

    /**
     * @return A random UUID (36 chars) which can be used as a name where uniqueness is required.
     * @see #generateRandomName(int)
     * @see UUID#randomUUID()
     */
    public static String generateRandomName() {
        final String name = UUID.randomUUID().toString().toUpperCase();
        return name;
    }

    /**
     * @return A random UUID which can be used as a name where uniqueness is required.
     * @param maxLength Normally, a generated UUID is 36 chars in length. If a lower number is specified here, it will
     *            be truncated.
     * @see #generateRandomName()
     * @see UUID#randomUUID()
     */
    public static String generateRandomName(final int maxLength) {
        String name = generateRandomName();
        if (name.length() > maxLength) {
            name = name.substring(0, maxLength - 1);
        }
        return name;
    }

}
