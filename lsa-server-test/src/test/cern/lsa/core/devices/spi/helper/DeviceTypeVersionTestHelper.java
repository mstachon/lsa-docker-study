/**
 * Copyright (c) 2014 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.lsa.core.devices.spi.helper;

import cern.accsoft.commons.util.Assert;
import cern.lsa.domain.devices.DeviceType;
import cern.lsa.domain.devices.DeviceTypeImplementation;
import cern.lsa.domain.devices.DeviceTypeVersion;
import cern.lsa.domain.devices.DeviceTypeVersionNumber;
import cern.lsa.domain.devices.spi.DeviceTypeVersionImpl;
import cern.lsa.domain.devices.spi.DeviceTypeVersionNumberImpl;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

/**
 * This class is used to conveniently create an instance of {@link DeviceTypeVersion} for testing purposes.
 *
 * @author Hanno Huether
 */
public class DeviceTypeVersionTestHelper extends NamedParameterJdbcDaoSupport {

    private static final String DEVICE_TYPE_VERSION_INSERT = "" //
            + "insert into DEVICE_TYPE_VERSIONS " //

            + "(DEVICE_TYPE_VERSION_ID, DEVICE_TYPE_ID, IMPLEMENTATION, " //
            + "DEVICE_TYPE_VERSION_MAJOR, DEVICE_TYPE_VERSION_MINOR) " //

            + "values (DEVICE_TYPE_VERSION_SEQ.NEXTVAL, " // DEVICE_TYPE_VERSION_ID
            + ":deviceTypeId, " //
            + ":implementation, " //
            + ":deviceTypeVersionMajor, " //
            + ":deviceTypeVersionMinor)";

    private static final String DEVICE_TYPE_VERSION_GET_ID = "" //
            + "select DEVICE_TYPE_VERSION_ID " //
            + "from DEVICE_TYPE_VERSIONS " //
            + "where DEVICE_TYPE_ID = :deviceTypeId " //
            + "and DEVICE_TYPE_VERSION_MAJOR = :deviceTypeVersionMajor " //
            + "and DEVICE_TYPE_VERSION_MINOR = :deviceTypeVersionMinor";

    /**
     * This method creates a {@link DeviceTypeVersion} in the database which can be utilized for testing. Be aware that
     * only the properties / columns absolutely needed for persisting it are currently set. To be used for testing only,
     * of course!
     *
     * @param deviceType The {@link DeviceType} that this {@link DeviceTypeVersion} shall be associated with.
     * @param deviceTypeVersionNumber The {@link DeviceTypeVersionNumber} of the {@code DeviceTypeVersion} to be created
     *            and persisted. This has to be unique for the {@link DeviceType} specified.
     * @return An instance of {@code DeviceTypeVersion} which is persistent in the database.
     */
    public DeviceTypeVersion persistNewInstance(final DeviceType deviceType,
                                                final DeviceTypeVersionNumber deviceTypeVersionNumber) {
        final DeviceTypeVersion deviceTypeVersion = createNewInstance(deviceType, deviceTypeVersionNumber);
        final DeviceTypeVersion deviceTypeVersionFromDb = persistInstance(deviceTypeVersion);
        return deviceTypeVersionFromDb;
    }

    public DeviceTypeVersionImpl createNewInstance(final DeviceType deviceType,
                                                   final DeviceTypeVersionNumber deviceTypeVersionNumber) {
        Assert.argNotNull(deviceType, "deviceType");
        Assert.argNotNull(deviceTypeVersionNumber, "deviceTypeVersionNumber");
        final DeviceTypeVersionImpl deviceTypeVersionImpl = new DeviceTypeVersionImpl(-1, deviceType,
                DeviceTypeImplementation.LSA, deviceTypeVersionNumber);
        return deviceTypeVersionImpl;
    }

    public DeviceTypeVersion persistInstance(final DeviceTypeVersion deviceTypeVersion) {
        final MapSqlParameterSource parametersForDeviceTypeVersion = new MapSqlParameterSource();
        parametersForDeviceTypeVersion.addValue("deviceTypeVersionId", deviceTypeVersion.getId());
        parametersForDeviceTypeVersion.addValue("deviceTypeId", deviceTypeVersion.getDeviceType().getId());
        parametersForDeviceTypeVersion.addValue("implementation", deviceTypeVersion.getImplementation().name());
        parametersForDeviceTypeVersion.addValue("deviceTypeVersionMajor", deviceTypeVersion.getVersionNumber()
                .getMajor());
        parametersForDeviceTypeVersion.addValue("deviceTypeVersionMinor", deviceTypeVersion.getVersionNumber()
                .getMinor());
        getNamedParameterJdbcTemplate().update(DEVICE_TYPE_VERSION_INSERT, parametersForDeviceTypeVersion);

        // Fetch ID from database
        // TODO HH: There probably is a way to load a device type version from the database via a finder. That way, the
        // SQL statement used here to do this could be replaced.
        final Long newId = getNamedParameterJdbcTemplate().queryForObject(DEVICE_TYPE_VERSION_GET_ID,
                parametersForDeviceTypeVersion, Long.class);

        final DeviceTypeVersionImpl deviceTypeVersionWithIdSet = new DeviceTypeVersionImpl(newId,
                deviceTypeVersion.getDeviceType(), deviceTypeVersion.getImplementation(),
                deviceTypeVersion.getVersionNumber());
        return deviceTypeVersionWithIdSet;
    }

    public DeviceTypeVersion persistNewInstanceWithArbitraryVersionNumber(final DeviceType deviceType) {
        final DeviceTypeVersionNumber deviceTypeVersionNumber = new DeviceTypeVersionNumberImpl(1, 0);
        final DeviceTypeVersion deviceTypeVersion = persistNewInstance(deviceType, deviceTypeVersionNumber);
        return deviceTypeVersion;
    }

    public DeviceTypeVersion createNewInstanceWithArbitraryVersionNumber(final DeviceType deviceType) {
        final DeviceTypeVersionNumber deviceTypeVersionNumber = new DeviceTypeVersionNumberImpl(1, 0);
        final DeviceTypeVersion deviceTypeVersion = createNewInstance(deviceType, deviceTypeVersionNumber);
        return deviceTypeVersion;
    }

    public class Builder {
        private final DeviceType deviceType;
        private boolean persist = true;

        public Builder(final DeviceType deviceType) {
            this.deviceType = deviceType;
        }

        public Builder setPersist(final boolean persist) {
            this.persist = persist;
            return this;
        }

        public DeviceTypeVersion build() {
            DeviceTypeVersion deviceTypeVersion = createNewInstanceWithArbitraryVersionNumber(deviceType);
            if (persist) {
                deviceTypeVersion = persistNewInstanceWithArbitraryVersionNumber(deviceType);
            }
            return deviceTypeVersion;
        }
    }

}
