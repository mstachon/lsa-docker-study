package cern.lsa.core.devices.spi.helper;

import cern.lsa.domain.devices.DeviceGroupType;

/**
 * Defines Device group types for tests.
 * 
 * @author rgorbono
 */
public enum TestDeviceGroupTypeEnum implements DeviceGroupType {

    /***/
    SOC("Group 1"),
    /***/
    WORKING_SET("Group 2"),
    /***/
    PROCESS("Group 3"),
    /***/
    DEVICES("Group 4"),
    /***/
    OP_CONFIG("Group 5"),
    /***/
    WORKING_SET_DEVICES("Group 6");

    private final String description;

    private TestDeviceGroupTypeEnum(String description) {
        this.description = description;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getDescription() {
        return description;
    }

}
