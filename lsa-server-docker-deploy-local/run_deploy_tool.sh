#!/usr/bin/env bash

CERN_USER=mstachon
PROJECT_NAME=lsa-server
APPLICATION_NAME=lsa-integration

sudo su <<HERE

sudo ./deploy_tool.sh ${CERN_USER} -y

rm -R ${PROJECT_NAME}
rm -R /opt/${PROJECT_NAME}/${APPLICATION_NAME}/

sudo  /user/copera/deploy-tool-rep/PRO/deploy ${PROJECT_NAME} -a ${APPLICATION_NAME} --dev -d -f
sudo cp -r /opt/${PROJECT_NAME}/ .

HERE