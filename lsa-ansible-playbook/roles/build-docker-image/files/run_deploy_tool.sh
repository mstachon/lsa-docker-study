#!/usr/bin/env bash

CERN_USER=mstachon
PROJECT_NAME=lsa-server
APPLICATION_NAME=lsa-integration


rm -R ${PROJECT_NAME}
rm -R /opt/${PROJECT_NAME}/${APPLICATION_NAME}/

/user/copera/deploy-tool-rep/PRO/deploy ${PROJECT_NAME} -a ${APPLICATION_NAME} --dev -d -f
cp -r /opt/${PROJECT_NAME}/ .

HERE