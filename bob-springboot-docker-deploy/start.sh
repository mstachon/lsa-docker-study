#!/usr/bin/env bash

CERN_USER=$1
PROJECT_NAME=$2
APPLICATION_NAME=$3
EXT=jvm

#/bin/bash fuse_conf.sh

/bin/bash /usr/local/lsa-project/deploy_tool.sh $CERN_USER

/usr/local/lsa-project/user/copera/deploy-tool-rep/PRO/deploy $PROJECT_NAME --dev -a $APPLICATION_NAME -d

export JAVA_HOME=/usr/

EXECUTION_SCRIPT=`ls /opt/${PROJECT_NAME}/${APPLICATION_NAME}/bin/`

/bin/bash /opt/${PROJECT_NAME}/${APPLICATION_NAME}/bin/${EXECUTION_SCRIPT}